﻿using System.Collections.Generic;

namespace adventofcode.Utilities
{
    public static class ToolsMap
    {
        public static List<(int x, int y)> GetNeighbours<T>(this List<List<T>> map, (int x, int y) pos, bool includeDiagonals = false)
        {
            var neighbours = new List<(int x, int y)>();
            if (includeDiagonals)
            {
                for (var dx = -1; dx < 2; ++dx)
                {
                    for (var dy = -1; dy < 2; ++dy)
                    {
                        var nx = pos.x + dx;
                        var ny = pos.y + dy;

                        if (ny >= 0 && ny < map.Count && nx >= 0 && nx < map[ny].Count)
                        {
                            neighbours.Add((nx, ny));
                        }
                    }
                }

                return neighbours;
            }

            if (pos.x > 0)
            {
                neighbours.Add((pos.x - 1, pos.y));
            }

            if (pos.y > 0)
            {
                neighbours.Add((pos.x, pos.y - 1));
            }

            if (pos.x < map[pos.y].Count - 1)
            {
                neighbours.Add((pos.x + 1, pos.y));
            }

            if (pos.y < map.Count - 1)
            {
                neighbours.Add((pos.x, pos.y + 1));
            }

            return neighbours;
        }

        public static T GetValue<T>(this List<List<T>> map, (int x, int y) pos)
        {
            return map[pos.y][pos.x];
        }

    }
}
