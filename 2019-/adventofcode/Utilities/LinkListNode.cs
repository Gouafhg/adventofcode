﻿using System.Collections.Generic;

namespace adventofcode.Utilities
{
    public class LinkListNode<T>
    {
        public T Value;

        public int Level;

        public LinkListNode<T> Prev;
        public LinkListNode<T> Next;

        public LinkListNode(T value)
        {
            Value = value;
        }

        public override string ToString() => Value.ToString();

        public void AddAfter(T value)
        {
            var nn = new LinkListNode<T>(value) { Next = Next, Prev = this };
            if (nn.Next != null)
            {
                nn.Next.Prev = nn;
            }
            Next = nn;
        }

        public LinkListNode<T> AddAfter(IEnumerable<T> list)
        {
            var n = this;
            foreach (var l in list)
            {
                var nn = new LinkListNode<T>(l) { Next = n.Next, Prev = n };
                if (nn.Next != null)
                {
                    nn.Next.Prev = nn;
                }

                n.Next = nn;
                n = nn;
            }

            return n;
        }

        public void Disconnect()
        {
            if (Prev != null)
            {
                Prev.Next = Next;
            }
            if (Next != null)
            {
                Next.Prev = Prev;
            }
        }

        public LinkListNode<T> GetFirst()
        {
            var n = this;
            while (n.Prev != null) n = n.Prev;
            return n;
        }

        public IEnumerable<T> GetValues()
        {
            var n = GetFirst();
            do
            {
                yield return n.Value;
                n = n.Next;
            } while (n.Next != null);

            yield return n.Value;
        }
    }
}
