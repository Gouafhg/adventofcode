﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace adventofcode.Utilities
{
    public class Grid<T>
    {
        public int Width => Nodes.Keys.Max(p => p.X) - Nodes.Keys.Min(p => p.X) + 1;
        public int Height => Nodes.Keys.Max(p => p.Y) - Nodes.Keys.Min(p => p.Y) + 1;
        public Dictionary<Point, Node<T>> Nodes;

        public Grid()
        {
            Nodes = new Dictionary<Point, Node<T>>(new PointEqualityComparer());
        }

        public Grid(int width, int height)
        {
            Nodes = new Dictionary<Point, Node<T>>(new PointEqualityComparer());
        }

        public Grid<T> Clone()
        {
            var result = new Grid<T>();
            foreach (var key in Nodes.Keys)
            {
                result.AddNode(key, Nodes[key] == null ? null : Nodes[key].Clone());
            }

            return result;
        }

        public void AddNode(Point p, Node<T> n)
        {
            if (!Nodes.ContainsKey(p))
            {
                Nodes.Add(p, null);
            }
            Nodes[p] = n;
        }

        public void AddNode(Node<T> n, int x, int y)
        {
            var p = new Point(x, y);
            AddNode(p, n);
        }

        public void ClearCell(Point p)
        {
            Nodes[p] = null;
        }

        public void SetNode(int x, int y, Node<T> n)
        {
            SetNode(new Point(x, y), n);
        }

        public void SetNode(Point p, Node<T> n)
        {
            if (!Nodes.ContainsKey(p))
            {
                Nodes.Add(p, null);
            }
            Nodes[p] = n;
        }

        public void SetNode(int x, int y, T t)
        {
            var p = new Point(x, y);
            SetNode(p, (Node<T>)t);
        }

        public void SetNode(Point p, T t)
        {
            SetNode(p, (Node<T>)t);
        }

        public bool FilledCell(int x, int y) => FilledCell(new Point(x, y)) && Get(x, y) != null;
        public bool FilledCell(Point p) => Nodes.ContainsKey(p) && Get(p) != null;
        public Node<T> Get(int x, int y) => Get(new Point(x, y));
        public Node<T> Get(Point p) => Nodes.ContainsKey(p) ? Nodes[p] : null;

        public Point GetCoordinates(Node<T> node)
        {
            return Nodes.First(kp => kp.Value == node).Key;
        }

        /*
        void SetAdjacentNodes(Node<T> node)
        {
            node.adjacentNodes.Clear();

            var p = GetCoordinates(node);

            if (p.X < width - 1)
            {
                var n1 = Get(p.X + 1, p.Y);
                if (n1 != null && n1.CanBeAdjacent) node.adjacentNodes.Add(n1, 1);
            }
            if (p.X > 0)
            {
                var n2 = Get(p.X - 1, p.Y);
                if (n2 != null && n2.CanBeAdjacent) node.adjacentNodes.Add(n2, 1);
            }
            if (p.Y < height - 1)
            {
                var n3 = Get(p.X, p.Y + 1);
                if (n3 != null && n3.CanBeAdjacent) node.adjacentNodes.Add(n3, 1);
            }
            if (p.Y > 0)
            {
                var n4 = Get(p.X, p.Y - 1);
                if (n4 != null && n4.CanBeAdjacent) node.adjacentNodes.Add(n4, 1);
            }
        }
        */

        public string[] Print()
        {
            return Print(x => true);
        }

        public string[] Print(Predicate<T> predicate)
        {
            var maxX = Nodes.Keys.Max(x => x.X);
            var minX = Nodes.Keys.Min(x => x.X);
            var maxY = Nodes.Keys.Max(x => x.Y);
            var minY = Nodes.Keys.Min(x => x.Y);
            var h = maxY - minY + 1;
            var offset = new Point(minX, minY);
            var newPoints = new Dictionary<Point, Point>();
            foreach (var p in Nodes.Keys)
            {
                newPoints.Add(p, p.Subtract(offset));
            }

            var screen = new string[h];
            for (var i = 0; i < h; ++i)
            {
                screen[i] = new string(' ', Width);
            }
            for (var i = 0; i < Height; ++i)
            {
                var row = newPoints.Where(x => h - x.Value.Y - 1 == i).Where(x => Nodes[x.Key] != null && predicate(Nodes[x.Key].Value));
                if (row.Any())
                {
                    var rowMax = row.Max(x => x.Value.X);
                    var rowString = "";

                    for (var j = 0; j < Width; ++j)
                    {
                        if (row.Any(x => x.Value.X == j))
                        {
                            var cellPoint = row.First(x => x.Value.X == j).Key;
                            var cellItem = Nodes[cellPoint].ToString();
                            rowString += cellItem[0];
                        }
                        else
                        {
                            rowString += ' ';
                        }
                    }
                    screen[i] = rowString;
                }
            }
            return screen;
        }
    }
}
