﻿using System;
using System.Collections.Generic;

namespace adventofcode.Utilities
{
    public class StateMachine2019
    {
        public Dictionary<int, long> OriginalRegister;
        public Dictionary<int, long> Register;
        public int Position;
        public bool UsePhase;
        public long Phase;
        public long Input;
        public long Output;
        public bool Halt;
        public int RelativeBase;
        public bool WaitingForInput;
        public bool InputSet;
        public List<long> Buffer;
        public List<long> InstructionsCalled;

        public StateMachine2019()
        {
            UsePhase = false;
            OriginalRegister = new Dictionary<int, long>();
            OriginalRegister.Add(0, 99);
            Buffer = new List<long>();
            InstructionsCalled = new List<long>();
        }

        public StateMachine2019(long[] register)
        {
            UsePhase = false;
            SetMem(register);
            Buffer = new List<long>();
            InstructionsCalled = new List<long>();
        }

        public void Reset()
        {
            Halt = false;
            WaitingForInput = false;
            InputSet = false;
            Position = 0;
            RelativeBase = 0;
            Input = 0;
            Output = 0;
            InstructionsCalled.Clear();
            ClearBuffer();
            ResetRegisters();
        }

        protected long GetValue(long value, int mode)
        {
            switch (mode)
            {
                case 1: return value;
                case 2:
                    {
                        var position = (int)(RelativeBase + value);
                        if (!Register.ContainsKey(position))
                        {
                            Register.Add(position, 0);
                        }
                        return Register[position];
                    }
                default:
                    {
                        var position = (int)(value);
                        if (!Register.ContainsKey(position))
                        {
                            Register.Add(position, 0);
                        }
                        return Register[position];
                    }
            }
        }

        protected int GetDestination(int destination, int mode)
        {
            switch (mode)
            {
                case 0: return destination;
                case 1: throw new NotImplementedException();
                case 2: return destination + RelativeBase;
                default: throw new NotImplementedException();
            }
        }

        public void ResetPosition() => Position = 0;

        public void SetPosition(int value) => Position = value;

        public void ResetRelativeBase() => RelativeBase = 0;

        public void ResetRegisters()
        {
            Register.Clear();
            foreach (var x in OriginalRegister)
            {
                Register.Add(x.Key, x.Value);
            }
        }

        public long Get(int position) => Register[position];

        public void Put(long value, int position)
        {
            if (Register.Keys.Contains(position))
            {
                Register[position] = value;
                return;
            }
            Register.Add(position, value);
        }

        public void SetMem(long[] mem)
        {
            Register = new Dictionary<int, long>();
            OriginalRegister = new Dictionary<int, long>();
            for (var i = 0; i < mem.Length; ++i)
            {
                OriginalRegister.Add(i, mem[i]);
            }
            ResetRegisters();
        }

        public void SetMem(long value, int length)
        {
            var newRegister = new long[length];
            for (var i = 0; i < length; ++i)
            {
                newRegister[i] = value;
            }
            SetMem(newRegister);
        }

        public void ClearBuffer()
        {
            Buffer.Clear();
        }

        public List<long> PopBuffer()
        {
            var result = Buffer;
            Buffer = new List<long>();
            return result;
        }

        public void SetPhase(long value)
        {
            Phase = value;
        }

        public void SetInput(long value)
        {
            InputSet = true;
            Input = value;
        }

        public void Opcode1(long value1, long value2, int destination, int mode1, int mode2, int mode3)
        {
            destination = GetDestination(destination, mode3);
            var v1 = GetValue(value1, mode1);
            var v2 = GetValue(value2, mode2);
            Put(v1 + v2, destination);
        }

        public void Opcode2(long value1, long value2, int destination, int mode1, int mode2, int mode3)
        {
            destination = GetDestination(destination, mode3);
            var v1 = GetValue(value1, mode1);
            var v2 = GetValue(value2, mode2);
            Put(v1 * v2, destination);
        }

        public void Opcode3(int destination, int mode)
        {
            destination = GetDestination(destination, mode);
            if (UsePhase)
            {
                Register[destination] = Phase;
                UsePhase = false;
            }
            else
            {
                Register[destination] = Input;
            }
        }

        public void Opcode4(long value, int mode)
        {
            var v1 = GetValue(value, mode);
            Output = v1;
            Buffer.Add(v1);
        }

        public void Opcode5(long value1, long value2, int mode1, int mode2)
        {
            var p1 = GetValue(value1, mode1);
            var p2 = GetValue(value2, mode2);
            if (p1 != 0)
            {
                Position = (int)p2;
            }
            else
            {
                Position += 3;
            }
        }

        public void Opcode6(long value1, long value2, int mode1, int mode2)
        {
            var p1 = GetValue(value1, mode1);
            var p2 = GetValue(value2, mode2);
            if (p1 == 0)
            {
                Position = (int)p2;
            }
            else
            {
                Position += 3;
            }
        }

        public void Opcode7(long value1, long value2, int destination, int mode1, int mode2, int mode3)
        {
            destination = GetDestination(destination, mode3);
            var p1 = GetValue(value1, mode1);
            var p2 = GetValue(value2, mode2);
            Put(p1 < p2 ? 1 : 0, destination);
        }

        public void Opcode8(long value1, long value2, int destination, int mode1, int mode2, int mode3)
        {
            destination = GetDestination(destination, mode3);
            var p1 = GetValue(value1, mode1);
            var p2 = GetValue(value2, mode2);
            Put(p1 == p2 ? 1 : 0, destination);
        }

        public void Opcode9(long value, int mode)
        {
            var v1 = GetValue(value, mode);
            RelativeBase += (int)v1;
        }

        public void GetToPrompt()
        {
            while (!Halt && !WaitingForInput) Execute();
        }

        public void Execute()
        {
            InstructionsCalled.Clear();
            if (Halt)
            {
                return;
            }

            int opcode;
            do
            {
                var mode1 = 0;
                var mode2 = 0;
                var mode3 = 0;
                var opcodeWithModes = Register[Position];
                var opcodeWithModesAsString = opcodeWithModes.ToString();
                var opcodeAsString = opcodeWithModesAsString[opcodeWithModesAsString.Length - 1].ToString();
                if (opcodeWithModesAsString.Length > 1)
                {
                    opcodeAsString = opcodeWithModesAsString[opcodeWithModesAsString.Length - 2] + opcodeAsString;
                }
                opcode = int.Parse(opcodeAsString);
                if (opcodeWithModesAsString.Length > 2)
                    mode1 = int.Parse(opcodeWithModesAsString[opcodeWithModesAsString.Length - 3].ToString());
                if (opcodeWithModesAsString.Length > 3)
                    mode2 = int.Parse(opcodeWithModesAsString[opcodeWithModesAsString.Length - 4].ToString());
                if (opcodeWithModesAsString.Length > 4)
                    mode3 = int.Parse(opcodeWithModesAsString[opcodeWithModesAsString.Length - 5].ToString());

                InstructionsCalled.Add(opcode);
                switch (opcode)
                {
                    case 1:
                        Opcode1(Register[Position + 1], Register[Position + 2], (int)Register[Position + 3], mode1, mode2, mode3);
                        Position += 4;
                        break;
                    case 2:
                        Opcode2(Register[Position + 1], Register[Position + 2], (int)Register[Position + 3], mode1, mode2, mode3);
                        Position += 4;
                        break;
                    case 3:
                        if (InputSet)
                        {
                            Opcode3((int)Register[Position + 1], mode1);
                            Position += 2;
                            InputSet = false;
                            WaitingForInput = false;
                        }
                        else
                        {
                            WaitingForInput = true;
                            return;
                        }
                        break;
                    case 4:
                        Opcode4(Register[Position + 1], mode1);
                        Position += 2;
                        break;
                    case 5:
                        Opcode5(Register[Position + 1], Register[Position + 2], mode1, mode2);
                        break;
                    case 6:
                        Opcode6(Register[Position + 1], Register[Position + 2], mode1, mode2);
                        break;
                    case 7:
                        Opcode7(Register[Position + 1], Register[Position + 2], (int)Register[Position + 3], mode1, mode2, mode3);
                        Position += 4;
                        break;
                    case 8:
                        Opcode8(Register[Position + 1], Register[Position + 2], (int)Register[Position + 3], mode1, mode2, mode3);
                        Position += 4;
                        break;
                    case 9:
                        Opcode9(Register[Position + 1], mode1);
                        Position += 2;
                        break;
                    case 99:
                        Halt = true;
                        break;
                    default:
                        throw new NotImplementedException();
                }
            }
            while (opcode != 99 && Register.ContainsKey(Position));
            if (!Register.ContainsKey(Position))
            {
                throw new IndexOutOfRangeException("Position: " + Position);
            }
            return;
        }
    }
}
