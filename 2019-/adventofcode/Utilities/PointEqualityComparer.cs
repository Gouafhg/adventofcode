﻿using System.Collections.Generic;
using System.Drawing;

namespace adventofcode.Utilities
{
    public class PointEqualityComparer : IEqualityComparer<Point>
    {
        public bool Equals(Point p1, Point p2)
        {
            return p1 == p2; // defer to Point's existing operator==
        }

        public int GetHashCode(Point obj)
        {
            unchecked // Overflow is fine, just wrap
            {
                var hash = 17;
                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + obj.X.GetHashCode();
                hash = hash * 23 + obj.Y.GetHashCode();
                return hash;
            }
        }
    }
}
