﻿namespace adventofcode.Utilities
{
    public struct PointAnother
    {
        public int X;
        public int Y;

        public PointAnother(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public override string ToString()
        {
            return "{ " + X + ", " + Y + " }";
        }

        public static bool operator ==(PointAnother a, PointAnother b)
        {
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(PointAnother a, PointAnother b)
        {
            return !(a == b);
        }

        public override bool Equals(object o)
        {
            if (!(o is PointAnother))
            {
                return false;
            }
            if (ReferenceEquals(o, this))
            {
                return true;
            }

            var other = (PointAnother)o;

            if (null == other)
                return false;

            return X == other.X && Y == other.X;
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hash = 92821;
                // Suitable nullity checks etc, of course :)
                hash = hash * 23 + X.GetHashCode();
                hash = hash * 23 + Y.GetHashCode();
                return hash;
            }
        }
    }
}
