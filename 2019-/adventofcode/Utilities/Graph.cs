﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.Utilities
{
    public class Graph<T>
    {
        public List<Node<T>> Nodes;

        public Graph()
        {
            Nodes = new List<Node<T>>();
        }

        public Graph(List<Node<T>> nodes)
        {
            this.Nodes = new List<Node<T>>(nodes);
        }

        public (List<Node<T>> path, int distance) ShortestPathDijsktra(Node<T> a, Node<T> b)
        {
            var dist = new Dictionary<Node<T>, int>();
            var prev = new Dictionary<Node<T>, Node<T>>();
            var q = new HashSet<Node<T>>();

            foreach (var node in Nodes)
            {
                if (node.AdjacentNodes.Any(kp => kp.Value < 0))
                {
                    //Dijkstra funkar inte så bra med negativa vikter
                    throw new ArgumentOutOfRangeException();
                }

                dist.Add(node, int.MaxValue);
                prev.Add(node, null);
                q.Add(node);
            }
            dist[a] = 0;

            while (q.Count > 0)
            {
                var u = q.OrderBy(n => dist[n]).First();
                q.Remove(u);

                if (dist[u] == int.MaxValue)
                {
                    break;
                }

                if (u == b)
                {
                    break;
                }

                foreach (var n in u.AdjacentNodes.Keys)
                {
                    var alt = dist[u] + u.AdjacentNodes[n];
                    if (alt < dist[n])
                    {
                        dist[n] = alt;
                        prev[n] = u;
                    }
                }
            }

            if (prev[b] == null)
            {
                return (null, -1);
            }

            var path = new List<Node<T>> { b };
            var from = b;
            var prevNode = prev[from];
            var distance = prevNode.AdjacentNodes[b];
            path.Add(prevNode);
            while (prevNode != a)
            {
                from = prevNode;
                prevNode = prev[from];
                distance += prevNode.AdjacentNodes[from];
                path.Add(prevNode);
            }
            path.Reverse();
            return (path, distance);
        }

        public (List<Node<T>> path, int distance) ShortestPathBfs(Node<T> a, Node<T> b)
        {
            var prev = new Dictionary<Node<T>, Node<T>>();
            var q = new Queue<Node<T>>();
            var searched = new HashSet<Node<T>>();

            prev.Add(a, null);
            q.Enqueue(a);
            searched.Add(a);

            while (q.Count > 0)
            {
                var current = q.Dequeue();

                if (current == b)
                {
                    break;
                }

                foreach (var n in current.AdjacentNodes.Keys)
                {
                    if (!searched.Contains(n))
                    {
                        searched.Add(n);
                        prev.Add(n, current);
                        q.Enqueue(n);
                    }
                }
            }

            if (!prev.ContainsKey(b))
            {
                return (path: null, distance: -1);
            }

            var path = new List<Node<T>> { b };
            var from = b;
            var prevNode = prev[from];
            path.Add(prevNode);
            while (prevNode != a)
            {
                from = prevNode;
                prevNode = prev[from];
                path.Add(prevNode);
            }
            path.Reverse();
            return (path: path, distance: path.Count);
        }

        public static (List<Node<T>> path, int distance) TravellingSalesmanProblem(Node<T> s, HashSet<Node<T>> nodesToSearch)
        {
            var oldGraph = new Graph<T>();
            oldGraph.Nodes.AddRange(nodesToSearch);

            var newGraph = new Graph<T>();
            var newoldNodes = new Dictionary<Node<T>, Node<T>>();
            var oldnewNodes = new Dictionary<Node<T>, Node<T>>();
            foreach (var n in oldGraph.Nodes.Where(n => n.CanBeAdjacent))
            {
                var newNode = n.Clone();
                newGraph.Nodes.Add(newNode);
                newoldNodes.Add(n, newNode);
                oldnewNodes.Add(newNode, n);
            }
            foreach (var n1 in oldGraph.Nodes.Where(n => n.CanBeAdjacent))
            {
                foreach (var n2 in oldGraph.Nodes.Where(n => n != n1 && n.CanBeAdjacent))
                {
                    if (newoldNodes[n1].AdjacentNodes.ContainsKey(newoldNodes[n2]))
                        continue;

                    var path = oldGraph.ShortestPathBfs(n1, n2);
                    newoldNodes[n1].AdjacentNodes.Add(newoldNodes[n2], path.distance);
                    newoldNodes[n2].AdjacentNodes.Add(newoldNodes[n1], path.distance);
                }
            }

            var minPath = new List<Node<T>>();
            var minDistance = int.MaxValue;
            var permutations = newGraph.Nodes.Where(n => n != newoldNodes[s]).Permute();
            foreach (var permutation in permutations)
            {
                var distance = 0;
                var pAsList = permutation.ToList();
                pAsList.Insert(0, newoldNodes[s]);
                for (var i = 0; i < pAsList.Count - 1; ++i)
                {
                    distance += pAsList[i].AdjacentNodes[pAsList[i + 1]];
                }

                if (distance < minDistance)
                {
                    minPath = pAsList;
                    minDistance = distance;
                }
            }

            for (var i = 0; i < minPath.Count; ++i)
            {
                minPath[i] = oldnewNodes[minPath[i]];
            }

            return (path: minPath, distance: minDistance);
        }
    }
}
