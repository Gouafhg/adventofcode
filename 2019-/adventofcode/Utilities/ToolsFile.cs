﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.Utilities
{
    public static class ToolsFile
    {
        public static string ReadFile(string path)
        {
            return System.IO.File.ReadAllText(path);
        }

        public static List<string> ReadFileLines(string path)
        {
            return System.IO.File.ReadLines(path).ToList();
        }

        public static List<string> GetLines(this string file, bool keepEmptyLines = false)
        {
            return keepEmptyLines 
                ? file
                    .Split(Environment.NewLine)
                    .Select((l, i) => (l, i))
                    .Where(x => x.i % 2 == 0)
                    .Select(x => x.l)
                    .ToList() 
                : file
                    .Replace("\r", "\n")
                    .Split('\n')
                    .Where(x => !string.IsNullOrWhiteSpace(x))
                    .ToList();
        }

        public static List<List<int>> GetIntMap(this string file)
        {
            return file
                   .Replace("\r", "\n")
                   .Split('\n')
                   .Where(x => !string.IsNullOrWhiteSpace(x))
                   .Select(x => x
                       .Select(y => y.ToString())
                       .Select(int.Parse)
                       .ToList())
                   .ToList();
        }

        public static List<List<char>> GetCharMap(this string file)
        {
            return file
                .Replace("\r", "\n")
                .Split('\n')
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .Select(x => x.ToList())
                .ToList();
        }

        public static List<List<string>> GetStringMap(this string file)
        {
            return file
                .Replace("\r", "\n")
                .Split('\n')
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .Select(x => x
                    .Select(y => y.ToString())
                    .ToList())
                .ToList();
        }
    }
}
