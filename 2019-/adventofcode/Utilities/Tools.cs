﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Drawing;
using System.Linq;
using System.Numerics;

namespace adventofcode.Utilities
{
    public static class Tools
    {
        public static Point Add(this Point a, Point b)
        {
            return new Point(a.X + b.X, a.Y + b.Y);
        }

        public static Point Subtract(this Point a, Point b)
        {
            return new Point(a.X - b.X, a.Y - b.Y);
        }

        public static int ManhattanDistance(Point a, Point b)
        {
            return Math.Abs(b.X - a.X) + Math.Abs(b.Y - a.Y);
        }

        public static int ManhattanDistance((int X, int Y) a, (int X, int Y) b)
        {
            return Math.Abs(b.X - a.X) + Math.Abs(b.Y - a.Y);
        }

        public static List<Point> CreateLine(Point start, Point direction, int steps)
        {
            var position = start;
            var gcd = Gcd(Math.Abs(direction.X), Math.Abs(direction.Y));
            direction = new Point(direction.X / (int)gcd, direction.Y / (int)gcd);

            var result = new List<Point>
            {
                start,
            };
            for (var i = 0; i < steps; ++i)
            {
                result.Add(position);
                position = position.Add(direction);
            }

            return result;
        }

        public static float DistancePointLine(Point a, Point b, Point c)
        {
            var x1 = a.X;
            var y1 = a.Y;
            var x2 = b.X;
            var y2 = b.Y;
            var x0 = c.X;
            var y0 = c.Y;

            return (float)(Math.Abs((y2 - y1) * x0 - (x2 - x1) * y0 + x2 * y1 - y2 * x1) / Math.Sqrt(Math.Pow((y2 - y1), 2) + Math.Pow((x2 - x1), 2)));
        }

        public static float DistancePointLine(PointF a, PointF b, PointF c)
        {
            var x1 = a.X;
            var y1 = a.Y;
            var x2 = b.X;
            var y2 = b.Y;
            var x0 = c.X;
            var y0 = c.Y;

            return (float)(Math.Abs((y2 - y1) * x0 - (x2 - x1) * y0 + x2 * y1 - y2 * x1) / Math.Sqrt(Math.Pow((y2 - y1), 2) + Math.Pow((x2 - x1), 2)));
        }

        public static double FindDistanceToSegment(PointF pt, PointF p1, PointF p2)
        {
            PointF closest;

            var dx = p2.X - p1.X;
            var dy = p2.Y - p1.Y;
            if ((Math.Abs(dx) < float.Epsilon) && (Math.Abs(dy) < float.Epsilon))
            {
                // It's a point not a line segment.
                dx = pt.X - p1.X;
                dy = pt.Y - p1.Y;
                return Math.Sqrt(dx * dx + dy * dy);
            }

            // Calculate the t that minimizes the distance.
            var t = ((pt.X - p1.X) * dx + (pt.Y - p1.Y) * dy) /
                    (dx * dx + dy * dy);

            // See if this represents one of the segment's
            // end points or a point in the middle.
            if (t < 0)
            {
                dx = pt.X - p1.X;
                dy = pt.Y - p1.Y;
            }
            else if (t > 1)
            {
                dx = pt.X - p2.X;
                dy = pt.Y - p2.Y;
            }
            else
            {
                closest = new PointF(p1.X + t * dx, p1.Y + t * dy);
                dx = pt.X - closest.X;
                dy = pt.Y - closest.Y;
            }

            return Math.Sqrt(dx * dx + dy * dy);
        }


        public static long Gcd(long u, long v)
        {
            // simple cases (termination)
            if (u == v)
                return u;

            if (u == 0)
                return v;

            if (v == 0)
                return u;

            // look for factors of 2
            if (u % 2 == 0) // u is even
                if (v % 2 == 1) // v is odd
                    return Gcd(u >> 1, v);
                else // both u and v are even
                    return Gcd(u >> 1, v >> 1) << 1;

            if (v % 2 == 0) // u is odd, v is even
                return Gcd(u, v >> 1);

            // reduce larger argument
            if (u > v)
                return Gcd((u - v) >> 1, v);

            return Gcd((v - u) >> 1, u);
        }

        public static float FloatMod(float a, float b)
        {
            var q = (int)(a / b);
            var r = a - q * b;
            return r;
        }

        public static double FloatMod(double a, double b)
        {
            var q = (int)(a / b);
            var r = a - q * b;
            return r;
        }

        public static PointF Add(this PointF a, PointF b)
        {
            return new PointF(a.X + b.X, a.Y + b.Y);
        }

        public static PointF Subtract(this PointF a, PointF b)
        {
            return new PointF(a.X - b.X, a.Y - b.Y);
        }

        public static float Distance(Point a, Point b)
        {
            return VectorLength(b.Subtract(a));
        }

        public static float Distance(PointF a, PointF b)
        {
            return VectorLength(b.Subtract(a));
        }

        public static bool FuzzyEqualsPoints(PointF a, PointF b)
        {
            return (Math.Abs(a.X - b.X) < float.Epsilon && Math.Abs(a.Y - b.Y) < float.Epsilon);
        }

        public static float CalculateAngle(Point u, bool counterclockwise = true)
        {
            var angle = Math.Atan2(u.Y, u.X);
            if (angle < 0) angle = Math.PI + (Math.PI + angle);

            if (!counterclockwise)
            {
                angle = 2 * Math.PI - angle;
            }

            return (float)angle;
        }

        public static float CalculateAngle(Point a, Point b, bool counterclockwise = true)
        {
            var u = b.Subtract(a);
            return CalculateAngle(u, counterclockwise);
        }

        public static float CalculateAngle(Point a, Point b, float rotationRadians, bool counterclockwise = true)
        {
            var angle = CalculateAngle(a, b, counterclockwise);
            angle += rotationRadians;
            angle = FloatMod(angle, (float)Math.PI * 2);

            return (float)angle;
        }

        public static int VectorDot(Point a, Point b)
        {
            return a.X * b.X + a.Y * b.Y;
        }

        public static float VectorDot(PointF a, PointF b)
        {
            return a.X * b.X + a.Y * b.Y;
        }

        public static float VectorLength(Point v)
        {
            return (float)Math.Sqrt(VectorDot(v, v));
        }

        public static float VectorLength(PointF v)
        {
            return (float)Math.Sqrt(VectorDot(v, v));
        }

        /// <summary>Similar to <see cref="string.Substring(int,int)"/>, only for arrays. Returns a new
        /// array containing <paramref name="length"/> items from the specified
        /// <paramref name="startIndex"/> onwards.</summary>
        public static T[] Subarray<T>(this T[] array, int startIndex, int length)
        {
            if (array == null)
                throw new ArgumentNullException("array");
            if (startIndex < 0)
                throw new ArgumentOutOfRangeException("startIndex", "startIndex cannot be negative.");
            if (length < 0 || startIndex + length > array.Length)
                throw new ArgumentOutOfRangeException("length", "length cannot be negative or extend beyond the end of the array.");
            var result = new T[length];
            Array.Copy(array, startIndex, result, 0, length);
            return result;
        }

        public static IEnumerable<T[]> SearchPattern<T>(this IEnumerable<T> seq, params Func<T[], T, bool>[] matches)
        {
            Contract.Requires(seq != null);
            Contract.Requires(matches != null);
            Contract.Requires(matches.Length > 0);

            // No need to create a new array if seq is already one
            var seqArray = seq as T[] ?? seq.ToArray();

            // Check every applicable position for the matching pattern
            for (var j = 0; j <= seqArray.Length - matches.Length; j++)
            {
                // If this position matches...
                if (Enumerable.Range(0, matches.Length).All(i =>
                        matches[i](seqArray.Subarray(j, i), seqArray[i + j])))
                {
                    // ... yield it
                    yield return seqArray.Subarray(j, matches.Length);

                    // and jump to the item after the match so we don’t get overlapping matches
                    j += matches.Length - 1;
                }
            }
        }

        public static IEnumerable<IEnumerable<T>> Permute<T>(this IEnumerable<T> sequence, int k)
        {
            return Combinations(sequence, k).Cast<IEnumerable<T>>().SelectMany(c => c.Permute());
        }

        public static IEnumerable<IEnumerable<T>> Permute<T>(this IEnumerable<T> sequence)
        {
            if (sequence == null)
            {
                yield break;
            }

            var list = sequence.ToList();

            if (!list.Any())
            {
                yield return Enumerable.Empty<T>();
            }
            else
            {
                var startingElementIndex = 0;

                foreach (var startingElement in list)
                {
                    var index = startingElementIndex;
                    var remainingItems = list.Where((e, i) => i != index);

                    foreach (var permutationOfRemainder in remainingItems.Permute())
                    {
                        yield return startingElement.ConcatSequenceToItem(permutationOfRemainder);
                    }

                    startingElementIndex++;
                }
            }
        }

        public static IEnumerable<T> ConcatSequenceToItem<T>(this T firstElement, IEnumerable<T> secondSequence)
        {
            yield return firstElement;
            if (secondSequence == null)
            {
                yield break;
            }

            foreach (var item in secondSequence)
            {
                yield return item;
            }
        }

        public static IEnumerable<T> ConcatItem<T>(this IEnumerable<T> sequence, T item)
        {
            return sequence.Concat(new[] { item });
        }

        private static bool NextCombination(IList<int> num, int n, int k)
        {
            bool finished;

            var changed = finished = false;

            if (k <= 0) return false;

            for (var i = k - 1; !finished && !changed; i--)
            {
                if (num[i] < n - 1 - (k - 1) + i)
                {
                    num[i]++;

                    if (i < k - 1)
                        for (var j = i + 1; j < k; j++)
                            num[j] = num[j - 1] + 1;
                    changed = true;
                }

                finished = i == 0;
            }

            return changed;
        }

        public static IEnumerable Combinations<T>(IEnumerable<T> elements, int k)
        {
            var elem = elements.ToArray();
            var size = elem.Length;

            if (k > size) yield break;

            var numbers = new int[k];

            for (var i = 0; i < k; i++)
                numbers[i] = i;

            do
            {
                yield return numbers.Select(n => elem[n]);
            } while (NextCombination(numbers, size, k));
        }

        // Function to find modular 
        // inverse of a under modulo  
        // Assuming a, m coprimes!?
        public static BigInteger ModulusInverse(BigInteger a, BigInteger m) => BigInteger.ModPow(a, m - 2, m);

        public static (BigInteger a, BigInteger b) ExtendedEuclid(BigInteger a, BigInteger b)
        {
            (var oldR, var r) = (a, b);
            (BigInteger oldS, BigInteger s) = (1, 0);
            (BigInteger oldT, BigInteger t) = (0, 1);

            while (r != 0)
            {
                var q = oldR / r;
                (oldR, r) = (r, oldR - q * r);
                (oldS, s) = (s, oldS - q * s);
                (oldT, t) = (t, oldT - q * t);
            }

            return (oldS, oldT);
        }

        public static string GetParantheses(string v)
        {
            if (string.IsNullOrWhiteSpace(v) || v[0] != '(') return v;
            var level = 0;
            var i = 0;
            while (i < v.Length && (level != 0 || v[i] != ')'))
            {
                if (v[i] == '(') level++;
                else if (v[i] == ')') level--;
                if (level == 0 && v[i] == ')')
                {
                    break;
                }

                i++;
            }

            return new string(v.Skip(1).Take(i - 1).ToArray());
        }

        public static string[] Split(this string s, string separator, StringSplitOptions splitOptions = StringSplitOptions.None) => s.Split(separator.ToCharArray(), splitOptions);
        public static string Join<T>(this IEnumerable<T> list, string separator = "") => string.Join(separator, list);

        public static long BinToDec(string binString) => Convert.ToInt64(binString, 2);
        public static string HexToBin(string hexstring) => string.Join(string.Empty, hexstring.Select(c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')));

        public static bool StartsWith<T>(this IEnumerable<T> list, IEnumerable<T> prefix)
        {
            return !prefix.Where((item, index) => !list.ElementAtOrDefault(index)?.Equals(item) ?? true).Any();
        }

        public static bool EndsWith<T>(this IEnumerable<T> list, IEnumerable<T> prefix)
        {
            using var listEnumerator = list.Reverse().GetEnumerator();
            using var prefixEnumerator = prefix.Reverse().GetEnumerator();

            while (prefixEnumerator.MoveNext())
            {
                if (!listEnumerator.MoveNext() || !listEnumerator.Current.Equals(prefixEnumerator.Current))
                {
                    return false;
                }
            }

            return true;
        }

        public static bool Contains<T>(this IList<T> list, IEnumerable<T> part) =>
            list.Select((_, i) => list.Skip(i)).Any(subList => subList.StartsWith(part));

        public static bool IsWithinRange(this (int X, int Y) p, int resX, int resY) => p.X >= 0 && p.X < resX && p.Y >= 0 && p.Y < resY;

        public static IEnumerable<int> FindAllIndices(this string text, string pattern)
        {
            var index = text.IndexOf(pattern, StringComparison.Ordinal);
            while (index != -1)
            {
                yield return index;
                index = text.IndexOf(pattern, index + 1, StringComparison.Ordinal);
            }
        }

        public static int IndexOf<T>(this IEnumerable<T> list, IEnumerable<T> part)
        {
            var index = 0;
            foreach (var item in list)
            {
                if (item.Equals(part.ElementAtOrDefault(index)))
                {
                    index++;
                    if (index == part.Count())
                    {
                        return index;
                    }
                }
                else
                {
                    index = 0;
                }
            }

            return -1;
        }

        public static IEnumerable<int> FindAllIndices<T>(this IEnumerable<T> list, IEnumerable<T> part)
        {
            var index = 0;
            foreach (var item in list)
            {
                if (item.Equals(part.ElementAtOrDefault(index)))
                {
                    index++;
                    if (index == part.Count())
                    {
                        yield return index;
                        index = 0;
                    }
                }
                else
                {
                    index = 0;
                }
            }
        }
    }
}