﻿using System;
using System.Collections.Generic;

namespace adventofcode.Utilities
{
    public class Node<T>
    {
        public Guid Id;
        public T Value;

        public bool CanBeAdjacent;
        public Dictionary<Node<T>, int> AdjacentNodes;

        public Node()
        {
            Id = new Guid();
            CanBeAdjacent = true;
            Value = default;
            AdjacentNodes = new Dictionary<Node<T>, int>();
        }

        public Node(T value)
        {
            Id = Guid.NewGuid();
            CanBeAdjacent = true;
            Value = value;
            AdjacentNodes = new Dictionary<Node<T>, int>();
        }

        public static explicit operator T(Node<T> n) => n.Value;
        public static explicit operator Node<T>(T t)
        {
            return new Node<T>(t);
        }

        public Node<T> Clone()
        {
            var result = new Node<T>
            {
                Id = new Guid(),
                Value = this.Value,
                CanBeAdjacent = this.CanBeAdjacent,
            };
            return result;
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}
