﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace adventofcode.Utilities._2021
{
    public class Bits
    {
        public string BinaryString;

        public List<long> Literals;
        public List<int> Versions;

        public Bits(string hexString)
        {
            SetBinaryFromHex(hexString);
        }

        public void SetBinaryFromHex(string hexString)
        {
            BinaryString = string.Join("", hexString.Select(x => x.ToString()).Select(Tools.HexToBin));
        }

        public void Execute()
        {
            Literals = new List<long>();
            Versions = new List<int>();
            ReadPacket(BinaryString, Literals, Versions);
        }

        private int ReadPacket(string data, List<long> returns, List<int> versions)
        {
            var bitsRead = 0;

            var packetVersion = data.Substring(0, 3);
            versions.Add((int) Tools.BinToDec(packetVersion));
            bitsRead += 3;

            var packetIdString = data.Substring(3, 3);
            var packetId = (int) Tools.BinToDec(packetIdString);
            bitsRead += 3;

            var rest = data.Skip(6).ToList();
            switch (packetId)
            {
                case 0:
                    {
                        var literals = new List<long>();
                        bitsRead += GetPackets(new string(rest.ToArray()), literals, versions);
                        returns.Add(literals.Sum());
                        break;
                    }
                case 1:
                    {
                        var literals = new List<long>();
                        bitsRead += GetPackets(new string(rest.ToArray()), literals, versions);
                        long returnValue = 1;
                        foreach (var v in literals) returnValue *= v;
                        returns.Add(returnValue);
                        break;
                    }
                case 2:
                    {
                        var literals = new List<long>();
                        bitsRead += GetPackets(new string(rest.ToArray()), literals, versions);
                        returns.Add(literals.Min());
                        break;
                    }
                case 3:
                    {
                        var literals = new List<long>();
                        bitsRead += GetPackets(new string(rest.ToArray()), literals, versions);
                        returns.Add(literals.Max());
                        break;
                    }
                case 4:
                    {
                        var fives = new List<string>();
                        for (var i = 0; i < rest.Count; i += 5)
                        {
                            var five = rest.Skip(i).Take(5).ToArray();
                            if (five.Length == 5)
                            {
                                fives.Add(new string(five));
                                if (five.First() == '0')
                                {
                                    break;
                                }
                            }
                        }

                        var literalString = string.Join("", fives.Select(x => new string(x.Skip(1).ToArray())));
                        var literal = Tools.BinToDec(literalString);
                        returns.Add(literal);
                        bitsRead += fives.Count * 5;
                        break;
                    }
                case 5:
                    {
                        var literals = new List<long>();
                        bitsRead += GetPackets(new string(rest.ToArray()), literals, versions);
                        Debug.Assert(literals.Count == 2);
                        returns.Add(literals[0] > literals[1] ? 1 : 0);
                        break;
                    }
                case 6:
                    {
                        var literals = new List<long>();
                        bitsRead += GetPackets(new string(rest.ToArray()), literals, versions);
                        Debug.Assert(literals.Count == 2);
                        returns.Add(literals[0] < literals[1] ? 1 : 0);
                        break;
                    }
                case 7:
                    {
                        var literals = new List<long>();
                        bitsRead += GetPackets(new string(rest.ToArray()), literals, versions);
                        Debug.Assert(literals.Count == 2);
                        returns.Add(literals[0] == literals[1] ? 1 : 0);
                        break;
                    }
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return bitsRead;
        }

        private int GetPackets(string rest, List<long> returns, List<int> versions)
        {
            var bitsRead = 0;

            var opId = rest.First();
            bitsRead++;

            if (opId == '0')
            {
                const int lengthLength = 15;
                var length = (int) Tools.BinToDec(new string(rest.Skip(1).Take(lengthLength).ToArray()));
                bitsRead += lengthLength;

                var subPacketString = rest.Skip(1).Skip(lengthLength).Take(length).ToList();
                while (subPacketString.Any())
                {
                    var bits = ReadPacket(new string(subPacketString.ToArray()), returns, versions);
                    bitsRead += bits;
                    subPacketString = subPacketString.Skip(bits).ToList();
                }
            }
            else //opId == 1
            {
                const int lengthLength = 11;
                bitsRead += lengthLength;
                var subPacketCount = Tools.BinToDec(new string(rest.Skip(1).Take(lengthLength).ToArray()));

                var subPacketString = rest.Skip(1).Skip(lengthLength).ToList();
                for (var i = 0; i < subPacketCount; ++i)
                {
                    var bits = ReadPacket(new string(subPacketString.ToArray()), returns, versions);
                    bitsRead += bits;
                    subPacketString = subPacketString.Skip(bits).ToList();
                }
            }

            return bitsRead;
        }
    }
}
