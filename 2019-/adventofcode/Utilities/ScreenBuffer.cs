﻿using System;

namespace adventofcode.Utilities
{
    public class ScreenBuffer
    {
        public int Width;
        public int Height;
        public char[][] Buffer;

        public ScreenBuffer() : this(0, 0)
        {
        }

        public ScreenBuffer(int width, int height)
        {
            this.Width = width;
            this.Height = height;
            ResetBuffer();
        }

        public void ResetBuffer()
        {
            ResetBuffer(' ');
        }

        public void ResetBuffer(char c)
        {
            Buffer = new char[Height][];
            for (var y = 0; y < Height; ++y)
            {
                Buffer[y] = new char[Width];
            }
            ClearBuffer(c);
        }

        public void ClearBuffer()
        {
            ClearBuffer(' ');
        }

        public void ClearBuffer(char c)
        {
            for (var y = 0; y < Height; ++y)
            {
                for (var x = 0; x < Width; ++x)
                {
                    Buffer[y][x] = c;
                }
            }
        }

        public void Print()
        {
            for (var y = Height - 1; y >= 0; --y)
            {
                Console.WriteLine(new string(Buffer[y]));
            }
        }

    }
}
