﻿using System.Drawing;

namespace adventofcode.Utilities
{
    public struct Vector3
    {
        public bool Equals(Vector3 other)
        {
            return X == other.X && Y == other.Y && Z == other.Z;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Vector3 v))
            {
                return false;
            }

            return this == v;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = X;
                hashCode = (hashCode * 397) ^ Y;
                hashCode = (hashCode * 401) ^ Z;
                return hashCode;
            }
        }

        public int X;
        public int Y;
        public int Z;

        public static bool operator ==(Vector3 a, Vector3 b)
        {
            return a.X == b.X && a.Y == b.Y && a.Z == b.Z;
        }

        public static bool operator !=(Vector3 a, Vector3 b)
        {
            return !(a == b);
        }

        public override string ToString()
        {
            return "{" + X.ToString().PadLeft(4) + ", " + Y.ToString().PadLeft(4) + ", " + Z.ToString().PadLeft(4) + "}";
        }

        public Vector3(int x, int y, int z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public Vector3(Vector3 v)
        {
            X = v.X;
            Y = v.Y;
            Z = v.Z;
        }

        public Vector3 Add(Vector3 a)
        {
            return new Vector3(this.X + a.X, this.Y + a.Y, this.Z + a.Z);
        }

        public static Vector3 Add(Vector3 a, Vector3 b)
        {
            return new Vector3(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }

        public static Vector3 Subtract(Vector3 a, Vector3 b)
        {
            return new Vector3(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        }

        public static Vector3 Parse(string s)
        {
            var result = new Vector3();
            var startFirst = 3;
            var endFirst = startFirst;
            while (s[endFirst] != ',') endFirst++;
            result.X = int.Parse(s.Substring(startFirst, endFirst - startFirst));

            var startSecond = endFirst + 4;
            var endSecond = startSecond;
            while (s[endSecond] != ',') endSecond++;
            result.Y = int.Parse(s.Substring(startSecond, endSecond - startSecond));

            var startThird = endSecond + 4;
            var endThird = startThird;
            while (s[endThird] != '>') endThird++;
            result.Z = int.Parse(s.Substring(startThird, endThird - startThird));

            return result;
        }

        public Point ToPointXy()
        {
            return new Point(X, Y);
        }

        public Point ToPointXz()
        {
            return new Point(X, Z);
        }

        public Point ToPointYz()
        {
            return new Point(Y, Z);
        }

    }
}
