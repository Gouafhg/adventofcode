﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace adventofcode.Utilities
{
    public enum InstructionEnum
    {
        Nop,
        Acc,
        Jmp,
    }

    public class StateMachine2020
    {
        public List<(InstructionEnum instruction, int arg)> OriginalRegister;
        public List<(InstructionEnum instruction, int arg)> Register;
        public int Position;
        public bool Halt;
        public List<BigInteger> Buffer;
        public List<int> InstructionsCalled;
        public BigInteger Accumulator = 0;

        public StateMachine2020()
        {
            OriginalRegister = new List<(InstructionEnum instruction, int arg)>();
            Register = new List<(InstructionEnum instruction, int arg)>();
            Buffer = new List<BigInteger>();
            InstructionsCalled = new List<int>();
            Reset();
        }

        public StateMachine2020(List<(InstructionEnum instruction, int arg)> register)
        {
            OriginalRegister = new List<(InstructionEnum instruction, int arg)>();
            Register = new List<(InstructionEnum instruction, int arg)>();
            Buffer = new List<BigInteger>();
            InstructionsCalled = new List<int>();
            Reset();
            SetMem(register);
        }

        public void Reset()
        {
            Halt = false;
            Position = 0;
            Accumulator = 0;
            InstructionsCalled.Clear();
            ClearBuffer();
            ResetRegisters();
        }

        public void ResetPosition() => Position = 0;

        public void SetPosition(int value) => Position = value;

        public void ResetRegisters()
        {
            Register.Clear();
            Register.AddRange(OriginalRegister);
        }

        public (InstructionEnum instruction, int arg) Get(int position) => Register[position];

        public void Put((InstructionEnum instruction, int arg) value, int position)
        {
            if (Register.Count > position)
            {
                Register[position] = value;
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }
        }

        public void SetMem(List<(InstructionEnum instruction, int arg)> register)
        {
            OriginalRegister.Clear();
            OriginalRegister.AddRange(register);
            ResetRegisters();
        }

        public void SetMem((InstructionEnum instruction, int arg) value, int length)
        {
            var newRegister = new (InstructionEnum instruction, int arg)[length];
            for (var i = 0; i < length; ++i)
            {
                newRegister[i] = value;
            }
            SetMem(newRegister.ToList());
        }

        public void ClearBuffer()
        {
            Buffer.Clear();
        }

        public List<BigInteger> PopBuffer()
        {
            var result = Buffer;
            Buffer = new List<BigInteger>();
            return result;
        }

        public void InstructionNoOperation(int arg)
        {
            Position++;
        }

        public void InstructionIncreaseDecreaseAccumulator(int arg)
        {
            Accumulator += arg;
            Position++;
        }

        public void InstructionJump(int arg)
        {
            Position += arg;
        }

        public void Execute()
        {
            InstructionsCalled.Clear();
            if (Halt)
            {
                return;
            }

            do
            {
                var instruction = Register[Position];

                InstructionsCalled.Add(Position);
                switch (instruction.instruction)
                {
                    case InstructionEnum.Nop:
                        InstructionNoOperation(instruction.arg);
                        break;
                    case InstructionEnum.Acc:
                        InstructionIncreaseDecreaseAccumulator(instruction.arg);
                        break;
                    case InstructionEnum.Jmp:
                        InstructionJump(instruction.arg);
                        break;
                    default:
                        throw new NotImplementedException();
                }

                if (InstructionsCalled.Contains(Position))
                {
                    Halt = true;
                }
            }
            while (Position < Register.Count && !Halt);
        }
    }
}
