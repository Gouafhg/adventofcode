﻿using System.Numerics;

namespace adventofcode.Utilities.Extensions
{
    public static class StringExtensions
    {
        public static BigInteger ToBigInt(this string s)
        {
            return BigInteger.Parse(s.Trim());
        }
        public static BigInteger ToBigInt(this char s)
        {
            return s.ToString().ToBigInt();
        }

        public static long ToLong(this string s)
        {
            return long.Parse(s.Trim());
        }
        public static long Tolong(this char s)
        {
            return s.ToString().ToLong();
        }

        public static int ToInt(this string s)
        {
            return int.Parse(s.Trim());
        }
        public static int ToInt(this char s)
        {
            return s.ToString().ToInt();
        }
    }
}
