﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.Utilities.EqualityComparers
{
    public class ListIntTupleComparer<T> : IEqualityComparer<(List<T>, int)>
    {
        public bool Equals((List<T>, int) x, (List<T>, int) y)
        {
            return x.Item1.SequenceEqual(y.Item1) && x.Item2 == y.Item2;
        }

        public int GetHashCode((List<T>, int) obj)
        {
            var hash = new HashCode();
            hash.Add(obj.Item2);
            obj.Item1.ForEach(x => hash.Add(x));
            return hash.ToHashCode();
        }
    }
}
