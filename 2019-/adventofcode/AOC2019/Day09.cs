﻿using adventofcode.Utilities;
using System;
using System.Linq;

namespace adventofcode.AOC2019
{
    public class Day09 : Day
    {
        //Globals
        private readonly StateMachine2019 _sm;

        //Constructor
        public Day09()
        {
            //Init globals
            _sm = new StateMachine2019();

            //Setup file
            var file = ToolsFile.ReadFile(DataPath + "day9.txt");

            //Test case
            var test1 = "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99";
            //var test2 = "1102,34915192,34915192,7,4,7,99,0";
            //var test3 = "104,1125899906842624,99";
            var input = file.Split(',').Select(x => long.Parse(x)).ToArray();

            //Do Pre-stuff
            _sm.SetMem(input);
        }

        protected override string Part1()
        {
            _sm.SetInput(1);
            _sm.UsePhase = false;
            _sm.ResetRelativeBase();
            _sm.ResetPosition();

            while (!_sm.Halt)
            {
                _sm.Execute();
                var output = _sm.Output;
                var writeOutput = string.Empty;
                if (_sm.Buffer.Count == 0)
                {
                    writeOutput = _sm.Halt ? "HALT" : "EOL";
                }
                else
                {
                    writeOutput = output.ToString();
                }
                Console.WriteLine("O: " + writeOutput);
            }

            return "";
        }

        protected override string Part2()
        {
            _sm.SetInput(2);
            _sm.UsePhase = false;
            _sm.ResetRelativeBase();
            _sm.ResetPosition();

            while (!_sm.Halt)
            {
                _sm.Execute();
                var output = _sm.Output;
                var writeOutput = string.Empty;
                if (_sm.Buffer.Count == 0)
                {
                    writeOutput = _sm.Halt ? "HALT" : "EOL";
                }
                else
                {
                    writeOutput = output.ToString();
                }
                Console.WriteLine("O: " + writeOutput);
            }

            return "";
        }
    }
}
