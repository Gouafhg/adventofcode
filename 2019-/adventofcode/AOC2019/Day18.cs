﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2019
{
    internal class Day18 : Day
    {
        private readonly long _steps;

        public Point ParseDirection(Direction d)
        {
            switch (d)
            {
                case Direction.North:
                    return new Point(0, -1);
                case Direction.South:
                    return new Point(0, 1);
                case Direction.West:
                    return new Point(-1, 0);
                case Direction.East:
                    return new Point(1, 0);
                default:
                    throw new NotImplementedException();
            }
        }

        private readonly List<Direction> _directions;
        private readonly int _xR;
        private readonly int _yR;
        private readonly Dictionary<char, (int x, int y)> _keys;
        private readonly Dictionary<char, Dictionary<char, (string doors, int distance)>> _keyPaths;
        private readonly Dictionary<char, HashSet<char>> _reachableKeys;
        private readonly Dictionary<char, (int x, int y)> _robotStartPositions;
        private readonly List<List<string>> _dungeon;

        private int DistanceToCollectKeys(char currentKey, HashSet<char> keysToCollect, Dictionary<(char key, string keys), int> cache)
        {
            if (!keysToCollect.Any())
            {
                return 0;
            }

            var cacheKey = (currentKey, new string(keysToCollect.OrderBy(x => x).ToArray()));
            if (cache.ContainsKey(cacheKey))
            {
                return cache[cacheKey];
            }

            var result = int.MaxValue;
            foreach (var key in _keyPaths[currentKey].Keys)
            {
                var d = _keyPaths[currentKey][key].distance + DistanceToCollectKeys(key, keysToCollect.Where(x => x != key).ToHashSet(), cache);
                result = Math.Min(result, d);
            }

            cache.Add(cacheKey, result);
            return result;
        }

        //Constructor
        public Day18()
        {
            //Init globals
            Part = 2;
            _directions = Enum.GetValues(typeof(Direction)).Cast<Direction>().ToList();
            //Setup file
            var file = Part == 1 ? ToolsFile.ReadFileLines(DataPath + "day18.txt") : ToolsFile.ReadFileLines(DataPath + "day18_2.txt");

            //Test case
            //8
            var test1 =
                "#########\n" +
                "#b.A.@.a#\n" +
                "#########\n";

            //86
            var test2 =
                "########################\n" +
                "#f.D.E.e.C.b.A.@.a.B.c.#\n" +
                "######################.#\n" +
                "#d.....................#\n" +
                "########################\n";

            //132
            var test3 =
                "########################\n" +
                "#...............b.C.D.f#\n" +
                "#.######################\n" +
                "#.....@.a.B.c.d.A.e.F.g#\n" +
                "########################\n";

            //136
            var test4 =
                "#################\n" +
                "#i.G..c...e..H.p#\n" +
                "########.########\n" +
                "#j.A..b...f..D.o#\n" +
                "########@########\n" +
                "#k.E..a...g..B.n#\n" +
                "########.########\n" +
                "#l.F..d...h..C.m#\n" +
                "#################\n";

            //81
            var test5 =
                "########################\n" +
                "#@..............ac.GI.b#\n" +
                "###d#e#f################\n" +
                "###A#B#C################\n" +
                "###g#h#i################\n" +
                "########################\n";

            //24
            var test6 =
                "###############\n" +
                "#d.ABC.#.....a#\n" +
                "######1#2######\n" +
                "###############\n" +
                "######3#4######\n" +
                "#b.....#.....c#\n" +
                "###############\n";

            //32
            var test7 =
                "#############\n" +
                "#DcBa.#.GhKl#\n" +
                "#.###1#2#I###\n" +
                "#e#d#####j#k#\n" +
                "###C#3#4###J#\n" +
                "#fEbA.#.FgHi#\n" +
                "#############\n";

            var test = test7.Split('\n').ToList();
            var input = file;

            _dungeon = input.Where(x => !string.IsNullOrWhiteSpace(x))
                .Select(x => x.ToCharArray().Select(y => y.ToString()).ToList()).ToList();

            var robots = new List<(int x, int y)>();
            _yR = _dungeon.Count;
            _xR = _dungeon[0].Count;
            _keys = new Dictionary<char, (int x, int y)>();
            _robotStartPositions = new Dictionary<char, (int x, int y)>();
            for (var y = 0; y < _yR; ++y)
                for (var x = 0; x < _xR; ++x)
                    if (Part == 1 && _dungeon[y][x][0] >= '@' && _dungeon[y][x][0] <= '@')
                    {
                        robots.Add((x, y));
                        _robotStartPositions.Add(_dungeon[y][x][0], (x, y));
                    }
                    else if (Part == 1 && _dungeon[y][x][0] >= '1' && _dungeon[y][x][0] <= '9')
                    {
                        robots.Add((x, y));
                        _robotStartPositions.Add(_dungeon[y][x][0], (x, y));
                    }
                    else if (_dungeon[y][x][0] >= 'a' && _dungeon[y][x][0] <= 'z') _keys.Add(_dungeon[y][x][0], (x, y));

            _keyPaths = new Dictionary<char, Dictionary<char, (string doors, int distance)>>();

            _reachableKeys = new Dictionary<char, HashSet<char>>();
            foreach (var robotPos in robots)
            {
                var robotChar = _dungeon[robotPos.y][robotPos.x][0];
                _reachableKeys.Add(robotChar, new HashSet<char>());
                _keyPaths.Add(robotChar, new Dictionary<char, (string doors, int distance)>());
                foreach (var (targetKey, targetPos) in _keys.Select(x => (x.Key, x.Value)))
                {
                    var keyQ = new Queue<(int x, int y)>();
                    var distances = new Dictionary<(int x, int y), int>();
                    var prev = new Dictionary<(int x, int y), (int x, int y)>();

                    distances.Add(robotPos, 0);

                    keyQ.Enqueue(robotPos);
                    while (keyQ.Any())
                    {
                        var current = keyQ.Dequeue();

                        var currentTile = _dungeon[current.y][current.x][0];

                        if (currentTile == targetKey)
                        {
                            var path = new List<(int x, int y)> { current };
                            while (prev.ContainsKey(path.Last())) path.Add(prev[path.Last()]);
                            path.Reverse();
                            var doors = path.Select(x => _dungeon[x.y][x.x][0]).Where(x => x >= 'A' && x <= 'Z')
                                .Select(x => x).ToArray();
                            Array.Sort(doors);
                            _reachableKeys[robotChar].Add(targetKey);
                            _keyPaths[robotChar].Add(targetKey, (new string(doors), path.Count - 1));
                            break;
                        }

                        foreach (var d in _directions)
                        {
                            var p = ParseDirection(d);
                            (int x, int y) newPos = (current.x + p.X, current.y + p.Y);

                            if (distances.Keys.Contains(newPos) && distances[newPos] <= distances[current] + 1
                                || newPos.x < 0 || newPos.x >= _xR
                                || newPos.y < 0 || newPos.y >= _yR
                                || _dungeon[newPos.y][newPos.x] == "#")
                                continue;

                            prev.Add(newPos, current);
                            distances.Add(newPos, distances[current] + 1);
                            keyQ.Enqueue(newPos);
                        }
                    }
                }
            }

            foreach (var (sourceKey, sourcePos) in _keys.Select(x => (x.Key, x.Value)))
            {
                _reachableKeys.Add(sourceKey, new HashSet<char>());
                _keyPaths.Add(sourceKey, new Dictionary<char, (string doors, int distance)>());
                foreach (var (targetKey, targetPos) in _keys.Select(x => (x.Key, x.Value)))
                {
                    if (sourceKey == targetKey)
                    {
                        continue;
                    }

                    var keyQ = new Queue<(int x, int y)>();
                    var distances = new Dictionary<(int x, int y), int>();
                    var prev = new Dictionary<(int x, int y), (int x, int y)>();

                    distances.Add(sourcePos, 0);

                    keyQ.Enqueue(sourcePos);
                    while (keyQ.Any())
                    {
                        var current = keyQ.Dequeue();
                        var currentTile = _dungeon[current.y][current.x][0];

                        if (currentTile == targetKey)
                        {
                            var path = new List<(int x, int y)> { current };
                            while (prev.ContainsKey(path.Last())) path.Add(prev[path.Last()]);
                            path.Reverse();
                            var doors = path.Select(x => _dungeon[x.y][x.x][0]).Where(x => x >= 'A' && x <= 'Z')
                                .Select(x => x).ToArray();
                            Array.Sort(doors);
                            _reachableKeys[sourceKey].Add(targetKey);
                            _keyPaths[sourceKey].Add(targetKey, (new string(doors), path.Count - 1));
                            break;
                        }

                        foreach (var d in _directions)
                        {
                            var p = ParseDirection(d);
                            (int x, int y) newPos = (current.x + p.X, current.y + p.Y);

                            if (distances.Keys.Contains(newPos) && distances[newPos] <= distances[current] + 1
                                || newPos.x < 0 || newPos.x >= _xR
                                || newPos.y < 0 || newPos.y >= _yR
                                || _dungeon[newPos.y][newPos.x] == "#")
                                continue;

                            prev.Add(newPos, current);
                            distances.Add(newPos, distances[current] + 1);
                            keyQ.Enqueue(newPos);
                        }
                    }
                }
            }

            Console.WriteLine("STARTING");
            Console.WriteLine();

            var robotSteps = new Dictionary<char, int>();
            foreach (var robot in _robotStartPositions)
            {
                var robotChar = robot.Key;
                var startPos = robot.Value;

                var robotTotalKeys = _reachableKeys[robotChar];
                var otherRobotKeys = _keys.Select(x => x.Key).ToHashSet();
                otherRobotKeys.ExceptWith(robotTotalKeys);
                otherRobotKeys.Add('.');
                otherRobotKeys.Add(robotChar);

                var longestKeys = string.Empty;

                var robotQ = new Queue<((int x, int y) pos, HashSet<char> gainedKeys, int distance)>();
                var discovered = new Dictionary<(int x, int y), Dictionary<string, int>>();
                Console.WriteLine();
                Console.WriteLine(robotChar + ": " + string.Join(string.Empty, robotTotalKeys.OrderBy(x => x)));
                robotQ.Enqueue((_robotStartPositions[robotChar], otherRobotKeys, 0));
                while (robotQ.Any())
                {
                    var (pos, gainedKeys, distance) = robotQ.Dequeue();

                    var currentTile = _dungeon[pos.y][pos.x];
                    var newKeys = new HashSet<char>(gainedKeys);
                    if (currentTile[0] >= 'a' && currentTile[0] <= 'z')
                    {
                        newKeys.Add(currentTile[0]);
                        var newLength = new string(newKeys.Where(x => x >= 'a' && x <= 'z').OrderBy(x => x).ToArray());
                        if (newLength.Length > longestKeys.Length)
                        {
                            longestKeys = newLength;
                            Console.WriteLine(robotChar + ": " + longestKeys);
                        }
                    }
                    if (newKeys.Count == _keys.Count + 2)
                    {
                        robotSteps.Add(robotChar, distance);
                        break;
                    }
                    var newGainedKeys = new string(newKeys.OrderBy(x => x).ToArray());

                    var bad = false;
                    if (!newKeys.Contains(currentTile.ToLower()[0]))
                    {
                        bad = true;
                    }
                    else if (discovered.ContainsKey(pos) && discovered[pos].ContainsKey(newGainedKeys) && discovered[pos][newGainedKeys] <= distance)
                    {
                        bad = true;
                    }
                    if (bad)
                    {
                        continue;
                    }

                    if (!discovered.ContainsKey(pos))
                    {
                        discovered.Add(pos, new Dictionary<string, int>());
                    }
                    if (!discovered[pos].ContainsKey(newGainedKeys))
                    {
                        discovered[pos].Add(newGainedKeys, distance);
                    }
                    else
                    {
                        discovered[pos][newGainedKeys] = distance;
                    }

                    foreach (var d in _directions)
                    {
                        var p = ParseDirection(d);
                        var newPos = (pos.x + p.X, pos.y + p.Y);
                        robotQ.Enqueue((newPos, newKeys, distance + 1));
                    }
                }
            }
            _steps = robotSteps.Sum(x => x.Value);
            return;

        }

        protected override string Part1()
        {
            return "STEPS: " + _steps;
        }

        protected override string Part2()
        {
            return "STEPS: " + _steps;
        }
    }
}