﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2019
{
    public class Day01
    {
        public string Execute()
        {
            var file = ToolsFile.ReadFileLines(Day.DataPath + "day1.txt");

            return Part2(file.Select(x => int.Parse(x)).ToList());
        }

        private string Part1(List<int> values)
        {
            var result = 0;
            foreach (var value in values)
            {
                var tempValue = value / 3;
                tempValue = Math.Max(0, tempValue - 2);
                result += tempValue;
            }
            return result.ToString();
        }

        private string Part2(List<int> values)
        {
            var result = 0;
            foreach (var value in values)
            {
                var tempValue = int.Parse(Part1(new List<int>() { value }));
                while (tempValue != 0)
                {
                    result += tempValue;
                    tempValue = int.Parse(Part1(new List<int>() { tempValue }));
                }
            }
            return result.ToString();
        }
    }
}
