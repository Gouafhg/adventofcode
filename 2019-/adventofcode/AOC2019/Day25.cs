﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace adventofcode.AOC2019
{
    public class Day25 : Day
    {
        public const bool WriteBinary = false;
        public const string ErrorA = "\n\n\n== Pressure-Sensitive Floor ==\nAnalyzing...\n\nDoors here lead:\n- north\n\nA loud, robotic voice says \"Alert! Droids on this ship are lighter";
        public const string ErrorB = "\n\n\n== Pressure-Sensitive Floor ==\nAnalyzing...\n\nDoors here lead:\n- north\n\nA loud, robotic voice says \"Alert! Droids on this ship are heavier";

        //Globals
        private readonly StateMachine2019 _sm;

        //Constructor
        public Day25()
        {
            //Init globals
            _sm = new StateMachine2019();
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(DataPath + "day25.txt");

            //Test case
            var input = file.Split(',').Select(x => long.Parse(x)).ToArray();
            if (WriteBinary)
            {
                var fs = new FileStream(DataPath + "day25.bin", FileMode.CreateNew);
                var br = new BinaryWriter(fs);
                foreach (var i in input)
                {
                    if (i >= 0 && i < 256)
                        br.Write((byte)i);
                    else
                        br.Write(i);
                }
                br.Close();
                fs.Close();
            }

            //Do Pre-stuff
            _sm.SetMem(input);
            InitMap();
            InitExtras();
        }

        public string ConvertBuffer(List<long> buffer)
        {
            var bufferAsBytes = buffer.Select(x => (byte)x).ToArray();
            return new string(ASCIIEncoding.ASCII.GetChars(bufferAsBytes));
        }

        public List<long> ConvertStringToLong(string s)
        {
            return s.Select(x => (long)x).ToList();
        }


        public void InitMap()
        {
            ScreenOffset = (50, 0);

            MapSize = (20, 20);

            Visited = new List<(int x, int y)>();
            MapOffset = (x: MapSize.x / 2, y: MapSize.y / 2);
            CurrentPosition = MapOffset;
            Visited.Add(CurrentPosition);
            ShowMap = true;
        }

        public void InitExtras()
        {
            Inventory = new List<string>();
            Commands = new Queue<string>();
        }

        public bool ShowMap;
        public List<(int x, int y)> Visited;
        public (int x, int y) MapSize;
        public string[,] Map;
        public (int x, int y) ScreenOffset;
        public (int x, int y) MapOffset;
        public (int x, int y) CurrentPosition;

        public List<string> Inventory;
        public bool ScriptRunning = false;
        public List<string> ScriptStartInventory;
        public Queue<string> Commands;

        public void PrintMap()
        {
            for (var y = 0; y < MapSize.y; ++y)
            {
                var line = string.Empty;
                for (var x = 0; x < MapSize.x; ++x)
                {
                    Console.SetCursorPosition(ScreenOffset.x + x, ScreenOffset.y + y);
                    if (CurrentPosition == (x, y))
                    {
                        Console.Write('X');
                    }
                    else if (Visited.Any(v => v.x == x && v.y == y))
                    {
                        Console.Write('0');
                    }
                }
            }
        }

        public bool? ParseInput(string input)
        {
            if (input == "exit")
            {
                return null;
            }
            if (input == "reset")
            {
                CurrentPosition = MapOffset;
                Visited = new List<(int x, int y)> { CurrentPosition };
                return false;
            }
            if (input == "hide")
            {
                ShowMap = false;
                return false;
            }
            else if (input == "show")
            {
                ShowMap = true;
                return false;
            }
            else if (input.Length > 5 && input.Substring(0, 4) == "take")
            {
                Inventory.Add(input.Substring(5));
            }
            else if (input.Length > 5 && input.Substring(0, 4) == "drop")
            {
                Inventory.Remove(input.Substring(5));
            }
            else if (input == "runscript")
            {
                var solved = false;
                ScriptStartInventory = new List<string>(Inventory);
                for (var i = 1; i <= ScriptStartInventory.Count; ++i)
                {
                    foreach (IEnumerable<string> combination in Tools.Combinations<string>(ScriptStartInventory, i))
                    {
                        var invToComplete = new List<string>();
                        foreach (var inv in ScriptStartInventory)
                        {
                            Commands.Enqueue("drop " + inv);
                        }
                        foreach (var inv in combination)
                        {
                            invToComplete.Add(inv);
                            Commands.Enqueue("take " + inv);
                        }
                        Commands.Enqueue("south");

                        while (!solved && Commands.Count > 0)
                        {
                            var c = Commands.Dequeue();
                            _sm.ClearBuffer();
                            var scriptInput = ConvertStringToLong(c);
                            scriptInput.Add(10);
                            foreach (var cInput in scriptInput)
                            {
                                _sm.SetInput(cInput);
                                _sm.Execute();
                            }
                            _sm.GetToPrompt();
                        }
                        var answer = ConvertBuffer(_sm.Buffer);
                        var compareLength = Math.Min(answer.Length, Math.Min(ErrorA.Length, ErrorB.Length));
                        var answerShort = answer.Substring(0, compareLength);
                        var errorAShort = ErrorA.Substring(0, compareLength);
                        var errorBShort = ErrorB.Substring(0, compareLength);
                        if (answerShort != errorAShort && answerShort != errorBShort)
                        {
                            solved = true;
                        }
                        if (solved) break;
                    }
                    if (solved) break;
                }
                Commands.Clear();
                ScriptRunning = true;
            }
            else
            {
                switch (input)
                {
                    case "east":
                        CurrentPosition.x++;
                        break;
                    case "west":
                        CurrentPosition.x--;
                        break;
                    case "north":
                        CurrentPosition.y--;
                        break;
                    case "south":
                        CurrentPosition.y++;
                        break;
                }
                Visited.Add(CurrentPosition);
            }
            return true;
        }

        protected override string Part1()
        {
            _sm.Reset();
            _sm.GetToPrompt();
            while (_sm.WaitingForInput)
            {
                Console.Clear();
                Console.WriteLine();
                Console.WriteLine(ConvertBuffer(_sm.Buffer));
                if (ShowMap) PrintMap();
                Console.SetCursorPosition(0, 25);
                var command = string.Empty;
                var key = Console.ReadKey();
                switch (key.Key)
                {
                    case ConsoleKey.LeftArrow:
                        command = "west";
                        break;
                    case ConsoleKey.RightArrow:
                        command = "east";
                        break;
                    case ConsoleKey.UpArrow:
                        command = "north";
                        break;
                    case ConsoleKey.DownArrow:
                        command = "south";
                        break;
                    default:
                        var prechar = key.KeyChar;
                        command = Console.ReadLine();
                        command = prechar + command;
                        break;
                }

                var action = ParseInput(command);
                if (action.HasValue && action.Value)
                {
                    if (!ScriptRunning)
                    {
                        Commands.Enqueue(command);
                    }
                    while (Commands.Count > 0)
                    {
                        var c = Commands.Dequeue();
                        _sm.ClearBuffer();
                        var input = ConvertStringToLong(c);
                        input.Add(10);
                        foreach (var i in input)
                        {
                            _sm.SetInput(i);
                            _sm.Execute();
                        }
                        _sm.GetToPrompt();
                    }
                }
                else if (!action.HasValue)
                {
                    _sm.ClearBuffer();
                    break;
                }
                ScriptRunning = false;
            }

            return ConvertBuffer(_sm.Buffer);
        }

        protected override string Part2()
        {
            return "";
        }
    }
}
