﻿using adventofcode.Utilities;
using System;
using System.Drawing;
using System.Linq;

namespace adventofcode.AOC2019
{
    public enum TileType
    {
        Empty = 0,
        Wall = 1,
        Block = 2,
        HorizontalPaddle = 3,
        Ball = 4,
    }

    public class Tile
    {
        public TileType Type;
        public bool Indestructible => Type != TileType.Empty;

        public string Name => Type.ToString();
        public bool Ball => Type == TileType.Ball;
        public bool Paddle => Type == TileType.HorizontalPaddle;

        public Tile()
        {
            Type = TileType.Empty;
        }

        public Tile(TileType t)
        {
            Type = t;
        }

        public override string ToString()
        {
            switch (Type)
            {
                case TileType.Block:
                    return "+";
                case TileType.Wall:
                    return "#";
                case TileType.HorizontalPaddle:
                    return "_";
                case TileType.Ball:
                    return "o";
                default:
                    return " ";
            }
        }
    }

    public class Day13 : Day
    {
        //Globals
        private readonly StateMachine2019 _sm;
        private long _gameScore = 0;
        private readonly Grid<Tile> _grid;

        //Constructor
        public Day13()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(DataPath + "day13.txt");

            //Test case
            var test1 = "";
            var input = file.Split(',').Select(x => long.Parse(x)).ToArray();

            //Do pre stuff
            _sm = new StateMachine2019();
            _sm.SetMem(input);
            _sm.UsePhase = false;
            _grid = new Grid<Tile>();
        }
        public Point BallPos;
        public Point PaddlePos;
        private int _scoreStep = 0;

        public void TickGameState()
        {
            _sm.ClearBuffer();
            _sm.Execute();
            _sm.Execute();
            _sm.Execute();
            if (_sm.Buffer.Count != 3) return;

            var op1 = _sm.Buffer[0];
            var op2 = _sm.Buffer[1];
            var op3 = _sm.Buffer[2];

            if (op1 == -1 && op2 == 0)
            {
                _gameScore = (int)op3;
                Console.WriteLine(_scoreStep.ToString().PadLeft(5) + ": Score: " + _gameScore);
                _scoreStep++;
            }
            else
            {
                var x = (int)op1;
                var y = (int)op2;
                var tileId = (TileType)op3;
                if (tileId == TileType.Ball)
                {
                    BallPos = new Point(x, y);
                }
                if (tileId == TileType.HorizontalPaddle)
                {
                    PaddlePos = new Point(x, y);
                }
            }
        }

        public void ReadGameState()
        {
            _sm.Put(1, 0);
            _sm.ResetPosition();
            _sm.Halt = false;

            _grid.Nodes.Clear();

            while (!_sm.Halt)
            {
                _sm.ClearBuffer();
                _sm.Execute();
                _sm.Execute();
                _sm.Execute();
                if (_sm.Buffer.Count < 3) continue;

                var x = _sm.Buffer[0];
                var y = _sm.Buffer[1];
                var tileId = _sm.Buffer[2];
                _grid.AddNode(new Node<Tile>(new Tile((TileType)tileId)), (int)x, (int)y);
            }
        }

        public void PrintGameScreen()
        {
            Console.Clear();
            var screen = _grid.Print(x => true);
            foreach (var row in screen) Console.WriteLine(row);
        }

        protected override string Part1()
        {
            _sm.SetInput(0);
            ReadGameState();
            return _grid.Nodes.Count(x => x.Value.Value.Type == TileType.Block).ToString();
        }

        public Point GetPaddlePos()
        {
            return _grid.Nodes.First(x => x.Value.Value.Type == TileType.HorizontalPaddle).Key;
        }
        public Point GetBallPoint()
        {
            return _grid.Nodes.First(x => x.Value.Value.Type == TileType.Ball).Key;
        }

        protected override string Part2()
        {
            _sm.SetInput(0);
            ReadGameState();
            Console.WriteLine("MemPosition: " + _sm.Position);
            Console.WriteLine();
            _sm.Halt = false;

            BallPos = GetBallPoint();
            PaddlePos = GetPaddlePos();

            _sm.Put(2, 0);
            _sm.ResetPosition();
            while (_grid.Nodes.Select(x => x.Value.Value).Any(n => n.Type == TileType.Block))
            {
                if (BallPos.X < PaddlePos.X)
                {
                    _sm.SetInput(-1);
                }
                else if (BallPos.X > PaddlePos.X)
                {
                    _sm.SetInput(1);
                }
                else
                {
                    _sm.SetInput(0);
                }

                TickGameState();
            }

            return "END";
        }
    }
}
