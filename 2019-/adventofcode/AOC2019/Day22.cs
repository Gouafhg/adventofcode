﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace adventofcode.AOC2019
{
    public class Day22 : Day
    {
        private readonly BigInteger _testCardCount = 10007;
        private readonly BigInteger _testIndex = 4284;
        private const int RunPart = 2;
        private const bool RunTest = false;

        //Globals
        private readonly List<string> _input;
        private readonly List<int> _cards;

        public BigInteger ReverseDealWithNewStack(BigInteger i, BigInteger d)
        {
            return d - 1 - i;
        }

        public BigInteger ReverseDealWithIncrement(BigInteger i, BigInteger n, BigInteger d)
        {
            var modinv = Tools.ModulusInverse(n, d);
            return (modinv * i) % d;
        }

        public BigInteger ReverseCut(BigInteger i, BigInteger n, BigInteger d)
        {
            return (i + n + d) % d;
        }

        public BigInteger ParseLine(BigInteger i, string line, BigInteger d)
        {
            if (line[0] == 'd')
            {
                var words = line.Split(' ').ToList();
                if (words[2][0] == 'n')
                {
                    return ReverseDealWithNewStack(i, d);
                }
                else
                {
                    var n = BigInteger.Parse(words[3]);
                    return ReverseDealWithIncrement(i, n, d);
                }
            }
            else
            {
                var words = line.Split(' ').ToList();
                var n = BigInteger.Parse(words[1]);
                return ReverseCut(i, n, d);
            }
        }

        //Constructor
        public Day22()
        {
            //Init globals
            Part = RunPart;
            _cards = new List<int>();

            //Setup file
            var file = ToolsFile.ReadFileLines(DataPath + "day22.txt");

            //Test case
            var test1 = @"deal with increment 7
deal into new stack
deal into new stack";

            var test2 = @"cut 6
deal with increment 7
deal into new stack";

            var test3 = @"deal with increment 7
deal with increment 9
cut -2";

            var test4 = @"deal into new stack
cut -2
deal with increment 7
cut 8
cut -4
deal with increment 7
cut 3
deal with increment 9
deal with increment 3
cut -1";
            var testInput = file;// test4.Split('\n').ToList();

            _input = RunTest ? testInput.ToList() : file.ToList();

            //Do Pre-stuff
        }

        public void CardDeal(List<int> cardsToDeal, int increment)
        {
            var tempCards = new Dictionary<int, int>();
            for (var i = 0; i < cardsToDeal.Count; ++i)
            {
                tempCards.Add(i * increment % cardsToDeal.Count, cardsToDeal[i]);
            }

            for (var i = 0; i < cardsToDeal.Count; ++i)
            {
                cardsToDeal[i] = tempCards[i];
            }
        }

        public void CardCut(List<int> cardsToCut, int number)
        {
            if (number > 0)
            {
                var tempList1 = _cards.Take(number).ToList();
                var tempList2 = _cards.Skip(number).Take(cardsToCut.Count - number).ToList();
                cardsToCut.Clear();
                cardsToCut.AddRange(tempList2);
                cardsToCut.AddRange(tempList1);
            }
            else
            {
                number = Math.Abs(number);
                var tempList1 = _cards.Take(cardsToCut.Count - number).ToList();
                var tempList2 = _cards.Skip(cardsToCut.Count - number).Take(number).ToList();
                cardsToCut.Clear();
                cardsToCut.AddRange(tempList2);
                cardsToCut.AddRange(tempList1);
            }
        }

        protected override string Part1()
        {
            var cardCount = RunTest ? _testCardCount : 10007L;

            for (var i = 0; i < cardCount; ++i)
            {
                _cards.Add(i);
            }

            if (RunTest)
            {
                Console.WriteLine();
                Console.Write("{ ");
                _cards.ForEach(c => Console.Write(c + " "));
                Console.Write(" }");
                Console.WriteLine();

            }
            foreach (var line in _input)
            {
                if (line[0] == 'd')
                {
                    var words = line.Split(' ').ToList();
                    if (words[1][0] == 'w')
                    {
                        CardDeal(_cards, int.Parse(words[3]));
                    }
                    else
                    {
                        _cards.Reverse();
                    }
                }
                else
                {
                    var words = line.Split(' ').ToList();
                    CardCut(_cards, int.Parse(words[1]));
                }

                if (RunTest)
                {
                    Console.WriteLine();
                    Console.Write("{ ");
                    _cards.ForEach(c => Console.Write(c + " "));
                    Console.Write(" }");
                    Console.WriteLine();
                }
            }

            if (RunTest)
            {
                Console.WriteLine("=======");
                Console.WriteLine();
                Console.Write("{ ");
                _cards.ForEach(c => Console.Write(c + " "));
                Console.Write(" }");
                Console.WriteLine();
                return "";
            }
            else
            {
                return _cards.IndexOf(2019).ToString();
            }
        }

        protected override string Part2()
        {
            var deckSize = RunTest ? _testCardCount : 119315717514047;
            BigInteger n = 101741582076661;
            BigInteger a = 80725416546647;
            BigInteger b = 85834995146770;
            BigInteger endsupAt = 2020;
            var beginsAt = (BigInteger.ModPow(a, n, deckSize) * endsupAt + b * (BigInteger.ModPow(a, n, deckSize) - 1) * Tools.ModulusInverse(a - 1, deckSize)) % deckSize;
            Console.WriteLine(beginsAt);
            Console.Read();

            _input.Reverse();

            //EQ A
            var index = RunTest ? _testIndex : 2020;

            Console.WriteLine("---");
            Console.WriteLine("Ends with " + index);
            Console.WriteLine();
            for (var currentPosition = 0L; currentPosition < _input.Count; currentPosition++)
            {
                var inputPosition = (int)(currentPosition % _input.Count);
                var line = _input[inputPosition];
                var oldIndex = index;
                index = ParseLine(index, line, deckSize);

                //Console.WriteLine(line);
                //Console.WriteLine($"{index} -> {oldIndex}");
                //Console.WriteLine();
            }

            Console.WriteLine("Starts with " + index);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("---");

            //EQ B
            index = RunTest ? _testIndex : 4040;

            Console.WriteLine("Ends with " + index);
            Console.WriteLine();
            for (var currentPosition = 0L; currentPosition < _input.Count; currentPosition++)
            {
                var inputPosition = (int)(currentPosition % _input.Count);
                var line = _input[inputPosition];
                index = ParseLine(index, line, deckSize);

                //Console.WriteLine(line);
                //Console.WriteLine($"{index} -> {oldIndex}");
                //Console.WriteLine();
            }
            Console.WriteLine("Starts with " + index);
            Console.Read();

            if (RunTest)
            {
                Console.WriteLine(index);
                Console.WriteLine();
            }
            else
            {
                return "END AT INDEX " + index;
            }
            return "";
        }
    }
}
