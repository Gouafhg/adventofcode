﻿using adventofcode.Utilities;
using System.Linq;

namespace adventofcode.AOC2019
{
    public class Day05 : Day
    {
        //Globals
        private readonly StateMachine2019 _sm;

        //Constructor
        public Day05()
        {
            _sm = new StateMachine2019();
            //Init globals

            //Setup file
            var file = ToolsFile.ReadFile(DataPath + "day5.txt").Split(',').Select(x => long.Parse(x)).ToArray();

            //Test case
            //test = ("3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99").Split(',').Select(x => int.Parse(x)).ToArray();

            //Do Pre-stuff
            _sm.SetMem(file);
        }

        protected override string Part1()
        {
            _sm.SetPhase(1);
            _sm.ResetPosition();
            _sm.Execute();
            return _sm.Output.ToString();
        }

        protected override string Part2()
        {
            _sm.SetPhase(5);
            _sm.ResetPosition();
            _sm.Execute();
            return _sm.Output.ToString();
        }
    }
}
