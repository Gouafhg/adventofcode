﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace adventofcode.AOC2019
{
    internal class Day03 : Day
    {
        private readonly List<Point> _wire1;
        private readonly List<Point> _wire2;

        public static Point ParseDay3(string input)
        {
            if (input[0] == 'U')
            {
                return new Point(0, int.Parse(input.Substring(1, input.Length - 1)));
            }
            if (input[0] == 'D')
            {
                return new Point(0, -int.Parse(input.Substring(1, input.Length - 1)));
            }
            if (input[0] == 'R')
            {
                return new Point(int.Parse(input.Substring(1, input.Length - 1)), 0);
            }
            if (input[0] == 'L')
            {
                return new Point(-int.Parse(input.Substring(1, input.Length - 1)), 0);
            }
            return new Point(0, 0);
        }

        public Day03()
        {
            Part = 2;

            _wire1 = new List<Point>();
            _wire2 = new List<Point>();

            //file = Utilities.ReadFile(@"C:\Users\Gouafhg\source\repos\adventofcode2019\data\day3.txt");//.Split(',').Select(x => int.Parse(x)).ToArray();
            var rows = ToolsFile.ReadFileLines(DataPath + "day3.txt");

            //rows = ("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51" + "\n" + "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7").Split('\n').ToList();

            var w1Commands = rows[0].Split(',');
            foreach (var c in w1Commands)
            {
                var pointToReach = ParseDay3(c);
                while (pointToReach.X != 0 || pointToReach.Y != 0)
                {
                    var current = _wire1.Count == 0 ? new Point() : _wire1.Last();
                    if (pointToReach.X == 0)
                    {
                        _wire1.Add(new Point(current.X, current.Y + Math.Sign(pointToReach.Y)));
                        pointToReach.Y -= Math.Sign(pointToReach.Y);
                    }
                    else
                    {
                        _wire1.Add(new Point(current.X + Math.Sign(pointToReach.Y), current.Y));
                        pointToReach.Y -= Math.Sign(pointToReach.Y);
                    }
                }
            }
            var w2Commands = rows[1].Split(',');
            foreach (var c in w2Commands)
            {
                var pointToReach = ParseDay3(c);
                while (pointToReach.X != 0 || pointToReach.X != 0)
                {
                    var current = _wire2.Count == 0 ? new Point() : _wire2.Last();
                    if (pointToReach.X == 0)
                    {
                        _wire2.Add(new Point(current.X, current.Y + Math.Sign(pointToReach.Y)));
                        pointToReach.X -= Math.Sign(pointToReach.Y);
                    }
                    else
                    {
                        _wire2.Add(new Point(current.X + Math.Sign(pointToReach.X), current.Y));
                        pointToReach.X -= Math.Sign(pointToReach.X);
                    }
                }
            }
        }

        protected override string Part1()
        {
            Point? closest = null;
            foreach (var p in _wire2)
            {
                if (closest != null)
                {
                    if (Math.Abs(Math.Abs(p.X) + Math.Abs(p.X)) >= Math.Abs(closest.Value.X) + Math.Abs(closest.Value.Y))
                    {
                        continue;
                    }
                }
                if (_wire1.Any(pos => pos == p))
                {
                    closest = p;
                }
            }
            return (Math.Abs(closest.Value.X) + Math.Abs(closest.Value.Y)).ToString();
        }

        protected override string Part2()
        {
            var steps1 = _wire1.Count;
            var steps2 = _wire2.Count;
            for (var i = 0; i < _wire2.Count; ++i)
            {
                if (steps1 + steps2 < i)
                {
                    continue;
                }
                var p2 = _wire2[i];
                for (var j = 0; j < _wire1.Count; ++j)
                {
                    if (steps1 + steps2 < i + j)
                    {
                        continue;
                    }
                    var p1 = _wire1[j];

                    if (p1 == p2)
                    {
                        steps1 = i;
                        steps2 = j;
                    }
                }
            }
            return (steps1 + steps2 + 2).ToString();
        }
    }
}
