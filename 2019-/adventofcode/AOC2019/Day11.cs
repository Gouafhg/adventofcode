﻿using adventofcode.Utilities;
using System;
using System.Drawing;
using System.Linq;

namespace adventofcode.AOC2019
{
    public class Day11 : Day
    {
        //Globals
        private const long DefaultColor = 0;
        //Dictionary<Point, long> Grid;
        private readonly Grid<long> _grid;
        private Point _robot;
        private Point _robotDirection;

        private readonly StateMachine2019 _sm;

        private readonly Point _north = new Point(0, 1);
        private readonly Point _west = new Point(-1, 0);
        private readonly Point _south = new Point(0, -1);
        private readonly Point _east = new Point(1, 0);

        //Constructor
        public Day11()
        {
            //Init globals
            Part = 2;
            _grid = new Grid<long>();
            _robot = new Point(0, 0);
            _robotDirection = new Point(0, 1);
            _grid.AddNode(_robot, (Node<long>)1);

            _sm = new StateMachine2019();

            //Setup file
            var file = ToolsFile.ReadFile(DataPath + "day11.txt");

            var test1 = "";

            var input = file.Split(',').Select(x => long.Parse(x)).ToArray();
            _sm.SetMem(input);
        }

        private Point GetNewDirection(int value)
        {
            if (value == 0)
            {
                if (_robotDirection == _north)
                {
                    return _west;
                }
                if (_robotDirection == _west)
                {
                    return _south;
                }
                if (_robotDirection == _south)
                {
                    return _east;
                }
                else
                {
                    return _north;
                }
            }
            else
            {
                if (_robotDirection == _north)
                {
                    return _east;
                }
                if (_robotDirection == _east)
                {
                    return _south;
                }
                if (_robotDirection == _south)
                {
                    return _west;
                }
                else
                {
                    return _north;
                }
            }
        }

        public void RunRobot()
        {
            _sm.ResetPosition();
            _sm.ResetRelativeBase();

            while (!_sm.Halt)
            {
                if (!_grid.FilledCell(_robot))
                    _grid.AddNode(_robot, (Node<long>)DefaultColor);
                _sm.GetToPrompt();
                _sm.SetInput(_grid.Get(_robot).Value);
                _sm.Execute();
                var paintTo = _sm.Output;
                if (_sm.Buffer.Count > 0)
                {
                    _grid.SetNode(_robot, (Node<long>)paintTo);
                    _sm.ClearBuffer();
                }
                _sm.Execute();
                var rotate = _sm.Output;
                if (_sm.Buffer.Count > 0)
                {
                    _robotDirection = GetNewDirection((int)rotate);
                    _sm.ClearBuffer();
                }
                _robot = _robot.Add(_robotDirection);
            }
        }

        protected override string Part1()
        {

            RunRobot();

            return _grid.Nodes.Count.ToString();
        }

        protected override string Part2()
        {
            RunRobot();
            var screen = _grid.Print(x => x == 1);

            var result = "";
            for (var i = 0; i < screen.Length; ++i)
            {
                result += screen[i] + Environment.NewLine;
            }
            return result;
        }
    }
}
