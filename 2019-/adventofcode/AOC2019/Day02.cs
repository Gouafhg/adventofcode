﻿using adventofcode.Utilities;
using System.Linq;

namespace adventofcode.AOC2019
{
    public class Day02
    {
        private readonly StateMachine2019 _sm;

        public Day02()
        {
            _sm = new StateMachine2019();
        }

        public string Execute(int part)
        {
            var reg = ToolsFile.ReadFile(Day.DataPath + "day2.txt").Split(',').Select(x => long.Parse(x)).ToArray();
            //var file = Utilities.ReadFileLines(@"C:\Users\Gouafhg\source\repos\adventofcode2019\data\day1.txt");

            _sm.SetMem(reg);

            if (part == 1) return Part1();
            else return Part2();
        }

        private string Part1()
        {
            _sm.Register[1] = 12;
            _sm.Register[2] = 2;
            _sm.ResetPosition();

            _sm.Execute();
            return _sm.Register[0].ToString();
        }

        private string Part2()
        {
            var nounFound = -1;
            var verbFound = -1;

            var resultFound = false;

            for (var n = 0; n < 100; ++n)
            {
                for (var v = 0; v < 100; ++v)
                {
                    _sm.ResetRegisters();
                    _sm.Register[1] = n;
                    _sm.Register[2] = v;
                    _sm.ResetPosition();
                    _sm.Execute();
                    if (_sm.Register[0] == 19690720)
                    {
                        verbFound = v;
                        resultFound = true;
                        break;
                    }
                }
                if (resultFound)
                {
                    nounFound = n;
                    break;
                }
            }
            return (100 * nounFound + verbFound).ToString();
        }
    }
}
