﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2019
{
    public class Day14 : Day
    {
        //Globals
        private readonly List<string> _input;

        //Constructor
        public Day14()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(DataPath + "day14.txt");

            //Test case
            var test1 = @"9 ORE => 2 A
8 ORE => 3 B
7 ORE => 5 C
3 A, 4 B => 1 AB
5 B, 7 C => 1 BC
4 C, 1 A => 1 CA
2 AB, 3 BC, 4 CA => 1 FUEL";

            var test2 = @"157 ORE => 5 NZVS
165 ORE => 6 DCFZ
44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL
12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ
179 ORE => 7 PSHF
177 ORE => 5 HKGWZ
7 DCFZ, 7 PSHF => 2 XJWVT
165 ORE => 2 GPVTF
3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT";

            var test3 = @"2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG
17 NVRVD, 3 JNWZP => 8 VPVL
53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL
22 VJHF, 37 MNCFX => 5 FWMGM
139 ORE => 4 NVRVD
144 ORE => 7 JNWZP
5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC
5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV
145 ORE => 6 MNCFX
1 NVRVD => 8 CXFTF
1 VJHF, 6 MNCFX => 4 RFSQX
176 ORE => 6 VJHF";

            var test4 = @"171 ORE => 8 CNZTR
7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
114 ORE => 4 BHXH
14 VRPVC => 6 BMBT
6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
5 BMBT => 4 WPTQ
189 ORE => 9 KTJDG
1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
12 VRPVC, 27 CNZTR => 2 XDBXC
15 KTJDG, 12 BHXH => 5 XCVML
3 BHXH, 2 VRPVC => 7 MZWV
121 ORE => 7 VRPVC
7 XCVML => 6 RJRHP
5 BHXH, 4 VRPVC => 5 LTCX";

            _input = file.Split(Environment.NewLine.ToArray()).Where(x => x != "").ToList();

            //Do Pre-stuff
        }

        public (string amount, string name) ParseResource(string s)
        {
            return (s.Split(' ')[0], s.Split(' ')[1]);
        }

        public (List<(string name, long amount)> reqs, (string name, long amount) res) ParseRow(string row)
        {
            var reqs = new List<(string name, long amount)>();

            var rowSides = row.Split("=>".ToArray()).Select(s => s.Trim()).Where(s => s != "").ToArray();
            var left = rowSides[0];
            var right = rowSides[1];

            var parsedRight = ParseResource(right);
            var res = (name: parsedRight.name, amount: long.Parse(parsedRight.amount));

            var leftResources = left.Split(',').Where(s => s != "").Select(s => s.Trim()).ToList();
            foreach (var r in leftResources.Select(s => ParseResource(s)))
            {
                reqs.Add((name: r.name, amount: long.Parse(r.amount)));
            }

            return (reqs: reqs, res: res);
        }

        protected override string Part1()
        {
            var resRows = _input.Select(r => ParseRow(r)).ToList();

            var debtCollection = new Dictionary<string, long>();
            debtCollection.Add("ORE", 0);
            resRows.ForEach(r => debtCollection.Add(r.res.name, 0));

            debtCollection["FUEL"] = 1;
            while (debtCollection.Any(kp => kp.Key != "ORE" && kp.Value > 0))
            {
                var resourceName = debtCollection.First(kp => kp.Key != "ORE" && kp.Value > 0).Key;
                var rowContainingResource = resRows.FindAll(r => r.res.name == resourceName).SingleOrDefault();
                var times = (long)Math.Ceiling((double)debtCollection[resourceName] / rowContainingResource.res.amount);
                debtCollection[resourceName] -= rowContainingResource.res.amount * times;
                rowContainingResource.reqs.ForEach(req => debtCollection[req.name] += req.amount * times);
            }
            return debtCollection["ORE"].ToString();
        }

        protected override string Part2()
        {
            var resRows = _input.Select(r => ParseRow(r)).ToList();

            var debtCollection = new Dictionary<string, long>();
            debtCollection.Add("ORE", 0);
            resRows.ForEach(r => debtCollection.Add(r.res.name, 0));

            long lastOkOreCount = 0;
            long lastOkFuelValue = 0;
            long fuelStart = 2144000;
            long fuelDelta = 1;
            var fuelValue = fuelStart;
            while (debtCollection["ORE"] < 1000000000000)
            {
                var keys = debtCollection.Keys.ToList();
                foreach (var k in keys) debtCollection[k] = 0;
                fuelValue += fuelDelta;
                debtCollection["FUEL"] = fuelValue;

                while (debtCollection.Any(kp => kp.Key != "ORE" && kp.Value > 0))
                {
                    var resourceName = debtCollection.First(kp => kp.Key != "ORE" && kp.Value > 0).Key;
                    var rowContainingResource = resRows.FindAll(r => r.res.name == resourceName).SingleOrDefault();
                    var times = (long)Math.Ceiling((double)debtCollection[resourceName] / rowContainingResource.res.amount);
                    debtCollection[resourceName] -= rowContainingResource.res.amount * times;
                    rowContainingResource.reqs.ForEach(req => debtCollection[req.name] += req.amount * times);
                }
                if (debtCollection["ORE"] < 1000000000000)
                {
                    lastOkFuelValue = fuelValue;
                    lastOkOreCount = debtCollection["ORE"];
                }
                //Console.WriteLine($"{fuelValue} FUEL COSTS {debtCollection["ORE"].ToString()} ORE");
            }

            return $"{lastOkFuelValue} FUEL COSTS {lastOkOreCount} ORE";
        }
    }
}
