﻿using adventofcode.Utilities;
using System;
using System.Linq;
using System.Text;

namespace adventofcode.AOC2019
{
    public class Day21 : Day
    {
        //Globals
        private readonly StateMachine2019 _sm;

        //Constructor
        public Day21()
        {
            //Init globals
            Part = 2;
            _sm = new StateMachine2019();

            //Setup file
            var file = ToolsFile.ReadFile(DataPath + "day21.txt");

            //Test case
            var test1 = @"";

            var input = file.Split(',').Select(x => long.Parse(x)).ToArray();

            //Do Pre-stuff
            _sm.SetMem(input);
            _sm.Reset();
        }

        protected override string Part1()
        {
            var program =

            "NOT A J\n" +

            "NOT C T\n" +
            "OR T J\n" +

            "AND D J\n" +

            "WALK\n";

            var programAsBytes = ASCIIEncoding.ASCII.GetBytes(program.ToCharArray());
            _sm.GetToPrompt();

            var partString = new string(Encoding.ASCII.GetChars(_sm.Buffer.Select(x => (byte)x).ToArray()));
            Console.WriteLine(partString);
            Console.WriteLine("========");
            _sm.ClearBuffer();

            for (var i = 0; i < programAsBytes.Length; ++i)
            {
                _sm.SetInput(programAsBytes[i]);
                _sm.Execute();
                _sm.GetToPrompt();
            }
            _sm.Execute();

            partString = new string(Encoding.ASCII.GetChars(_sm.Buffer.Select(x => (byte)x).ToArray()));

            return partString;
        }

        protected override string Part2()
        {
            var program =

            "NOT C J\n" +
            "NOT B T\n" +
            "OR T J\n" +

            "AND D J\n" +
            "AND H J\n" +

            "NOT A T\n" +
            "OR T J\n" +

            "RUN\n";

            var programAsBytes = ASCIIEncoding.ASCII.GetBytes(program.ToCharArray());
            _sm.GetToPrompt();

            var partString = new string(Encoding.ASCII.GetChars(_sm.Buffer.Select(x => (byte)x).ToArray()));
            Console.WriteLine(partString);
            Console.WriteLine("========");
            _sm.ClearBuffer();

            for (var i = 0; i < programAsBytes.Length; ++i)
            {
                _sm.SetInput(programAsBytes[i]);
                _sm.Execute();
                _sm.GetToPrompt();
            }
            _sm.Execute();

            partString = new string(Encoding.ASCII.GetChars(_sm.Buffer.Select(x => (byte)x).ToArray()));

            return $"{partString} ({_sm.Output})";
        }
    }
}
