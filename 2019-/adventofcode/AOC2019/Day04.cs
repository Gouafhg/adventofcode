﻿using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2019
{
    internal class Day04 : Day
    {
        //Globals
        private readonly int _low = 357253;

        private readonly int _high = 892942;
        //Constructor
        public Day04()
        {
            //Init globals

            //Setup file
            //file = Utilities.ReadFile(@"C:\Users\Gouafhg\source\repos\adventofcode2019\data\day3.txt");//.Split(',').Select(x => int.Parse(x)).ToArray();
            //var rows = Utilities.ReadFileLines(@"C:\Users\Gouafhg\source\repos\adventofcode2019\data\day3.txt");

            //Test case
            //rows = ("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51" + "\n" + "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7").Split('\n').ToList();

            //Do Pre-stuff
        }

        public bool ValidPassword1(string password)
        {
            var valid = true;
            for (var i = 1; i < password.Length; ++i)
            {
                if (int.Parse(password[i].ToString()) < int.Parse(password[i - 1].ToString()))
                {
                    valid = false;
                    break;
                }
            }
            if (valid)
            {
                var adjacentFound = false;
                for (var i = 1; i < password.Length; ++i)
                {
                    if (int.Parse(password[i].ToString()) == int.Parse(password[i - 1].ToString()))
                    {
                        adjacentFound = true;
                        break;
                    }
                }
                if (!adjacentFound)
                {
                    valid = false;
                }
            }
            return valid;
        }

        public bool ValidPassword2(string password)
        {
            var valid = true;
            for (var i = 1; i < password.Length; ++i)
            {
                if (int.Parse(password[i].ToString()) < int.Parse(password[i - 1].ToString()))
                {
                    valid = false;
                    break;
                }
            }
            if (valid)
            {
                var adjacentFound = false;
                var repeatCount = new Dictionary<char, int>();
                for (var i = 1; i < password.Length; ++i)
                {
                    if (int.Parse(password[i].ToString()) == int.Parse(password[i - 1].ToString()))
                    {
                        adjacentFound = true;
                        if (repeatCount.ContainsKey(password[i]))
                        {
                            repeatCount[password[i]]++;
                        }
                        else
                        {
                            repeatCount.Add(password[i], 2);
                        }
                    }
                }
                if (!adjacentFound || !repeatCount.Any(kp => repeatCount[kp.Key] == 2))
                {
                    valid = false;
                }
            }
            return valid;
        }

        protected override string Part1()
        {
            //var pass = "123789";
            //return ValidPassword(pass) ? 1 : 0;

            var allValues = new HashSet<string>();
            for (var i = _low; i <= _high; ++i)
            {
                allValues.Add(i.ToString());
            }

            var toRemove = new HashSet<string>();
            foreach (var value in allValues)
            {
                if (!ValidPassword1(value))
                {
                    toRemove.Add(value);
                }
            }


            return (allValues.Count - toRemove.Count).ToString();
        }

        protected override string Part2()
        {
            //var pass = "111122";
            //return ValidPassword2(pass) ? 1 : 0;

            var allValues = new HashSet<string>();
            for (var i = _low; i <= _high; ++i)
            {
                allValues.Add(i.ToString());
            }

            var toRemove = new HashSet<string>();
            foreach (var value in allValues)
            {
                if (!ValidPassword2(value))
                {
                    toRemove.Add(value);
                }
            }


            return (allValues.Count - toRemove.Count).ToString();
        }
    }
}
