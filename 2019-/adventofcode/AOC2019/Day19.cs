﻿using adventofcode.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2019
{
    internal class Day19 : Day
    {
        //Globals
        private readonly StateMachine2019 _sm;

        //Constructor
        public Day19()
        {
            //Init globals
            Part = 1;
            _sm = new StateMachine2019();

            //Setup file
            var file = ToolsFile.ReadFile(DataPath + "day19.txt");

            //Test case
            var test1 = @"";

            var input = file.Split(',').Select(x => long.Parse(x)).ToArray();

            //Do Pre-stuff
            _sm.SetMem(input);

        }

        public bool GetXy(int x, int y)
        {
            _sm.Reset();

            _sm.GetToPrompt();
            _sm.SetInput(x);
            _sm.Execute();

            _sm.GetToPrompt();
            _sm.SetInput(y);
            _sm.Execute();

            return _sm.Output == 1;
        }

        protected override string Part1()
        {
            var xBufferSize = 100;
            var yBufferSize = 100;
            var screenBuffer = new string[yBufferSize][];
            for (var i = 0; i < 100; ++i) screenBuffer[i] = new string[xBufferSize];
            (int x, int y) hitPosition = (-1, -1);

            var totalHits = 0;
            var foundAll = false;

            for (var aY = 0; aY < 10000 && !foundAll; ++aY)
            {
                var foundOnLine = false;
                for (var aX = 0; aX < 10000 && !foundAll; ++aX)
                {
                    var p1 = GetXy(aX, aY);
                    if (!p1 && foundOnLine)
                    {
                        break;
                    }
                    foundOnLine = p1;
                    if (p1)
                    {
                        var points = new List<bool>
                        {

                            GetXy(aX + 99, aY),
                            GetXy(aX, aY + 99),
                            GetXy(aX + 99, aY + 99),
                        };

                        foundAll = points.All(p => p);

                        if (foundAll)
                        {
                            hitPosition = (aX, aY);
                        }
                    }
                }
            }

            return (totalHits + " at " + (hitPosition.x * 10000 + hitPosition.y));
        }

        protected override string Part2()
        {
            return "";
        }
    }
}
