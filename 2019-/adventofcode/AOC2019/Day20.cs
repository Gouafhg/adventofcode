﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace adventofcode.AOC2019
{
    public class SourceTarget : Node<string>
    {
        public SourceTarget() : base() { }

        public SourceTarget(string nodeValue) : base(nodeValue) { }
    }

    public class Portal : Node<string>
    {
        public Portal LinkedPortal;

        public int Direction;

        public Portal() : base() { }

        public Portal(string nodeValue) : base(nodeValue) { }
    }

    public class SearchPath
    {
        public Guid Id;
        public int Level;

        public SearchPath()
        {
            Level = 0;
            Id = new Guid();
        }

        public SearchPath(int level)
        {
            Level = level;
            Id = new Guid();
        }
    }

    public class Day20 : Day
    {
        public Point ParseDirection(Direction d)
        {
            switch (d)
            {
                case Direction.North:
                    return new Point(0, -1);
                case Direction.South:
                    return new Point(0, 1);
                case Direction.West:
                    return new Point(-1, 0);
                case Direction.East:
                    return new Point(1, 0);
                default:
                    throw new NotImplementedException();
            }
        }

        public Direction GetOppositeDirection(Direction d)
        {
            switch (d)
            {
                case Direction.South: return Direction.North;
                case Direction.North: return Direction.South;
                case Direction.West: return Direction.East;
                case Direction.East: return Direction.West;
                default: throw new NotImplementedException();
            }
        }

        public Direction GetLeftDirection(Direction d)
        {
            switch (d)
            {
                case Direction.South: return Direction.East;
                case Direction.North: return Direction.West;
                case Direction.West: return Direction.South;
                case Direction.East: return Direction.North;
                default: throw new NotImplementedException();
            }
        }

        public Direction GetRightDirection(Direction d)
        {
            switch (d)
            {
                case Direction.South: return Direction.West;
                case Direction.North: return Direction.East;
                case Direction.West: return Direction.North;
                case Direction.East: return Direction.South;
                default: throw new NotImplementedException();
            }
        }

        public Direction ParseToDirection(Point d)
        {
            if (d.Y == 0)
            {
                if (d.X == -1)
                {
                    return Direction.West;
                }
                return Direction.East;
            }
            if (d.Y == 1)
            {
                return Direction.North;
            }
            return Direction.South;
        }

        //Globals
        private readonly List<string> _input;
        private Grid<string> _grid;
        private Graph<string> _graph;

        private SourceTarget _sourceNode;
        private SourceTarget _destinationNode;
        private readonly HashSet<Direction> _directions;

        //Constructor
        public Day20()
        {
            _directions = Enum.GetValues(typeof(Direction)).Cast<Direction>().ToHashSet();
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFileLines(DataPath + "day20.txt");

            //Test case
            var test1 =
@"         A           
         A           
  #######.#########  
  #######.........#  
  #######.#######.#  
  #######.#######.#  
  #######.#######.#  
  #####  B    ###.#  
BC...##  C    ###.#  
  ##.##       ###.#  
  ##...DE  F  ###.#  
  #####    G  ###.#  
  #########.#####.#  
DE..#######...###.#  
  #.#########.###.#  
FG..#########.....#  
  ###########.#####  
             Z       
             Z       ";

            var test2 = @"                   A               
                   A               
  #################.#############  
  #.#...#...................#.#.#  
  #.#.#.###.###.###.#########.#.#  
  #.#.#.......#...#.....#.#.#...#  
  #.#########.###.#####.#.#.###.#  
  #.............#.#.....#.......#  
  ###.###########.###.#####.#.#.#  
  #.....#        A   C    #.#.#.#  
  #######        S   P    #####.#  
  #.#...#                 #......VT
  #.#.#.#                 #.#####  
  #...#.#               YN....#.#  
  #.###.#                 #####.#  
DI....#.#                 #.....#  
  #####.#                 #.###.#  
ZZ......#               QG....#..AS
  ###.###                 #######  
JO..#.#.#                 #.....#  
  #.#.#.#                 ###.#.#  
  #...#..DI             BU....#..LF
  #####.#                 #.#####  
YN......#               VT..#....QG
  #.###.#                 #.###.#  
  #.#...#                 #.....#  
  ###.###    J L     J    #.#.###  
  #.....#    O F     P    #.#...#  
  #.###.#####.#.#####.#####.###.#  
  #...#.#.#...#.....#.....#.#...#  
  #.#####.###.###.#.#.#########.#  
  #...#.#.....#...#.#.#.#.....#.#  
  #.###.#####.###.###.#.#.#######  
  #.#.........#...#.............#  
  #########.###.###.#############  
           B   J   C               
           U   P   P               ";

            var test3 =
@"             Z L X W       C                 
             Z P Q B       K                 
  ###########.#.#.#.#######.###############  
  #...#.......#.#.......#.#.......#.#.#...#  
  ###.#.#.#.#.#.#.#.###.#.#.#######.#.#.###  
  #.#...#.#.#...#.#.#...#...#...#.#.......#  
  #.###.#######.###.###.#.###.###.#.#######  
  #...#.......#.#...#...#.............#...#  
  #.#########.#######.#.#######.#######.###  
  #...#.#    F       R I       Z    #.#.#.#  
  #.###.#    D       E C       H    #.#.#.#  
  #.#...#                           #...#.#  
  #.###.#                           #.###.#  
  #.#....OA                       WB..#.#..ZH
  #.###.#                           #.#.#.#  
CJ......#                           #.....#  
  #######                           #######  
  #.#....CK                         #......IC
  #.###.#                           #.###.#  
  #.....#                           #...#.#  
  ###.###                           #.#.#.#  
XF....#.#                         RF..#.#.#  
  #####.#                           #######  
  #......CJ                       NM..#...#  
  ###.#.#                           #.###.#  
RE....#.#                           #......RF
  ###.###        X   X       L      #.#.#.#  
  #.....#        F   Q       P      #.#.#.#  
  ###.###########.###.#######.#########.###  
  #.....#...#.....#.......#...#.....#.#...#  
  #####.#.###.#######.#######.###.###.#.#.#  
  #.......#.......#.#.#.#.#...#...#...#.#.#  
  #####.###.#####.#.#.#.#.###.###.#.###.###  
  #.......#.....#.#...#...............#...#  
  #############.#.#.###.###################  
               A O F   N                     
               A A D   M                     ";

            var test = test3.Split('\n');
            _input = file.ToList();

            //Do Pre-stuff
            InitiateFloor();
        }

        public void InitiateFloor()
        {
            _grid = new Grid<string>();
            for (var y = 0; y < _input.Count; ++y)
            {
                for (var x = 0; x < _input[y].Length; ++x)
                {
                    var inputChar = _input[y][x];
                    var inputCharAsValue = (byte)inputChar;
                    if (inputCharAsValue >= 65 && inputCharAsValue < 91)
                    {
                        _grid.AddNode(new Point(x, y), new Portal("" + inputChar));
                    }
                    else if (inputChar == '.')
                    {
                        _grid.AddNode(new Point(x, y), (Node<string>)("" + inputChar));
                    }

                }
            }
            foreach (var n in _grid.Nodes)
            {
                if (n.Value.AdjacentNodes.Count == 4) continue;
                foreach (var d in _directions)
                {
                    var an = _grid.Get(n.Key.Add(ParseDirection(d)));
                    if (an != null && an.CanBeAdjacent && !n.Value.AdjacentNodes.ContainsKey(an))
                    {
                        n.Value.AdjacentNodes.Add(an, 1);
                        an.AdjacentNodes.Add(n.Value, 1);
                    }
                }
            }

            _graph = new Graph<string>();
            foreach (var n in _grid.Nodes.Values)
            {
                _graph.Nodes.Add(n);
            }

            var portals = _graph.Nodes.OfType<Portal>().ToList();

            var toRemove = new List<Portal>();
            for (var i = 0; i < portals.Count; ++i)
            {
                var n = portals[i];
                if (n.AdjacentNodes.All(an => an.Key.Value != "."))
                {
                    n.AdjacentNodes.Single().Key.Value += n.Value;
                    toRemove.Add(n);
                }
            }
            foreach (var n in toRemove)
            {
                _grid.ClearCell(_grid.GetCoordinates(n));
                _graph.Nodes.Remove(n);
                portals.Remove(n);
                foreach (var p in portals)
                {
                    p.AdjacentNodes.Remove(n);
                }
            }

            foreach (var portal in portals)
            {
                portal.Value = new string(portal.Value.ToCharArray().OrderBy(x => x).ToArray());
            }

            portals = portals.OrderBy(x => x.Value).ToList();

            var sd = portals.Where(p => portals.Count(op => op.Value == p.Value) == 1).ToList();
            _sourceNode = new SourceTarget(sd[0].Value);
            _destinationNode = new SourceTarget(sd[1].Value);
            foreach (var sourceAdjacentNode in sd[0].AdjacentNodes)
            {
                _sourceNode.AdjacentNodes.Add(sourceAdjacentNode.Key, sourceAdjacentNode.Value);
            }
            foreach (var destinationAdjacentNode in sd[1].AdjacentNodes)
            {
                _destinationNode.AdjacentNodes.Add(destinationAdjacentNode.Key, destinationAdjacentNode.Value);
            }

            toRemove.Clear();
            toRemove.Add(sd[0]);
            toRemove.Add(sd[1]);
            var sourcePoint = _grid.GetCoordinates(sd[0]);
            var destinationPoint = _grid.GetCoordinates(sd[1]);
            foreach (var n in toRemove)
            {
                _grid.ClearCell(sourcePoint);
                _grid.ClearCell(destinationPoint);
                _graph.Nodes.Remove(sd[0]);
                _graph.Nodes.Remove(sd[1]);
                portals.Remove(n);
            }

            foreach (var n in _graph.Nodes)
            {
                if (n.AdjacentNodes.ContainsKey(sd[0]))
                {
                    n.AdjacentNodes.Remove(sd[0]);
                    n.AdjacentNodes.Add(_sourceNode, 1);
                }
                if (n.AdjacentNodes.ContainsKey(sd[1]))
                {
                    n.AdjacentNodes.Remove(sd[1]);
                    n.AdjacentNodes.Add(_destinationNode, 1);
                }
            }

            _grid.SetNode(sourcePoint, _sourceNode);
            _grid.SetNode(destinationPoint, _destinationNode);
            _graph.Nodes.Add(_sourceNode);
            _graph.Nodes.Add(_destinationNode);

            var midPoint = new Point(_grid.Width / 2, _grid.Height / 2);
            foreach (var portal in portals)
            {
                portal.LinkedPortal = portals.Single(p => p.Value == portal.Value && p != portal);
            }
            if (Part == 2)
            {
                foreach (var portal in portals)
                {
                    var p1 = portal;
                    var p2 = portal.LinkedPortal;
                    var p1C = _grid.GetCoordinates(p1);
                    var p2C = _grid.GetCoordinates(p2);
                    var w = _grid.Width;
                    var h = _grid.Height;
                    if (p1C.Y == 119 || p1C.Y == 1 || p1C.X == 1 || p1C.X == 123)
                    {
                        p1.Direction = 1;
                        p2.Direction = -1;
                    }
                    else if (p2C.Y == 119 || p2C.Y == 1 || p2C.X == 1 || p2C.X == 123)
                    {
                        p1.Direction = -1;
                        p2.Direction = 1;
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
            }
            foreach (var node in _graph.Nodes)
            {
                foreach (var st in _graph.Nodes.OfType<SourceTarget>())
                {
                    node.AdjacentNodes.Remove(st);
                }
            }
        }

        public (List<Node<string>> path, int distance) ShortestPathBfs(Node<string> start, Node<string> destination)
        {
            var prev = new Dictionary<(int level, Node<string> node), (int level, Node<string> node)>();
            var q = new Queue<(int level, Node<string> node)>();
            var searchedLevels = new List<int>();
            var searchedNodes = new List<Node<string>>();

            prev.Add((0, start), (0, null));
            q.Enqueue((0, start));
            searchedLevels.Add(0);
            searchedNodes.Add(start);

            var dc = _grid.GetCoordinates(destination);
            while (q.Count > 0)
            {
                var current = q.Dequeue();

                if (current.level == 0 && current.node == destination)
                {
                    break;
                }

                if (current.node is Portal cPortal)
                {
                    var oldLevel = current.level;
                    current.level += cPortal.Direction;
                    current.node = cPortal.LinkedPortal.AdjacentNodes.Single().Key;
                    prev.Add(current, prev[(oldLevel, cPortal)]);
                    searchedLevels.Add(current.level);
                    searchedNodes.Add(cPortal.LinkedPortal);
                    searchedLevels.Add(current.level);
                    searchedNodes.Add(current.node);
                }

                foreach (var n in current.node.AdjacentNodes.Keys)
                {
                    var result = Enumerable.Range(0, searchedNodes.Count).Where(i => searchedNodes[i] == n).ToList();
                    var hasSearched = result.Any(i => searchedLevels[i] == current.level);
                    if (!hasSearched && !(current.level == 0 && n is Portal nPortal && nPortal.Direction > 0))
                    {
                        searchedLevels.Add(current.level);
                        searchedNodes.Add(n);
                        prev.Add((current.level, n), current);
                        q.Enqueue((current.level, n));
                    }
                }
            }

            if (!prev.ContainsKey((0, destination)))
            {
                return (path: null, distance: -1);
            }

            var path = new List<Node<string>> { destination };
            var from = (0, (Node<string>)destination);
            var prevNode = prev[from];
            var distance = 0;
            path.Add(prevNode.node);
            while (prevNode.node != start)
            {
                from = prevNode;
                prevNode = prev[from];
                distance += 1;
                path.Add(prevNode.node);
            }
            distance++;
            path.Reverse();

            return (path: path, distance: distance);
        }

        protected override string Part1()
        {
            var shortestPath = ShortestPathBfs(_sourceNode.AdjacentNodes.Single().Key, _destinationNode.AdjacentNodes.Single().Key);
            var i = 1;
            shortestPath.path.ForEach(n => Console.WriteLine((i++).ToString().PadRight(5) + ": " + _grid.GetCoordinates(n).ToString().PadRight(20) + n.Value.ToString()));
            Console.WriteLine();
            Console.WriteLine();
            return shortestPath.distance.ToString();
        }

        protected override string Part2()
        {
            var shortestPath = ShortestPathBfs(_sourceNode.AdjacentNodes.Single().Key, _destinationNode.AdjacentNodes.Single().Key);
            var i = 1;
            shortestPath.path.ForEach(n => Console.WriteLine((i++).ToString().PadRight(5) + ": " + _grid.GetCoordinates(n).ToString().PadRight(20) + n.Value.ToString()));
            Console.WriteLine();
            Console.WriteLine();
            return shortestPath.distance.ToString();
        }
    }
}
