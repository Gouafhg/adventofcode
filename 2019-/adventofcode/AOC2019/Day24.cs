﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace adventofcode.AOC2019
{
    public class Day24 : Day
    {
        public Point ParseDirection(Direction d)
        {
            switch (d)
            {
                case Direction.North:
                    return new Point(0, -1);
                case Direction.South:
                    return new Point(0, 1);
                case Direction.West:
                    return new Point(-1, 0);
                case Direction.East:
                    return new Point(1, 0);
                default:
                    throw new NotImplementedException();
            }
        }

        public Direction GetOppositeDirection(Direction d)
        {
            switch (d)
            {
                case Direction.South: return Direction.North;
                case Direction.North: return Direction.South;
                case Direction.West: return Direction.East;
                case Direction.East: return Direction.West;
                default: throw new NotImplementedException();
            }
        }

        public Direction GetLeftDirection(Direction d)
        {
            switch (d)
            {
                case Direction.South: return Direction.East;
                case Direction.North: return Direction.West;
                case Direction.West: return Direction.South;
                case Direction.East: return Direction.North;
                default: throw new NotImplementedException();
            }
        }

        public Direction GetRightDirection(Direction d)
        {
            switch (d)
            {
                case Direction.South: return Direction.West;
                case Direction.North: return Direction.East;
                case Direction.West: return Direction.North;
                case Direction.East: return Direction.South;
                default: throw new NotImplementedException();
            }
        }

        public Direction ParseToDirection(Point d)
        {
            if (d.Y == 0)
            {
                if (d.X == -1)
                {
                    return Direction.West;
                }
                return Direction.East;
            }
            if (d.Y == 1)
            {
                return Direction.North;
            }
            return Direction.South;
        }













        //Globals
        private readonly HashSet<Direction> _directions;
        private HashSet<ulong> _biodiversities;
        private readonly List<(int level, char[,] grid)> _charGrids;
        private readonly string[] _input;


        public char[,] InitializeGrid()
        {
            var result = new char[5, 5];
            for (var i = 0; i < 25; ++i)
            {
                result[i / 5, i % 5] = '.';
            }
            result[2, 2] = '?';
            return result;
        }

        //Constructor
        public Day24()
        {
            _directions = Enum.GetValues(typeof(Direction)).Cast<Direction>().ToHashSet();

            //Init globals
            Part = 2;
            _biodiversities = new HashSet<ulong>();
            //Setup file
            var file = "#.#..\n.....\n.#.#.\n.##..\n.##.#";

            //Test case
            var test = "....#\n#..#.\n#.?##\n..#..\n#....";

            _input = file.Split('\n');

            //Do Pre-stuff
            var firstGrid = InitializeGrid();
            for (var y = 0; y < _input.Length; ++y)
            {
                for (var x = 0; x < _input[y].Length; ++x)
                {
                    firstGrid[x, y] = _input[y][x];
                }
            }

            _charGrids = new List<(int, char[,])>();
            _charGrids.Add((0, firstGrid));
        }

        public void Tick()
        {
            var old = new List<(int level, char[,] grid)>();
            foreach (var tuple in _charGrids)
            {
                var buffer = new char[5, 5];
                Buffer.BlockCopy(tuple.grid, 0, buffer, 0, 5 * 5 * sizeof(char));
                old.Add((tuple.level, buffer));
            }

            foreach (var tuple in old)
            {
                var currentRealTupleIndex = _charGrids.FindIndex(t => t.level == tuple.level);
                var currentRealTuple = _charGrids[currentRealTupleIndex];
                for (var y = 0; y < 5; ++y)
                {
                    for (var x = 0; x < 5; ++x)
                    {
                        if (x == 2 && y == 2)
                        {
                            continue;
                        }

                        var neighbours = 0;
                        var currentPoint = new Point(x, y);
                        foreach (var d in _directions)
                        {
                            var directionAsPoint = ParseDirection(d);
                            var newPoint = currentPoint.Add(directionAsPoint);
                            if (newPoint.X == 2 && newPoint.Y == 2)
                            {
                                var innerGridIndex = old.FindIndex(t => t.level == tuple.level + 1);
                                if (innerGridIndex == -1)
                                {
                                    continue;
                                }
                                var innerGrid = old[innerGridIndex].grid;
                                if (currentPoint.X == 3)
                                {
                                    for (var iy = 0; iy < 5; ++iy)
                                    {
                                        if (innerGrid[4, iy] == '#')
                                        {
                                            neighbours++;
                                        }
                                    }
                                }
                                else if (currentPoint.X == 1)
                                {
                                    for (var iy = 0; iy < 5; ++iy)
                                    {
                                        if (innerGrid[0, iy] == '#')
                                        {
                                            neighbours++;
                                        }
                                    }
                                }
                                else if (currentPoint.Y == 3)
                                {
                                    for (var ix = 0; ix < 5; ++ix)
                                    {
                                        if (innerGrid[ix, 4] == '#')
                                        {
                                            neighbours++;
                                        }
                                    }
                                }
                                else if (currentPoint.Y == 1)
                                {
                                    for (var ix = 0; ix < 5; ++ix)
                                    {
                                        if (innerGrid[ix, 0] == '#')
                                        {
                                            neighbours++;
                                        }
                                    }
                                }
                            }
                            else if (newPoint.X >= 0 && newPoint.X < 5 && newPoint.Y >= 0 && newPoint.Y < 5)
                            {
                                if (tuple.grid[newPoint.X, newPoint.Y] == '#')
                                {
                                    neighbours++;
                                }
                            }
                            else
                            {
                                var outergridIndex = old.FindIndex(t => t.level == tuple.level - 1);
                                if (outergridIndex == -1)
                                {
                                    continue;
                                }
                                var outerGrid = old[outergridIndex].grid;
                                if (newPoint.X == 5)
                                {
                                    if (outerGrid[3, 2] == '#')
                                    {
                                        neighbours++;
                                    }
                                }
                                else if (newPoint.X == -1)
                                {
                                    if (outerGrid[1, 2] == '#')
                                    {
                                        neighbours++;
                                    }
                                }
                                if (newPoint.Y == 5)
                                {
                                    if (outerGrid[2, 3] == '#')
                                    {
                                        neighbours++;
                                    }
                                }
                                if (newPoint.Y == -1)
                                {
                                    if (outerGrid[2, 1] == '#')
                                    {
                                        neighbours++;
                                    }
                                }
                            }
                        }

                        if (tuple.grid[x, y] == '#')
                        {
                            if (neighbours != 1)
                            {
                                currentRealTuple.grid[x, y] = '.';
                            }
                        }
                        else
                        {
                            if (neighbours == 1 || neighbours == 2)
                            {
                                currentRealTuple.grid[x, y] = '#';
                            }
                        }
                    }
                }
            }
            var centerLevel = old.Max(t => t.level);
            var centerGridIndex = old.FindIndex(t => t.level == centerLevel);
            var centerGrid = old[centerGridIndex].grid;

            var newGridCenter = false;
            for (var y = 1; y <= 3 && !newGridCenter; ++y)
            {
                for (var x = 1; x <= 3; ++x)
                {
                    if ((x == 2) || (y == 2))
                    {
                        if (centerGrid[x, y] == '#')
                        {
                            newGridCenter = true;
                            break;
                        }
                    }
                }
            }
            if (newGridCenter)
            {
                var oldCenterGrid = InitializeGrid();
                var newCenterGrid = InitializeGrid();
                var oldTuple = (level: centerLevel + 1, grid: oldCenterGrid);
                var newTuple = (level: centerLevel + 1, grid: newCenterGrid);
                old.Add(oldTuple);
                _charGrids.Add(newTuple);

                var currentRealTuple = newTuple;
                for (var y = 0; y < 5; ++y)
                {
                    for (var x = 0; x < 5; ++x)
                    {
                        if (x == 2 && y == 2)
                        {
                            continue;
                        }

                        var neighbours = 0;
                        var currentPoint = new Point(x, y);
                        foreach (var d in _directions)
                        {
                            var directionAsPoint = ParseDirection(d);
                            var newPoint = currentPoint.Add(directionAsPoint);
                            if (newPoint.X == 2 && newPoint.Y == 2)
                            {
                                var innerGridIndex = old.FindIndex(t => t.level == oldTuple.level + 1);
                                if (innerGridIndex == -1)
                                {
                                    continue;
                                }
                                var innerGrid = old[innerGridIndex].grid;
                                if (currentPoint.X == 3)
                                {
                                    for (var iy = 0; iy < 5; ++iy)
                                    {
                                        if (innerGrid[4, iy] == '#')
                                        {
                                            neighbours++;
                                        }
                                    }
                                }
                                else if (currentPoint.X == 1)
                                {
                                    for (var iy = 0; iy < 5; ++iy)
                                    {
                                        if (innerGrid[0, iy] == '#')
                                        {
                                            neighbours++;
                                        }
                                    }
                                }
                                else if (currentPoint.Y == 3)
                                {
                                    for (var ix = 0; ix < 5; ++ix)
                                    {
                                        if (innerGrid[ix, 4] == '#')
                                        {
                                            neighbours++;
                                        }
                                    }
                                }
                                else if (currentPoint.Y == 1)
                                {
                                    for (var ix = 0; ix < 5; ++ix)
                                    {
                                        if (innerGrid[ix, 0] == '#')
                                        {
                                            neighbours++;
                                        }
                                    }
                                }
                            }
                            else if (newPoint.X >= 0 && newPoint.X < 5 && newPoint.Y >= 0 && newPoint.Y < 5)
                            {
                                if (oldTuple.grid[newPoint.X, newPoint.Y] == '#')
                                {
                                    neighbours++;
                                }
                            }
                            else
                            {
                                var outergridIndex = old.FindIndex(t => t.level == oldTuple.level - 1);
                                if (outergridIndex == -1)
                                {
                                    continue;
                                }
                                var outerGrid = old[outergridIndex].grid;
                                if (newPoint.X == 5)
                                {
                                    if (outerGrid[3, 2] == '#')
                                    {
                                        neighbours++;
                                    }
                                }
                                else if (newPoint.X == -1)
                                {
                                    if (outerGrid[1, 2] == '#')
                                    {
                                        neighbours++;
                                    }
                                }
                                if (newPoint.Y == 5)
                                {
                                    if (outerGrid[2, 3] == '#')
                                    {
                                        neighbours++;
                                    }
                                }
                                if (newPoint.Y == -1)
                                {
                                    if (outerGrid[2, 1] == '#')
                                    {
                                        neighbours++;
                                    }
                                }
                            }
                        }

                        if (oldTuple.grid[x, y] == '#')
                        {
                            if (neighbours != 1)
                            {
                                currentRealTuple.grid[x, y] = '.';
                            }
                        }
                        else
                        {
                            if (neighbours == 1 || neighbours == 2)
                            {
                                currentRealTuple.grid[x, y] = '#';
                            }
                        }
                    }
                }
            }

            var topLevel = old.Min(t => t.level);
            var topLevelIndex = old.FindIndex(t => t.level == topLevel);
            var topLevelGrid = old[topLevelIndex].grid;

            var newTopLevel = false;
            for (var y = 0; y < 5; ++y)
            {
                if (topLevelGrid[0, y] == '#')
                {
                    newTopLevel = true;
                    break;
                }
                if (topLevelGrid[4, y] == '#')
                {
                    newTopLevel = true;
                    break;
                }
            }
            for (var x = 0; x < 5 && !newTopLevel; ++x)
            {
                if (topLevelGrid[x, 0] == '#')
                {
                    newTopLevel = true;
                    break;
                }
                if (topLevelGrid[x, 4] == '#')
                {
                    newTopLevel = true;
                    break;
                }
            }
            if (newTopLevel)
            {
                var oldTopGrid = InitializeGrid();
                var newTopGrid = InitializeGrid();
                var oldTuple = (level: topLevel - 1, grid: oldTopGrid);
                var newTuple = (level: topLevel - 1, grid: newTopGrid);
                old.Add(oldTuple);
                _charGrids.Add(newTuple);

                var currentRealTuple = newTuple;
                for (var y = 0; y < 5; ++y)
                {
                    for (var x = 0; x < 5; ++x)
                    {
                        if (x == 2 && y == 2)
                        {
                            continue;
                        }

                        var neighbours = 0;
                        var currentPoint = new Point(x, y);
                        foreach (var d in _directions)
                        {
                            var directionAsPoint = ParseDirection(d);
                            var newPoint = currentPoint.Add(directionAsPoint);
                            if (newPoint.X == 2 && newPoint.Y == 2)
                            {
                                var innerGridIndex = old.FindIndex(t => t.level == oldTuple.level + 1);
                                if (innerGridIndex == -1)
                                {
                                    continue;
                                }
                                var innerGrid = old[innerGridIndex].grid;
                                if (currentPoint.X == 3)
                                {
                                    for (var iy = 0; iy < 5; ++iy)
                                    {
                                        if (innerGrid[4, iy] == '#')
                                        {
                                            neighbours++;
                                        }
                                    }
                                }
                                else if (currentPoint.X == 1)
                                {
                                    for (var iy = 0; iy < 5; ++iy)
                                    {
                                        if (innerGrid[0, iy] == '#')
                                        {
                                            neighbours++;
                                        }
                                    }
                                }
                                else if (currentPoint.Y == 3)
                                {
                                    for (var ix = 0; ix < 5; ++ix)
                                    {
                                        if (innerGrid[ix, 4] == '#')
                                        {
                                            neighbours++;
                                        }
                                    }
                                }
                                else if (currentPoint.Y == 1)
                                {
                                    for (var ix = 0; ix < 5; ++ix)
                                    {
                                        if (innerGrid[ix, 0] == '#')
                                        {
                                            neighbours++;
                                        }
                                    }
                                }
                            }
                            else if (newPoint.X >= 0 && newPoint.X < 5 && newPoint.Y >= 0 && newPoint.Y < 5)
                            {
                                if (oldTuple.grid[newPoint.X, newPoint.Y] == '#')
                                {
                                    neighbours++;
                                }
                            }
                            else
                            {
                                var outergridIndex = old.FindIndex(t => t.level == oldTuple.level - 1);
                                if (outergridIndex == -1)
                                {
                                    continue;
                                }
                                var outerGrid = old[outergridIndex].grid;
                                if (newPoint.X == 5)
                                {
                                    if (outerGrid[3, 2] == '#')
                                    {
                                        neighbours++;
                                    }
                                }
                                else if (newPoint.X == -1)
                                {
                                    if (outerGrid[1, 2] == '#')
                                    {
                                        neighbours++;
                                    }
                                }
                                if (newPoint.Y == 5)
                                {
                                    if (outerGrid[2, 3] == '#')
                                    {
                                        neighbours++;
                                    }
                                }
                                if (newPoint.Y == -1)
                                {
                                    if (outerGrid[2, 1] == '#')
                                    {
                                        neighbours++;
                                    }
                                }
                            }
                        }

                        if (oldTuple.grid[x, y] == '#')
                        {
                            if (neighbours != 1)
                            {
                                currentRealTuple.grid[x, y] = '.';
                            }
                        }
                        else
                        {
                            if (neighbours == 1 || neighbours == 2)
                            {
                                currentRealTuple.grid[x, y] = '#';
                            }
                        }
                    }
                }
            }
        }

        //public ulong CalculateBiodiversity()
        //{
        //ulong result = 0;
        //var i = 0;
        //for (int y = 0; y < charGrid.Count; ++y)
        //{
        //    var line = charGrid[y];
        //    for (int x = 0; x < line.Count; ++x)
        //    {
        //        if (charGrid[y][x] == '#')
        //        {
        //            result += (ulong)Math.Pow(2, i);
        //        }
        //        i++;
        //    }
        //}
        //return result;
        //}

        public void Print()
        {
            var orderedGrid = _charGrids.OrderBy(t => t.level);
            foreach (var tuple in orderedGrid)
            {
                Console.WriteLine("============");
                Console.WriteLine("Depth: " + tuple.level);
                Console.WriteLine();

                for (var y = 0; y < 5; ++y)
                {
                    var line = string.Empty;
                    for (var x = 0; x < 5; ++x)
                    {
                        line += tuple.grid[x, y];
                    }
                    Console.WriteLine(line);
                }

                Console.WriteLine("============");
                Console.WriteLine();
            }

        }

        protected override string Part1()
        {
            //while (true)
            //{
            //    var d = CalculateBiodiversity();
            //    if (!Biodiversities.Add(d))
            //    {
            //        return d.ToString();
            //    }
            //    Tick();
            //}

            return "";
        }

        protected override string Part2()
        {
            for (var i = 0; i < 200; ++i)
            {
                //Console.WriteLine($"After {i} steps");
                //Console.WriteLine();
                //Print();
                //Console.ReadKey();
                //Console.WriteLine();
                //Console.WriteLine();
                //Console.WriteLine();
                //Console.WriteLine();
                //Console.WriteLine();
                Tick();
            }
            var bugs = 0;
            foreach (var grid in _charGrids.Select(x => x.grid))
            {
                for (var i = 0; i < 25; ++i)
                {
                    var x = i % 5;
                    var y = i / 5;
                    if (grid[x, y] == '#')
                    {
                        bugs++;
                    }
                }
            }
            return bugs.ToString();
        }
    }
}
