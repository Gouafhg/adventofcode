﻿using System;
using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2019
{
    public class Moon
    {
        public string Name;
        public Vector3 Position;
        public Vector3 Velocity;
        public Vector3 DeltaVelocity;
        public int PotentialEnergy => Math.Abs(Position.X) + Math.Abs(Position.Y) + Math.Abs(Position.Z);
        public int KineticEnergy => Math.Abs(Velocity.X) + Math.Abs(Velocity.Y) + Math.Abs(Velocity.Z);
        public (int posX, int posY, int posZ, int velX, int velY, int velZ) State => (Position.X, Position.Y, Position.Z, Velocity.X, Velocity.Y, Velocity.Z);

        public Moon()
        {
            Name = string.Empty;
            Position = new Vector3();
            Velocity = new Vector3();
        }

        public Moon(Vector3 v)
        {
            Name = string.Empty;
            Position = new Vector3(v);
            Velocity = new Vector3();
        }

        public Moon(string name, Vector3 v)
        {
            this.Name = name;
            Position = new Vector3(v);
            Velocity = new Vector3();
        }

        public override string ToString()
        {
            return "name = " + Name + ", pos = <x=" + Position.X + ", y=" + Position.Y + ", z=" + Position.Z + ">, vel = <x=" + Velocity.X + ", y=" + Velocity.Y + ", z=" + Velocity.Z + ">";
        }
    }

    public class Day12 : Day
    {
        //Globals
        private List<Moon> _moons;
        private readonly string[] _input;

        public string RenderMoons()
        {
            var result = string.Empty;
            foreach (var moon in _moons)
            {
                result += moon + Environment.NewLine;
            }

            return result;
        }

        public void ApplyGravity()
        {
            var moonCalulations = new Dictionary<Moon, List<Moon>>();
            foreach (var moon in _moons)
            {
                moonCalulations.Add(moon, new List<Moon>());
            }

            var oldVelocities = new Dictionary<Moon, Vector3>();
            _moons.ForEach(m => oldVelocities.Add(m, new Vector3(m.Velocity)));
            foreach (var moon in _moons)
            {
                foreach (var otherMoon in _moons.Where(m => m != moon))
                {
                    if (moonCalulations[moon].Contains(otherMoon))
                    {
                        continue;
                    }
                    moonCalulations[moon].Add(otherMoon);
                    moonCalulations[otherMoon].Add(moon);

                    if (moon.Position.X < otherMoon.Position.X)
                    {
                        moon.Velocity.X++;
                        otherMoon.Velocity.X--;
                    }
                    else if (moon.Position.X > otherMoon.Position.X)
                    {
                        moon.Velocity.X--;
                        otherMoon.Velocity.X++;
                    }
                    if (moon.Position.Y < otherMoon.Position.Y)
                    {
                        moon.Velocity.Y++;
                        otherMoon.Velocity.Y--;
                    }
                    else if (moon.Position.Y > otherMoon.Position.Y)
                    {
                        moon.Velocity.Y--;
                        otherMoon.Velocity.Y++;
                    }
                    if (moon.Position.Z < otherMoon.Position.Z)
                    {
                        moon.Velocity.Z++;
                        otherMoon.Velocity.Z--;
                    }
                    else if (moon.Position.Z > otherMoon.Position.Z)
                    {
                        moon.Velocity.Z--;
                        otherMoon.Velocity.Z++;
                    }
                }
            }
            _moons.ForEach(m => m.DeltaVelocity = Vector3.Subtract(m.Velocity, oldVelocities[m]));
        }

        public void ApplyVelocity()
        {
            foreach (var moon in _moons)
            {
                moon.Position = Vector3.Add(moon.Position, moon.Velocity);
            }
        }

        public void TimeStep()
        {
            ApplyGravity();
            ApplyVelocity();
        }

        //Constructor
        public Day12()
        {
            //Init globals
            Part = 2;


            //Setup file
            //var file = Utilities.ReadFile(@"C:\Users\Gouafhg\source\repos\adventofcode2019\data\day12.txt");
            var file =
@"<x=-8, y=-18, z=6>
<x=-11, y=-14, z=4>
<x=8, y=-3, z=-10>
<x=-2, y=-16, z=1>";


            //Test case
            var test1 =
@"<x=-1, y=0, z=2>
<x=2, y=-10, z=-7>
<x=4, y=-8, z=8>
<x=3, y=5, z=-1>";

            var test2 =
@"<x=-8, y=-10, z=0>
<x=5, y=5, z=10>
<x=2, y=-7, z=3>
<x=9, y=-8, z=-3>";

            _input = file.Split(Environment.NewLine.ToArray()).Where(x => x != "").ToArray();

            //Do Pre-stuff
        }

        public void InitMoons()
        {
            var io = new Moon("Io", Vector3.Parse(_input[0]));
            var europa = new Moon("Europa", Vector3.Parse(_input[1]));
            var ganymede = new Moon("Ganymede", Vector3.Parse(_input[2]));
            var callisto = new Moon("Callisto", Vector3.Parse(_input[3]));

            _moons = new List<Moon> { io, europa, ganymede, callisto };
        }

        protected override string Part1()
        {
            InitMoons();

            for (var i = 0; i < 1000; ++i)
            {
                TimeStep();
            }

            foreach (var moon in _moons)
            {
                Console.WriteLine(moon.ToString());
            }
            Console.WriteLine();

            var energySum = _moons.Sum(x => x.PotentialEnergy * x.KineticEnergy);

            return energySum.ToString();
        }

        protected override string Part2()
        {
            var sl = new List<string>
            {
                "Step;PrevStep;Name;PosX;PosY;PosZ;VelX;VelY;VelZ",
            };

            InitMoons();
            var startingStates = new List<(int posX, int posY, int posZ, int velX, int velY, int velZ)>();
            for (var i = 0; i < 4; ++i)
            {
                startingStates.Add(_moons[i].State);
            }

            var currentStep = 0;

            while (true)
            {
                //x
                var xSame = true;
                for (var i = 0; i < 4; ++i)
                {
                    if (startingStates[i].posX != _moons[i].State.posX || startingStates[i].velX != _moons[i].State.velX)
                    {
                        xSame = false;
                    }
                }
                if (xSame)
                {
                    Console.WriteLine(currentStep + ": X");
                }

                //y
                var ySame = true;
                for (var i = 0; i < 4; ++i)
                {
                    if (startingStates[i].posY != _moons[i].State.posY || startingStates[i].velY != _moons[i].State.velY)
                    {
                        ySame = false;
                    }
                }
                if (ySame)
                {
                    Console.WriteLine(currentStep + ": Y");
                }

                //z
                var zSame = true;
                for (var i = 0; i < 4; ++i)
                {
                    if (startingStates[i].posZ != _moons[i].State.posZ || startingStates[i].velZ != _moons[i].State.velZ)
                    {
                        zSame = false;
                    }
                }
                if (zSame)
                {
                    Console.WriteLine(currentStep + ": Z");
                }


                TimeStep();
                currentStep++;
            }
        }
    }
}
