﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;

namespace adventofcode.AOC2019
{
    public enum Direction { North = 1, South = 2, West = 3, East = 4 }

    public class Day15 : Day
    {
        //Globals
        private readonly StateMachine2019 _sm;
        private readonly Grid<char> _grid;
        private readonly Graph<char> _graph;

        private readonly Point _startingPoint;
        private Point _robot;
        private Point _oxygenSystem;
        private bool _foundOxygenSystem;
        private Direction _currentDirection;
        private readonly Stack<Direction> _robotPath;

        //Constructor
        public Day15()
        {
            //Init globals
            Part = 2;
            _sm = new StateMachine2019();
            _grid = new Grid<char>();
            _foundOxygenSystem = false;

            _startingPoint = new Point();

            _robot = new Point(_startingPoint.X, _startingPoint.Y);
            _robotPath = new Stack<Direction>();

            //Setup file
            var file = ToolsFile.ReadFile(DataPath + "day15.txt");

            //Test case
            var test1 = @"";

            var input = file.Split(',').Select(x => long.Parse(x)).ToArray();

            //Do Pre-stuff
            _sm.SetMem(input);
            Prepare();
            _graph = new Graph<char>(_grid.Nodes.Select(kp => kp.Value).ToList());
        }

        public Point ParseDirection(Direction d)
        {
            switch (d)
            {
                case Direction.North:
                    return new Point(0, 1);
                case Direction.South:
                    return new Point(0, -1);
                case Direction.West:
                    return new Point(-1, 0);
                case Direction.East:
                    return new Point(1, 0);
                default:
                    throw new NotImplementedException();
            }
        }
        public Direction GetOppositeDirection(Direction d)
        {
            switch (d)
            {
                case Direction.South: return Direction.North;
                case Direction.North: return Direction.South;
                case Direction.West: return Direction.East;
                case Direction.East: return Direction.West;
                default: throw new NotImplementedException();
            }
        }

        public Direction ParseToDirection(Point d)
        {
            if (d.Y == 0)
            {
                if (d.X == -1)
                {
                    return Direction.West;
                }
                return Direction.East;
            }
            if (d.Y == 1)
            {
                return Direction.North;
            }
            return Direction.South;
        }

        public void ParseOutput(long value, bool pushPath)
        {
            switch (value)
            {
                case 0:
                    var wallPoint = _robot.Add(ParseDirection(_currentDirection));
                    _grid.AddNode(wallPoint, (Node<char>)'W');
                    break;
                case 1:
                    _robot = _robot.Add(ParseDirection(_currentDirection));
                    if (pushPath) _robotPath.Push(_currentDirection);
                    _grid.AddNode(_robot, (Node<char>)'.');
                    break;
                case 2:
                    _robot = _robot.Add(ParseDirection(_currentDirection));
                    _foundOxygenSystem = true;
                    _oxygenSystem = new Point(_robot.X, _robot.Y);
                    if (pushPath) _robotPath.Push(_currentDirection);
                    _grid.AddNode(_oxygenSystem, (Node<char>)'D');
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        public void Prepare()
        {
            var random = new Random();
            var goodDirections = new List<Direction>();
            var directions = Enum.GetValues(typeof(Direction)).Cast<Direction>().ToList();
            for (var i = 0; i < 50000; ++i)
            {
                var backtrace = false;
                goodDirections.Clear();
                foreach (var d in directions)
                {
                    var newPos = _robot.Add(ParseDirection(d));
                    if (_grid.Get(newPos) == null)
                    {
                        goodDirections.Add(d);
                    }
                }
                if (goodDirections.Count == 0)
                {
                    backtrace = true;
                    if (_robotPath.Count > 0)
                    {
                        _currentDirection = GetOppositeDirection(_robotPath.Pop());
                    }
                    else
                    {
                        var r = (random.Next() % directions.Count);
                        _currentDirection = directions[r];
                    }
                }
                else
                {
                    var r = (random.Next() % goodDirections.Count);
                    _currentDirection = goodDirections[r];
                }
                _sm.SetInput((long)_currentDirection);

                _sm.Execute();
                ParseOutput(_sm.Output, !backtrace);
            }
            var screen = _grid.Print(x => x != '.').ToList();
            var robString = _robot.ToString().PadRight(12);
            var oxyString = _foundOxygenSystem ? _oxygenSystem.ToString().PadRight(12) : new string(' ', 12);
            screen.Add($"{Environment.NewLine}{Environment.NewLine}RobotPos: {robString} OxygenPos: {oxyString}");

            Console.Clear();
            for (var y = 0; y < screen.Count; ++y)
            {
                Console.WriteLine(screen[y]);
            }
            Thread.Sleep(16);

            foreach (var n in _grid.Nodes)
            {
                if (n.Value.Value == 'W')
                {
                    n.Value.CanBeAdjacent = false;
                }
            }
            foreach (var n in _grid.Nodes)
            {
                if (n.Value.CanBeAdjacent)
                {
                    if (n.Value.AdjacentNodes.Count == 4) continue;
                    foreach (var d in directions)
                    {
                        var an = _grid.Get(n.Key.Add(ParseDirection(d)));
                        if (an.CanBeAdjacent && !n.Value.AdjacentNodes.ContainsKey(an))
                        {
                            n.Value.AdjacentNodes.Add(an, 1);
                            an.AdjacentNodes.Add(n.Value, 1);
                        }
                    }
                }
            }
        }

        protected override string Part1()
        {
            Console.WriteLine("START PART 1");
            var path = _graph.ShortestPathDijsktra(_grid.Get(new Point(0, 0)), _grid.Get(_oxygenSystem));
            return path.distance.ToString();
        }

        protected override string Part2()
        {
            Console.WriteLine("START PART 2");
            var oxygenNode = _grid.Get(_oxygenSystem);
            var paths = new Dictionary<Node<char>, int>();
            foreach (var n in _graph.Nodes)
            {
                if (n.CanBeAdjacent)
                {
                    var sp = _graph.ShortestPathBfs(n, oxygenNode);
                    paths.Add(n, sp.Item2);
                }
            }

            var maxDistance = int.MinValue;
            Node<char> maxNode = null;
            foreach (var key in paths.Keys)
            {
                if (maxNode == null || paths[key] > maxDistance)
                {
                    maxNode = key;
                    maxDistance = paths[key];
                }
            }

            return maxDistance.ToString();
        }
    }
}
