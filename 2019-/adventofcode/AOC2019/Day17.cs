﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace adventofcode.AOC2019
{
    public class Day17 : Day
    {
        public Point ParseDirection(Direction d)
        {
            switch (d)
            {
                case Direction.North:
                    return new Point(0, -1);
                case Direction.South:
                    return new Point(0, 1);
                case Direction.West:
                    return new Point(-1, 0);
                case Direction.East:
                    return new Point(1, 0);
                default:
                    throw new NotImplementedException();
            }
        }
        public Direction GetOppositeDirection(Direction d)
        {
            switch (d)
            {
                case Direction.South: return Direction.North;
                case Direction.North: return Direction.South;
                case Direction.West: return Direction.East;
                case Direction.East: return Direction.West;
                default: throw new NotImplementedException();
            }
        }

        public Direction GetLeftDirection(Direction d)
        {
            switch (d)
            {
                case Direction.South: return Direction.East;
                case Direction.North: return Direction.West;
                case Direction.West: return Direction.South;
                case Direction.East: return Direction.North;
                default: throw new NotImplementedException();
            }
        }

        public Direction GetRightDirection(Direction d)
        {
            switch (d)
            {
                case Direction.South: return Direction.West;
                case Direction.North: return Direction.East;
                case Direction.West: return Direction.North;
                case Direction.East: return Direction.South;
                default: throw new NotImplementedException();
            }
        }

        public Direction ParseToDirection(Point d)
        {
            if (d.Y == 0)
            {
                if (d.X == -1)
                {
                    return Direction.West;
                }
                return Direction.East;
            }
            if (d.Y == 1)
            {
                return Direction.North;
            }
            return Direction.South;
        }

        //Globals
        private readonly StateMachine2019 _sm;
        private readonly Grid<char> _grid;
        private readonly Graph<char> _graph;
        private Point _robot;
        private Direction _robotDirection;
        private readonly Dictionary<Point, Node<char>> _intersections;

        //Constructor
        public Day17()
        {
            //Init globals
            Part = 2;
            _sm = new StateMachine2019();
            _grid = new Grid<char>();
            _graph = new Graph<char>();
            _robot = new Point(0, 0);
            _intersections = new Dictionary<Point, Node<char>>();

            //Setup file
            var file = ToolsFile.ReadFile(DataPath + "day17.txt");

            //Test case
            var test1 = @"";

            var input = file.Split(',').Select(x => long.Parse(x)).ToArray();

            //Do Pre-stuff
            _sm.SetMem(input);
        }

        private void Prepare()
        {
            var xPos = 0;
            var yPos = 0;

            while (!_sm.Halt)
            {
                _sm.ClearBuffer();
                _sm.Execute();
                if (_sm.Buffer.Count == 0) continue;

                var smValue = (int)_sm.Output;
                var value = (char)smValue;
                if (value == '\n')
                {
                    yPos++;
                    xPos = 0;
                    continue;
                }

                if (value == '^' || value == 'v' || value == '<' || value == '>')
                {
                    _robot = new Point(xPos, yPos);
                    _grid.AddNode(new Point(xPos, yPos), (Node<char>)'#');
                    xPos++;
                    switch (value)
                    {
                        case '^':
                            _robotDirection = Direction.North;
                            break;
                        case 'v':
                            _robotDirection = Direction.South;
                            break;
                        case '<':
                            _robotDirection = Direction.West;
                            break;
                        case '>':
                            _robotDirection = Direction.East;
                            break;
                    }
                }
                else
                {
                    _grid.AddNode(new Point(xPos, yPos), (Node<char>)value);
                    xPos++;
                }
            }

            var directions = Enum.GetValues(typeof(Direction)).Cast<Direction>().ToList();
            foreach (var n in _grid.Nodes)
            {
                if (n.Value.Value != '#')
                {
                    n.Value.CanBeAdjacent = false;
                }
            }

            foreach (var n in _grid.Nodes)
            {
                if (n.Value.CanBeAdjacent)
                {
                    if (n.Value.AdjacentNodes.Count == 4) continue;
                    foreach (var d in directions)
                    {
                        var an = _grid.Get(n.Key.Add(ParseDirection(d)));
                        if (an != null && an.CanBeAdjacent && !n.Value.AdjacentNodes.ContainsKey(an))
                        {
                            n.Value.AdjacentNodes.Add(an, 1);
                            an.AdjacentNodes.Add(n.Value, 1);
                        }
                    }
                }
            }

            var keys = _grid.Nodes.Keys.ToList();
            foreach (var key in keys)
            {
                var n = _grid.Nodes[key];

                if (n.AdjacentNodes.Count == 4)
                {
                    if (n.AdjacentNodes.All(x => x.Key.Value == '#'))
                    {
                        n.Value = 'O';
                        _intersections.Add(key, n);
                    }
                }
            }

            _graph.Nodes.AddRange(_grid.Nodes.Values);
        }

        public string FindPath()
        {
            var sb = new StringBuilder();

            while (true)
            {
                var forwardPoint = _robot.Add(ParseDirection(_robotDirection));
                var forwardNode = _grid.Get(forwardPoint);
                var leftDir = GetLeftDirection(_robotDirection);
                var leftPoint = _robot.Add(ParseDirection(leftDir));
                var leftNode = _grid.Get(leftPoint);
                var rightDir = GetRightDirection(_robotDirection);
                var rightPoint = _robot.Add(ParseDirection(rightDir));
                var rightNode = _grid.Get(rightPoint);
                if (forwardNode != null && forwardNode.CanBeAdjacent)
                {
                    _robot = forwardPoint;
                    sb.AppendLine("F");
                }
                else if (leftNode != null && leftNode.CanBeAdjacent)
                {
                    _robotDirection = leftDir;
                    sb.AppendLine("L");
                }
                else if (rightNode != null && rightNode.CanBeAdjacent)
                {
                    _robotDirection = rightDir;
                    sb.AppendLine("R");
                }
                else
                {
                    break;
                }
            }

            var result = string.Empty;
            var commands = sb.ToString().Split("\r\n".ToCharArray()).Where(x => x != "").ToList();
            var commandPosition = 0;
            while (commandPosition < commands.Count)
            {
                switch (commands[commandPosition])
                {
                    case "F":
                        var jump = commands.Skip(commandPosition).TakeWhile(x => x == "F").Count();
                        result += jump + ",";
                        commandPosition += jump;
                        break;
                    case "L":
                        result += "L,";
                        commandPosition++;
                        break;
                    case "R":
                        result += "R,";
                        commandPosition++;
                        break;
                }
            }

            result = result.Substring(0, result.Length - 1);
            System.IO.File.WriteAllText(@"D:\SHORTPATH.TXT", result);

            return result;
        }

        protected override string Part1()
        {
            _sm.SetInput(0);
            Prepare();

            var sum = 0;
            foreach (var intersection in _intersections)
            {
                sum += intersection.Key.X * intersection.Key.Y;
            }

            var screen = _grid.Print();
            foreach (var line in screen)
            {
                Console.WriteLine(line);
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            return "SUM: " + sum;
        }

        protected override string Part2()
        {
            _sm.Reset();
            _sm.Execute();
            Console.WriteLine(new string(Encoding.ASCII.GetChars(_sm.Buffer.Select(x => (byte)x).ToArray())));

            var path = "A,B,A,C,B,C,B,C,A,C\nL,10,R,12,R,12\nR,6,R,10,L,10\nR,10,L,10,L,12,R,6\nn\n";
            var asciiPath = Encoding.ASCII.GetBytes(path);

            Console.WriteLine("========");

            _sm.Reset();
            _sm.Register[0] = 2;

            _sm.GetToPrompt();
            var partString = string.Empty;
            for (var i = 0; i < asciiPath.Length; ++i)
            {
                _sm.SetInput(asciiPath[i]);
                _sm.Execute();
                _sm.GetToPrompt();
            }
            partString = new string(Encoding.ASCII.GetChars(_sm.Buffer.Select(x => (byte)x).ToArray()));
            Console.WriteLine(partString);
            Console.WriteLine("========");
            _sm.ClearBuffer();
            _sm.Execute();
            return _sm.Output.ToString();
        }
    }
}
