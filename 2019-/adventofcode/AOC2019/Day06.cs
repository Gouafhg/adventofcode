﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2019
{
    public class Day06 : Day
    {
        //Globals
        private readonly Graph<string> _graph;

        //Constructor
        public Day06()
        {
            //Init globals
            _graph = new Graph<string>();

            //Setup file
            var file = ToolsFile.ReadFileLines(DataPath + "day6.txt").Select(x =>
            {
                var parts = x.Split(')');

                return new KeyValuePair<string, string>(parts[0], parts[1]);
            }).ToList();

            //Test case
            var test = ("COM)B,B)C,C)D,D)E,E)F,B)G,G)H,D)I,E)J,J)K,K)L").Split(',').Select(x =>
            {
                var parts = x.Split(')');

                return new KeyValuePair<string, string>(parts[0], parts[1]);
            }).ToList();

            //Do Pre-stuff
            foreach (var kp in file)
            {
                var keyNode = new Node<string>(kp.Key);
                var valueNode = new Node<string>(kp.Value);
                if (!_graph.Nodes.Any(n => n.Value == kp.Key))
                {
                    _graph.Nodes.Add(keyNode);
                }
                else
                {
                    keyNode = _graph.Nodes.Find(x => x.Value == kp.Key);
                }
                if (!_graph.Nodes.Any(n => n.Value == kp.Value))
                {
                    _graph.Nodes.Add(valueNode);
                }
                else
                {
                    valueNode = _graph.Nodes.Find(x => x.Value == kp.Value);
                }

                keyNode.AdjacentNodes.Add(valueNode, 1);
                valueNode.AdjacentNodes.Add(keyNode, 1);
            }
        }

        protected override string Part1()
        {
            var orbitCount = 0;
            var baseNodes = new HashSet<Node<string>>();
            var nodes = new List<Node<string>>();
            var baseNode = _graph.Nodes.Find(n => n.Value == "COM");
            nodes.AddRange(_graph.Nodes.Where(x => x.Value != "COM"));
            for (var i = 0; i < nodes.Count; i++)
            {
                var node = nodes[i];
                Console.WriteLine(i);
                orbitCount += _graph.ShortestPathBfs(node, baseNode).Item2;
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            return orbitCount.ToString();
        }

        protected override string Part2()
        {
            var you = _graph.Nodes.Find(n => n.Value == "YOU");
            var san = _graph.Nodes.Find(n => n.Value == "SAN");
            var path = _graph.ShortestPathBfs(you, san);
            return (path.Item2 - 2).ToString();
        }
    }
}
