﻿using adventofcode.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2019
{
    public class Day23 : Day
    {
        //Globals
        private readonly List<StateMachine2019> _niCs;
        private readonly StateMachine2019 _nat;
        private readonly List<Queue<(long x, long y)>> _messageQueue;
        private (int destination, long x, long y) _naTmessage;
        private readonly HashSet<long> _yValuesToZero;
        private bool _networkIdle;

        //Constructor
        public Day23()
        {
            //Init globals
            Part = 2;
            _yValuesToZero = new HashSet<long>();
            _niCs = new List<StateMachine2019>();
            _messageQueue = new List<Queue<(long x, long y)>>();
            _nat = new StateMachine2019();
            _networkIdle = false;

            //Setup file
            var file = ToolsFile.ReadFile(DataPath + "day23.txt");

            var input = file.Split(',').Select(x => long.Parse(x)).ToArray();

            //Do Pre-stuff
            for (var i = 0; i < 50; ++i)
            {
                var sm = new StateMachine2019(input);
                sm.GetToPrompt();
                sm.SetInput(i);
                sm.Execute();
                _niCs.Add(sm);
                _messageQueue.Add(new Queue<(long x, long y)>());
            }
            _nat.SetMem(input);
            _naTmessage = (-1, -1, -1);
        }

        protected override string Part1()
        {
            while (true)
            {
                for (var i = 0; i < _niCs.Count; ++i)
                {
                    if (_messageQueue[i].Count == 0)
                    {
                        _niCs[i].GetToPrompt();
                        _niCs[i].SetInput(-1);
                        _niCs[i].Execute();
                    }
                    else
                    {
                        var m = _messageQueue[i].Dequeue();
                        _niCs[i].GetToPrompt();
                        _niCs[i].SetInput(m.x);
                        _niCs[i].Execute();
                        _niCs[i].GetToPrompt();
                        _niCs[i].SetInput(m.y);
                        _niCs[i].Execute();
                    }
                    var position = 0;
                    while (position < _niCs[i].Buffer.Count)
                    {
                        var packet = (destination: (int)_niCs[i].Buffer[position], x: _niCs[i].Buffer[position + 1], y: _niCs[i].Buffer[position + 2]);
                        if (packet.destination == 255)
                        {
                            return packet.y.ToString();
                        }
                        else
                        {
                            _messageQueue[packet.destination].Enqueue((packet.x, packet.y));
                        }
                        position += 3;
                    }
                    _niCs[i].ClearBuffer();
                }
            }

            return "END?";
        }

        protected override string Part2()
        {
            var sentTozero = false;
            var secondYDestZero = -1;

            var networkCycleCount = 0;
            while (true)
            {
                while (!_networkIdle)
                {
                    for (var i = 0; i < _niCs.Count; ++i)
                    {
                        if (_messageQueue[i].Count == 0)
                        {
                            _niCs[i].GetToPrompt();
                            _niCs[i].SetInput(-1);
                            _niCs[i].Execute();
                        }
                        else
                        {
                            var m = _messageQueue[i].Dequeue();
                            _niCs[i].GetToPrompt();
                            _niCs[i].SetInput(m.x);
                            _niCs[i].Execute();
                            _niCs[i].GetToPrompt();
                            _niCs[i].SetInput(m.y);
                            _niCs[i].Execute();
                        }
                        var position = 0;
                        while (position < _niCs[i].Buffer.Count)
                        {
                            var packet = (destination: (int)_niCs[i].Buffer[position], x: _niCs[i].Buffer[position + 1], y: _niCs[i].Buffer[position + 2]);
                            if (packet.destination == 255)
                            {
                                _naTmessage = (0, packet.x, packet.y);
                            }
                            else
                            {
                                _messageQueue[packet.destination].Enqueue((packet.x, packet.y));
                            }
                            position += 3;
                        }
                        _niCs[i].ClearBuffer();
                    }

                    if (_messageQueue.All(m => m.Count == 0))
                    {
                        networkCycleCount++;
                    }
                    else
                    {
                        networkCycleCount = 0;
                    }
                    if (networkCycleCount == 3)
                    {
                        _networkIdle = true;
                        networkCycleCount = 0;
                    }
                }

                _niCs[_naTmessage.destination].GetToPrompt();
                _niCs[_naTmessage.destination].SetInput(_naTmessage.x);
                _niCs[_naTmessage.destination].Execute();
                _niCs[_naTmessage.destination].GetToPrompt();
                _niCs[_naTmessage.destination].SetInput(_naTmessage.y);
                _niCs[_naTmessage.destination].Execute();
                if (!_yValuesToZero.Add(_naTmessage.y))
                {
                    return _naTmessage.y.ToString();
                }
                _networkIdle = false;
            }
        }
    }
}
