﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace adventofcode.AOC2019
{
    public class Day10 : Day
    {
        //Globals
        private readonly Dictionary<Point, HashSet<Point>> _visibles;
        private readonly List<Point> _points;

        private readonly int _gridWidth;
        private readonly int _gridHeight;

        //Constructor
        public Day10()
        {
            //Init globals
            Part = 1;
            _visibles = new Dictionary<Point, HashSet<Point>>();
            _points = new List<Point>();

            //Setup file
            var file = ToolsFile.ReadFile(DataPath + "day10.txt");

            //Test case
            var test0 =
@".#..#
.....
#####
....#
...##";
            var test1 =
@"......#.#.
#..#.#....
..#######.
.#.#.###..
.#..#.....
..#....#.#
#..#....#.
.##.#..###
##...#..#.
.#....####";
            var test2 =
                @"#.#...#.#.
.###....#.
.#....#...
##.#.#.#.#
....#.#.#.
.##..###.#
..#...##..
..##....##
......#...
.####.###.";
            var test3 =
                @".#..#..###
####.###.#
....###.#.
..###.##.#
##.##.#.#.
....###..#
..#.#..#.#
#..#.#.###
.##...##.#
.....#.#..";
            var test4 =
                @".#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##";

            var test5 = @".#....#####...#..
##...##.#####..##
##...#...#.#####.
..#.....X...###..
..#.#.....#....##";

            var input = file.Split(Environment.NewLine.ToCharArray()).Where(x => x != "").ToList();
            _gridWidth = input[0].Length;
            _gridHeight = input.Count;
            for (var y = 0; y < _gridHeight; ++y)
            {
                for (var x = 0; x < _gridWidth; ++x)
                {
                    if (input[y][x] == '#' || input[y][x] == 'X')
                    {
                        var p = new Point(x, y);
                        _points.Add(p);
                    }
                }
            }
        }

        public static float CalculateAngle(Point p, Point center)
        {
            var u = p.Subtract(center);
            u = new Point(u.X, -u.Y);

            var angle = Math.Atan2(u.Y, u.X);
            if (angle < 0) angle = Math.PI + (Math.PI + angle);
            angle = 2 * Math.PI - angle + Math.PI / 2;

            angle = Tools.FloatMod(angle, Math.PI * 2);
            return (float)angle;
        }

        private void CalculateVisibles()
        {
            foreach (var a in _points)
            {
                if (!_visibles.Keys.Contains(a))
                {
                    _visibles.Add(a, null);
                }
                _visibles[a] = new HashSet<Point>(_points.Where(x => x != a));
            }

            //Do Pre-stuff
            foreach (var a in _points)
            {
                foreach (var b in _points.Where(x => x != a))
                {
                    RemoveInvisibles(a, b);
                }
            }
        }

        private void CalculateVisibles(Point a)
        {
            if (!_visibles.Keys.Contains(a))
            {
                _visibles.Add(a, null);
            }
            _visibles[a] = new HashSet<Point>(_points.Where(x => x != a));

            //Do Pre-stuff
            foreach (var b in _points.Where(x => x != a))
            {
                RemoveInvisibles(a, b);
            }
        }

        private void RemoveInvisibles(Point a, Point b)
        {
            if (!_visibles[a].Contains(b))
                return;

            var dir = new Point(b.X - a.X, b.Y - a.Y);
            var gcd = Tools.Gcd(Math.Abs(dir.X), Math.Abs(dir.Y));
            dir = new Point(dir.X / (int)gcd, dir.Y / (int)gcd);

            var pointsInLine = new HashSet<Point>();

            for (var currP = new Point(a.X, a.Y); currP.X < _gridWidth && currP.X >= 0 && currP.Y < _gridHeight && currP.Y >= 0; currP = currP.Add(dir))
            {
                if (_visibles[a].Any(x => x.X == currP.X && x.Y == currP.Y))
                {
                    pointsInLine.Add(_visibles[a].First(x => x.X == currP.X && x.Y == currP.Y));
                }
            }

            if (pointsInLine.Count > 0)
            {
                var closest = pointsInLine.OrderBy(x => Tools.Distance(a, x)).First();
                _visibles[a].RemoveWhere(x => x != closest && pointsInLine.Contains(x));
            }
        }

        protected override string Part1()
        {
            CalculateVisibles();
            var maxVisiblePoint = _points.First();
            foreach (var p in _points)
            {
                if (_visibles[p].Count > _visibles[maxVisiblePoint].Count)
                {
                    maxVisiblePoint = p;
                }
            }

            return maxVisiblePoint + " with " + _visibles[maxVisiblePoint].Count + " visible points";
        }

        protected override string Part2()
        {
            CalculateVisibles();
            var monitorStation = _points.First();
            foreach (var p in _points)
            {
                if (_visibles[p].Count > _visibles[monitorStation].Count)
                {
                    monitorStation = p;
                }
            }

            var pointsDestroyed = new List<Point>();

            var pointBettedOnIndex = 200;
            while (pointsDestroyed.Count < pointBettedOnIndex)
            {
                CalculateVisibles(monitorStation);
                var angles = new Dictionary<Point, float>();
                foreach (var a in _visibles[monitorStation]) angles.Add(a, CalculateAngle(a, monitorStation));
                var visiblesInAngleOrder = angles.Keys.OrderBy(x => angles[x]).ToList();
                foreach (var p in visiblesInAngleOrder)
                {
                    pointsDestroyed.Add(p);
                    _points.Remove(p);
                }
            }

            var pointBettedOn = pointsDestroyed[pointBettedOnIndex - 1];
            var returnValue = pointBettedOn.X * 100 + pointBettedOn.Y;

            return returnValue.ToString();
        }
    }
}
