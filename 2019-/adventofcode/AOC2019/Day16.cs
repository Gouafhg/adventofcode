﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2019
{
    public class Day16 : Day
    {
        private const bool Verbose = false;
        private const int RunPart = 2;

        //Globals
        private readonly List<int> _basePattern;
        private readonly List<int> _input;
        private List<int> _zeroes;

        //Constructor
        public Day16()
        {
            //Init globals
            Part = RunPart;

            //Setup file
            var file = "59768092839927758565191298625215106371890118051426250855924764194411528004718709886402903435569627982485301921649240820059827161024631612290005106304724846680415690183371469037418126383450370741078684974598662642956794012825271487329243583117537873565332166744128845006806878717955946534158837370451935919790469815143341599820016469368684893122766857261426799636559525003877090579845725676481276977781270627558901433501565337409716858949203430181103278194428546385063911239478804717744977998841434061688000383456176494210691861957243370245170223862304663932874454624234226361642678259020094801774825694423060700312504286475305674864442250709029812379";

            //Test case
            var test10 = @"12345678";
            var test11 = @"80871224585914546619083218645595";
            var test12 = @"19617804207202209144916044189917";
            var test13 = @"69317163492948606335995924319873";
            var test20 = "123456";
            var test21 = @"03036732577212944063491565474664";
            var test22 = @"02935109699940807407585447034323";
            var test23 = @"03081770884921959731165446850517";

            _input = file.Select(x => int.Parse(string.Empty + x)).ToList();

            //Do Pre-stuff
            _basePattern = new List<int> { 0, 1, 0, -1 };
        }

        public void DoPhase1(ref List<int> someInput)
        {
            var inputCount = someInput.Count;
            var output = new List<int>(_zeroes);

            for (var i = 0; i < inputCount / 2; ++i)
            {
                var pattern = new List<int>();
                foreach (var pV in _basePattern)
                {
                    for (var j = 0; j < i + 1; ++j)
                    {
                        pattern.Add(pV);
                    }
                }

                var value = 0;
                for (var j = pattern.Count / 4; j < pattern.Count + 1; ++j)
                {
                    var patternIndex = j % pattern.Count;
                    if (pattern[patternIndex] == 0)
                    {
                        j += pattern.Count / 4 - 1;
                    }
                    if (pattern[patternIndex] > 0)
                    {
                        for (var k = j - 1; k < inputCount; k += pattern.Count)
                        {
                            value += someInput[k];
                        }
                    }
                    else if (pattern[patternIndex] < 0)
                    {
                        for (var k = j - 1; k < inputCount; k += pattern.Count)
                        {
                            value -= someInput[k];
                        }
                    }
                }
                //var value2 = 0;
                //for (int j = i; j < inputCount; j++)
                //{
                //    value2 += pattern[(j + 1) % pattern.Count] * someInput[j];
                //}

                var digitOne = int.Parse(string.Empty + value.ToString().Last());
                output[i] = digitOne;
            }

            var sum = 0;
            for (var i = inputCount - 1; i >= inputCount / 2; --i)
            {
                sum += someInput[i];
                var digitOne = int.Parse(string.Empty + sum.ToString().Last());
                output[i] = digitOne;
            }
            someInput.Clear();
            someInput.AddRange(output);
        }

        public List<int> DoPhase2(List<int> someInput)
        {
            var output = new List<int>(someInput.Count);
            for (var outputPosition = 0; outputPosition < someInput.Count; ++outputPosition)
            {
                var pattern = new List<int>();
                foreach (var pV in _basePattern)
                {
                    for (var j = 0; j < outputPosition + 1; ++j)
                    {
                        pattern.Add(pV);
                    }
                }

                var pieceLength = _input.Count * (outputPosition + 1);
                var cellCount = someInput.Count % (pieceLength * 2);
                var pieceJump = someInput.Count / (pieceLength * 2);
                var startSkip = pieceJump * pieceLength * 2;
                var sum = 0;
                if (pattern.Count > _input.Count * 4)
                {
                    var times = pattern.Count / _input.Count;
                    cellCount -= _input.Count * times;
                }
                while (cellCount > pattern.Count / 4 - 1)
                {
                    cellCount -= pattern.Count / 4 - 1;
                    if (cellCount > pattern.Count / 4 - 1)
                    {
                        var positiveTake = Math.Min(pattern.Count / 4, cellCount);
                        sum += someInput.Skip(startSkip + pattern.Count / 4 - 1).Take(positiveTake).Sum(x => x);
                        cellCount -= positiveTake;
                    }
                    cellCount -= pattern.Count / 4;
                    if (cellCount > 0)
                    {
                        var negativeTake = Math.Min(pattern.Count / 4, cellCount);
                        sum -= someInput.Skip(startSkip + 3 * pattern.Count / 4).Take(negativeTake).Sum(x => x);
                        cellCount -= negativeTake;
                    }
                    cellCount--;
                }
                //var negatives = currentInput.Skip(pieceJump * pieceLength).Skip(pattern.Count / 4 - 1).Skip(3 * pattern.Count / 4).Take(pattern.Count / 4).Sum(x => x);
                //for (int i = 0; i < cellCount; ++i)
                //{
                //    var zeros = currentInput.Skip(pieceJump)
                //    var positives =
                //    var patternIndex = (i + 1) % pattern.Count;
                //    sum += currentInput[pieceJump * pieceLength + i] * pattern[patternIndex];
                //}

                var digitOne = int.Parse(string.Empty + sum.ToString().Last());
                //Console.Write(digitOne);
                output.Add(digitOne);
            }
            return output;
        }

        protected override string Part1()
        {
            var currentInput = new List<int>(_input);
            _zeroes = new List<int>();
            for (var i = 0; i < currentInput.Count; ++i) _zeroes.Add(0);

            if (Verbose)
            {
                var result = string.Empty;
                currentInput.ForEach(x => result += x.ToString());
                Console.WriteLine("INPUT: " + result);
                Console.WriteLine();
            }

            for (var phase = 0; phase < 100; ++phase)
            {
                DoPhase1(ref currentInput);

                if (Verbose)
                {
                    Console.WriteLine($"AFTER {phase + 1} PHASES {new string(currentInput.Select(x => x.ToString()[0]).ToArray())}");
                    Console.WriteLine();
                    //Console.ReadKey();
                }
            }

            var returnString = string.Empty;
            currentInput.ForEach(x => returnString += x.ToString());
            return returnString.Substring(0, 8);
        }

        protected override string Part2()
        {
            //var testinput = "03036732577212944063491565474664".ToCharArray().Select(x => long.Parse(x.ToString())).ToList();
            var testinput = "59768092839927758565191298625215106371890118051426250855924764194411528004718709886402903435569627982485301921649240820059827161024631612290005106304724846680415690183371469037418126383450370741078684974598662642956794012825271487329243583117537873565332166744128845006806878717955946534158837370451935919790469815143341599820016469368684893122766857261426799636559525003877090579845725676481276977781270627558901433501565337409716858949203430181103278194428546385063911239478804717744977998841434061688000383456176494210691861957243370245170223862304663932874454624234226361642678259020094801774825694423060700312504286475305674864442250709029812379".ToCharArray().Select(x => long.Parse(x.ToString())).ToList();
            var inputLength = testinput.Count * 10000;

            var offset = int.Parse(new string(testinput.Take(7).Select(x => x.ToString()[0]).ToArray()));

            var list = new List<long>
            {
                Capacity = inputLength - offset + 1,
            };
            for (var i = offset; i < inputLength; ++i)
            {
                list.Add(testinput[i % testinput.Count]);
            }

            var newList = new List<long>
            {
                Capacity = list.Capacity,
            };

            for (var i = 0; i < 100; i++)
            {

                newList.Clear();
                Console.WriteLine();
                Console.Write(i + "\t");
                for (var a = list.Count - 1; a >= 0; --a)
                {
                    var val = list[a] + (a < list.Count - 1 ? newList[list.Count - 2 - a] : 0);
                    newList.Add(val % 10);
                    if (a % (list.Count / 25) == 0)
                        Console.Write(".");
                }
                newList.Reverse();
                list.Clear();
                list.AddRange(newList);
            }
            return new string(list.Take(8).Select(x => x.ToString()[0]).ToArray());
        }
    }
}
