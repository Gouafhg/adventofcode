﻿using System;
using System.Collections.Generic;
using System.Linq;

using Row = System.Collections.Generic.List<int>;
using Image = System.Collections.Generic.List<System.Collections.Generic.List<int>>;
using Layer = System.Collections.Generic.List<System.Collections.Generic.List<System.Collections.Generic.List<int>>>;
using adventofcode.Utilities;

namespace adventofcode.AOC2019
{
    public class Day08 : Day
    {
        //Globals
        public const char WhiteChar = '.';
        public const char BlackChar = ' ';


        private readonly string _input;

        private readonly int _width = 25;
        private readonly int _height = 6;

        private readonly List<Layer> _layers;

        //Constructor
        public Day08()
        {
            //Init globals
            var layerSize = _width * _height;

            //Setup file
            var file = ToolsFile.ReadFile(DataPath + "day8.txt");

            //Test case
            var test = "123456789012";
            _input = file;

            var tooMany = _input.Length % layerSize;
            if (tooMany > 0)
                _input = _input.Remove(_input.Length - tooMany);

            //Do Pre-stuff
            var layerCount = _input.Length / layerSize;


            _layers = new List<Layer>();
            var position = 0;
            for (var i = 0; i < _input.Length; i += layerSize)
            {
                var layerString = _input.Substring(position, layerSize);
                var layer = new Layer();
                var image = new Image();
                for (var j = 0; j < layerString.Length; j += _width)
                {
                    var row = new Row();
                    var rowString = layerString.Substring(j, _width);
                    position += _width;
                    foreach (var p in rowString)
                    {
                        row.Add(int.Parse(p.ToString()));
                    }
                    image.Add(row);
                    if (image.Count == _height)
                    {
                        layer.Add(image);
                        image = new Image();
                    }
                }
                _layers.Add(layer);
            }
        }

        protected override string Part1()
        {
            var minZero = int.MaxValue;
            var result = int.MinValue;
            var layers = new List<string>();
            for (var i = 0; i < _input.Length; i += _width * _height)
            {
                var layer = _input.Substring(i, _width * _height);
                layers.Add(layer);
            }
            foreach (var layer in layers)
            {
                var zeroCount = layer.Count(x => x == '0');
                if (zeroCount < minZero)
                {
                    minZero = zeroCount;
                    var oneCount = layer.Count(x => x == '1');
                    var twoCount = layer.Count(x => x == '2');
                    result = oneCount * twoCount;
                }
            }
            return result.ToString();
        }

        protected override string Part2()
        {
            var image = new char?[_height][];
            for (var i = 0; i < _height; ++i) image[i] = new char?[_width];
            for (var i = 0; i < _height; ++i)
            {
                for (var j = 0; j < _width; ++j)
                {
                    image[i][j] = null;
                }
            }

            for (var i = 0; i < _layers.Count; ++i)
            {
                for (var j = 0; j < _layers[i].Count; ++j)
                {
                    for (var y = 0; y < _height; ++y)
                    {
                        for (var x = 0; x < _width; ++x)
                        {
                            if (image[y][x] == null)
                            {
                                var pixel = _layers[i][j][y][x];
                                if (pixel == 0)
                                {
                                    image[y][x] = BlackChar;
                                }
                                else if (pixel == 1)
                                {
                                    image[y][x] = WhiteChar;
                                }
                            }
                        }
                    }
                }
            }

            for (var i = 0; i < _height; ++i)
            {
                for (var j = 0; j < _width; ++j)
                {
                    if (image[i][j] == null)
                    {
                        image[i][j] = ' ';
                    }
                }

            }
            for (var i = 0; i < _height; ++i)
            {
                var rowString = new string(image[i].Select(x => x.Value).ToArray());
                Console.WriteLine(rowString);
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();

            return "";
        }
    }
}
