﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2019
{
    public class Day07 : Day
    {
        //Globals
        private readonly StateMachine2019 _smA;
        private readonly StateMachine2019 _smB;
        private readonly StateMachine2019 _smC;
        private readonly StateMachine2019 _smD;
        private readonly StateMachine2019 _smE;

        //Constructor
        public Day07()
        {
            //Init globals
            _smA = new StateMachine2019();
            _smB = new StateMachine2019();
            _smC = new StateMachine2019();
            _smD = new StateMachine2019();
            _smE = new StateMachine2019();
            //Setup file
            var file = ToolsFile.ReadFile(DataPath + "day7.txt").Split(',').Select(x => long.Parse(x)).ToArray();

            //Test case
            var test1 = ("3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0").Split(',').Select(x => long.Parse(x)).ToArray();
            var test2 = ("3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0").Split(',').Select(x => long.Parse(x)).ToArray();
            var test3 = ("3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0").Split(',').Select(x => long.Parse(x)).ToArray();

            var test4 = ("3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5").Split(',').Select(x => long.Parse(x)).ToArray();
            var test5 = ("3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5, 54, 1105, 1, 12, 1, 53, 54, 53, 1008, 54, 0, 55, 1001, 55, 1, 55, 2, 53, 55, 53, 4,53, 1001, 56, -1, 56, 1005, 56, 6, 99, 0, 0, 0, 0, 10").Split(',').Select(x => long.Parse(x)).ToArray();

            //Do Pre-stuff
            _smA.SetMem(file);
            _smB.SetMem(file);
            _smC.SetMem(file);
            _smD.SetMem(file);
            _smE.SetMem(file);
        }

        protected override string Part1()
        {
            long maxOuput = int.MinValue;

            var usedPhases = new HashSet<int>();
            for (var phaseA = 0; phaseA < 5; ++phaseA)
            {
                usedPhases.Clear();
                usedPhases.Add(phaseA);
                for (var phaseB = 0; phaseB < 5; ++phaseB)
                {
                    usedPhases.Clear();
                    usedPhases.Add(phaseA);
                    if (usedPhases.Contains(phaseB))
                    {
                        continue;
                    }
                    usedPhases.Add(phaseB);
                    for (var phaseC = 0; phaseC < 5; ++phaseC)
                    {
                        usedPhases.Clear();
                        usedPhases.Add(phaseA);
                        usedPhases.Add(phaseB);
                        if (usedPhases.Contains(phaseC))
                        {
                            continue;
                        }
                        usedPhases.Add(phaseC);
                        for (var phaseD = 0; phaseD < 5; ++phaseD)
                        {
                            usedPhases.Clear();
                            usedPhases.Add(phaseA);
                            usedPhases.Add(phaseB);
                            usedPhases.Add(phaseC);
                            if (usedPhases.Contains(phaseD))
                            {
                                continue;
                            }
                            usedPhases.Add(phaseD);
                            for (var phaseE = 0; phaseE < 5; ++phaseE)
                            {
                                if (usedPhases.Contains(phaseE))
                                {
                                    continue;
                                }

                                var phaseCombination = phaseA + phaseB.ToString() + phaseC + phaseD + phaseE;
                                Console.WriteLine("Phase: " + phaseCombination);

                                _smA.UsePhase = true;
                                _smB.UsePhase = true;
                                _smC.UsePhase = true;
                                _smD.UsePhase = true;
                                _smE.UsePhase = true;
                                _smA.SetPhase(phaseA);
                                _smB.SetPhase(phaseB);
                                _smC.SetPhase(phaseC);
                                _smD.SetPhase(phaseD);
                                _smE.SetPhase(phaseE);

                                _smA.ResetRegisters();
                                _smB.ResetRegisters();
                                _smC.ResetRegisters();
                                _smD.ResetRegisters();
                                _smE.ResetRegisters();

                                _smA.Output = 0;
                                _smB.Output = 0;
                                _smC.Output = 0;
                                _smD.Output = 0;
                                _smE.Output = 0;

                                _smA.SetInput(0);
                                _smA.ResetPosition();
                                _smA.Execute();

                                _smB.SetInput(_smA.Output);
                                _smB.ResetPosition();
                                _smB.Execute();

                                _smC.SetInput(_smB.Output);
                                _smC.ResetPosition();
                                _smC.Execute();

                                _smD.SetInput(_smC.Output);
                                _smD.ResetPosition();
                                _smD.Execute();

                                _smE.SetInput(_smD.Output);
                                _smE.ResetPosition();
                                _smE.Execute();

                                if (_smE.Output > maxOuput)
                                {
                                    maxOuput = _smE.Output;
                                }
                            }
                        }
                    }
                }
            }

            return maxOuput.ToString();
        }

        protected override string Part2()
        {
            long maxOutput = int.MinValue;

            var usedPhases = new HashSet<int>();
            for (var phaseA = 5; phaseA < 10; ++phaseA)
            {
                usedPhases.Clear();
                usedPhases.Add(phaseA);
                for (var phaseB = 5; phaseB < 10; ++phaseB)
                {
                    usedPhases.Clear();
                    usedPhases.Add(phaseA);
                    if (usedPhases.Contains(phaseB))
                    {
                        continue;
                    }
                    usedPhases.Add(phaseB);
                    for (var phaseC = 5; phaseC < 10; ++phaseC)
                    {
                        usedPhases.Clear();
                        usedPhases.Add(phaseA);
                        usedPhases.Add(phaseB);
                        if (usedPhases.Contains(phaseC))
                        {
                            continue;
                        }
                        usedPhases.Add(phaseC);
                        for (var phaseD = 5; phaseD < 10; ++phaseD)
                        {
                            usedPhases.Clear();
                            usedPhases.Add(phaseA);
                            usedPhases.Add(phaseB);
                            usedPhases.Add(phaseC);
                            if (usedPhases.Contains(phaseD))
                            {
                                continue;
                            }
                            usedPhases.Add(phaseD);
                            for (var phaseE = 5; phaseE < 10; ++phaseE)
                            {
                                if (usedPhases.Contains(phaseE))
                                {
                                    continue;
                                }

                                _smA.Halt = false;
                                _smB.Halt = false;
                                _smC.Halt = false;
                                _smD.Halt = false;
                                _smE.Halt = false;
                                _smA.Output = 0;
                                _smB.Output = 0;
                                _smC.Output = 0;
                                _smD.Output = 0;
                                _smE.Output = 0;

                                _smA.ResetRegisters();
                                _smB.ResetRegisters();
                                _smC.ResetRegisters();
                                _smD.ResetRegisters();
                                _smE.ResetRegisters();
                                _smA.ResetPosition();
                                _smB.ResetPosition();
                                _smC.ResetPosition();
                                _smD.ResetPosition();
                                _smE.ResetPosition();

                                _smA.SetPhase(phaseA);
                                _smB.SetPhase(phaseB);
                                _smC.SetPhase(phaseC);
                                _smD.SetPhase(phaseD);
                                _smE.SetPhase(phaseE);
                                _smA.UsePhase = true;
                                _smB.UsePhase = true;
                                _smC.UsePhase = true;
                                _smD.UsePhase = true;
                                _smE.UsePhase = true;


                                _smA.SetInput(0);
                                var allHalt = false;
                                while (!allHalt)
                                {
                                    _smA.Execute();

                                    _smB.SetInput(_smA.Output);
                                    _smB.Execute();

                                    _smC.SetInput(_smB.Output);
                                    _smC.Execute();

                                    _smD.SetInput(_smC.Output);
                                    _smD.Execute();

                                    _smE.SetInput(_smD.Output);
                                    _smE.Execute();

                                    _smA.SetInput(_smE.Output);
                                    allHalt = _smA.Halt && _smB.Halt && _smC.Halt && _smD.Halt && _smE.Halt;
                                }

                                if (_smE.Output > maxOutput)
                                {
                                    maxOutput = _smE.Output;
                                }
                            }
                        }
                    }
                }
            }

            return maxOutput.ToString();
        }
    }
}
