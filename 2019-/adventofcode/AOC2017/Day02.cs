﻿using adventofcode.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2017
{
    public class Day02 : Day
    {
        private readonly List<string> _input;

        //Constructor
        public Day02()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\PATH\day.txt");
            var file = ToolsFile.ReadFile(DataPathHemma + @"2017\day02.txt");

            //Test case
            var test1 = @"";

            var chosenInput = file;
            _input = chosenInput.Replace("\r", "\n").Split('\n').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var checksum = 0;
            foreach (var row in _input)
            {
                var rowMin = row.Split('\t').Select(int.Parse).Min();
                var rowMax = row.Split('\t').Select(int.Parse).Max();
                var diff = rowMax - rowMin;
                checksum += diff;
            }
            return checksum.ToString();
        }

        protected override string Part2()
        {
            var checksum = 0;
            foreach (var row in _input)
            {
                var values = row.Split('\t').Select(int.Parse).ToList();
                var found = false;
                for (var i = 0; i < values.Count; ++i)
                {
                    for (var j = 0; j < values.Count; ++j)
                    {
                        if (i == j)
                            continue;

                        if (values[i] % values[j] == 0)
                        {
                            found = true;
                            checksum += values[i] / values[j];
                            break;
                        }
                    }
                    if (found)
                        break;
                }

            }
            return checksum.ToString();
        }
    }
}
