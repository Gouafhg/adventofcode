﻿using adventofcode.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2017
{
    public class Day01 : Day
    {
        //Globals
        private readonly List<int> _input;

        //Constructor
        public Day01()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\PATH\day.txt");
            var file = ToolsFile.ReadFile(DataPathHemma + @"2017\day01.txt");

            //Test case
            var test1 = @"1122";
            var test2 = @"1111";
            var test3 = @"1234";
            var test4 = @"91212129";

            var chosenInput = file;
            _input = chosenInput.Replace("\r", string.Empty).Replace("\n", string.Empty).Select(x => int.Parse(x.ToString())).ToList();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var sum = 0;
            for (var i = 0; i < _input.Count - 1; ++i)
            {
                if (_input[i] == _input[i + 1])
                {
                    sum += _input[i];
                }
            }

            if (_input.Last() == _input.First())
            {
                sum += _input.Last();
            }

            return sum.ToString();
        }

        protected override string Part2()
        {
            var offset = _input.Count / 2;

            var sum = _input.Where((t, i) => t == _input[(i + offset) % _input.Count]).Sum();

            return sum.ToString();
        }
    }
}
