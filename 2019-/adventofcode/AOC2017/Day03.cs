﻿using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2017
{
    public class Day03 : Day
    {
        private readonly List<string> _input;

        private int[,] _grid = new int[1001, 1001];

        //Constructor
        public Day03()
        {
            //Init globals
            Part = 1;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\PATH\day.txt");
            var file = 368078;

            //Test case
            var test1 = @"";

            var chosenInput = test1;
            _input = chosenInput.Replace("\r", "\n").Split('\n').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

            //Do Pre-stuff
        }

        //public void Populate()
        //{
        //    var mid = grid.GetLength(0) / 2 + 1;
        //    var pos = (x: mid, y: mid);
        //    grid[mid, mid] = 1;
        //    grid[mid + 1, mid] = 2;
        //    grid[mid, mid - 1] = 4;
        //    grid[mid - 1, mid] = 6;
        //    grid[mid, mid + 1] = 8;

        //    for (var x = mid + 1; x < 1001; ++x)
        //    {
        //        grid[x, mid] = 2 + (x - mid - 1) * 8 + grid[x - 1, mid];
        //    }
        //    for (var y = mid - 1; y >= 0; ++y)
        //    {
        //        grid[x, mid] = 2 + (x - mid - 1) * 8 + grid[x - 1, mid];
        //    }
        //    for (var x = mid - 1; x >= 0; --x)
        //    {
        //        grid[x, mid] = 2 + (x - mid - 1) * 8 + grid[x - 1, mid];
        //    }
        //    for (var y = mid + 1; y < 1001; ++y)
        //    {
        //        grid[x, mid] = 2 + (x - mid - 1) * 8 + grid[x - 1, mid];
        //    }

        //    pos = (pos.x + 1, pos.y);
        //    grid[pos.x, pos.y] = 2;
        //    for (int i = 1; i < 500; ++i)
        //    {

        //    }

        //}

        protected override string Part1()
        {
            return "";
        }

        protected override string Part2()
        {
            return "";
        }
    }
}
