﻿using adventofcode.AOC2024;
using System;

namespace adventofcode
{
    internal static class Program
    {
        private static void Main()
        {
            var today = new Day24();
            
            Console.WriteLine($"{today.GetType().Name}");
            Console.WriteLine();

            Console.WriteLine(today.Execute());
            Console.ReadKey();
        }
    }
}
