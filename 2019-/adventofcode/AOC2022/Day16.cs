﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using adventofcode.Utilities;

namespace adventofcode.AOC2022
{
    [DebuggerDisplay("{Name}")]
    public class Valve
    {
        public string Name;
        public int FlowRate;

        public int WouldGive(int openAtMinute, int maxMinutes)
        {
            if (openAtMinute > maxMinutes)
            {
                return 0;
            }
            
            return (maxMinutes - openAtMinute) * FlowRate;
        }

        public readonly HashSet<Valve> Neighbours = new HashSet<Valve>();

        public string ReadLine;


        public static Valve ReadFromString(string line)
        {
            var parts = line.Split(' ');

            return new Valve
            {
                ReadLine = line,
                Name = parts[1].Trim(),
                FlowRate = int.Parse(line.Split('=')[1].Split(';')[0]),
            };
        }
    }

    public class Day16 : Day
    {
        //Globals
        private readonly List<string> _input;
        private readonly List<Valve> _valves = new List<Valve>();
        private readonly Valve _startPosition;

        private void ConnectNeighBours()
        {
            foreach (var valve in _valves)
            {
                var neighbourNames = valve.ReadLine.Contains("valves")
                    ? Regex.Split(valve.ReadLine, "valves")[1].Trim().Split(',')
                    : Regex.Split(valve.ReadLine, "valve")[1].Trim().Split(',');

                foreach (var name in neighbourNames)
                {
                    var neighbour = _valves.First(z => z.Name == name.Trim());
                    valve.Neighbours.Add(neighbour);
                }
            }
        }

        private IEnumerable<Valve> ValvesWithFlow => _valves.Where(x => x.FlowRate > 0);

        private readonly List<(Valve source, Valve target, List<Valve> path)> _pathsBetweenValves =
            new List<(Valve source, Valve target, List<Valve> path)>();

        private readonly Dictionary<Valve, Dictionary<Valve, int>> _stepsBetweenValves =
            new Dictionary<Valve, Dictionary<Valve, int>>();

        //Constructor
        public Day16()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2022\day16.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"2022\day16.txt");

            //Test case
            var test1 = @"Valve AA has flow rate=0; tunnels lead to valves DD, II, BB
Valve BB has flow rate=13; tunnels lead to valves CC, AA
Valve CC has flow rate=2; tunnels lead to valves DD, BB
Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE
Valve EE has flow rate=3; tunnels lead to valves FF, DD
Valve FF has flow rate=0; tunnels lead to valves EE, GG
Valve GG has flow rate=0; tunnels lead to valves FF, HH
Valve HH has flow rate=22; tunnel leads to valve GG
Valve II has flow rate=0; tunnels lead to valves AA, JJ
Valve JJ has flow rate=21; tunnel leads to valve II";

            _input = file.GetLines();

            //Do Pre-stuff
            foreach (var line in _input)
            {
                _valves.Add(Valve.ReadFromString(line));
            }

            ConnectNeighBours();

            _startPosition = _valves.First(z => z.Name == "AA");

            foreach (var source in _valves)
            {
                _stepsBetweenValves.Add(source, new Dictionary<Valve, int>());
                foreach (var target in _valves)
                {
                    if (source == target)
                    {
                        continue;
                    }

                    var prev = new Dictionary<Valve, Valve>();
                    var distances = new Dictionary<Valve, int>();
                    foreach (var valve in _valves)
                    {
                        prev.Add(valve, null);
                        distances.Add(valve, int.MaxValue);
                    }
                    distances[source] = 0;
                    
                    var valvesLeft = new List<Valve>(_valves);
                    
                    while (valvesLeft.Any())
                    {
                        var current = valvesLeft.OrderBy(x => distances[x]).First();
                        valvesLeft.Remove(current);

                        if (current == target)
                        {
                            break;
                        }
                        
                        foreach (var valve in current.Neighbours)
                        {
                            var newdist = distances[current] + 1;
                            if (distances[valve] > newdist)
                            {
                                prev[valve] = current;
                                distances[valve] = newdist;
                            }
                        }
                    }

                    if (prev[target] == null) continue;
                    
                    var path = new List<Valve>() { target };
                    while (prev[path.Last()] != null)
                    {
                        path.Add(prev[path.Last()]);
                    }

                    path.Reverse();
                        
                    _pathsBetweenValves.Add((source, target, path.ToList()));
                    _stepsBetweenValves[source].Add(target, path.Count);
                }
            }
        }

        private string GetValvesLeftString(IEnumerable<Valve> valves) => string.Join("", valves.Select(x => x.Name));

        private int GetPressurePart1(IReadOnlyCollection<Valve> usedValves, List<Valve> valvesLeft, int minutesTaken, ref Dictionary<(int minutesTaken, string lastValve, string valvesLeft), int> cache)
        {
            if (minutesTaken > 30)
            {
                return 0;
            }
            
            if (!valvesLeft.Any())
            {
                return 0;
            }

            var lastValve = usedValves.Any() ? usedValves.Last() : _startPosition;

            var valvesLeftString = GetValvesLeftString(valvesLeft);
            
            var bestPressure = int.MinValue;
            foreach (var valve in valvesLeft)
            {
                var cacheKey = (stepsTaken: minutesTaken, lastValve.Name, GetValvesLeftString(valvesLeft));

                if (cache.TryGetValue(cacheKey, out var cachePressure))
                {
                    return cachePressure;
                }

                var totalMinutesTaken = minutesTaken + _stepsBetweenValves[lastValve][valve];
                var pressure = valve.WouldGive(totalMinutesTaken, 30);

                var newUsedValves = new List<Valve>(usedValves) { valve };
                var newValvesLeft = new List<Valve>(valvesLeft);
                newValvesLeft.Remove(valve);

                var totalPressure = pressure + GetPressurePart1(newUsedValves, newValvesLeft, totalMinutesTaken, ref cache);

                if (totalPressure > bestPressure)
                {
                    bestPressure = totalPressure;
                }
            }
            cache.Add((minutesTaken, lastValve.Name, valvesLeftString), bestPressure);

            return bestPressure;
        }
        
        private int GetPressurePart2(IReadOnlyCollection<Valve> usedValves, List<Valve> valvesLeft, int minutesTaken, ref Dictionary<(int minutesTaken, string lastValve, string valvesLeft), int> cache)
        {
            if (minutesTaken > 26)
            {
                return 0;
            }
            
            if (!valvesLeft.Any())
            {
                return 0;
            }

            var lastValve = usedValves.Any() ? usedValves.Last() : _startPosition;

            var valvesLeftString = GetValvesLeftString(valvesLeft);
            
            var bestPressure = int.MinValue;
            foreach (var valve in valvesLeft)
            {
                var cacheKey = (stepsTaken: minutesTaken, lastValve.Name, GetValvesLeftString(valvesLeft));

                if (cache.TryGetValue(cacheKey, out var cachePressure))
                {
                    return cachePressure;
                }

                var totalMinutesTaken = minutesTaken + _stepsBetweenValves[lastValve][valve];
                var pressure = valve.WouldGive(totalMinutesTaken, 26);

                var newUsedValves = new List<Valve>(usedValves) { valve };
                var newValvesLeft = new List<Valve>(valvesLeft);
                newValvesLeft.Remove(valve);

                var totalPressure = pressure + GetPressurePart2(newUsedValves, newValvesLeft, totalMinutesTaken, ref cache);

                if (totalPressure > bestPressure)
                {
                    bestPressure = totalPressure;
                }
            }
            cache.Add((minutesTaken, lastValve.Name, valvesLeftString), bestPressure);

            return bestPressure;
        }

        protected override string Part1()
        {
            var cache = new Dictionary<(int minutesTaken, string lastValve, string valvesLeft), int>();

            var usedValves = new List<Valve>();
            var valvesLeft = ValvesWithFlow.ToList();
            var pressure = GetPressurePart1(usedValves, valvesLeft, 0, ref cache);
            return pressure.ToString();
        }

        protected override string Part2()
        {
            var cache = new Dictionary<(int minutesTaken, string lastValve, string valvesLeft), int>();
            
            var valvesWithFlow = ValvesWithFlow.ToList();

            var bestPressure = int.MinValue;
            for (var valveCountMe = 0; valveCountMe < valvesWithFlow.Count; ++valveCountMe)
            {
                foreach (IEnumerable<Valve> myValveConfig in Tools.Combinations(valvesWithFlow, valveCountMe))
                {
                    var usedValvesMe = new List<Valve>();
                    var valvesLeftMe = myValveConfig.ToList();
                    var valvesLeftElephant = valvesWithFlow.Except(valvesLeftMe).ToList();
                    var pressureMe = GetPressurePart2(usedValvesMe, valvesLeftMe, 0, ref cache);

                    var usedValvesElephant = new List<Valve>();
                    var pressureElephant = GetPressurePart2(usedValvesElephant, valvesLeftElephant, 0, ref cache);

                    var totalPressure = pressureMe + pressureElephant;
                    if (totalPressure > bestPressure)
                    {
                        bestPressure = totalPressure;
                    }
                }
                
                Console.WriteLine($"Me {valveCountMe}, Pressure: {bestPressure}");
            }

            return bestPressure.ToString();
        }
    }
}