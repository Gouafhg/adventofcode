﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading;
using adventofcode.AOC2022.DayStuff.Day19;
using adventofcode.Utilities;

namespace adventofcode.AOC2022
{
    public class Day19 : Day
    {
        //Globals
        private readonly List<string> _input;
        private readonly List<Day19Blueprint> _blueprints = new List<Day19Blueprint>();

        //Constructor
        public Day19()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2022\day19.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test1 =
                @"Blueprint 1:  Each ore robot costs 4 ore.  Each clay robot costs 2 ore.  Each obsidian robot costs 3 ore and 14 clay.  Each geode robot costs 2 ore and 7 obsidian.
Blueprint 2:  Each ore robot costs 2 ore.  Each clay robot costs 3 ore.  Each obsidian robot costs 3 ore and 8 clay.  Each geode robot costs 3 ore and 12 obsidian.";

            _input = file.GetLines();

            //Do Pre-stuff
            foreach (var line in _input)
            {
                _blueprints.Add(Day19Blueprint.ReadFromLine(line));
            }
        }

        private void CalculateBlueprint(Day19Blueprint blueprint)
        {
            var startRobots = (1, 0, 0, 0);
            var startResources = (0, 0, 0, 0);

            var maxValues = blueprint.GetMaxGeodeBy(startRobots, startResources, 0,
                new Dictionary<(int, int, int, int, int, int, int, int, int minutesPassed), ((int ore, int clay, int
                    obsidian, int geode) robots, (int ore, int clay, int obsidian, int geode) resources)>());

            _geodeCounts.Add(blueprint, maxValues.resources.geode);
            Console.WriteLine(
                $"{blueprint.Id.ToString(),-5}: {_geodeCounts[blueprint].ToString(),5} ( {blueprint.Id * _geodeCounts[blueprint]} )");
        }

        private readonly Dictionary<Day19Blueprint, int> _geodeCounts = new Dictionary<Day19Blueprint, int>();

        protected override string Part1()
        {
            var threadList = _blueprints.Select(blueprint => new Thread(() => CalculateBlueprint(blueprint))).ToList();
            threadList.ForEach(x => x.Start());
            threadList.ForEach(x => x.Join());

            var qualityLevel = _geodeCounts.Select(x => x.Key.Id * x.Value).Sum();
            return qualityLevel.ToString();
        }

        protected override string Part2()
        {
            var threadList = _blueprints.Take(3).Select(blueprint => new Thread(() => CalculateBlueprint(blueprint))).ToList();
            threadList.ForEach(x => x.Start());
            threadList.ForEach(x => x.Join());

            var result = _geodeCounts.Values.Aggregate(BigInteger.One, (current, geodeCount) => current * geodeCount);
            return result.ToString();
        }
    }
}