﻿using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2022
{
    public class Day08 : Day
    {
        //Globals
        private readonly List<string> _input;

        private readonly (int height, bool visible, int sceneRight, int sceneUp, int sceneLeft, int sceneDown)[,] _trees;

        //Constructor
        public Day08()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2022\day08.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test1 = @"30373
25512
65332
33549
35390";

            _input = file.GetLines();

            //Do Pre-stuff
            _trees = new (int height, bool visible, int sceneRight, int sceneUp, int sceneLeft, int sceneDown)[_input[0].Length, _input.Count];
            for (var i = 0; i < _input.Count; i++)
            {
                for (var j = 0; j < _input[i].Length; ++j)
                {
                    _trees[i, j] = (int.Parse(_input[j][i].ToString()), false, 0, 0, 0, 0);
                }
            }
        }

        protected override string Part1()
        {
            for (var x = 0; x < _trees.GetLength(0); ++x)
            {
                for (var y = 0; y < _trees.GetLength(1); ++y)
                {
                    if (x == 0 || y == 0 || x == _trees.GetLength(0) - 1 || y == _trees.GetLength(1) - 1)
                    {
                        _trees[x, y].visible = true;
                        continue;
                    }

                    var visibleFromLeft = true;
                    for (var x2 = x - 1; x2 >= 0 && visibleFromLeft; --x2)
                    {
                        if (_trees[x2, y].height >= _trees[x, y].height)
                        {
                            visibleFromLeft = false;
                        }
                    }

                    var visibleFromRight = true;
                    for (var x2 = x + 1; x2 < _trees.GetLength(0) && visibleFromRight; ++x2)
                    {
                        if (_trees[x2, y].height >= _trees[x, y].height)
                        {
                            visibleFromRight = false;
                        }
                    }

                    var visibleFromBottom = true;
                    for (var y2 = y + 1; y2 < _trees.GetLength(1) && visibleFromBottom; ++y2)
                    {
                        if (_trees[x, y2].height >= _trees[x, y].height)
                        {
                            visibleFromBottom = false;
                        }
                    }

                    var visibleFromTop = true;
                    for (var y2 = y - 1; y2 >= 0 && visibleFromTop; --y2)
                    {
                        if (_trees[x, y2].height >= _trees[x, y].height)
                        {
                            visibleFromTop = false;
                        }
                    }

                    _trees[x, y].visible = visibleFromLeft || visibleFromRight || visibleFromTop || visibleFromBottom;
                }
            }

            var visibleTreeCount = 0;
            for (var x = 0; x < _trees.GetLength(0); ++x)
            {
                for (var y = 0; y < _trees.GetLength(1); ++y)
                {
                    if (_trees[x, y].visible)
                    {
                        visibleTreeCount++;
                    }
                }
            }

            return visibleTreeCount.ToString();
        }

        protected override string Part2()
        {
            for (var x = 0; x < _trees.GetLength(0); ++x)
            {
                for (var y = 0; y < _trees.GetLength(1); ++y)
                {
                    var heightsOnTheWay = new List<int>();
                    for (var x2 = x - 1; x2 >= 0; --x2)
                    {
                        if (!heightsOnTheWay.Any() || !heightsOnTheWay.Any(a => a >= _trees[x, y].height))
                        {
                            heightsOnTheWay.Add(_trees[x2, y].height);   
                        }
                        else
                        {
                            break;
                        }
                    }
                    _trees[x, y].sceneLeft = heightsOnTheWay.Count;

                    heightsOnTheWay.Clear();
                    for (var x2 = x + 1; x2 < _trees.GetLength(0); ++x2)
                    {
                        if (!heightsOnTheWay.Any() || !heightsOnTheWay.Any(a => a >= _trees[x, y].height))
                        {
                            heightsOnTheWay.Add(_trees[x2, y].height);
                        }
                        else
                        {
                            break;
                        }
                    }
                    _trees[x, y].sceneRight = heightsOnTheWay.Count;

                    heightsOnTheWay.Clear();
                    for (var y2 = y - 1; y2 >= 0; --y2)
                    {
                        if (!heightsOnTheWay.Any() || !heightsOnTheWay.Any(a => a >= _trees[x, y].height))
                        {
                            heightsOnTheWay.Add(_trees[x, y2].height);
                        }
                        else
                        {
                            break;
                        }
                    }
                    _trees[x, y].sceneUp = heightsOnTheWay.Count;

                    heightsOnTheWay.Clear();
                    for (var y2 = y + 1; y2 < _trees.GetLength(1); ++y2)
                    {
                        if (!heightsOnTheWay.Any() || !heightsOnTheWay.Any(a => a >= _trees[x, y].height))
                        {
                            heightsOnTheWay.Add(_trees[x, y2].height);
                        }
                        else
                        {
                            break;
                        }
                    }
                    _trees[x, y].sceneDown = heightsOnTheWay.Count;
                }
            }

            (int posX, int posY) treepos = (0, 0);
            var highscore = 0;
            var scenicScore = new int[_trees.GetLength(0), _trees.GetLength(1)];
            for (var x = 0; x < _trees.GetLength(0); ++x)
            {
                for (var y = 0; y < _trees.GetLength(1); ++y)
                {
                    scenicScore[x, y] = _trees[x, y].sceneRight * _trees[x, y].sceneUp * _trees[x, y].sceneLeft * _trees[x, y].sceneDown;
                    if (scenicScore[x, y] > highscore)
                    {
                        highscore = scenicScore[x, y];
                        treepos = (x, y);
                    }
                }
            }

            return highscore.ToString();
        }
    }
}
