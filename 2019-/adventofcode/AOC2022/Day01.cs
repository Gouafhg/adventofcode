﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using adventofcode.Utilities;

namespace adventofcode.AOC2022
{
    public class Day01 : Day
    {
        private class Elf
        {
            public Elf()
            {
                Food = new List<int>();
            }
            
            public readonly List<int> Food;
            public BigInteger Calories => Food.Sum();
        }
        
        //Globals
        private readonly List<Elf> _elves;
        private readonly List<string> _input;

        //Constructor
        public Day01()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFileLines(@"C:\THEGANGGIT\adventofcode\2019-\data\2022\day01.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"2022\day01.txt");

            //Test case
            var test1 = @"";

            _input = test1.GetLines();

            _elves = new List<Elf>();
            var currentElf = new Elf();
            //Do Pre-stuff
            for (var i = 0; i < file.Count; i++)
            {
                while (i < file.Count && !string.IsNullOrWhiteSpace(file[i]))
                {
                    currentElf.Food.Add(int.Parse(file[i]));
                    i++;
                }

                _elves.Add(currentElf);
                currentElf = new Elf();
            }

            if (currentElf.Food.Any())
            {
                _elves.Add(currentElf);
            }
        }

        protected override string Part1()
        {
            var cals = _elves.Max(x => x.Calories);
            return cals.ToString();
        }

        protected override string Part2()
        {
            var cals = _elves.OrderByDescending(x => x.Calories).Take(3).ToList();
            var sum = cals[0].Calories + cals[1].Calories + cals[2].Calories;
            return sum.ToString();
        }
    }
}
