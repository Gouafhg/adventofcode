﻿using System;
using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2022
{
    public class Day09 : Day
    {
        private class RopeKnot
        {
            public RopeKnot Parent = null;
            public RopeKnot Child = null;

            public (int x, int y) Head;
            public (int x, int y) Tail;

            public List<(int x, int y)> HeadPositions = new List<(int x, int y)>() { { (0, 0) } };
            public List<(int x, int y)> TailsPositions = new List<(int x, int y)>() { { (0, 0) } };


            private void PrintLol(RopeKnot head)
            {
                var r = new List<RopeKnot> { head };
                var c = head.Child;
                while (c != null)
                {
                    r.Add(c);
                    c = c.Child;
                }

                Console.Clear();
                for (var lol = 0; lol < r.Count; ++lol)
                {
                    Console.SetCursorPosition(lol * 10 + 3, 0);
                    Console.Write(lol == 0 ? "H" : lol.ToString());
                    c = r[lol];
                    var positions = new List<(int x, int y)>(c.HeadPositions);
                    positions.Reverse();
                    for (var lol2 = 0; lol2 < positions.Count; ++lol2)
                    {
                        Console.SetCursorPosition(lol * 10, lol2 + 1);
                        Console.Write(positions[lol2].ToString());
                    }
                }
            }

            public void MoveHead(string input = null)
            {
                if (Parent == null)
                {
                    var leftPart = input.Split(' ')[0];
                    var rightPart = int.Parse(input.Split(' ')[1]);

                    for (var i = 0; i < rightPart; ++i)
                    {
                        switch (leftPart)
                        {
                            case "L":
                                Head = (Head.x - 1, Head.y);
                                break;
                            case "R":
                                Head = (Head.x + 1, Head.y);
                                break;
                            case "U":
                                Head = (Head.x, Head.y - 1);
                                break;
                            case "D":
                                Head = (Head.x, Head.y + 1);
                                break;
                        }

                        HeadPositions.Add(Head);

                        UpdateTails();

                        if (Child == null)
                        {
                            continue;
                        }

                        Child.MoveHead();

                        //PrintLol(this);
                    }
                }
                else
                {
                    Head = Parent.Tail;
                    HeadPositions.Add(Head);

                    UpdateTails();

                    if (Child == null)
                    {
                        return;
                    }

                    Child.MoveHead();
                }

            }

            private void UpdateTails()
            {
                var (x, y) = (Head.x - Tail.x, Head.y - Tail.y);

                if (Math.Abs(x) <= 1 && Math.Abs(y) <= 1)
                {
                    return;
                }

                var sameRow = y == 0;
                var sameColumn = x == 0;

                if (sameRow)
                {
                    var signMove = Math.Sign(x);
                    if (signMove != 0)
                    {
                        Tail = (Tail.x + signMove, Tail.y);
                    }
                }
                else if (sameColumn)
                {
                    var signMove = Math.Sign(y);
                    if (signMove != 0)
                    {
                        Tail = (Tail.x, Tail.y + signMove);
                    }
                }

                else if (Math.Abs(x) > Math.Abs(y))
                {
                    var signMove = Math.Sign(x);
                    if (signMove != 0)
                    {
                        Tail = (Head.x - signMove, Head.y);
                    }
                }
                else if (Math.Abs(y) > Math.Abs(x))
                {
                    var signMove = Math.Sign(y);
                    if (signMove != 0)
                    {
                        Tail = (Head.x, Head.y - signMove);
                    }
                }
                else if (Math.Abs(x) == Math.Abs(y))
                {
                    var signMoveX = Math.Sign(x);
                    var signMoveY = Math.Sign(y);
                    Tail = (Head.x - signMoveX, Head.y - signMoveY);
                }

                TailsPositions.Add(Tail);
            }
        }

        //Globals
        private readonly List<string> _input;

        //Constructor
        public Day09()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2022\day09.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test1 = @"R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2";

            var test2 = @"R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20";

            _input = file.GetLines();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var rope = new RopeKnot();
            foreach (var row in _input)
            {
                rope.MoveHead(row);
            }

            return rope.TailsPositions.Distinct().Count().ToString();
        }

        protected override string Part2()
        {
            var rope = new List<RopeKnot>();
            var head = new RopeKnot();
            rope.Add(head);
            var current = head;
            for (var i = 1; i < 10; i++)
            {
                var next = new RopeKnot() { Parent = current };
                current.Child = next;
                current = next;
                rope.Add(current);
            }
            var tail = current;

            foreach (var row in _input)
            {
                head.MoveHead(row);
            }

            return tail.HeadPositions.Distinct().Count().ToString();
        }
    }
}
