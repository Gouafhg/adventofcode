﻿using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2022
{
    public class Day12 : Day
    {
        //Globals
        private readonly List<string> _input;

        private readonly (int x, int y) _start;
        private readonly (int x, int y) _end;
        private readonly int[,] _grid;
        private readonly (int x, int y) _resolution;

        private readonly List<(int x, int y)> _possibleStarts;

        //Constructor
        public Day12()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2022\day12.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test1 = @"Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi";

            _input = file.GetLines();

            _start = (0, 0);
            _end = (0, 0);
            //Do Pre-stuff
            _resolution = (_input[0].Length, _input.Count);
            _grid = new int[_resolution.x, _resolution.y];

            _possibleStarts = new List<(int x, int y)>();
            for (var y = 0; y < _input.Count; ++y)
            {
                for (var x = 0; x < _input[y].Length; ++x)
                {
                    switch (_input[y][x])
                    {
                        case 'S':
                            _start = (x, y);
                            _grid[x, y] = 'a' - 'a';
                            break;
                        case 'E':
                            _end = (x, y);
                            _grid[x, y] = 'z' - 'a';
                            break;
                        default:
                            _grid[x, y] = _input[y][x] - 'a';
                            break;
                    }

                    if (_grid[x, y] == 0)
                    {
                        _possibleStarts.Add((x, y));
                    }
                }
            }

        }

        private List<(int x, int y)> GetNeighbours((int x, int y) pos)
        {
            var result = new List<(int, int)>();

            var up = (x: pos.x, y: pos.y - 1);
            var down = (x: pos.x, y: pos.y + 1);
            var left = (x: pos.x - 1, y: pos.y);
            var right = (x: pos.x + 1, y: pos.y);

            var heightAtPos = _grid[pos.x, pos.y];

            if (up.y >= 0 && _grid[up.x, up.y] - heightAtPos <= 1)
            {
                result.Add(up);
            }

            if (down.y < _resolution.y && _grid[down.x, down.y] - heightAtPos <= 1)
            {
                result.Add(down);
            }

            if (left.x >= 0 && _grid[left.x, left.y] - heightAtPos <= 1)
            {
                result.Add(left);
            }

            if (right.x < _resolution.x && _grid[right.x, right.y] - heightAtPos <= 1)
            {
                result.Add(right);
            }

            return result;
        }


        protected override string Part1()
        {
            var distances = new Dictionary<(int, int), int>();
            var previous = new Dictionary<(int, int), (int, int)?>();
            for (var y = 0; y < _input.Count; ++y)
            {
                for (var x = 0; x < _grid.GetLength(0); ++x)
                {
                    var d = int.MaxValue;
                    if (_start == (x, y))
                    {
                        d = 0;
                    }
                    distances.Add((x, y), d);
                    previous.Add((x, y), null);
                }
            }

            var q = new List<(int x, int y)> { _start };
            while (q.Any())
            {
                var u = q.OrderBy(x => distances[x]).First();
                q.Remove(u);

                if (u == _end)
                {
                    break;
                }

                var neighbours = GetNeighbours(u);
                foreach (var neighbour in neighbours)
                {
                    var alt = distances[u] + 1;
                    if (alt < distances[neighbour])
                    {
                        distances[neighbour] = alt;
                        previous[neighbour] = u;
                        q.Add(neighbour);
                    }
                }
            }

            var path = new List<(int x, int y)>();
            var current = _end;
            path.Add(current);
            while (current != _start)
            {
                if (previous[current] == null)
                {
                    break;
                }
                
                current = previous[current].Value;
                path.Add(current);
            }

            path.Reverse();

            return path.Skip(1).Count().ToString();
        }

        protected override string Part2()
        {
            var shortestPath = int.MaxValue;
            foreach (var start in _possibleStarts)
            {
                var distances = new Dictionary<(int, int), int>();
                var previous = new Dictionary<(int, int), (int, int)?>();
                for (var y = 0; y < _input.Count; ++y)
                {
                    for (var x = 0; x < _grid.GetLength(0); ++x)
                    {
                        var d = int.MaxValue;
                        if (start == (x, y))
                        {
                            d = 0;
                        }

                        distances.Add((x, y), d);
                        previous.Add((x, y), null);
                    }
                }

                var q = new List<(int x, int y)> { start };
                while (q.Any())
                {
                    var u = q.OrderBy(x => distances[x]).First();
                    q.Remove(u);

                    if (u == _end)
                    {
                        break;
                    }

                    var neighbours = GetNeighbours(u);
                    foreach (var neighbour in neighbours)
                    {
                        var alt = distances[u] + 1;
                        if (alt < distances[neighbour])
                        {
                            distances[neighbour] = alt;
                            previous[neighbour] = u;
                            q.Add(neighbour);
                        }
                    }
                }

                var path = new List<(int x, int y)>();
                var current = _end;
                path.Add(current);
                while (current != start)
                {
                    if (previous[current] == null)
                    {
                        break;
                    }

                    current = previous[current].Value;
                    path.Add(current);
                }

                if (path.Count == 1)
                {
                    continue;
                }

                if (path.Count - 1 < shortestPath)
                {
                    shortestPath = path.Count - 1;
                }
            }

            return shortestPath.ToString();
        }
    }
}