﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using adventofcode.Utilities;
using adventofcode.Utilities.Extensions;

namespace adventofcode.AOC2022
{
    public enum Day18CubeType
    {
        Lava,
        Water,
        Air,
        Unknown,
    }

    [DebuggerDisplay("{Position}, {Type}")]
    public class Day18Cube
    {
        public (int x, int y, int z) Position;
        public Day18CubeType Type = Day18CubeType.Unknown;
        public List<Day18Cube> Neighbours = new List<Day18Cube>();
    }

    public class Day18 : Day
    {
        //Globals
        private readonly List<string> _input;
        private readonly Dictionary<(int x, int y, int z), int> _grid = new Dictionary<(int x, int y, int z), int>();

        //Constructor
        public Day18()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2022\day18.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            var test1 = @"1,1,1
2,1,1";
            //Test case
            var test2 = @"2,2,2
1,2,2
3,2,2
2,1,2
2,3,2
2,2,1
2,2,3
2,2,4
2,2,6
1,2,5
3,2,5
2,1,5
2,3,5";

            _input = file.GetLines();

            //Do Pre-stuff
            foreach (var line in _input)
            {
                var parts = line.Split(',').Select(x => x.ToInt()).ToList();
                _grid.Add((parts[0], parts[1], parts[2]), 0);
            }
        }

        protected override string Part1()
        {
            foreach (var key in _grid.Keys.ToList())
            {
                if (!_grid.Keys.Any(x => x.x == key.x - 1 && x.y == key.y && x.z == key.z))
                {
                    _grid[key]++;
                }

                if (!_grid.Keys.Any(x => x.x == key.x + 1 && x.y == key.y && x.z == key.z))
                {
                    _grid[key]++;
                }

                if (!_grid.Keys.Any(x => x.x == key.x && x.y == key.y - 1 && x.z == key.z))
                {
                    _grid[key]++;
                }

                if (!_grid.Keys.Any(x => x.x == key.x && x.y == key.y + 1 && x.z == key.z))
                {
                    _grid[key]++;
                }

                if (!_grid.Keys.Any(x => x.x == key.x && x.y == key.y && x.z == key.z - 1))
                {
                    _grid[key]++;
                }

                if (!_grid.Keys.Any(x => x.x == key.x && x.y == key.y && x.z == key.z + 1))
                {
                    _grid[key]++;
                }
            }

            return _grid.Sum(x => x.Value).ToString();
        }

        public bool CubeGridContains((int x, int y, int z) position, Day18CubeType? type = null)
        {
            if (type == null)
            {
                return _cubeGrid.Any(x => x.Position == position);
            }

            return _cubeGrid.Any(x => x.Position == position && x.Type == type.Value);
        }

        public Day18Cube GetCube((int x, int y, int z) position)
        {
            return _cubeGrid.FirstOrDefault(x => x.Position == position);
        }

        private List<Day18Cube> _cubeGrid;

        protected override string Part2()
        {
            _cubeGrid = _grid.Keys.Select(x => new Day18Cube() { Position = x, Type = Day18CubeType.Lava }).ToList();

            var minX = _grid.Keys.Select(x => x.x).Min() - 1;
            var maxX = _grid.Keys.Select(x => x.x).Max() + 1;
            var minY = _grid.Keys.Select(x => x.y).Min() - 1;
            var maxY = _grid.Keys.Select(x => x.y).Max() + 1;
            var minZ = _grid.Keys.Select(x => x.z).Min() - 1;
            var maxZ = _grid.Keys.Select(x => x.z).Max() + 1;

            for (var x = minX; x <= maxX; ++x)
            {
                for (var y = minY; y <= maxY; ++y)
                {
                    for (var z = minZ; z <= maxZ; ++z)
                    {
                        if (CubeGridContains((x, y, z)))
                            continue;

                        var type = x == minX || y == minY || z == minZ || x == maxX || y == maxY || z == maxZ ? Day18CubeType.Water : Day18CubeType.Unknown;
                        _cubeGrid.Add(new Day18Cube() { Position = (x, y, z), Type = type });
                    }
                }
            }

            for (var x = minX; x < maxX; ++x)
            {
                for (var y = minY; y < maxY; ++y)
                {
                    for (var z = minZ; z < maxZ; ++z)
                    {
                        var cube = GetCube((x, y, z));

                        var n = new List<Day18Cube>
                        {
                            GetCube((x - 1, y, z)),
                            GetCube((x + 1, y, z)),
                            GetCube((x, y - 1, z)),
                            GetCube((x, y + 1, z)),
                            GetCube((x, y, z - 1)),
                            GetCube((x, y, z + 1)),
                        }.Where(a => a != null).ToList();

                        cube.Neighbours = n;
                    }
                }
            }

            while (_cubeGrid.Any(a =>
                       a.Type == Day18CubeType.Water && a.Neighbours.Any(b => b.Type == Day18CubeType.Unknown)))
            {

                _cubeGrid.Where(a => a.Type == Day18CubeType.Water).ToList().ForEach(b =>
                    b.Neighbours.Where(c => c.Type != Day18CubeType.Lava).ToList()
                        .ForEach(c => c.Type = Day18CubeType.Water));
            }

            return _cubeGrid.Where(x => x.Type == Day18CubeType.Lava)
                .Select(x => x.Neighbours.Count(y => y.Type == Day18CubeType.Water)).Sum().ToString();
        }
    }
}
