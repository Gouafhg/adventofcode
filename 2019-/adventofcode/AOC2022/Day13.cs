﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2022
{
    [DebuggerDisplay("{DebugString}")]
    public class CompareItem
    {
        public string DebugString =>
            $"{IntegerPart?.ToString() ?? $"[{string.Join(", ", ListPart.Select(x => x.DebugString))}]"}";

        public CompareItem()
        {
            IntegerPart = null;
        }

        public CompareItem(int integer)
        {
            IntegerPart = integer;
        }

        public CompareItem(List<CompareItem> list)
        {
            ListPart = list;
        }

        public int? IntegerPart;
        public List<CompareItem> ListPart = new List<CompareItem>();

        public bool IsInteger => IntegerPart.HasValue;
        public bool IsList => !IsInteger;

        private static CompareItem ReadEscaped(string line, out int readCharacters)
        {
            return new CompareItem
            {
                ListPart = new List<CompareItem> { ReadFromString(line, out readCharacters) },
            };
        }

        public static CompareItem ReadFromString(string line, out int readCharacters)
        {
            var compareItem = new CompareItem
            {
                ListPart = new List<CompareItem>(),
            };

            var part = string.Empty;
            for (var i = 0; i < line.Length; ++i)
            {
                switch (line[i])
                {
                    case '[':
                        compareItem.ListPart.Add(ReadEscaped(line.Substring(i + 1), out var partReadCharacters));
                        i += partReadCharacters + 1;
                        continue;

                    case ']':
                        if (!string.IsNullOrWhiteSpace(part))
                        {
                            compareItem.ListPart.Add(new CompareItem(int.Parse(part)));
                        }

                        readCharacters = i;
                        return compareItem;

                    case ',':
                        if (!string.IsNullOrWhiteSpace(part))
                        {
                            compareItem.ListPart.Add(new CompareItem(int.Parse(part)));
                        }

                        part = string.Empty;
                        continue;
                }

                part += line[i];
            }

            readCharacters = line.Length;
            return compareItem;
        }

        public static bool? Compare(CompareItem left, CompareItem right)
        {
            if (left.IsInteger && right.IsInteger)
            {
                if (left.IntegerPart == right.IntegerPart)
                {
                    return null;
                }
                
                return left.IntegerPart < right.IntegerPart;
            }

            if (left.IsList && right.IsList)
            {
                for (var i = 0; i < left.ListPart.Count; ++i)
                {
                    if (i >= right.ListPart.Count)
                    {
                        return false;
                    }

                    var comparison = Compare(left.ListPart[i], right.ListPart[i]); 
                    if (comparison != null)
                    {
                        return comparison;
                    }
                }

                if (left.ListPart.Count < right.ListPart.Count)
                {
                    return true;
                }
                
                return null;
            }

            if (left.IsInteger && right.IsList)
            {
                return Compare(left.Convert(), right);
            }

            return Compare(left, right.Convert());
        }

        private CompareItem Convert() => new CompareItem(new List<CompareItem> { new CompareItem(IntegerPart.Value) });
    }

    public class ComparePair
    {
        public CompareItem Left;
        public CompareItem Right;

        private void ReadLeft(string left)
        {
            Left = CompareItem.ReadFromString(left, out _);
        }

        private void ReadRight(string right)
        {
            Right = CompareItem.ReadFromString(right, out _);
        }

        public void ReadFromLines(string left, string right)
        {
            ReadLeft(left);
            ReadRight(right);
        }

        public bool Compare()
        {
            return CompareItem.Compare(Left, Right) ?? true;
        }

        public bool IsInRightOrder => Compare();
    }

    public class Day13 : Day
    {
        //Globals
        private readonly List<string> _input;
        private readonly List<ComparePair> _pairs = new List<ComparePair>();
        private readonly List<CompareItem> packets = new List<CompareItem>();

        //Constructor
        public Day13()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2022\day13.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test1 = @"[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]";

            _input = file.GetLines();

            //Do Pre-stuff
            for (var i = 0; i < _input.Count; i += 2)
            {
                var pair = new ComparePair();
                packets.Add(CompareItem.ReadFromString(_input[i], out _));
                packets.Add(CompareItem.ReadFromString(_input[i + 1], out _));
                pair.ReadFromLines(_input[i], _input[i + 1]);
                _pairs.Add(pair);
            }
        }

        protected override string Part1()
        {
            var sum = 0;
            for (var i = 0; i < _pairs.Count; ++i)
            {
                if (_pairs[i].IsInRightOrder)
                {
                    sum += i + 1;
                }
            }

            return sum.ToString();
        }

        protected override string Part2()
        {
            var p1 = CompareItem.ReadFromString("[[2]]", out _);
            var p2 = CompareItem.ReadFromString("[[6]]", out _);
            packets.Add(p1);
            packets.Add(p2);
            
            var orderedPackets = packets.Take(2).ToList();
            if (CompareItem.Compare(packets[0], packets[1]) == false)
            {
                orderedPackets.Reverse();
            }

            var packetsLeft = packets.Skip(2).ToList();
            while (packetsLeft.Any())
            {
                var packet = packetsLeft.First();
                packetsLeft.RemoveAt(0);

                var found = false;
                for (var i = 0; i < orderedPackets.Count; ++i)
                {
                    if (CompareItem.Compare(packet, orderedPackets[i]) == true)
                    {
                        orderedPackets.Insert(i, packet);
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    orderedPackets.Add(packet);
                }
            }
            
            return ((orderedPackets.IndexOf(p1) + 1) * (orderedPackets.IndexOf(p2) + 1)).ToString();
        }
    }
}