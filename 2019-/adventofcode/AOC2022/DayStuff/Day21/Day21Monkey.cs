﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using adventofcode.Utilities.Extensions;

namespace adventofcode.AOC2022.DayStuff.Day21
{
    public class Day21Monkey
    {
        public static readonly Day21Monkey DefaultMonkey = new Day21Monkey() { Name = "null" };

        public string Name;
        public bool IsRoot => Name == "root";
        public bool IsHuman => Name == "humn";

        public readonly Dictionary<Day21Monkey, BigInteger?> MonkeyReferences =
            new Dictionary<Day21Monkey, BigInteger?>();

        public readonly List<Day21Monkey> Listeners = new List<Day21Monkey>();

        public Day21MonkeyOperatorEnum? Operation;
        public BigInteger? Result;
        private bool _hasError;

        private string GetDay21MonkeyOperatorEnumString(Day21MonkeyOperatorEnum op)
        {
            return op switch
            {
                Day21MonkeyOperatorEnum.Add => "+",
                Day21MonkeyOperatorEnum.Subtract => "-",
                Day21MonkeyOperatorEnum.Multiply => "*",
                Day21MonkeyOperatorEnum.Divide => "/",
                Day21MonkeyOperatorEnum.Equals => "=",
                _ => throw new ArgumentOutOfRangeException(nameof(op), op, null),
            };
        }

        private bool GetsFromHuman()
        {
            if (IsHuman)
            {
                return true;
            }
            
            foreach (var monkey in MonkeyReferences.Keys)
            {
                if (monkey.GetsFromHuman())
                {
                    return true;
                }
            }

            return false;
        }
        
        public override string ToString()
        {
            if (MonkeyReferences.IsEmpty())
            {
                return Name + ": _";
            }

            if (IsHuman)
            {
                return Name;
            }

            if (MonkeyReferences.Keys.Any(x => x.IsHuman))
            {
                return
                    $"humn {GetDay21MonkeyOperatorEnumString(Operation.Value)} {MonkeyReferences.First(x => !x.Key.IsHuman).Value}";
            }

            if (GetsFromHuman())
            {
                var monkeyStringValues = MonkeyReferences.Select(x => x.Key == DefaultMonkey ? x.Value.ToString() : x.Key.ToString());
                string monkeyString;
                switch (Operation)
                {
                    case Day21MonkeyOperatorEnum.Add:
                        monkeyString = string.Join(" + ", monkeyStringValues);
                        break;
                    case Day21MonkeyOperatorEnum.Subtract:
                        monkeyString = string.Join(" - ", monkeyStringValues);
                        break;
                    case Day21MonkeyOperatorEnum.Multiply:
                        monkeyString = string.Join(" * ", monkeyStringValues);
                        break;
                    case Day21MonkeyOperatorEnum.Divide:
                        monkeyString = string.Join(" / ", monkeyStringValues);
                        break;
                    default:
                        monkeyString = string.Join(" = ", monkeyStringValues);
                        break;
                }
            
                if (MonkeyReferences.Count > 1)
                {
                    monkeyString = $"({monkeyString})";
                }
            
                return monkeyString;
            }

            return Result?.ToString() ?? "_";
        }

        public void Process()
        {
            if (Operation is Day21MonkeyOperatorEnum.Divide && MonkeyReferences.Skip(1).Any(x =>
                    x.Key._hasError || x.Key.Result.HasValue && x.Key.Result == 0 || x.Value == 0))
            {
                _hasError = true;
            }

            if (_hasError || MonkeyReferences.Keys.Any(x => x._hasError))
            {
                return;
            }

            if (Result.HasValue)
            {
                if (IsRoot)
                {
                    return;
                }

                SendInfo();
            }

            var keys = MonkeyReferences.Keys.Where(x => x != DefaultMonkey).ToList();
            foreach (var monkey in keys)
            {
                MonkeyReferences[monkey] = monkey.Result;
            }

            if (MonkeyReferences.Any(x => x.Key != DefaultMonkey && x.Value == null))
            {
                return;
            }

            if (Operation is Day21MonkeyOperatorEnum.Equals)
            {
                if (MonkeyReferences.Values.Any(x => x == null))
                {
                    return;
                }

                Result = MonkeyReferences.Values.All(x => x == MonkeyReferences.Values.First())
                    ? BigInteger.One
                    : BigInteger.MinusOne;
                
                SendInfo();
                
                return;
            }
            
            Result = Operation switch
            {
                Day21MonkeyOperatorEnum.Add => MonkeyReferences.Values.Aggregate((result, item) => result + item),
                Day21MonkeyOperatorEnum.Subtract => MonkeyReferences.Values.Aggregate((result, item) => result - item),
                Day21MonkeyOperatorEnum.Multiply => MonkeyReferences.Values.Aggregate((result, item) => result * item),
                Day21MonkeyOperatorEnum.Divide => MonkeyReferences.Values.Aggregate((result, item) => result / item),
                _ => MonkeyReferences.Values.Single(),
            };

            SendInfo();
        }

        private void SendInfo()
        {
            if (!Result.HasValue || IsRoot)
            {
                return;
            }

            foreach (var listener in Listeners)
            {
                listener.Process();
            }
        }
    }
}