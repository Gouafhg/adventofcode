﻿using System.Collections.Generic;
using System.Numerics;

namespace adventofcode.AOC2022.DayStuff.Day10
{
    public class Day10CentralProcessingUnit
    {
        public Day10CathodeRayTube CathodeRayTube;

        public List<(BigInteger Cycle, BigInteger X, BigInteger Strength)> SignalStrength = new List<(BigInteger Cycle, BigInteger X, BigInteger Strength)>();

        public BigInteger X = 0;

        public int Cycle = 1;

        public List<Day10CentralProcessingUnitInstruction> Instructions = new List<Day10CentralProcessingUnitInstruction>();

        public Day10CentralProcessingUnit()
        {
            CathodeRayTube = new Day10CathodeRayTube(this);
        }

        public void Reset()
        {
            Cycle = 0;
            X = 1;
            Instructions.ForEach(x => x.Executed = false);
            SignalStrength.Clear();
        }

        public void Execute()
        {
            Reset();

            var executionQueue = new Queue<Day10CentralProcessingUnitInstruction>();
            Instructions.ForEach(x => executionQueue.Enqueue(x));

            var processed = new List<Day10CentralProcessingUnitInstruction>();
            Day10CentralProcessingUnitInstruction processing = null;
            while (executionQueue.Count > 0 || processing != null)
            {
                if ((Cycle + 20) % 40 == 0)
                {
                    SignalStrength.Add((Cycle, X, Cycle * X));
                }

                processing?.RunCycle();
                CathodeRayTube.RunCycle();
                if (processing == null || processing.Executed)
                {
                    if (processing != null)
                    {
                        processed.Add(processing);
                    }
                    processing = executionQueue.Count > 0 ? executionQueue.Dequeue() : null;
                    processing?.Execute();
                }

                Cycle++;
            }
        }
    }
}
