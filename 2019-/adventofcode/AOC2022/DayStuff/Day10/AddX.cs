﻿namespace adventofcode.AOC2022.DayStuff.Day10
{
    public class AddX : Day10CentralProcessingUnitInstruction
    {
        public int Argument;

        public AddX(Day10CentralProcessingUnit cpu, int argument) : base(2, cpu)
        {
            Argument = argument;
        }

        public override void Action()
        {
            CPU.X += Argument;
        }
    }
}
