﻿namespace adventofcode.AOC2022.DayStuff.Day10
{
    public abstract class Day10CentralProcessingUnitInstruction
    {
        public readonly int TimeToExecute;

        public Day10CentralProcessingUnit CPU;
        public bool Executed = false;
        public int EndCycle;

        public Day10CentralProcessingUnitInstruction(int timeToExecute, Day10CentralProcessingUnit cpu)
        {
            TimeToExecute = timeToExecute;
            CPU = cpu;
        }

        public void Execute()
        {
            EndCycle = CPU.Cycle + TimeToExecute;
        }

        public abstract void Action();
        public void RunCycle()
        {
            if (CPU.Cycle == EndCycle)
            {
                Action();
                Executed = true;
            }
        }
    }
}
