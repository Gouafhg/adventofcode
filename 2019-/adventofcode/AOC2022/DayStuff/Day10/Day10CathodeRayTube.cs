﻿using System.Text;

namespace adventofcode.AOC2022.DayStuff.Day10
{
    public class Day10CathodeRayTube
    {
        Day10CentralProcessingUnit CentralProcessingUnit;

        char[,] grid = new char[40, 6];
        const int PIXEL_COUNT = 40 * 6;
        int pos = 0;

        public Day10CathodeRayTube(Day10CentralProcessingUnit cpu)
        {
            CentralProcessingUnit = cpu;

            for (var y = 0; y < 6; ++y)
            {
                for (var x = 0; x < 40; ++x)
                {
                    grid[x, y] = '.';
                }
            }
        }

        public void Reset()
        {
            grid = new char[40, 6];
            pos = 0;
        }

        public void RunCycle()
        {
            var spriteStart = CentralProcessingUnit.X - 1;
            var spriteStop= CentralProcessingUnit.X + 1;

            var pixelPos = pos % 40;
            var row = pos / 40;

            grid[pixelPos, row] = (pixelPos >= spriteStart && pixelPos <= spriteStop) ? '#' : '.';

            //Console.Clear();
            //Console.WriteLine($"Sprite position: {spriteStart} - {spriteStop}");
            //Console.WriteLine();
            //Console.WriteLine(Show());

            pos++;
            if (pos >= PIXEL_COUNT)
            {
                pos = 0;
            }
        }

        public string Show()
        {
            var sb = new StringBuilder();
            for (var y = 0; y < 6; ++y)
            {
                var l = string.Empty;
                for (var x = 0; x < 40; ++x)
                {
                    //Console.SetCursorPosition(x, y);
                    //Console.Write(grid[x, y]);
                    l += grid[x, y];
                }
                sb.AppendLine(l);
            }
            return sb.ToString();
        }
    }
}
