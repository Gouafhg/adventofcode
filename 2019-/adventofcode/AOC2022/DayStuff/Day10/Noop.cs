﻿namespace adventofcode.AOC2022.DayStuff.Day10
{
    public class Noop : Day10CentralProcessingUnitInstruction
    {
        public Noop(Day10CentralProcessingUnit cpu) : base(1, cpu) { }

        public override void Action()
        {

        }
    }
}
