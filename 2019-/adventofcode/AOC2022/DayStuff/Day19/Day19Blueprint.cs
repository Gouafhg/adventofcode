﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using adventofcode.Utilities.Extensions;

namespace adventofcode.AOC2022.DayStuff.Day19
{
    public class Day19Blueprint
    {
        private static readonly int TotalMinutes;
        private const int CacheDepths = 30;

        public readonly int Id;

        private readonly Dictionary<Day19ResourceTypeenum, Dictionary<Day19ResourceTypeenum, int>> _costs =
            new Dictionary<Day19ResourceTypeenum, Dictionary<Day19ResourceTypeenum, int>>();

        private Day19Blueprint(int id)
        {
            Id = id;
        }

        static Day19Blueprint()
        {
            TotalMinutes = Day.Part == 1 ? 24 : 32;
        }

        public static Day19Blueprint ReadFromLine(string line)
        {
            var robotCosts = new Dictionary<string, string>();

            var id = line.Split(':')[0].Split(' ')[1].ToInt();
            var robotsPart = line.Split(':')[1].Trim().Split('.').Select(x => x.Trim())
                .Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
            foreach (var robotPart in robotsPart)
            {
                var robotType = robotPart.Split(' ')[1].Trim();
                var robotCost = Regex.Split(robotPart, "costs")[1].Trim();
                robotCosts.Add(robotType, robotCost);
            }

            var result = new Day19Blueprint(id);
            foreach (var key in robotCosts.Keys)
            {
                var costs = Regex.Split(robotCosts[key], "and").Select(x => x.Trim())
                    .ToDictionary(
                        x => (Day19ResourceTypeenum)Enum.Parse(typeof(Day19ResourceTypeenum), x.Split(' ')[1], true),
                        x => x.Split(' ')[0].ToInt());
                foreach (Day19ResourceTypeenum resource in Enum.GetValues(typeof(Day19ResourceTypeenum)))
                {
                    if (costs.ContainsKey(resource)) continue;
                    costs.Add(resource, 0);
                }

                var r = (Day19ResourceTypeenum)Enum.Parse(typeof(Day19ResourceTypeenum), key, true);
                result._costs.Add(r, new Dictionary<Day19ResourceTypeenum, int>
                {
                    { Day19ResourceTypeenum.Ore, costs[Day19ResourceTypeenum.Ore] },
                    { Day19ResourceTypeenum.Clay, costs[Day19ResourceTypeenum.Clay] },
                    { Day19ResourceTypeenum.Obsidian, costs[Day19ResourceTypeenum.Obsidian] },
                    { Day19ResourceTypeenum.Geode, costs[Day19ResourceTypeenum.Geode] },
                });
            }

            return result;
        }

        private bool CanBuild(Day19ResourceTypeenum robotType, (int ore, int clay, int obsidian, int geode) resources)
        {
            var cost = _costs[robotType];
            return resources.ore >= cost[Day19ResourceTypeenum.Ore] &&
                   resources.clay >= cost[Day19ResourceTypeenum.Clay] &&
                   resources.obsidian >= cost[Day19ResourceTypeenum.Obsidian];
        }

        private ((int ore, int clay, int obsidian, int geode) robotsAfter,
            (int ore, int clay, int obsidian, int geode) resourcesAfter)
            Build(Day19ResourceTypeenum robotType, (int ore, int clay, int obsidian, int geode) robotsBefore,
                (int ore, int clay, int obsidian, int geode) resourcesBefore)
        {
            var cost = _costs[robotType];

            var resourcesAfter = (resourcesBefore.ore - cost[Day19ResourceTypeenum.Ore],
                resourcesBefore.clay - cost[Day19ResourceTypeenum.Clay],
                resourcesBefore.obsidian - cost[Day19ResourceTypeenum.Obsidian], resourcesBefore.geode);

            var ore = robotsBefore.ore;
            var clay = robotsBefore.clay;
            var obsidian = robotsBefore.obsidian;
            var geode = robotsBefore.geode;
            switch (robotType)
            {
                case Day19ResourceTypeenum.Ore:
                    ore++;
                    break;
                case Day19ResourceTypeenum.Clay:
                    clay++;
                    break;
                case Day19ResourceTypeenum.Obsidian:
                    obsidian++;
                    break;
                case Day19ResourceTypeenum.Geode:
                    geode++;
                    break;
            }

            return ((ore, clay, obsidian, geode), resourcesAfter);
        }


        public ((int ore, int clay, int obsidian, int geode) robots,
            (int ore, int clay, int obsidian, int geode) resources)
            GetMaxGeodeBy(
                (int ore, int clay, int obsidian, int geode) robotsBefore,
                (int ore, int clay, int obsidian, int geode) resourcesBefore,
                int minutesPassed,
                IDictionary<(int, int, int, int, int, int, int, int, int minutesPassed),
                        ((int ore, int clay, int obsidian, int geode) robots, (int ore, int clay, int obsidian, int
                        geode) resources
                        )>
                    cache)
        {
            var minutesLeft = TotalMinutes - minutesPassed;
            if (!WillBeAbleToBuildMoreRobots(robotsBefore, resourcesBefore, minutesLeft))
            {
                return (robotsBefore,
                    (resourcesBefore.ore + robotsBefore.ore * minutesLeft,
                        resourcesBefore.clay + robotsBefore.clay * minutesLeft,
                        resourcesBefore.obsidian + robotsBefore.obsidian * minutesLeft,
                        resourcesBefore.geode + robotsBefore.geode * minutesLeft));
            }

            if (minutesPassed >= TotalMinutes)
            {
                return (robotsBefore, resourcesBefore);
            }

            if (minutesPassed <= CacheDepths && cache.TryGetValue((robotsBefore.ore,
                    robotsBefore.clay,
                    robotsBefore.obsidian,
                    robotsBefore.geode,
                    resourcesBefore.ore,
                    resourcesBefore.clay,
                    resourcesBefore.obsidian,
                    resourcesBefore.geode,
                    minutesPassed), out var cacheValue))
            {
                return (cacheValue.robots, cacheValue.resources);
            }

            //Spend, and add robots (not used in collecting)
            var possibilities =
                new List<((int ore, int clay, int obsidian, int geode) robots,
                    (int ore, int clay, int obsidian, int geode) resources,
                    int minutesPassed)>();

            if (CanBuild(Day19ResourceTypeenum.Geode, resourcesBefore))
            {
                var build = Build(
                    Day19ResourceTypeenum.Geode,
                    robotsBefore,
                    (resourcesBefore.ore + robotsBefore.ore,
                        resourcesBefore.clay + robotsBefore.clay,
                        resourcesBefore.obsidian + robotsBefore.obsidian,
                        resourcesBefore.geode + robotsBefore.geode));

                possibilities.Add((build.robotsAfter, build.resourcesAfter, minutesPassed + 1));
            }
            else
            {
                possibilities.AddRange(Enum.GetValues(typeof(Day19ResourceTypeenum))
                    .Cast<Day19ResourceTypeenum>()
                    .Where(x => x != Day19ResourceTypeenum.Geode && CanBuild(x, resourcesBefore))
                    .Select(x => Build(
                        x,
                        robotsBefore,
                        (resourcesBefore.ore + robotsBefore.ore,
                            resourcesBefore.clay + robotsBefore.clay,
                            resourcesBefore.obsidian + robotsBefore.obsidian,
                            resourcesBefore.geode + robotsBefore.geode)))
                    .Select(x => (x.robotsAfter, x.resourcesAfter, minutesPassed + 1)));
                possibilities.Add((
                    robotsBefore,
                    (resourcesBefore.ore + robotsBefore.ore,
                        resourcesBefore.clay + robotsBefore.clay,
                        resourcesBefore.obsidian + robotsBefore.obsidian,
                        resourcesBefore.geode + robotsBefore.geode),
                    minutesPassed + 1));
            }

            //Collect as robot count was before spending

            // for (var i = 0; i < possibilities.Count; ++i)
            // {
            //     var possibility = possibilities[i];
            //     
            //     possibility.resources = (possibility.resources.ore + robotsBefore.ore,
            //         possibility.resources.clay + robotsBefore.clay, 
            //         possibility.resources.obsidian + robotsBefore.obsidian,
            //         possibility.resources.geode + robotsBefore.geode);
            // }

            // for (var i = 0; i < possibilities.Count; ++i)
            // {
            //     possibilities[i] = (possibilities[i].robots, possibilities[i].resources, minutesPassed + 1);
            // }

            var possibiliyResults =
                possibilities.Select(x => GetMaxGeodeBy(x.robots, x.resources, x.minutesPassed, cache))
                    .ToList();

            var maxValues = possibiliyResults.OrderByDescending(x => x.resources.geode).First();

            if (minutesPassed <= CacheDepths)
            {
                cache.Add((robotsBefore.ore,
                    robotsBefore.clay,
                    robotsBefore.obsidian,
                    robotsBefore.geode,
                    resourcesBefore.ore,
                    resourcesBefore.clay,
                    resourcesBefore.obsidian,
                    resourcesBefore.geode, minutesPassed), (maxValues.robots, maxValues.resources));
            }

            return (maxValues.robots, maxValues.resources);
        }

        private bool WillBeAbleToBuildMoreRobots((int ore, int clay, int obsidian, int geode) robots,
            (int ore, int clay, int obsidian, int geode) resources,
            int minutesLeft)
        {
            return Enum.GetValues(typeof(Day19ResourceTypeenum))
                .Cast<Day19ResourceTypeenum>()
                .Any(resourceType => 
                    _costs[resourceType][Day19ResourceTypeenum.Ore] <= resources.ore + robots.ore * minutesLeft &&
                    _costs[resourceType][Day19ResourceTypeenum.Clay] <= resources.clay + robots.clay  * minutesLeft &&
                    _costs[resourceType][Day19ResourceTypeenum.Obsidian] <= resources.obsidian + robots.obsidian  * minutesLeft);
        }
    }
}