namespace adventofcode.AOC2022.DayStuff.Day19
{
    public enum Day19ResourceTypeenum
    {
        Geode,
        Obsidian,
        Clay,
        Ore,
    }
}