﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace adventofcode.AOC2022.DayStuff.Day17
{
    public class Day17TetrisGrid
    {
        private readonly string _moveMent;
        public long TickCount;
        public long PieceCount;
        private Day17TetrisPiece _piece;
        public readonly List<bool[]> Grid = new List<bool[]>();

        public Day17TetrisGrid(string moveMent)
        {
            _moveMent = moveMent;
        }

        private int GetColumnHeight(int i)
        {
            var height = Grid.Count - 1;
            while (height >= 0 && !Grid[height][i])
            {
                height--;
            }

            return height;
        }

        private long[] GetColumnHeights()
        {
            var heights = new long[7];
            for (var i = 0; i < 7; ++i)
            {
                heights[i] = GetColumnHeight(i);
            }

            return heights;
        }

        public long GetTowerHeight()
        {
            return GetColumnHeights().Max() + 1;
        }

        private long MovementIndex => TickCount % _moveMent.Length;

        private long PieceIndex => PieceCount % 5;

        private long _lastTickCount;

        private List<long> diffList = new List<long>();

        private int GetIndexOfSubSequence<T>(List<T> sequence, List<T> subsequence)
        {
            if (sequence.Count < subsequence.Count)
            {
                return -1;
            }

            var lol =
                Enumerable
                    .Range(0, sequence.Count - subsequence.Count + 1)
                    .Cast<int?>()
                    .FirstOrDefault(n => sequence.Skip(n.Value).Take(subsequence.Count).SequenceEqual(subsequence));

            return lol ?? -1;
        }

        private bool ContainsSubsequence<T>(List<T> sequence, List<T> subsequence)
        {
            if (sequence.Count < subsequence.Count)
            {
                return false;
            }

            return
                Enumerable
                    .Range(0, sequence.Count - subsequence.Count + 1)
                    .Any(n => sequence.Skip(n).Take(subsequence.Count).SequenceEqual(subsequence));
        }


        private readonly List<(long tickDiff, long tickCount, long stoneCount, long TowerHeight)> _history =
            new List<(long tickDiff, long tickCount, long stoneCount, long TowerHeight)>();

        public static int Part;
        public static long FirstStoneCount;
        public static long FirstHeight;
        public static long RepeatStoneCount;
        public static long RepeatHeight;
        public static long ExtraHeight;

        private void AddPiece()
        {
            if (_piece != null)
            {
                return;
            }

            if (Part == 2 && PieceIndex == 0)
            {
                var diff = TickCount - _lastTickCount;
                _history.Add((diff, TickCount, PieceCount, GetTowerHeight()));
                diffList.Add(diff);

                if (diffList.Count > 10)
                {
                    var firstPiece = diffList.Take(diffList.Count - 7).ToList();
                    var lastPiece = diffList.Skip(diffList.Count - 7).Take(7).ToList();
                    if (ContainsSubsequence(firstPiece, lastPiece))
                    {
                        var index = GetIndexOfSubSequence(firstPiece, lastPiece);

                        var repeatSteps = firstPiece.Skip(index).Count();

                        var skipTickCount = _history.Skip(index - 1).First().tickCount;
                        var lastTickCount = _history.Skip(index).Take(repeatSteps).Last().tickCount;

                        var skipPieceCount = _history.Skip(index - 1).First().stoneCount;
                        var lastPieceCount = _history.Skip(index).Take(repeatSteps).Last().stoneCount;

                        var skipHeight = _history.Skip(index - 1).First().TowerHeight;
                        var lastHeight = _history.Skip(index).Take(repeatSteps).Last().TowerHeight;

                        var diffTickCount = lastTickCount - skipTickCount;
                        var diffPieceCount = lastPieceCount - skipPieceCount;
                        var diffHeight = lastHeight - skipHeight;

                        FirstStoneCount = skipPieceCount;
                        FirstHeight = skipHeight;
                        RepeatStoneCount = diffPieceCount;
                        RepeatHeight = diffHeight;

                        BigInteger total = 1000000000000;
                        total -= FirstStoneCount;
                        BigInteger.DivRem(total, RepeatStoneCount, out var rem);

                        if (rem != 0)
                        {
                            var h = _history.First(x => x.stoneCount == skipPieceCount + rem);
                            ExtraHeight = h.TowerHeight - skipHeight;
                        }

                        throw new Exception();
                    }
                }

                _lastTickCount = TickCount;
            }

            var height = 0;
            while (height < Grid.Count && Grid[height].Any(x => x))
            {
                height++;
            }

            height += 3;

            _piece = new Day17TetrisPiece(this, Day17TetrisPiece.GetFill(PieceCount), height);

            while (Grid.Count <= height + _piece.Fill.Count)
            {
                AddRow();
            }

            PieceCount++;
        }

        private void StickPiece()
        {
            while (Grid.Count < _piece.LowestRowIndex + _piece.Fill.Count)
            {
                AddRow();
            }

            for (var i = _piece.LowestRowIndex; i < _piece.LowestRowIndex + _piece.Fill.Count; ++i)
            {
                for (var j = 0; j < 7; ++j)
                {
                    Grid[i][j] |= _piece.Fill[i - _piece.LowestRowIndex][j];
                }
            }

            _piece = null;
        }

        public void PrintGrid(int maxHeight = int.MaxValue, int maxRows = 40, bool includeFallingPieces = true)
        {
            Console.SetCursorPosition(0, 0);

            var to = Math.Min(maxHeight, Grid.Count - 1);
            var from = Math.Max(0, to - maxRows);
            for (var i = to; i >= from; --i)
            {
                if (includeFallingPieces && _piece != null && i >= _piece.LowestRowIndex &&
                    i < _piece.LowestRowIndex + _piece.Fill.Count)
                {
                    var row = new char[7];

                    for (var j = 0; j < 7; ++j)
                    {
                        if (_piece.Fill[i - _piece.LowestRowIndex][j])
                        {
                            row[j] = '@';
                        }
                        else if (Grid[i][j])
                        {
                            row[j] = '#';
                        }
                        else
                        {
                            row[j] = '.';
                        }
                    }

                    Console.WriteLine($"{i.ToString(),4}" + string.Join("", row));
                }
                else
                {
                    Console.WriteLine($"{i.ToString(),4}" + string.Join("", Grid[i].Select(x => x ? "#" : ".")));
                }
            }
        }

        public void PrintMoveString()
        {
            Console.SetCursorPosition(25, 0);
            Console.Write(_moveMent);
            Console.SetCursorPosition(25, 1);
            if (MovementIndex == 0)
            {
                Console.Write("-");
            }
            else
            {
                // Console.Write(new string('_', (int)BigInteger.ModPow(_tickCount, 1, _moveMent.Length) - 1) + "-");
            }
        }

        public void Tick()
        {
            if (_piece == null)
            {
                AddPiece();
                return;
            }

            _piece.MoveLeftRight(_moveMent[(int)MovementIndex]);
            _piece.MoveDown();
            TickCount++;

            if (!_piece.Stuck) return;
            StickPiece();
        }

        public void AddRow()
        {
            Grid.Add(new bool[7]);
        }
    }
}