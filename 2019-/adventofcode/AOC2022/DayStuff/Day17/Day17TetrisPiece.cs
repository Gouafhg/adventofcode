﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2022.DayStuff.Day17
{
    public class Day17TetrisPiece
    {
        private readonly Day17TetrisGrid _parent;
        public readonly List<bool[]> Fill;
        public int LowestRowIndex;
        public bool Stuck;

        public static List<bool[]> GetFill(long pieceCount)
        {
            var i = pieceCount % 5;

            var fill = i switch
            {
                0 => new List<bool[]>
                {
                    new bool[] { false, false, true, true, true, true, false },
                },
                1 => new List<bool[]>
                {
                    new bool[] { false, false, false, true, false, false, false },
                    new bool[] { false, false, true, true, true, false, false },
                    new bool[] { false, false, false, true, false, false, false },
                },
                2 => new List<bool[]>
                {
                    new bool[] { false, false, false, false, true, false, false },
                    new bool[] { false, false, false, false, true, false, false },
                    new bool[] { false, false, true, true, true, false, false },
                },
                3 => new List<bool[]>
                {
                    new bool[] { false, false, true, false, false, false, false },
                    new bool[] { false, false, true, false, false, false, false },
                    new bool[] { false, false, true, false, false, false, false },
                    new bool[] { false, false, true, false, false, false, false },
                },
                4 => new List<bool[]>
                {
                    new bool[] { false, false, true, true, false, false, false },
                    new bool[] { false, false, true, true, false, false, false },
                },
                _ => throw new ArgumentOutOfRangeException(),
            };

            fill.Reverse();
            return fill;
        }

        public Day17TetrisPiece(Day17TetrisGrid parent, List<bool[]> fill, int lowestRowIndex)
        {
            _parent = parent;
            Fill = new List<bool[]>(fill);
            LowestRowIndex = lowestRowIndex;
            Stuck = false;
        }

        public void MoveLeftRight(char c)
        {
            var left = c == '<';
            if (left)
            {
                if (Fill.Any(x => x.First()))
                {
                    return;
                }

                while (_parent.Grid.Count < LowestRowIndex + Fill.Count)
                {
                    _parent.AddRow();
                }

                for (var i = LowestRowIndex; i < LowestRowIndex + Fill.Count; ++i)
                {
                    for (var j = 1; j < 7; ++j)
                    {
                        if (_parent.Grid[i][j - 1] && Fill[i - LowestRowIndex][j])
                        {
                            return;
                        }
                    }
                }

                foreach (var row in Fill)
                {
                    for (var i = 0; i < row.Length - 1; ++i)
                    {
                        row[i] = row[i + 1];
                    }

                    row[row.Length - 1] = false;
                }
            }
            else
            {
                if (Fill.Any(x => x.Last()))
                {
                    return;
                }

                while (_parent.Grid.Count < LowestRowIndex + Fill.Count)
                {
                    _parent.AddRow();
                }

                for (var i = LowestRowIndex; i < LowestRowIndex + Fill.Count; ++i)
                {
                    for (var j = 0; j < 6; ++j)
                    {
                        if (_parent.Grid[i][j + 1] && Fill[i - LowestRowIndex][j])
                        {
                            return;
                        }
                    }
                }

                foreach (var row in Fill)
                {
                    for (var i = row.Length - 1; i > 0; --i)
                    {
                        row[i] = row[i - 1];
                    }

                    row[0] = false;
                }
            }
        }

        public void MoveDown()
        {
            if (LowestRowIndex == 0)
            {
                Stuck = true;
                return;
            }

            if (LowestRowIndex >= _parent.Grid.Count)
            {
                LowestRowIndex--;
            }

            for (var i = LowestRowIndex; i < LowestRowIndex + Fill.Count; ++i)
            {
                var gridRowBelow = _parent.Grid[i - 1];
                var fillRow = Fill[i - LowestRowIndex];
                for (var j = 0; j < 7; ++j)
                {
                    if (gridRowBelow[j] && fillRow[j])
                    {
                        Stuck = true;
                    }
                }
            }

            if (!Stuck)
            {
                LowestRowIndex--;
            }
        }
    }
}