﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using adventofcode.Utilities;
using adventofcode.Utilities.Extensions;

namespace adventofcode.AOC2022
{
    public class SensorBeaconMap
    {
        public SensorBeaconMap(List<Sensor> sensors)
        {
            Sensors = sensors;
        }

        public readonly List<Sensor> Sensors;

        public HashSet<(int x, int y)> Beacons => Sensors.Select(x => x.ClosestBeacon).ToHashSet();

        public bool PointVisible((int x, int y) p)
        {
            return Sensors.Any(x => Tools.ManhattanDistance(x.Position, p) <= x.Dist);
        }
    }

    [DebuggerDisplay(
        "Sensor at x={Position.x}, y={Position.y}: closest beacon is at x={ClosestBeacon.x}, y={ClosestBeacon.y}")]
    public class Sensor
    {
        public (int x, int y) Position;
        public (int x, int y) ClosestBeacon;
        public int Dist;

        public readonly List<(Sensor s, int y, int fromX, int toX)> Rows = new List<(Sensor s, int y, int fromX, int toX)>();

        public readonly HashSet<(int x, int y)> Area = new HashSet<(int x, int y)>();

        public static Sensor ReadFromLine(string line, int capAt)
        {
            var result = new Sensor();
            var parts = line.Split(' ');
            
            var xFromString = parts[2].Split('=')[1];
            var yFromString = parts[3].Split('=')[1];
            var beaconXFromString = parts[8].Split('=')[1];
            var beaconYFromString = parts[9].Split('=')[1];

            var xAsint = xFromString.Substring(0, xFromString.Length - 1);
            var yAsInt = yFromString.Substring(0, yFromString.Length - 1);
            var beaconXAsInt = beaconXFromString.Substring(0, beaconXFromString.Length - 1);
            var beaconYAsInt = beaconYFromString;

            result.Position = (xAsint.ToInt(), yAsInt.ToInt());
            result.ClosestBeacon = (beaconXAsInt.ToInt(), beaconYAsInt.ToInt());
            result.Dist = Tools.ManhattanDistance(result.Position, result.ClosestBeacon);

            for (var y = result.Position.y - result.Dist; y <= result.Position.y + result.Dist; ++y)
            {
                if (y < 0 || y > capAt)
                {
                    continue;
                }
                
                var w = result.Dist - Math.Abs(y - result.Position.y);
                var cappedRow = (
                    result,
                    ylol: y,
                    Math.Max(0, result.Position.x - w),
                    Math.Min(capAt, result.Position.x + w));
                var row = (
                    result, 
                    ylol: y, 
                    result.Position.x - w, 
                    result.Position.x + w);
                result.Rows.Add(cappedRow);
            }

            return result;
        }
    }

    public class Day15 : Day
    {
        //Globals
        private readonly List<string> _input;

        private readonly SensorBeaconMap _sensorBeaconMap;

        //Constructor
        public Day15()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2022\day15.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test1 = @"Sensor at x=2, y=18: closest beacon is at x=-2, y=15
Sensor at x=9, y=16: closest beacon is at x=10, y=16
Sensor at x=13, y=2: closest beacon is at x=15, y=3
Sensor at x=12, y=14: closest beacon is at x=10, y=16
Sensor at x=10, y=20: closest beacon is at x=10, y=16
Sensor at x=14, y=17: closest beacon is at x=10, y=16
Sensor at x=8, y=7: closest beacon is at x=2, y=10
Sensor at x=2, y=0: closest beacon is at x=2, y=10
Sensor at x=0, y=11: closest beacon is at x=2, y=10
Sensor at x=20, y=14: closest beacon is at x=25, y=17
Sensor at x=17, y=20: closest beacon is at x=21, y=22
Sensor at x=16, y=7: closest beacon is at x=15, y=3
Sensor at x=14, y=3: closest beacon is at x=15, y=3
Sensor at x=20, y=1: closest beacon is at x=15, y=3";

            _input = file.GetLines();

            //Do Pre-stuff
            var sensors = _input.Select(x => Sensor.ReadFromLine(x, Part == 1 ? int.MaxValue : _input.Count == 14 ? 20 : 4000000)).ToList();

            _sensorBeaconMap = new SensorBeaconMap(sensors);
        }

        public List<(int from, int to)> GetCoverageAtLine(ILookup<int, (Sensor s, int y, int fromX, int toX)> rowLookup, int y)
        {
            if (!rowLookup[y].Any())
            {
                return new List<(int from, int to)>();
            }

            var orderedCoverage = rowLookup[y].OrderBy(x => x.fromX).ToList();
            var result = new List<(int from, int to)>
            {
                (orderedCoverage.First().fromX, orderedCoverage.First().toX),
            };
            foreach (var s in orderedCoverage.Skip(1))
            {
                var intervalExists = result.Any(z =>
                    (s.fromX >= z.from && s.fromX <= z.to) || (s.toX >= z.from && s.toX <= z.to));
                if (intervalExists)
                {
                    var interval = result.First(z =>
                        (s.fromX >= z.from && s.fromX <= z.to) || (s.toX >= z.from && s.toX <= z.to));
                    var ind = result.IndexOf(interval);
                    result[ind] = (Math.Min(result[ind].from, s.fromX),
                        Math.Max(result[ind].to, s.toX));
                    continue;
                }

                intervalExists = result.Any(z => s.fromX == z.to + 1);
                if (intervalExists)
                {
                    var interval = result.First(z => s.fromX == z.to + 1);
                    var ind = result.IndexOf(interval);
                    result[ind] = (result[ind].from, s.toX);
                    continue;
                }

                result.Add((s.fromX, s.toX));
            }

            return result;
        }

        protected override string Part1()
        {
            var rowToCheck = _input.Count == 14 ? 20 : 2000000;
            var rowLookup = _sensorBeaconMap.Sensors.SelectMany(x => x.Rows).ToLookup(x => x.y);

            var lineCoverage = GetCoverageAtLine(rowLookup, rowToCheck);
            
            var hitCells = lineCoverage.Sum(segment => segment.to - segment.from + 1) - _sensorBeaconMap.Beacons.Count(x => x.y == rowToCheck);
            return hitCells.ToString();
        }

        protected override string Part2()
        {
            var rowLookup = _sensorBeaconMap.Sensors.SelectMany(x => x.Rows).ToLookup(x => x.y);

            var cap = _input.Count == 14 ? 20 : 4000000;
            
            var lists = new List<(int y, List<(int from, int to)> list)>();
            for (var y = 0; y <= cap; ++y)
            {
                var lineCoverage = GetCoverageAtLine(rowLookup, y);
                lists.Add((y, lineCoverage));
            }

            var weirdLists = lists.Where(x => x.list.Count > 1).ToList();

            if (weirdLists.Count > 1)
            {
                return "weird > 2";
            }
            
            var p = (x: weirdLists.First().list.First().to + 1, y: weirdLists.First().y);
            var visible = _sensorBeaconMap.PointVisible(p);

            if (visible)
            {
                return "visible";
            }

            var tuning = (BigInteger)p.x * 4000000 + p.y;
            return tuning.ToString();
        }
    }
}