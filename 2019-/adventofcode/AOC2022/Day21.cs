﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using adventofcode.AOC2022.DayStuff.Day21;
using adventofcode.Utilities;

namespace adventofcode.AOC2022
{
    public enum Day21MonkeyOperatorEnum
    {
        Add,
        Subtract,
        Multiply,
        Divide,
        Equals,
    }
    
    public class Day21 : Day
    {
        //Globals
        private readonly List<string> _input;
        private List<Day21Monkey> _monkeys;
        private Day21Monkey _root;
        private Day21Monkey _humn;

        //Constructor
        public Day21()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2022\day21.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"2022\day21.txt");

            //Test case
            var test1 = @"root: pppw + sjmn
dbpl: 5
cczh: sllz + lgvd
zczc: 2
ptdq: humn - dvpt
dvpt: 3
lfqf: 4
humn: 5
ljgn: 2
sjmn: drzm * dbpl
sllz: 4
pppw: cczh / lfqf
lgvd: ljgn * ptdq
drzm: hmdt - zczc
hmdt: 32";

            _input = file.GetLines();

            //Do Pre-stuff
            _monkeys = new List<Day21Monkey>();
            var monkeyReferences = new Dictionary<Day21Monkey, List<string>>();
            foreach (var line in _input)
            {
                var monkey = new Day21Monkey
                {
                    Name = line.Split(':').First().Trim(),
                };
                _monkeys.Add(monkey);

                monkeyReferences.Add(monkey, new List<string>());

                var monkeyWork = line.Split(':').Last().Trim();
                if (monkeyWork.Contains('+'))
                {
                    monkey.Operation = Day21MonkeyOperatorEnum.Add;
                    var parts = monkeyWork.Split('+');
                    if (BigInteger.TryParse(parts.First().Trim(), out var v1))
                    {
                        monkey.MonkeyReferences.Add(Day21Monkey.DefaultMonkey, v1);
                    }
                    else
                    {
                        monkeyReferences[monkey].Add(parts.First().Trim());
                    }

                    if (parts.Length <= 1) continue;

                    if (BigInteger.TryParse(parts.Last().Trim(), out var v2))
                    {
                        monkey.MonkeyReferences[Day21Monkey.DefaultMonkey] += v2;
                    }
                    else
                    {
                        monkeyReferences[monkey].Add(parts.Last().Trim());
                    }
                }
                else if (monkeyWork.Contains('-'))
                {
                    monkey.Operation = Day21MonkeyOperatorEnum.Subtract;
                    var parts = monkeyWork.Split('-');
                    if (BigInteger.TryParse(parts.First().Trim(), out var v1))
                    {
                        monkey.MonkeyReferences.Add(Day21Monkey.DefaultMonkey, v1);
                    }
                    else
                    {
                        monkeyReferences[monkey].Add(parts.First().Trim());
                    }

                    if (parts.Length <= 1) continue;

                    if (BigInteger.TryParse(parts.Last().Trim(), out var v2))
                    {
                        monkey.MonkeyReferences[Day21Monkey.DefaultMonkey] -= v2;
                    }
                    else
                    {
                        monkeyReferences[monkey].Add(parts.Last().Trim());
                    }
                }
                else if (monkeyWork.Contains('*'))
                {
                    monkey.Operation = Day21MonkeyOperatorEnum.Multiply;
                    var parts = monkeyWork.Split('*');
                    if (BigInteger.TryParse(parts.First().Trim(), out var v1))
                    {
                        monkey.MonkeyReferences.Add(Day21Monkey.DefaultMonkey, v1);
                    }
                    else
                    {
                        monkeyReferences[monkey].Add(parts.First().Trim());
                    }

                    if (parts.Length <= 1) continue;

                    if (BigInteger.TryParse(parts.Last().Trim(), out var v2))
                    {
                        monkey.MonkeyReferences[Day21Monkey.DefaultMonkey] *= v2;
                    }
                    else
                    {
                        monkeyReferences[monkey].Add(parts.Last().Trim());
                    }
                }
                else if (monkeyWork.Contains('/'))
                {
                    monkey.Operation = Day21MonkeyOperatorEnum.Divide;
                    var parts = monkeyWork.Split('/');
                    if (BigInteger.TryParse(parts.First().Trim(), out var v1))
                    {
                        monkey.MonkeyReferences.Add(Day21Monkey.DefaultMonkey, v1);
                    }
                    else
                    {
                        monkeyReferences[monkey].Add(parts.First().Trim());
                    }

                    if (parts.Length <= 1) continue;

                    if (BigInteger.TryParse(parts.Last().Trim(), out var v2))
                    {
                        monkey.MonkeyReferences[Day21Monkey.DefaultMonkey] /= v2;
                    }
                    else
                    {
                        monkeyReferences[monkey].Add(parts.Last().Trim());
                    }
                }
                else
                {
                    monkey.MonkeyReferences.Add(Day21Monkey.DefaultMonkey, BigInteger.Parse(monkeyWork.Trim()));
                }
            }

            var monkeysByName = _monkeys.ToDictionary(x => x.Name, x => x);
            foreach (var monkey in _monkeys)
            {
                foreach (var monkeyReference in monkeyReferences[monkey])
                {
                    monkey.MonkeyReferences.Add(monkeysByName[monkeyReference], null);
                }
            }

            foreach (var monkey in _monkeys)
            {
                foreach (var reference in monkey.MonkeyReferences.Keys)
                {
                    reference.Listeners.Add(monkey);
                }
            }

            _root = _monkeys.First(x => x.IsRoot);
            _humn = _monkeys.First(x => x.IsHuman);

            if (Part == 2)
            {
                _root.Operation = Day21MonkeyOperatorEnum.Equals;
            }
            
            foreach (var monkey in _monkeys.Where(x =>
                         x.MonkeyReferences.Keys.Count == 1 &&
                         x.MonkeyReferences.Keys.First() == Day21Monkey.DefaultMonkey))
            {
                monkey.Process();
            }
        }

        protected override string Part1()
        {
            var monkeyIndex = 0;
            while (!_root.Result.HasValue)
            {
                var monkey = _monkeys[monkeyIndex];
                monkey.Process();

                monkeyIndex = (monkeyIndex + 1) % _monkeys.Count;
            }

            return _root.Result?.ToString() ?? "FEL";
        }

        protected override string Part2()
        {
            var monkeyIndex = 0;
            while (!_root.Result.HasValue)
            {
                var monkey = _monkeys[monkeyIndex];
                monkey.Process();

                monkeyIndex = (monkeyIndex + 1) % _monkeys.Count;
            }

            return _root.ToString();
        }
    }
}