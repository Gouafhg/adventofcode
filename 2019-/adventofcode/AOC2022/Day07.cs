﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using adventofcode.Utilities;

namespace adventofcode.AOC2022
{
    public class Day07 : Day
    {
        [DebuggerDisplay("{Name} {Size}")]
        public class DirectoryFile
        {
            public DirectoryFile(Directory parent, string name, BigInteger size) 
            {
                Parent = parent;
                Name = name;
                Size = size;
            }

            public Directory Parent;
            public string Name = string.Empty;
            public BigInteger Size = 0;
        }

        [DebuggerDisplay("{Name} (dir)")]
        public class Directory
        {
            public Directory(Directory parent, string name)
            {
                Parent = parent;
                Name = name;
            }

            public BigInteger TotalSize()
            {
                var fileSize = BigInteger.Zero;
                foreach (var file in Files) 
                {
                    fileSize += file.Size;
                }

                var dirSize = BigInteger.Zero;
                foreach (var dir in SubDirectories)
                {
                    dirSize += dir.TotalSize();
                }

                return fileSize + dirSize;
            }

            public string Name;
            public Directory Parent;
            public List<Directory> SubDirectories = new List<Directory>();
            public List<DirectoryFile> Files = new List<DirectoryFile>();

            public void AddDirectory(string name)
            {
                if (SubDirectories.Any(x => x.Name == name))
                {
                    return;
                }
                SubDirectories.Add(new Directory(this, name));
            }

            public void AddFile(string f)
            {
                var size = BigInteger.Parse(f.Split(' ')[0]);
                var name = f.Split(' ')[1];
                Files.Add(new DirectoryFile(this, name, size));
            }

            public List<Directory> GetRecursizeDirectories()
            {
                return new[] { this }.Concat(SubDirectories.SelectMany(x => x.GetRecursizeDirectories())).ToList();
            }
        }

        List<string> RunCommand(ref int pos, out (string command, string parameter) command)
        {
            command.command = _input[pos].Split(' ')[1];
            command.parameter = string.Empty;
            if (command.command == "cd")
            {
                command.parameter = _input[pos].Split(' ')[2];
            }

            var result = new List<string>();
            pos++;
            while (pos < _input.Count && _input[pos][0] != '$')
            {
                result.Add(_input[pos]);
                pos++;
            }

            return result;
        }

        //Globals
        private readonly List<string> _input;
        Directory Root;

        //Constructor
        public Day07()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2022\day07.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
//            var test1 = @"$ cd /
//$ ls
//dir a
//14848514 b.txt
//8504156 c.dat
//dir d
//$ cd a
//$ ls
//dir e
//29116 f
//2557 g
//62596 h.lst
//$ cd e
//$ ls
//584 i
//$ cd ..
//$ cd ..
//$ cd d
//$ ls
//4060174 j
//8033020 d.log
//5626152 d.ext
//7214296 k";

            _input = file.GetLines();

            //Do Pre-stuff
            var commandResults = new List<((string command, string parameter) command, List<string> results)>();
            var pos = 0;
            while (pos < _input.Count)
            {
                var r = RunCommand(ref pos, out var c);
                commandResults.Add((c, r));
            }

            Root = new Directory(null, "/");
            var current = Root;

            foreach (var r in commandResults)
            {
                if (r.command.command == "cd")
                {
                    if (r.command.parameter == "..")
                    {
                        current = current.Parent;
                        continue;
                    }
                    if (r.command.parameter == "/")
                    {
                        current = Root;
                        continue;
                    }

                    current.AddDirectory(r.command.parameter);
                    current = current.SubDirectories.First(x => x.Name == r.command.parameter);
                }
                else if (r.command.command == "ls")
                {
                    foreach (var row in r.results)
                    {
                        var parts = row.Split(' ');
                        if (parts[0] == "dir")
                        {
                            current.AddDirectory(parts[1]);
                        }
                        else
                        {
                            current.AddFile(row);
                        }
                    }
                }
            }

        }

        protected override string Part1()
        {
            var allDirs = Root.GetRecursizeDirectories();

            var dirsizedic = allDirs.ToDictionary(x => x, x => x.TotalSize());

            var lessThan100k = dirsizedic.Where(x => x.Value <= 100000).Select(x => x.Value).ToList();
            var totSize = BigInteger.Zero;
            foreach (var dir in lessThan100k)
            {
                totSize += dir;
            }

            return totSize.ToString();
        }

        protected override string Part2()
        {
            var totalDiskSize = 70000000;

            var unused = totalDiskSize - Root.TotalSize();

            var mustBeFreed = 30000000 - unused;

            var allDirs = Root.GetRecursizeDirectories();

            var dirsizedic = allDirs.ToDictionary(x => x, x => x.TotalSize());

            var biggerdiskSpaceLeft = dirsizedic.Where(x => x.Value >= mustBeFreed).ToDictionary(x => x.Key, x => x.Value);

            return biggerdiskSpaceLeft.OrderBy(x => x.Value).First().Value.ToString();
        }
    }
}