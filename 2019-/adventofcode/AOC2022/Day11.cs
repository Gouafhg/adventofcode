﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using adventofcode.Utilities;

namespace adventofcode.AOC2022
{
    [DebuggerDisplay("{InspectedItems}")]
    public class Monkey
    {
        public int MonkeyIndex;
        public static List<Monkey> Monkeys = new List<Monkey>();

        public BigInteger InspectedItems = 0;

        public List<MonkeyItem> Items = new List<MonkeyItem>();

        public Action<MonkeyItem> Operation;
        public Func<MonkeyItem, bool> Test;

        public int ConditionTrue;
        public int ConditionFalse;

        public void TakeTurn(/*bool divide*/)
        {
            InspectedItems += Items.Count;

            foreach (var item in Items)
            {
                Operation(item);
                //if (divide)
                //{
                //    newItem /= 3;
                //}
                var toMonkey = Test(item) ? ConditionTrue: ConditionFalse;

                Monkeys[toMonkey].Items.Add(item);
            }
            Items.Clear();
        }
    }

    public class MonkeyItem
    {
        public static List<int> Keys = new List<int>();
        public Dictionary<int, int> Mod = Keys.ToDictionary(x => x, x => 0);

        public MonkeyItem(int v)
        {
            SetValue(v);
        }

        public void SetValue(int v)
        {
            foreach (var key in Keys)
            {
                Mod[key] = v % key;
            }
        }

        public void Add(MonkeyItem item)
        {
            foreach (var key in Keys)
            {
                Mod[key] += item.Mod[key];
            }
            Limit();
        }

        public void Multiply(MonkeyItem item)
        {
            foreach (var key in Keys)
            {
                Mod[key] *= item.Mod[key];
            }
            Limit();
        }

        public void Add(int v)
        {
            foreach (var key in Keys)
            {
                Mod[key] += v % key;
            }
            Limit();
        }
        public void Multiply(int v)
        {
            foreach (var key in Keys)
            {
                Mod[key] *= v % key;
            }
            Limit();
        }

        private void Limit()
        {
            foreach (var key in Keys)
            {
                Mod[key] = Mod[key] % key;
            }
        }
    }

    public class Day11 : Day
    {
        //Globals
        private readonly List<string> _input;

        //Constructor
        public Day11()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2022\day11.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test1 = @"Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1";

            _input = file.GetLines();

            //Do Pre-stuff
            for (var rowIndex = 0; rowIndex < _input.Count; rowIndex += 6)
            {
                var key = int.Parse(_input[rowIndex + 3].Split(':')[1].Split(' ').Last().Trim());
                MonkeyItem.Keys.Add(key);
            }

            for (var rowIndex = 0; rowIndex < _input.Count; rowIndex += 6)
            {
                var items = _input[rowIndex + 1].Split(':')[1].Split(',').Select(x => int.Parse(x.Trim()));
                var operation = _input[rowIndex + 2].Split('=')[1].Split(' ').Skip(2).Take(2);
                var key = int.Parse(_input[rowIndex + 3].Split(':')[1].Split(' ').Last().Trim());
                var conditionTrue = int.Parse(_input[rowIndex + 4].Split(' ').Last());
                var conditionFalse = int.Parse(_input[rowIndex + 5].Split(' ').Last());

                var monkey = new Monkey();
                monkey.MonkeyIndex = Monkey.Monkeys.Count;
                monkey.Items.AddRange(items.Select(x => new MonkeyItem(x)));
                if (operation.First() == "+")
                {
                    monkey.Operation = x => x.Add(int.Parse(operation.Last()));
                }
                else if (operation.Last() != "old")
                {
                    monkey.Operation = x => x.Multiply(int.Parse(operation.Last()));
                }
                else
                {
                    monkey.Operation = x => x.Multiply(x);
                }
                monkey.Test = x => x.Mod[key] == 0;
                monkey.ConditionTrue = conditionTrue;
                monkey.ConditionFalse = conditionFalse;
                Monkey.Monkeys.Add(monkey);
            }

            //var test = true;

            //if (test)
            //{
            //    var Monkey0 = new Monkey
            //    {
            //        Items = new List<MonkeyItem> { new MonkeyItem(79), new MonkeyItem(98) },
            //        //Items = new List<BigInteger> { 79, 2*7*7 },
            //        Operation = new Action<MonkeyItem>(x => x.Multiply(19)),
            //        Test = new Func<MonkeyItem, bool>(x => x.Mod[23] == 0),
            //        ConditionTrue = 2,
            //        ConditionFalse = 3
            //    };
            //    monkeys.Add(Monkey0);

            //    var Monkey1 = new Monkey
            //    {
            //        Items = new List<MonkeyItem> { new MonkeyItem(54), new MonkeyItem(65), new MonkeyItem(75), new MonkeyItem(74) },
            //        //Items = new List<BigInteger> { 2*3*3*3, 5*13, 3*5*5, 2*37 },
            //        Operation = new Action<MonkeyItem>(x => x.Add(6)),
            //        Test = new Func<MonkeyItem, bool>(x => x.Mod[19] == 0),
            //        ConditionTrue = 2,
            //        ConditionFalse = 0
            //    };
            //    monkeys.Add(Monkey1);

            //    var Monkey2 = new Monkey
            //    {
            //        Items = new List<MonkeyItem> { new MonkeyItem(79), new MonkeyItem(60), new MonkeyItem(97) },
            //        //Items = new List<BigInteger> { 79, 2 * 2 * 3 * 5, 97 },
            //        Operation = new Action<MonkeyItem>(x => x.Multiply(x)),
            //        Test = new Func<MonkeyItem, bool>(x => x.Mod[13] == 0),
            //        ConditionTrue = 1,
            //        ConditionFalse = 3
            //    };
            //    monkeys.Add(Monkey2);

            //    var Monkey3 = new Monkey
            //    {
            //        Items = new List<MonkeyItem> { new MonkeyItem(74) },
            //        //Items = new List<BigInteger> { 2*37 },
            //        Operation = new Action<MonkeyItem>(x => x.Add(3)),
            //        Test = new Func<MonkeyItem, bool>(x => x.Mod[17] == 0),
            //        ConditionTrue = 0,
            //        ConditionFalse = 1
            //    };
            //    monkeys.Add(Monkey3);
            //}
            //else
            //{
            //    //var Monkey0 = new Monkey
            //    //{
            //    //    Items = new List<MonkeyItem> { new MonkeyItem(91), new MonkeyItem(58), new MonkeyItem(52), new MonkeyItem(69), new MonkeyItem(95), new MonkeyItem(54) },
            //    //    Operation = new Action<MonkeyItem>(x => x.Multiply(13),
            //    //    Test = new Func<MonkeyItem, bool>(x => x % 7 == 0),
            //    //    ConditionTrue = 1,
            //    //    ConditionFalse = 5
            //    //};
            //    //monkeys.Add(Monkey0);

            //    //var Monkey1 = new Monkey
            //    //{
            //    //    Items = new List<MonkeyItem> { new MonkeyItem(80), new MonkeyItem(80), new MonkeyItem(97), new MonkeyItem(84) },
            //    //    Operation = new Func<BigInteger, BigInteger>(x => x * x),
            //    //    Test = new Func<BigInteger, bool>(x => x % 3 == 0),
            //    //    ConditionTrue = 3,
            //    //    ConditionFalse = 5
            //    //};
            //    //monkeys.Add(Monkey1);

            //    //var Monkey2 = new Monkey
            //    //{
            //    //    Items = new List<MonkeyItem> { new MonkeyItem(86), new MonkeyItem(92), new MonkeyItem(71) },
            //    //    Operation = new Func<BigInteger, BigInteger>(x => x + 7),
            //    //    Test = new Func<BigInteger, bool>(x => x % 2 == 0),
            //    //    ConditionTrue = 0,
            //    //    ConditionFalse = 4
            //    //};
            //    //monkeys.Add(Monkey2);

            //    //var Monkey3 = new Monkey
            //    //{
            //    //    Items = new List<MonkeyItem> { new MonkeyItem(96), new MonkeyItem(90), new MonkeyItem(99), new MonkeyItem(76), new MonkeyItem(79), new MonkeyItem(85), new MonkeyItem(98), new MonkeyItem(61) },
            //    //    Operation = new Func<BigInteger, BigInteger>(x => x + 4),
            //    //    Test = new Func<BigInteger, bool>(x => x % 11 == 0),
            //    //    ConditionTrue = 7,
            //    //    ConditionFalse = 6
            //    //};
            //    //monkeys.Add(Monkey3);

            //    //var Monkey4 = new Monkey
            //    //{
            //    //    Items = new List<MonkeyItem> { new MonkeyItem(60), new MonkeyItem(83), new MonkeyItem(68), new MonkeyItem(64), new MonkeyItem(73) },
            //    //    Operation = new Func<BigInteger, BigInteger>(x => x * 19),
            //    //    Test = new Func<BigInteger, bool>(x => x % 17 == 0),
            //    //    ConditionTrue = 1,
            //    //    ConditionFalse = 0
            //    //};
            //    //monkeys.Add(Monkey4);

            //    //var Monkey5 = new Monkey
            //    //{
            //    //    Items = new List<MonkeyItem> { new MonkeyItem(96), new MonkeyItem(52), new MonkeyItem(52), new MonkeyItem(94), new MonkeyItem(76), new MonkeyItem(51), new MonkeyItem(57) },
            //    //    Operation = new Func<BigInteger, BigInteger>(x => x + 3),
            //    //    Test = new Func<BigInteger, bool>(x => x % 5 == 0),
            //    //    ConditionTrue = 7,
            //    //    ConditionFalse = 3
            //    //};
            //    //monkeys.Add(Monkey5);

            //    //var Monkey6 = new Monkey
            //    //{
            //    //    Items = new List<MonkeyItem> { new MonkeyItem(75) },
            //    //    Operation = new Func<BigInteger, BigInteger>(x => x + 5),
            //    //    Test = new Func<BigInteger, bool>(x => x % 13 == 0),
            //    //    ConditionTrue = 4,
            //    //    ConditionFalse = 2
            //    //};
            //    //monkeys.Add(Monkey6);

            //    //var Monkey7 = new Monkey
            //    //{
            //    //    Items = new List<MonkeyItem> { new MonkeyItem(83), new MonkeyItem(75) },
            //    //    Operation = new Func<BigInteger, BigInteger>(x => x + 1),
            //    //    Test = new Func<BigInteger, bool>(x => x % 19 == 0),
            //    //    ConditionTrue = 2,
            //    //    ConditionFalse = 6
            //    //};
            //    //monkeys.Add(Monkey7);
            //}
        }

        public void TakeRound(/*bool divide*/)
        {
            foreach (var monkey in Monkey.Monkeys)
            {
                monkey.TakeTurn(/*divide*/);
            }
        }

        protected override string Part1()
        {
            for (var i = 0; i < 20; ++i)
            {
                TakeRound(/*true*/);
            }

            var topMonkeys = Monkey.Monkeys.OrderByDescending(x => x.InspectedItems).Take(2).ToList();

            var result = topMonkeys[0].InspectedItems * topMonkeys[1].InspectedItems;

            return result.ToString();
        }

        public void PrintMonkeys()
        {

        }

        protected override string Part2()
        {
            for (var i = 0; i < 10000; ++i)
            {
                //Console.WriteLine(i.ToString());
                TakeRound(/*false*/);
            }

            var topMonkeys = Monkey.Monkeys.OrderByDescending(x => x.InspectedItems).Take(2).ToList();

            var result = topMonkeys[0].InspectedItems * topMonkeys[1].InspectedItems;

            return result.ToString();
        }
    }
}
