﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2022
{
    [DebuggerDisplay("{Position}")]
    public class Sand
    {
        public static readonly HashSet<(int x, int y)> SandPositions = new HashSet<(int x, int y)>();
        
        public (int x, int y) Position = Day14.SandSource;
        public bool Stuck = false;

        public void SetToRest()
        {
            Stuck = true;
            SandPositions.Add(Position);
        }
    }

    public class Day14 : Day
    {
        //Globals
        private readonly List<string> _input;

        public static (int x, int y) SandSource = (500, 0);
        private readonly List<Sand> _sand = new List<Sand>();
        private readonly HashSet<(int x, int y)> _rock = new HashSet<(int x, int y)>();

        //Constructor
        public Day14()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2022\day14.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test1 = @"498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9";

            _input = file.GetLines();

            //Do Pre-stuff
            foreach (var line in _input)
            {
                var parts = line.Split("->", StringSplitOptions.RemoveEmptyEntries).Select(x => (x: x.Trim().Split(',').Select(int.Parse).ToList()[0],
                    y: x.Split(',').Select(int.Parse).ToList()[1])).ToList();
                for (var i = 1; i < parts.Count; ++i)
                {
                    if (parts[i].x > parts[i - 1].x)
                    {
                        for (var x = parts[i - 1].x; x <= parts[i].x; ++x)
                        {
                            _rock.Add((x, parts[i].y));
                        }
                    }
                    else if (parts[i - 1].x > parts[i].x)
                    {
                        for (var x = parts[i].x; x <= parts[i - 1].x; ++x)
                        {
                            _rock.Add((x, parts[i].y));
                        }
                    }
                    else if (parts[i].y > parts[i - 1].y)
                    {
                        for (var y = parts[i - 1].y; y <= parts[i].y; ++y)
                        {
                            _rock.Add((parts[i].x, y));
                        }
                    }
                    else
                    {
                        for (var y = parts[i].y; y <= parts[i - 1].y; ++y)
                        {
                            _rock.Add((parts[i].x, y));
                        }
                    }
                }
            }
        }

        protected override string Part1()
        {
            var lastSandCount = 0;
            while (_sand.Count == 0 || lastSandCount != _sand.Count)
            {
                var iterations = 0;
                while (iterations < 1100 && _sand.Any(x => !x.Stuck))
                {
                    iterations++;
                    var sand = _sand.First(x => !x.Stuck);

                    var down = (x: sand.Position.x, y: sand.Position.y + 1);
                    if (!_rock.Contains(down) && !_sand.Any(x => x.Position == down))
                    {
                        sand.Position = down;
                        continue;
                    }

                    var downLeft = (x: sand.Position.x - 1, y: sand.Position.y + 1);
                    if (!_rock.Contains(downLeft) && !_sand.Any(x => x.Position == downLeft))
                    {
                        sand.Position = downLeft;
                        continue;
                    }

                    var downRight = (x: sand.Position.x + 1, y: sand.Position.y + 1);
                    if (!_rock.Contains(downRight) && !_sand.Any(x => x.Position == downRight))
                    {
                        sand.Position = downRight;
                        continue;
                    }

                    sand.Stuck = true;
                }

                lastSandCount = _sand.Count;
                _sand.Add(new Sand());
                _sand.RemoveAll(x => x.Position.y > 1000);
            }

            return _sand.Count(x => x.Stuck).ToString();
        }

        protected override string Part2()
        {
            var maxDepth = _rock.Max(x => x.y) + 2;

            Sand sand = null;
            while (sand?.Position != SandSource)
            {
                sand = new Sand();
                while (!sand.Stuck)
                {
                    if (sand.Position.y == maxDepth - 1)
                    {
                        sand.SetToRest();
                        continue;
                    }
                    
                    var down = (x: sand.Position.x, y: sand.Position.y + 1);
                    if (!_rock.Contains(down) && !Sand.SandPositions.Contains(down))
                    {
                        sand.Position = down;
                        continue;
                    }

                    var downLeft = (x: sand.Position.x - 1, y: sand.Position.y + 1);
                    if (!_rock.Contains(downLeft) && !Sand.SandPositions.Contains(downLeft))
                    {
                        sand.Position = downLeft;
                        continue;
                    }

                    var downRight = (x: sand.Position.x + 1, y: sand.Position.y + 1);
                    if (!_rock.Contains(downRight) && !Sand.SandPositions.Contains(downRight))
                    {
                        sand.Position = downRight;
                        continue;
                    }

                    Sand.SandPositions.Add(sand.Position);
                    sand.SetToRest();
                }
            }
 
            return Sand.SandPositions.Count.ToString();
        }
    }
}