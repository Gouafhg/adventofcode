﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using adventofcode.AOC2022.DayStuff.Day10;
using adventofcode.Utilities;

namespace adventofcode.AOC2022
{

    public class Day10 : Day
    {
        //Globals
        private readonly List<string> _input;


        Day10CentralProcessingUnit cpu = new Day10CentralProcessingUnit();

        //Constructor
        public Day10()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2022\day10.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"2022\day10.txt");
            var file = ToolsFile.ReadFile(@"D:\GIT\adventofcode\2019-\data\2022\day10.txt");

            //Test case
            var test1 = @"noop
addx 3
addx -5";

            var test2 = @"addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop";

            _input = file.GetLines();

            //Do Pre-stuff

            foreach (var line in _input)
            {
                var parts = line.Split(' ');
                if (parts[0] == "noop")
                {
                    cpu.Instructions.Add(new Noop(cpu));
                }
                if (parts[0] == "addx")
                {
                    var arg = parts[1];
                    cpu.Instructions.Add(new AddX(cpu, int.Parse(arg)));
                }
            }
        }

        protected override string Part1()
        {
            cpu.Execute();
            return cpu.SignalStrength.Where(x => x.Cycle <= 220).Aggregate(BigInteger.Zero, (lol, next) => lol += next.Strength, x => x).ToString();
        }

        protected override string Part2()
        {
            cpu.Execute();

            return cpu.CathodeRayTube.Show();
        }
    }
}
