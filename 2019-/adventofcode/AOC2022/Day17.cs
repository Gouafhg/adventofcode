﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using adventofcode.AOC2022.DayStuff.Day17;
using adventofcode.Utilities;

namespace adventofcode.AOC2022
{
    public class Day17 : Day
    {
        //Globals
        private readonly List<string> _input;
        private readonly Day17TetrisGrid _grid;

        //Constructor
        public Day17()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2022\day17.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test1 = @">>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>";

            _input = file.GetLines();

            //Do Pre-stuff

            Day17TetrisGrid.Part = Part;
            _grid = new Day17TetrisGrid(_input.First());
        }

        protected override string Part1()
        {
            while (_grid.PieceCount < 2023)
            {
                _grid.Tick();
            }

            return _grid.GetTowerHeight().ToString();
        }

        protected override string Part2()
        {
            var yup = false;
            try
            {
                while (_grid.TickCount < int.MaxValue)
                {
                    _grid.Tick();
                }
            }
            catch (Exception e)
            {
                yup = true;
            }

            if (!yup) return "HIT SKA MAN INTE KOMMA";
            
            BigInteger total = 1000000000000;
            total -= Day17TetrisGrid.FirstStoneCount;
            total = BigInteger.DivRem(total, Day17TetrisGrid.RepeatStoneCount, out var rem);
                
            BigInteger height = Day17TetrisGrid.FirstHeight;
            height += total * Day17TetrisGrid.RepeatHeight;
            height += Day17TetrisGrid.ExtraHeight;
                
            return height.ToString();

        }
    }
}