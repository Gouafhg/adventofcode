﻿using adventofcode.Utilities;
using System.Collections.Generic;

namespace adventofcode.AOC2022
{
    public enum GameAction { Rock, Paper, Scissors };

    internal class Day02 : Day
    {
        public int GetRoundPoints(GameAction they, GameAction you)
        {
            return GameActionPoints[you] + they switch
            {
                GameAction.Rock => you switch
                {
                    GameAction.Rock => 3,
                    GameAction.Paper => 6,
                    GameAction.Scissors => 0,
                },
                GameAction.Paper => you switch
                {
                    GameAction.Rock => 0,
                    GameAction.Paper => 3,
                    GameAction.Scissors => 6,
                },
                GameAction.Scissors => you switch
                {
                    GameAction.Rock => 6,
                    GameAction.Paper => 0,
                    GameAction.Scissors => 3,
                },
            };
        }

        public GameAction GetMine(GameAction they, char myChar)
        {
            if (myChar == 'Y')
            {
                return they;
            }

            return they switch
            {
                GameAction.Rock => myChar switch
                {
                    'X' => GameAction.Scissors,
                    'Z' => GameAction.Paper,
                },
                GameAction.Paper => myChar switch
                {
                    'X' => GameAction.Rock,
                    'Z' => GameAction.Scissors,
                },
                GameAction.Scissors => myChar switch
                {
                    'X' => GameAction.Paper,
                    'Z' => GameAction.Rock,
                },
            };
        }

        public static Dictionary<GameAction, int> GameActionPoints = new Dictionary<GameAction, int>() 
        { 
            { GameAction.Rock, 1 }, 
            { GameAction.Paper, 2 }, 
            { GameAction.Scissors, 3 },
        };

        public static Dictionary<char, GameAction> charGameActionDictionary = new Dictionary<char, GameAction>()
        {
            { 'A', GameAction.Rock },
            { 'B', GameAction.Paper},
            { 'C', GameAction.Scissors },
            { 'X', GameAction.Rock },
            { 'Y', GameAction.Paper},
            { 'Z', GameAction.Scissors },
        };

        //Globals
        private readonly List<string> _input;

        private readonly List<(GameAction they, GameAction you)> rounds = new List<(GameAction they, GameAction you)>();



        //Constructor
        public Day02()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFileLines(@"C:\THEGANGGIT\adventofcode\2019-\data\2022\day02.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test1 = @"";

            _input = file;

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            foreach (var line in _input)
            {
                rounds.Add((charGameActionDictionary[line[0]], charGameActionDictionary[line[2]]));
            }

            var sum = 0;
            foreach (var round in rounds)
            {
                sum += GetRoundPoints(round.they, round.you);
            }
            return sum.ToString();
        }

        protected override string Part2()
        {
            foreach (var line in _input)
            {
                var they = charGameActionDictionary[line[0]];
                rounds.Add((they, GetMine(they, line[2])));
            }

            var sum = 0;
            foreach (var round in rounds)
            {
                sum += GetRoundPoints(round.they, round.you);
            }
            return sum.ToString();
        }
    }
}
