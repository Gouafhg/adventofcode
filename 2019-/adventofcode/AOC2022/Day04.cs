﻿using System.Collections.Generic;
using adventofcode.Utilities;
using adventofcode.Utilities.Extensions;

namespace adventofcode.AOC2022
{
    public class Day04 : Day
    {
        //Globals
        private readonly List<string> _input;

        //Constructor
        public Day04()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2022\day04.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test1 = @"2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8";

            _input = file.GetLines();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var useless = 0;
            foreach (var line in _input)
            {
                var parts = line.Split(',');
                var from1 = parts[0].Split('-')[0].ToInt();
                var to1 = parts[0].Split('-')[1].ToInt();
                var from2 = parts[1].Split('-')[0].ToInt();
                var to2 = parts[1].Split('-')[1].ToInt();

                if (from1 <= from2 && to1 >= to2)
                {
                    useless++;
                }
                else if (from2 <= from1 && to2 >= to1)
                {
                    useless++;
                }
            }

            return useless.ToString();
        }

        protected override string Part2()
        {
            var useless = 0;
            foreach (var line in _input)
            {
                var parts = line.Split(',');
                var from1 = parts[0].Split('-')[0].ToInt();
                var to1 = parts[0].Split('-')[1].ToInt();
                var from2 = parts[1].Split('-')[0].ToInt();
                var to2 = parts[1].Split('-')[1].ToInt();

                if (from1 <= to2 && to1 >= from2)
                {
                    useless++;
                }
                else if (from2 <= to1 && to2 >= from1)
                {
                    useless++;
                }
            }

            return useless.ToString();
        }
    }
}
