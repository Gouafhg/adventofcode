﻿using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;
using adventofcode.Utilities.Extensions;

namespace adventofcode.AOC2022
{
    public class Day05 : Day
    {
        //Globals
        private readonly List<string> _input;

        private readonly List<Stack<char>> stacks = new List<Stack<char>>();
        private readonly List<(int count, int fromStack, int toStack)> _commands = new List<(int count, int fromStack, int toStack)>();

        //Constructor
        public Day05()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFileLines(@"C:\THEGANGGIT\adventofcode\2019-\data\2022\day05.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test1 = @"";

            _input = file;

            var rowIndex = 0;
            while (!string.IsNullOrWhiteSpace(_input[rowIndex]))
            {
                rowIndex++;
            }

            rowIndex--;

            var stackIndices = new List<int>();
            for (var posIndex = 0; posIndex < _input[rowIndex].Length; ++posIndex)
            {
                if (char.IsDigit(_input[rowIndex][posIndex]))
                {
                    stackIndices.Add(posIndex);
                }
            }

            foreach (var stackIndex in stackIndices)
            {
                stacks.Add(new Stack<char>());
            }
            rowIndex--;
            while (rowIndex >= 0)
            {
                for (var i = 0; i < stackIndices.Count; ++i)
                {
                    var c = _input[rowIndex][stackIndices[i]];
                    if (c == ' ')
                    {
                        continue;
                    }
                    stacks[i].Push(c);
                }
                rowIndex--;
            }

            rowIndex = 0;
            while (!string.IsNullOrWhiteSpace(_input[rowIndex]))
            {
                rowIndex++;
            }
            rowIndex++;

            while (rowIndex < _input.Count)
            {
                var row = _input[rowIndex];

                _commands.Add((row.Split(' ')[1].ToInt(), row.Split(' ')[3].ToInt() - 1, row.Split(' ')[5].ToInt() - 1));
                rowIndex++;
            }

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            foreach (var command in _commands)
            {
                for (var i = 0; i < command.count; ++i)
                {
                    var c = stacks[command.fromStack].Pop();
                    stacks[command.toStack].Push(c);
                }
            }

            return string.Join("", stacks.Select(x => x.Peek()));
        }

        protected override string Part2()
        {
            foreach (var command in _commands)
            {
                var newTop = new List<char>();
                for (var i = 0; i < command.count; ++i)
                {
                    var c = stacks[command.fromStack].Pop();
                    newTop.Add(c);
                }
                newTop.Reverse();
                foreach (var c in newTop)
                {
                    stacks[command.toStack].Push(c);
                }
            }

            return string.Join("", stacks.Select(x => x.Peek()));
        }
    }
}
