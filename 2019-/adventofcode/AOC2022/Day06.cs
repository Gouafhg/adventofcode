﻿using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2022
{
    public class Day06 : Day
    {
        //Globals
        private readonly string _input;

        //Constructor
        public Day06()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2022\day06.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test1 = @"mjqjpqmgbljsphdztnvjfqwrcgsmlb";

            _input = file;

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var i = 0;
            while (i < _input.Length - 4) 
            {
                var s = _input.Skip(i).Take(4);

                var d = new Dictionary<char, int>();

                foreach (var c in s)
                {
                    if (d.ContainsKey(c)) { d[c]++; }
                    else { d.Add(c, 1); }
                }

                if (d.Values.All(x => x == 1))
                {
                    break;
                }

                i++;
            }
            return (i + 4).ToString();
        }

        protected override string Part2()
        {
            var i = 0;
            while (i < _input.Length - 14)
            {
                var s = _input.Skip(i).Take(14);

                var d = new Dictionary<char, int>();

                foreach (var c in s)
                {
                    if (d.ContainsKey(c)) { d[c]++; }
                    else { d.Add(c, 1); }
                }

                if (d.Values.All(x => x == 1))
                {
                    break;
                }

                i++;
            }
            return (i + 14).ToString();
        }
    }
}
