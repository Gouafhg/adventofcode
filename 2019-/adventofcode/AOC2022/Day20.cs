﻿using System;
using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;
using adventofcode.Utilities.Extensions;

namespace adventofcode.AOC2022
{
    public class Day20 : Day
    {
        //Globals
        private readonly List<string> _input;

        //Constructor
        public Day20()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2022\day20.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"2022\day20.txt");

            //Test case
            var test1 = @"1
2
-3
3
-2
0
4";

            _input = file.GetLines();

            //Do Pre-stuff
        }

        private static List<long> BuildNewSequence(IReadOnlyList<long> originalSequence,
            IReadOnlyList<int> newIndices)
        {
            var oldNewIndexConnection = new List<(int originalIndex, int newIndex)>();
            for (var i = 0; i < originalSequence.Count; ++i)
            {
                oldNewIndexConnection.Add((i, newIndices[i]));
            }

            oldNewIndexConnection = oldNewIndexConnection.OrderBy(x => x.newIndex).ToList();

            return originalSequence.Select((t, i) => originalSequence[oldNewIndexConnection[i].originalIndex]).ToList();
        }

        private static void MoveDown(IList<int> indexReference, int positionInReference, long value, bool bruteForce)
        {
            if (bruteForce)
            {
                for (var j = 0; j > value; --j)
                {
                    MoveDown(indexReference, positionInReference);
                }

                return;
            }

            var rounds = (int)Math.Abs(value / indexReference.Count);
            if (rounds > 0)
            {
                var oldIndex = indexReference[positionInReference];
                for (var i = 0; i < indexReference.Count; ++i)
                {
                    if (indexReference[i] > oldIndex)
                    {
                        indexReference[i]--;
                    }
                }
                
                indexReference.RemoveAt(positionInReference);
                
                for (var i = 0; i < indexReference.Count; ++i)
                {
                    indexReference[i] = (indexReference[i] + rounds) % indexReference.Count;
                }

                for (var i = 0; i < indexReference.Count; ++i)
                {
                    if (indexReference[i] >= oldIndex)
                    {
                        indexReference[i]++;
                    }
                }
                
                indexReference.Insert(positionInReference, oldIndex);
                
                value += (long)indexReference.Count * rounds;
            }

            for (var j = 0; j > value; --j)
            {
                MoveDown(indexReference, positionInReference);
            }
        }

        private static void MoveUp(IList<int> indexReference, int positionInReference, long value, bool bruteForce)
        {
            if (bruteForce)
            {
                for (var j = 0; j < value; ++j)
                {
                    MoveUp(indexReference, positionInReference);
                }

                return;
            }

            var rounds = (int)Math.Abs(value / indexReference.Count);
            if (rounds > 0)
            {
                var oldIndex = indexReference[positionInReference];
                for (var i = 0; i < indexReference.Count; ++i)
                {
                    if (indexReference[i] > oldIndex)
                    {
                        indexReference[i]--;
                    }
                }
                
                indexReference.RemoveAt(positionInReference);
                
                for (var i = 0; i < indexReference.Count; ++i)
                {
                    indexReference[i] = (indexReference[i] - rounds) % indexReference.Count;
                    if (indexReference[i] < 0)
                    {
                        indexReference[i] += indexReference.Count;
                    }
                }

                for (var i = 0; i < indexReference.Count; ++i)
                {
                    if (indexReference[i] >= oldIndex)
                    {
                        indexReference[i]++;
                    }
                }
                
                indexReference.Insert(positionInReference, oldIndex);
                
                value -= (long)indexReference.Count * rounds;
            }

            for (var j = 0; j < value; ++j)
            {
                MoveUp(indexReference, positionInReference);
            }
        }

        private static void MoveDown(IList<int> indexReference, int positionInReference)
        {
            var index = indexReference[positionInReference];

            if (index == 0)
            {
                var indexOfLast = indexReference.IndexOf(indexReference.Count - 1);
                indexReference[positionInReference] = indexReference.Count - 1;
                indexReference[indexOfLast] = 0;
                return;
            }

            var indexOfPrev = indexReference.IndexOf(index - 1);
            indexReference[indexOfPrev]++;
            indexReference[positionInReference]--;
        }

        private static void MoveUp(IList<int> indexReference, int positionInReference)
        {
            var index = indexReference[positionInReference];

            if (index == indexReference.Count - 1)
            {
                var indexOfFirst = indexReference.IndexOf(0);
                indexReference[positionInReference] = 0;
                indexReference[indexOfFirst] = indexReference.Count - 1;
                return;
            }

            var indexOfNext = indexReference.IndexOf(index + 1);
            indexReference[indexOfNext]--;
            indexReference[positionInReference]++;
        }

        private static void Move(IList<int> indexReference, int positionInReference, long value, bool bruteForce = false)
        {
            switch (value)
            {
                case > 0:
                {
                    MoveUp(indexReference, positionInReference, value, bruteForce);

                    break;
                }
                case < 0:
                {
                    MoveDown(indexReference, positionInReference, value, bruteForce);

                    break;
                }
            }
        }


        protected override string Part1()
        {
            var originalSequence = _input.Select(x => x.ToLong()).ToList();
            var indexReference = new List<int>();

            for (var i = 0; i < originalSequence.Count; ++i)
            {
                indexReference.Add(i);
            }

            for (var i = 0; i < originalSequence.Count; ++i)
            {
                if (originalSequence.Count != 7 && i % 500 == 0)
                {
                    Console.WriteLine($"{i} ...");
                }
                
                var value = originalSequence[i];
                
                if (originalSequence.Count == 7)
                {
                    Console.WriteLine($"Original index: {i}");
                    Console.WriteLine($"Value: {value}");
                    Console.WriteLine($"Indices before [{string.Join("], [", indexReference.Skip(i - 3).Take(7))}]");
                    Console.WriteLine(
                        $"Sequence before: {string.Join(", ", BuildNewSequence(originalSequence, indexReference))}");
                }

                Move(indexReference, i, value);

                if (originalSequence.Count == 7)
                {
                    Console.WriteLine($"Indices after [{string.Join("], [", indexReference.Skip(i - 3).Take(7))}]");
                    Console.WriteLine(
                        $"Sequence after: {string.Join(", ", BuildNewSequence(originalSequence, indexReference))}");
                    Console.WriteLine();
                    Console.ReadKey();
                }
            }

            var newSequence = BuildNewSequence(originalSequence, indexReference);

            if (originalSequence.Count == 7)
                Console.WriteLine(string.Join(", ", newSequence));

            var firstIndex = newSequence.IndexOf(0);
            var i1 = (firstIndex + 1000) % newSequence.Count;
            var i2 = (firstIndex + 2000) % newSequence.Count;
            var i3 = (firstIndex + 3000) % newSequence.Count;
            Console.WriteLine(
                $"0: {firstIndex,-4}, i1: {i1,-4}, i2: {i2,-4}, i3: {i3,-4}");

            var v1 = newSequence[i1];
            var v2 = newSequence[i2];
            var v3 = newSequence[i3];

            return (v1 + v2 + v3).ToString();
        }

        protected override string Part2()
        {
            const int decryptionKey = 811589153;
            
            var originalSequence = _input.Select(x => x.ToLong() * decryptionKey).ToList();
            var indexReference = new List<int>();

            for (var i = 0; i < originalSequence.Count; ++i)
            {
                indexReference.Add(i);
            }

            for (var mixCount = 0; mixCount < 10; ++mixCount)
            {
                Console.WriteLine($"Mix: {mixCount}");
                if (originalSequence.Count == 7)
                {
                    Console.WriteLine(
                        $"Indices before [{string.Join("], [", indexReference)}]");
                    Console.WriteLine(
                        $"Sequence before: {string.Join(", ", BuildNewSequence(originalSequence, indexReference))}");
                }
                
                for (var i = 0; i < originalSequence.Count; ++i)
                {
                    if (originalSequence.Count == 7 || originalSequence.Count != 7 && i % 500 == 0)
                    {
                        Console.WriteLine($"{i} ...");
                    }

                    var value = originalSequence[i];


                    Move(indexReference, i, value);
                }
                
                if (originalSequence.Count == 7)
                {
                    Console.WriteLine($"Indices after [{string.Join("], [", indexReference)}]");
                    Console.WriteLine(
                        $"Sequence after: {string.Join(", ", BuildNewSequence(originalSequence, indexReference))}");
                    Console.WriteLine();
                }
            }

            var newSequence = BuildNewSequence(originalSequence, indexReference);

            if (originalSequence.Count == 7)
                Console.WriteLine(string.Join(", ", newSequence));

            var firstIndex = newSequence.IndexOf(0);
            var i1 = (firstIndex + 1000) % newSequence.Count;
            var i2 = (firstIndex + 2000) % newSequence.Count;
            var i3 = (firstIndex + 3000) % newSequence.Count;
            Console.WriteLine(
                $"0: {firstIndex,-4}, i1: {i1,-4}, i2: {i2,-4}, i3: {i3,-4}");

            var v1 = newSequence[i1];
            var v2 = newSequence[i2];
            var v3 = newSequence[i3];

            return (v1 + v2 + v3).ToString();
        }
    }
}