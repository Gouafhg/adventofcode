﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2022
{
    public class Day03 : Day
    {
        //Globals
        private readonly List<string> _input;

        List<int> GetPoints(List<char> charList)
        {
            return charList.Select(x =>
            {
                if (char.IsLower(x))
                {
                    return (int)x - (int)'a' + 1;
                }

                return (int)x - (int)'A' + 27;
            }).ToList();
        }

        //Constructor
        public Day03()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2022\day03.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test1 = @"vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw";

            _input = file.GetLines();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var doubles = new List<char>();
            foreach (var line in _input)
            {
                var comp1 = line.Take(line.Length / 2).ToList();
                var comp2 = line.Skip(line.Length / 2).ToList();

                doubles.AddRange(comp1.Where(x => comp2.Contains(x)).Distinct());
            }

            var points = GetPoints(doubles);
            return points.Sum().ToString();
        }


        protected override string Part2()
        {
            var badges = new List<char>();
            for (var i = 0; i < _input.Count; i += 3)
            {
                var sack1 = _input[i];
                var sack2 = _input[i + 1];
                var sack3 = _input[i + 2];

                var common = sack1.Where(x => sack2.Contains(x)).Where(x => sack3.Contains(x)).Distinct().ToList();
                Debug.Assert(common.Count == 1);
                badges.Add(common.Single());
            }

            var points = GetPoints(badges);
            return points.Sum().ToString();
        }
    }
}
