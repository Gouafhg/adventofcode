﻿using System;
using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2022
{
    public enum Direction
    {
        East = 0,
        North = 1,
        West = 2,
        South = 3,
    }

    public class Day22Map
    {
        public Dictionary<(int x, int y), Direction> movementHistory =
            new Dictionary<(int x, int y), Direction>();

        public void AddHistory((int x, int y) pos, Direction dir)
        {
            if (movementHistory.ContainsKey(pos))
            {
                movementHistory[pos] = dir;
                return;
            }

            movementHistory.Add(pos, dir);
        }

        public void Print()
        {
            Console.Clear();
            for (var y = 0; y < Grid.GetLength(1); ++y)
            {
                var row = string.Empty;
                for (var x = 0; x < Grid.GetLength(0); ++x)
                {
                    if (movementHistory.TryGetValue((x, y), out var dir))
                    {
                        row += dir switch
                        {
                            Direction.East => ">",
                            Direction.West => "<",
                            Direction.North => "^",
                            Direction.South => "v",
                            _ => throw new ArgumentOutOfRangeException(),
                        };
                        continue;
                    }

                    row += Grid[x, y];
                }

                Console.WriteLine(row);
            }
        }

        public char[,] Grid;

        public static Day22Map ReadFromLines(string[] lines)
        {
            var maxlength = lines.Max(x => x.Length);
            var result = new Day22Map
            {
                Grid = new char[maxlength, lines.Length],
            };

            for (var y = 0; y < lines.Length; ++y)
            {
                for (var x = 0; x < maxlength; ++x)
                {
                    if (x >= lines[y].Length)
                    {
                        result.Grid[x, y] = ' ';
                        continue;
                    }

                    result.Grid[x, y] = lines[y][x];
                }
            }

            return result;
        }

        public (int x, int y) GetStartPos()
        {
            for (var x = 0; x < Grid.GetLength(0); ++x)
            {
                if (Grid[x, 0] != ' ')
                {
                    return (x, 0);
                }
            }

            return (-1, -1);
        }

        private int FaceSize => Grid.GetLength(0) / 4;

        private int GetCubeFace((int x, int y) pos)
        {
            var fX = pos.x / FaceSize;
            var fY = pos.y / FaceSize;
            return fX switch
            {
                0 => 2,
                1 => 3,
                3 => 6,
                _ => fY switch
                {
                    0 => 1,
                    1 => 4,
                    _ => 5,
                },
            };
        }

        private (int cubeFace, Direction dir) GetNextFace(int cubeFace, Direction dir)
        {
            return cubeFace switch
            {
                1 when dir == Direction.North => (2, Direction.South),
                1 when dir == Direction.East => (6, Direction.West),
                1 when dir == Direction.West => (3, Direction.South),
                2 when dir == Direction.North => (1, Direction.South),
                2 when dir == Direction.South => (5, Direction.North),
                2 when dir == Direction.West => (6, Direction.North),
                3 when dir == Direction.North => (1, Direction.East),
                3 when dir == Direction.South => (5, Direction.East),
                4 when dir == Direction.East => (6, Direction.South),
                5 when dir == Direction.South => (2, Direction.North),
                5 when dir == Direction.West => (3, Direction.North),
                _ => dir switch
                {
                    Direction.North => (4, Direction.West),
                    Direction.South => (2, Direction.East),
                    _ => (1, Direction.West),
                },
            };
        }

        public bool CanMove((int x, int y) position, Direction direction)
        {
            var nextPos = GetNextPos(position, direction);
            return Grid[nextPos.x, nextPos.y] != '#';
        }

        private (int x, int y) GetOppositePosition((int x, int y) position, Direction direction)
        {
            int pX;
            int pY;
            switch (direction)
            {
                case Direction.East:
                    pX = position.x - 1;
                    pY = position.y;
                    break;
                case Direction.West:
                    pX = position.x + 1;
                    pY = position.y;
                    break;
                case Direction.South:
                    pX = position.x;
                    pY = position.y - 1;
                    break;
                case Direction.North:
                default:
                    pX = position.x;
                    pY = position.y + 1;
                    break;
            }

            if (pX < 0 || pX >= Grid.GetLength(0) || pY < 0 || pY >= Grid.GetLength(1) || Grid[pX, pY] == ' ')
            {
                if (Day.Part == 1)
                {
                    return direction switch
                    {
                        Direction.East => (pX + 1, pY),
                        Direction.West => (pX - 1, pY),
                        Direction.South => (pX, pY + 1),
                        _ => (pX, pY - 1),
                    };
                }
            }

            return GetOppositePosition((pX, pY), direction);
        }

        public (int x, int y) GetNextPos((int x, int y) position, Direction direction)
        {
            int pX;
            int pY;
            switch (direction)
            {
                case Direction.East:
                    pX = position.x + 1;
                    pY = position.y;
                    break;
                case Direction.West:
                    pX = position.x - 1;
                    pY = position.y;
                    break;
                case Direction.South:
                    pX = position.x;
                    pY = position.y + 1;
                    break;
                case Direction.North:
                default:
                    pX = position.x;
                    pY = position.y - 1;
                    break;
            }

            if (pX < 0 || pX >= Grid.GetLength(0) || pY < 0 || pY >= Grid.GetLength(1) || Grid[pX, pY] == ' ')
            {
                if (Day.Part == 1)
                {
                    return GetOppositePosition((pX, pY), direction);
                }

                var currentFace = GetCubeFace(position);
                var (nextFace, nextDir) = GetNextFace(currentFace, direction);
                if (nextFace == 1)
                {
                    if (nextDir == Direction.South)
                    {
                        return (FaceSize - position.x + 2 * FaceSize, 0);
                    }

                    if (nextDir == Direction.East)
                    {
                        return (2 * FaceSize, position.x - FaceSize);
                    }

                    if (nextDir == Direction.West)
                    {
                        return (3 * FaceSize - 1, 3 * FaceSize - position.y);
                    }
                }

                if (nextFace == 2)
                {
                    if (nextDir == Direction.South)
                    {
                        return (FaceSize - (3 * FaceSize - position.x), FaceSize);
                    }

                    if (nextDir == Direction.North)
                    {
                        return (FaceSize - (3 * FaceSize - position.x), 2 * FaceSize - 1);
                    }
                }

                if (nextFace == 3)
                {
                    if (nextDir == Direction.South)
                    {
                        return (FaceSize + position.y, FaceSize);
                    }

                    if (nextDir == Direction.North)
                    {
                        return (2 * FaceSize - (position.y - 2 * FaceSize) , 2 * FaceSize - 1);
                    }
                }

                if (nextFace == 4)
                {
                    return (3 * FaceSize - 1, 3 * FaceSize - (3 * FaceSize - position.y));
                }
            }

            return (pX, pY);
        }
    }

    public class Day22 : Day
    {
        //Globals
        private readonly List<string> _input;
        private readonly Day22Map _map;
        private List<string> _commands = new List<string>();

        private static Direction Turn(Direction current, string LR)
        {
            var currentInt = (int)current;
            var dir = LR == "L" ? 1 : -1;
            currentInt += dir + 4;
            currentInt %= 4;
            return (Direction)currentInt;
        }

        //Constructor
        public Day22()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2022\day22.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"2022\day22.txt");

            //Test case
            var test1 = @"        ...#
        .#..
        #...
        ....
...#.......#
........#...
..#....#....
..........#.
        ...#....
        .....#..
        .#......
        ......#.

10R5L5R10L4R5L5";

            _input = test1.GetLines();

            //Do Pre-stuff
            var mapPart = _input.Take(_input.Count - 1).ToArray();
            var commandPart = _input.Last();
            _map = Day22Map.ReadFromLines(mapPart);
            var currentCommand = string.Empty;
            foreach (var t in commandPart)
            {
                if (t == 'R' || t == 'L')
                {
                    _commands.Add(currentCommand);
                    _commands.Add(t.ToString());
                    currentCommand = string.Empty;
                }
                else
                {
                    currentCommand += t;
                }
            }

            _commands.Add(currentCommand);
        }

        protected override string Part1()
        {
            var pos = _map.GetStartPos();
            var dir = Direction.East;

            _map.AddHistory(pos, dir);

            // Console.WriteLine($"{pos.ToString()} - {dir.ToString()}");
            foreach (var command in _commands)
            {
                if (int.TryParse(command, out var steps))
                {
                    for (var i = 0; i < steps; ++i)
                    {
                        if (_map.CanMove(pos, dir))
                        {
                            pos = _map.GetNextPos(pos, dir);
                            _map.AddHistory(pos, dir);
                            // Console.WriteLine($"{pos.ToString()} - {dir.ToString()}");
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else
                {
                    dir = Turn(dir, command);
                    _map.AddHistory(pos, dir);
                    // Console.WriteLine($"{pos.ToString()} - {dir.ToString()}");
                }
            }

            Console.WriteLine(".");
            Console.ReadKey();
            _map.Print();

            var row = pos.y + 1;
            var column = pos.x + 1;
            var facing = dir switch
            {
                Direction.East => 0,
                Direction.South => 1,
                Direction.West => 2,
                _ => 3,
            };

            var result = 1000 * row + 4 * column + facing;
            return result.ToString();
        }

        protected override string Part2()
        {
            return "";
        }
    }
}