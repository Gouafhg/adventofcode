﻿using System;
using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2021
{
    public class Day20 : Day
    {
        //Globals
        private readonly List<string> _input;

        private readonly string _imageEnhancement;
        private readonly List<List<char>> _image;

        //Constructor
        public Day20()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\PATH\day.txt");
            var file = ToolsFile.ReadFile(DataPathHemma + @"2021\day20.txt");

            //Test case
            var test1 = @"..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

#..#.
#....
##..#
..#..
..###";

            _input = file.GetLines();

            //Do Pre-stuff
            _imageEnhancement = _input.First();
            _image = string.Join(Environment.NewLine, _input.Skip(1).ToList()).GetCharMap();
        }

        public void PrintMap(List<List<char>> charMap)
        {
            charMap.ForEach(x => Console.WriteLine(new string(x.ToArray())));
        }

        public List<List<char>> CloneCharmap(List<List<char>> charMap)
        {
            return string.Join(Environment.NewLine, charMap.Select(x => new string(x.ToArray()))).GetCharMap();
        }

        public void Enlarge(List<List<char>> charMap, char filler)
        {
            charMap.Insert(0, new List<char>());
            for (var i = 0; i < charMap[1].Count; ++i)
            {
                charMap.First().Add(filler);
            }
            charMap.Add(new List<char>());
            for (var i = 0; i < charMap[1].Count; ++i)
            {
                charMap.Last().Add(filler);
            }

            foreach (var line in charMap)
            {
                line.Insert(0, filler);
                line.Add(filler);
            }
        }

        public void Shrink(List<List<char>> charMap)
        {
            charMap.RemoveAt(0);
            charMap.RemoveAt(charMap.Count - 1);

            foreach (var line in charMap)
            {
                line.RemoveAt(0);
                line.RemoveAt(line.Count - 1);
            }
        }

        public List<List<char>> Tick(string imageEnhancement, List<List<char>> inputImage, char filler)
        {
            var output = CloneCharmap(inputImage);

            const int enl = 50;
            for (var i = 0; i < enl; ++i)
            {
                Enlarge(output, filler);
            }

            var bigInput = CloneCharmap(output);
            Enlarge(bigInput, filler);
            for (var y = 0; y < output.Count; ++y)
            {
                for (var x = 0; x < output[y].Count; ++x)
                {
                    var compString = string.Empty;
                    for (var yy = 0; yy < 3; ++yy)
                    {
                        compString += new string(bigInput[y + yy].Skip(x).Take(3).ToArray());
                    }

                    var vString = compString.Replace("#", "1").Replace(".", "0");
                    var v = (int)Tools.BinToDec(vString);

                    output[y][x] = imageEnhancement[v];
                }
            }

            for (var i = 0; i < enl - 1; ++i)
            {
                Shrink(output);
            }

            return output;
        }

        protected override string Part1()
        {
            Console.WriteLine(_imageEnhancement);
            Console.WriteLine();
            PrintMap(_image);
            Console.WriteLine();
            var testIm = CloneCharmap(_image);
            Shrink(testIm);
            PrintMap(testIm);
            Console.WriteLine();
            Console.WriteLine();

            var output = CloneCharmap(_image);

            PrintMap(output);
            Console.WriteLine();
            Console.WriteLine();

            for (var i = 0; i < 2; ++i)
            {
                output = Tick(_imageEnhancement, output, i % 2 == 0 ? '.' : '#');
                PrintMap(output);
                Console.WriteLine();
            }

            return output.SelectMany(x => x).Count(x => x == '#').ToString();
        }

        protected override string Part2()
        {
            var output = CloneCharmap(_image);

            for (var i = 0; i < 50; ++i)
            {
                output = Tick(_imageEnhancement, output, i % 2 == 0 ? '.' : '#');
            }

            return output.SelectMany(x => x).Count(x => x == '#').ToString();
        }
    }
}
