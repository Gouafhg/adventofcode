﻿using adventofcode.Utilities;
using System.Linq;
using adventofcode.Utilities._2021;

namespace adventofcode.AOC2021
{
    public class Day16 : Day
    {
        //Globals
        private readonly Bits _bits;

        //Constructor
        public Day16()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\PATH\day.txt");
            var file = ToolsFile.ReadFile(DataPathHemma + @"2021\day16.txt");

            //Test case
            var test0 = @"D2FE28";
            var test1 = @"38006F45291200";
            var test2 = @"EE00D40C823060";
            var test3 = @"8A004A801A8002F478";
            var test4 = @"620080001611562C8802118E34";
            var test5 = @"C0015000016115A2E0802F182340";
            var test6 = @"A0016C880162017C3686B18A3D4780";

            var test7 = @"C200B40A82";
            var test8 = @"04005AC33890";
            var test9 = @"C200B40A82";
            var test10 = @"C200B40A82";
            var test11 = @"C200B40A82";
            var test12 = @"C200B40A82";
            var test13 = @"C200B40A82";
            var test14 = @"C200B40A82";

            _bits = new Bits(file);
        }

        protected override string Part1()
        {
            _bits.Execute();
            return _bits.Versions.Sum(x => x).ToString();
        }

        protected override string Part2()
        {
            _bits.Execute();
            return string.Join(", ", _bits.Literals.Select(x => x.ToString()));
        }

    }
}
