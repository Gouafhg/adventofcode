﻿using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2021
{
    public class Board
    {
        public int[,] Grid = new int[5, 5];
        public static List<int> DrawnNumbers = new List<int>();

        public void Read(List<string> stringGrid)
        {
            for (var i = 0; i < 5; i++)
            {
                var row = stringGrid[i];
                var numbers = row.Split(' ').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                for (var j = 0; j < 5; j++)
                {
                    Grid[i, j] = int.Parse(numbers[j]);
                }
            }
        }

        public bool Win()
        {
            for (var i = 0; i < 5; i++)
            {
                if (DrawnNumbers.Contains(Grid[i, 0]))
                    if (DrawnNumbers.Contains(Grid[i, 1]))
                        if (DrawnNumbers.Contains(Grid[i, 2]))
                            if (DrawnNumbers.Contains(Grid[i, 3]))
                                if (DrawnNumbers.Contains(Grid[i, 4]))
                                    return true;

                if (DrawnNumbers.Contains(Grid[0, i]))
                    if (DrawnNumbers.Contains(Grid[1, i]))
                        if (DrawnNumbers.Contains(Grid[2, i]))
                            if (DrawnNumbers.Contains(Grid[3, i]))
                                if (DrawnNumbers.Contains(Grid[4, i]))
                                    return true;
            }

            return false;
        }

        public int Score()
        {
            var sum = 0;
            for (var i = 0; i < 5; i++)
            {
                for (var j = 0; j < 5; j++)
                {
                    if (!DrawnNumbers.Contains(Grid[i, j]))
                    {
                        sum += Grid[i, j];
                    }

                }
            }

            return sum * DrawnNumbers.Last();
        }

        public static void Draw(int number)
        {
            DrawnNumbers.Add(number);
        }
    }

    public class Day04 : Day
    {
        //Globals
        private readonly List<string> _input;

        private readonly List<int> _numbersToDraw;
        private readonly List<Board> _boards = new List<Board>();

        //Constructor
        public Day04()
        {
            //Init globals
            Part = 1;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\PATH\day.txt");
            var file = ToolsFile.ReadFile(DataPathHemma + @"2021\day04.txt");

            //Test case
            var test1 = @"7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7";

            var chosenInput = file;
            _input = chosenInput.Replace("\r", "\n").Split('\n').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

            //Do Pre-stuff

            _numbersToDraw = _input.First().Split(',').Select(int.Parse).ToList();
            for (var i = 1; i < _input.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(_input[i]))
                {
                    continue;
                }

                var board = new Board();
                var grid = _input.Skip(i).Take(5).ToList();
                board.Read(grid);
                _boards.Add(board);
                i += 4;
            }
        }

        protected override string Part1()
        {
            foreach (var numbertoDraw in _numbersToDraw)
            {
                Board.Draw(numbertoDraw);
                foreach (var board in _boards.Where(board => board.Win()))
                {
                    return board.Score().ToString();
                }
            }

            return "lol";
        }

        protected override string Part2()
        {
            var hasWin = new List<Board>();
            var left = new List<Board>(_boards);
            foreach (var numbertoDraw in _numbersToDraw)
            {
                Board.Draw(numbertoDraw);
                foreach (var board in left.Where(board => board.Win()))
                {
                    hasWin.Add(board);
                    if (_boards.Count == hasWin.Count)
                    {
                        return board.Score().ToString();
                    }
                }

                left.RemoveAll(hasWin.Contains);
            }

            return "lol";
        }
    }
}
