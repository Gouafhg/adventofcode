﻿using adventofcode.Utilities;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
// ReSharper disable EmptyEmbeddedStatement

namespace adventofcode.AOC2021
{

    public class Day18 : Day
    {
        //Globals
        private readonly List<Snailfish> _input;

        //Constructor
        public Day18()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\PATH\day.txt");
            var file = ToolsFile.ReadFile(DataPathHemma + @"2021\day18.txt");

            //Test case
            var test1 = @"[1,1]
[2,2]
[3,3]
[4,4]";

            var test2 = @"[1,1]
[2,2]
[3,3]
[4,4]
[5,5]";
            var test3 = @"[1,1]
[2,2]
[3,3]
[4,4]
[5,5]
[6,6]";
            var test4 = @"[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]
[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]
[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]
[7,[5,[[3,8],[1,4]]]]
[[2,[2,2]],[8,[8,1]]]
[2,9]
[1,[[[9,3],9],[[9,0],[0,7]]]]
[[[5,[7,4]],7],1]
[[[[4,2],2],6],[8,7]]";

            var test5 = @"[[[[4,3],4],4],[7,[[8,4],9]]]
[1,1]";

            var test6 = @"[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]";

            var chosenInput = file;

            var lines = chosenInput.GetLines();
            _input = lines.Select(x => new Snailfish(x)).ToList();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var r = _input[0];
            for (var i = 1; i < _input.Count; ++i)
            {
                r = Snailfish.Add(r, _input[i]);
            }

            return r.Magnitude().ToString();
        }

        protected override string Part2()
        {
            var maxMag = int.MinValue;
            foreach (var p in _input.Permute(2))
            {
                var c1 = p.First().Clone();
                var c2 = p.Last().Clone();
                var copy = Snailfish.Add(c1, c2);
                var mag = copy.Magnitude();
                if (mag > maxMag) maxMag = mag;
            }

            return maxMag.ToString();
        }
    }

    public class Snailfish
    {
        public Snailfish Parent;

        public Snailfish Left;
        public Snailfish Right;

        public int? Value;
        public bool HasValue => Value.HasValue;

        public override string ToString() => HasValue ? Value.Value.ToString() : $"[{Left},{Right}]";

        public int Magnitude()
        {
            if (HasValue)
            {
                return Value.Value;
            }

            return 3 * Left.Magnitude() + 2 * Right.Magnitude();
        }

        public Snailfish Clone()
        {
            return new Snailfish(ToString());
        }

        public Snailfish()
        {
            Left = null;
            Right = null;
            Value = null;
        }

        public Snailfish(int value)
        {
            Left = null;
            Right = null;
            Value = value;
        }

        public Snailfish(string line)
        {
            if (line[0] == '[')
            {
                var level = 1;
                var pos = 1;
                var l = string.Empty;
                var r = string.Empty;
                var commaFound = false;
                do
                {
                    if (line[pos] == '[')
                    {
                        level++;
                    }
                    else if (line[pos] == ']')
                    {
                        level--;
                    }
                    else if (level == 1 && line[pos] == ',')
                    {
                        commaFound = true;
                    }

                    if (!commaFound)
                    {
                        l += line[pos];
                    }
                    else
                    {
                        r += line[pos];
                    }

                    pos++;
                } while (level > 0);

                r = r.Remove(0, 1);
                r = r.Remove(r.Length - 1, 1);

                Left = new Snailfish(l) { Parent = this };
                Right = new Snailfish(r) { Parent = this };

                Debug.Assert(ToString() == line);
            }
            else
            {
                if (int.TryParse(line, out var v))
                {
                    Value = v;
                }
                else
                {
                    var l = line.Split(',')[0];
                    var r = line.Split(',')[1];
                    Left = new Snailfish(int.Parse(l)) { Parent = this };
                    Right = new Snailfish(int.Parse(r)) { Parent = this };
                }
            }
        }

        public static Snailfish Add(Snailfish a, Snailfish b)
        {
            var r = new Snailfish { Left = a, Right = b };
            a.Parent = r;
            b.Parent = r;

            while (Reduce(r)) ;

            return r;
        }

        public static bool Reduce(Snailfish a)
        {
            if (ReduceExplode(a, 0))
            {
                return true;
            }

            if (ReduceSplit(a))
            {
                return true;
            }

            return false;
        }

        public static bool ReduceExplode(Snailfish a, int d)
        {
            if (a.HasValue)
            {
                return false;
            }

            if (d >= 4 && a.Left.HasValue)
            {
                var toAddLeft = a.Parent;
                while (toAddLeft != null && toAddLeft.ContainsLeft(a))
                {
                    toAddLeft = toAddLeft.Parent;
                }
                if (toAddLeft != null)
                {
                    if (toAddLeft.Left.HasValue)
                    {
                        toAddLeft.Left.Value += a.Left.Value;
                    }
                    else
                    {
                        var r = toAddLeft.Left;
                        while (!r.HasValue)
                        {
                            r = r.Right;
                        }

                        r.Value += a.Left.Value;
                    }
                }

                var toAddRight = a.Parent;
                while (toAddRight != null && toAddRight.ContainsRight(a))
                {
                    toAddRight = toAddRight.Parent;
                }
                if (toAddRight != null)
                {
                    if (toAddRight.Right.HasValue)
                    {
                        toAddRight.Right.Value += a.Right.Value;
                    }
                    else
                    {
                        var l = toAddRight.Right;
                        while (!l.HasValue)
                        {
                            l = l.Left;
                        }

                        l.Value += a.Right.Value;
                    }
                }

                a.Value = 0;
                a.Left = null;
                a.Right = null;
                return true;
            }

            var v = ReduceExplode(a.Left, d + 1);
            if (!v)
            {
                v = ReduceExplode(a.Right, d + 1);
            }

            return v;
        }

        private bool ContainsLeft(Snailfish a)
        {
            if (this == a)
            {
                return true;
            }

            var l = Left;
            if (l == a || (l != null && l.ContainsLeft(a)))
            {
                return true;
            }

            return false;
        }

        private bool ContainsRight(Snailfish a)
        {
            if (this == a)
            {
                return true;
            }

            var r = Right;
            if (r == a || (r != null && r.ContainsRight(a)))
            {
                return true;
            }

            return false;
        }

        public static bool ReduceSplit(Snailfish a)
        {
            if (a.HasValue)
            {
                if (a.Value >= 10)
                {
                    a.Split();
                    return true;
                }

                return false;
            }

            var v = ReduceSplit(a.Left);
            if (!v)
            {
                v = ReduceSplit(a.Right);
            }

            return v;
        }

        public void Split()
        {
            var r = Value.Value % 2;
            Left = new Snailfish(Value.Value / 2) { Parent = this };
            Right = new Snailfish(Value.Value / 2 + r) { Parent = this };
            Value = null;
        }
    }
}
