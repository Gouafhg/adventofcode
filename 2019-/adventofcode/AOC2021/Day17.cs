﻿using System;
using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2021
{
    public class Day17 : Day
    {
        //Globals
        private string _input;

        private (long x, long y) _min, _max;

        //Constructor
        public Day17()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\PATH\day.txt");
            var file = ToolsFile.ReadFile(DataPathHemma + @"2021\day17.txt");

            //Test case
            var test1 = @"target area: x=20..30, y=-10..-5";

            _input = file;

            var xMin = new string(_input.Split('x')[1].Skip(1).TakeWhile(x => x != '.').ToArray());
            var xMax = new string(_input.Split('x')[1].Skip(1 + xMin.Length).SkipWhile(x => x == '.').TakeWhile(x => x != ',').ToArray());
            var yMin = new string(_input.Split('y')[1].Skip(1).TakeWhile(x => x != '.').ToArray());
            var yMax = new string(_input.Split('y')[1].Skip(1 + yMin.Length).SkipWhile(x => x == '.').TakeWhile(x => x != ',').ToArray());

            _min = (long.Parse(xMin), long.Parse(yMin));
            _max = (long.Parse(xMax), long.Parse(yMax));

            if (_min.x > _max.x)
            {
                _min = (-_min.x, _min.y);
                _max = (-_max.x, _max.y);
            }
            //Do Pre-stuff
        }

        public void Tick(ref (long x, long y) pos, ref (long x, long y) vel)
        {
            pos = (pos.x + vel.x, pos.y + vel.y);
            if (vel.x < 0)
            {
                vel = (vel.x + 1, vel.y - 1);
            }
            else if (vel.x == 0)
            {
                vel = (vel.x, vel.y - 1);
            }
            else
            {
                vel = (vel.x - 1, vel.y - 1);
            }
        }

        protected override string Part1()
        {
            var maxStartVelX = Math.Abs(_max.x) > Math.Abs(_min.x) ? _max.x : _min.x;

            var maxY = long.MinValue;

            for (var startVelY = 10000; startVelY >= 0; startVelY--)
            {
                long internalMaxY = 0;

                for (var startVelX = maxStartVelX; startVelX != 0; startVelX--)
                {
                    (long x, long y) pos = (0, 0);
                    (long x, long y) vel = (startVelX, startVelY);
                    var b = false;
                    while (!b)
                    {
                        Tick(ref pos, ref vel);
                        if (pos.y > internalMaxY)
                        {
                            internalMaxY = pos.y;
                        }

                        if (pos.x >= _min.x && pos.x <= _max.x && pos.y >= _min.y && pos.y <= _max.y)
                        {
                            if (internalMaxY > maxY)
                            {
                                maxY = internalMaxY;
                            }

                            b = true;
                        }
                        else if (vel.x == 0 && pos.x < _min.x || pos.x > _max.x)
                        {
                            b = true;
                        }
                        else if (pos.x > _max.x)
                        {
                            b = true;
                        }
                        else if (pos.y < _min.y)
                        {
                            b = true;
                        }
                    }
                }
            }

            return maxY.ToString();
        }

        protected override string Part2()
        {
            var maxStartVelX = Math.Abs(_max.x) > Math.Abs(_min.x) ? _max.x : _min.x;
            //var maxStartVelY = Math.Abs(_max.y) > Math.Abs(_min.x) ? _max.y : _min.y;

            //var maxY = long.MinValue;
            //var startStartVelY = _min.y;

            var contains = new List<(long x, long y)>();
            for (var startVelY = 10000; startVelY >= -10000; startVelY--)
            {
                long internalMaxY = 0;

                for (var startVelX = maxStartVelX; startVelX != 0; startVelX--)
                {
                    (long x, long y) pos = (0, 0);
                    (long x, long y) vel = (startVelX, startVelY);
                    var b = false;
                    while (!b)
                    {
                        Tick(ref pos, ref vel);
                        if (pos.y > internalMaxY)
                        {
                            internalMaxY = pos.y;
                        }

                        if (pos.x >= _min.x && pos.x <= _max.x && pos.y >= _min.y && pos.y <= _max.y)
                        {
                            contains.Add((startVelX, startVelY));
                            b = true;
                        }
                        else if (vel.x == 0 && pos.x < _min.x || pos.x > _max.x)
                        {
                            b = true;
                        }
                        else if (pos.x > _max.x)
                        {
                            b = true;
                        }
                        else if (pos.y < _min.y)
                        {
                            b = true;
                        }
                    }
                }
            }

            return contains.Count.ToString();
        }
    }
}
