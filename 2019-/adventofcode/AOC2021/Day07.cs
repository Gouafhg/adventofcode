﻿using System;
using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2021
{
    public class Day07 : Day
    {
        //Globals
        private readonly List<int> _input;

        //Constructor
        public Day07()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\PATH\day.txt");
            var file = ToolsFile.ReadFile(DataPath + @"2021\day07.txt");

            //Test case
            var test1 = @"16,1,2,0,4,2,7,1,2,14";

            var chosenInput = file;
            _input = chosenInput.Replace("\r", "\n").Split('\n').First(x => !string.IsNullOrWhiteSpace(x))
                .Split(',').Select(int.Parse).ToList();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var min = _input.Min();
            var max = _input.Max();

            var least = int.MaxValue;
            var pos = -1;
            for (var i = min; i <= max; i++)
            {
                var sum = _input.Sum(x => Math.Abs(x - i));
                if (sum < least)
                {
                    pos = i;
                    least = sum;
                }
            }

            return pos + ", " + least;
        }

        protected override string Part2()
        {
            var min = _input.Min();
            var max = _input.Max();

            var least = int.MaxValue;
            var pos = -1;
            for (var i = min; i <= max; i++)
            {
                var sum = _input.Sum(x => Asum(Math.Abs(x - i)));
                if (sum < least)
                {
                    pos = i;
                    least = sum;
                }
            }

            return pos + ", " + least;
        }

        public int Asum(int n)
        {
            return n * (1 + n) / 2;
        }
    }
}
