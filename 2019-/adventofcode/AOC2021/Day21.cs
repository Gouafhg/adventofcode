﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using adventofcode.Utilities;

namespace adventofcode.AOC2021
{
    public class Day21 : Day
    {
        //Globals
        private readonly List<string> _input;

        private Day21Game _game;
        //Constructor
        public Day21()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\PATH\day.txt");
            var file = ToolsFile.ReadFile(DataPathHemma + @"2021\day21.txt");

            //Test case
            var test1 = @"Player 1 starting position: 4
Player 2 starting position: 8";

            _input = file.GetLines();

            //Do Pre-stuff
            _game = new Day21Game();
            var (p1Pos, p2Pos) = (int.Parse(_input.First().Last().ToString()), int.Parse(_input.Last().Last().ToString()));
            _game.Reset(p1Pos, p2Pos);
        }

        protected override string Part1()
        {
            while (!_game.SimpleEnded) _game.SimpleTick();

            var loserScore = _game.Player1.Score == 1000 ? _game.Player2.Score : _game.Player1.Score;

            return (loserScore * _game.DieRolled).ToString();
        }

        protected override string Part2()
        {
            while (!_game.Ended()) _game.DiracTick();

            var winnerScore = _game.P1W > _game.P2W ? _game.P1W : _game.P2W;
            return winnerScore.ToString();
        }
    }

    public class Day21Player
    {
        public int Score;
        public int Position;

        public override string ToString()
        {
            return $"{Score} {Position}";
        }

        public int Tick(int dieValue)
        {
            Position += dieValue;
            Position--;
            Position %= 10;
            Position++;

            Score += Position;
            return Score;
        }
    }

    public class Day21Game
    {
        public Day21Player Player1;
        public Day21Player Player2;

        public int DieValue;
        public int DieRolled;

        public Day21Player Winner;
        public Day21Player Loser;

        public BigInteger P1W, P2W;

        public bool SimpleEnded => Winner != null;

        public readonly Dictionary<(int p1Score, int p1Pos, int p2Score, int p2Pos), long> SubGames =
            new Dictionary<(int p1Score, int p1Pos, int p2Score, int p2Pos), long>();

        private readonly Dictionary<int, int> _diceResults;

        public Day21Game()
        {
            Player1 = new Day21Player();
            Player2 = new Day21Player();
            DieValue = 1;
            DieRolled = 0;

            Winner = null;
            Loser = null;

            P1W = 0;
            P2W = 0;

            for (var p1Pos = 1; p1Pos <= 10; p1Pos++)
            {
                for (var p2Pos = 1; p2Pos <= 10; p2Pos++)
                {
                    for (var p1Score = 0; p1Score <= 20; p1Score++)
                    {
                        for (var p2Score = 0; p2Score <= 20; p2Score++)
                        {
                            SubGames.Add((p1Score, p1Pos, p2Score, p2Pos), 0);
                        }
                    }
                }
            }

            var diceSums = new List<int>();
            for (var first = 1; first <= 3; ++first)
            {
                for (var second = 1; second <= 3; ++second)
                {
                    for (var third = 1; third <= 3; ++third)
                    {
                        diceSums.Add(first + second + third);
                    }
                }
            }
            _diceResults = diceSums.Distinct()
                .ToDictionary(x => x, x => diceSums.Count(y => y == x));
        }

        public void Reset(int player1Pos, int player2Pos)
        {
            Player1.Score = 0;
            Player2.Score = 0;
            DieValue = 0;

            Player1.Position = player1Pos;
            Player2.Position = player2Pos;

            SubGames[(0, player1Pos, 0, player2Pos)]++;
        }

        public int RollDie()
        {
            DieRolled++;

            DieValue++;

            DieValue--;
            DieValue %= 100;
            DieValue++;

            return DieValue;
        }

        public int RollThree()
        {
            var rollValue = 0;
            for (var i = 0; i < 3; ++i)
            {
                rollValue += RollDie();
            }

            return rollValue;
        }

        public void SimpleTick()
        {
            if (Player1.Tick(RollThree()) == 1000)
            {
                Winner = Player1;
                Loser = Player2;
                return;
            }

            if (Player2.Tick(RollThree()) == 1000)
            {
                Winner = Player2;
                Loser = Player1;
            }
        }

        public void DiracTick()
        {
            for (var p1Pos = 1; p1Pos <= 10; p1Pos++)
            {
                for (var p2Pos = 1; p2Pos <= 10; p2Pos++)
                {
                    for (var p1Score = 0; p1Score <= 20; p1Score++)
                    {
                        for (var p2Score = 0; p2Score <= 20; p2Score++)
                        {
                            var gameCount = SubGames[(p1Score, p1Pos, p2Score, p2Pos)];

                            var key = (p1Score, p1Pos, p2Score, p2Pos);

                            SubGames[key] = 0;

                            foreach (var dKey1 in _diceResults.Keys)
                            {
                                var newP1Pos = p1Pos;
                                newP1Pos += dKey1;
                                newP1Pos--;
                                newP1Pos %= 10;
                                newP1Pos++;
                                var newP1Score = Math.Min(21, p1Score + newP1Pos);

                                if (newP1Score < 21)
                                {
                                    foreach (var dKey2 in _diceResults.Keys)
                                    {
                                        var newP2Pos = p2Pos;
                                        newP2Pos += dKey2;
                                        newP2Pos--;
                                        newP2Pos %= 10;
                                        newP2Pos++;
                                        var newP2Score = Math.Min(21, p2Score + newP2Pos);
                                        if (newP2Score < 21)
                                        {
                                            var v = SubGames[(newP1Score, newP1Pos, newP2Score, newP2Pos)];
                                            SubGames[(newP1Score, newP1Pos, newP2Score, newP2Pos)] = checked(v + gameCount * _diceResults[dKey1] * _diceResults[dKey2]);
                                        }
                                        else
                                        {
                                            P2W += gameCount * _diceResults[dKey1] * _diceResults[dKey2];
                                        }
                                    }
                                }
                                else
                                {
                                    P1W += gameCount * _diceResults[dKey1];
                                }
                            }
                        }
                    }
                }
            }
        }

        public bool Ended()
        {
            return !SubGames.Values.Any(x => x > 0);
        }
    }
}
