﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using adventofcode.Utilities;
using adventofcode.Utilities.EqualityComparers;

namespace adventofcode.AOC2021
{
    public class Rule
    {
        public List<int> Key;
        public List<int> Result;

        public override string ToString() => $"({string.Join(", ", Key.Select(x => x.ToString()))}) -> ({string.Join(", ", Result.Select(x => x.ToString()))})";

        public bool Match(List<int> key)
        {
            if (key.Count != Key.Count)
            {
                return false;
            }

            return !Key.Where((t, i) => t != key[i]).Any();
        }

        public bool Match(int a, int b) => Key[0] == a && Key[1] == b;
    }

    public class Day14 : Day
    {
        private const int RuleLevels = 20;

        private readonly List<int> _startValues;
        private readonly List<Rule> _rules;

        private readonly Dictionary<(List<int>, int), (List<int> result, Dictionary<int, BigInteger>)> _ruleDic;

        private readonly List<int> _values;

        private readonly List<string> _input;

        //Constructor
        public Day14()
        {
            //Init globals
            Part = 1;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\PATH\day.txt");
            var file = @"OOBFPNOPBHKCCVHOBCSO

NS -> H
NN -> P
FF -> O
HF -> C
KN -> V
PO -> B
PS -> B
FB -> N
ON -> F
OK -> F
OO -> K
KS -> F
FN -> F
KC -> H
NC -> N
NB -> C
KH -> S
SV -> B
BC -> S
KB -> B
SC -> S
KP -> H
FS -> K
NK -> K
OC -> H
NH -> C
PH -> F
OS -> V
BB -> C
CC -> F
CF -> H
CP -> V
VB -> N
VC -> F
PK -> V
NV -> N
FO -> S
CK -> O
BH -> K
PN -> B
PP -> S
NF -> B
SF -> K
VF -> H
HS -> F
NP -> F
SH -> V
SK -> K
PC -> V
BO -> H
HN -> P
BK -> O
BP -> S
OP -> N
SP -> N
KK -> C
HB -> H
OF -> C
VH -> C
HO -> N
FK -> V
NO -> H
KF -> S
KO -> V
PF -> K
HV -> C
SO -> F
SS -> F
VN -> K
HH -> B
OB -> S
CH -> B
FH -> B
CO -> V
HK -> F
VK -> K
CN -> V
SB -> K
PV -> O
PB -> F
VV -> P
CS -> N
CB -> C
BS -> F
HC -> B
SN -> P
VP -> P
OV -> P
BV -> P
FC -> N
KV -> S
CV -> F
BN -> S
BF -> C
OH -> F
VO -> B
FP -> S
FV -> V
VS -> N
HP -> B";

            //Test case
            var test1 = @"NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C";

            _input = file.GetLines();

            var charToIntMap = string.Join(string.Empty, _input).Distinct().ToDictionary(x => x, _ => 0);
            charToIntMap.Remove(' ');
            charToIntMap.Remove('-');
            charToIntMap.Remove('>');

            var valueChars = charToIntMap.Keys.OrderBy(x => x).ToList();
            foreach (var key in valueChars)
            {
                charToIntMap[key] = valueChars.IndexOf(key);
            }

            _startValues = _input.First().Select(x => charToIntMap[x]).ToList();

            _values = charToIntMap.Values.OrderBy(x => x).ToList();

            //Do Pre-stuff
            _rules = _input
                .Skip(1)
                .Select(x => (x.Split(' ').First(), x.Split(' ').Last()))
                .Select(x => new Rule
                {
                    Key = x.Item1.Select(y => charToIntMap[y]).ToList(),
                    Result = x.Item2.Select(y => charToIntMap[y]).ToList(),
                }).ToList();

            if (Part == 2)
            {
                _ruleDic =
                    new Dictionary<(List<int>, int), (List<int> result, Dictionary<int, BigInteger>)>(
                        new ListIntTupleComparer<int>());
                foreach (var rule in _rules)
                {
                    for (var i = 1; i <= RuleLevels; ++i)
                    {
                        var list = ExpandRule(rule, i).ToList();
                        list.RemoveAt(list.Count - 1);

                        var valDic = _values.ToDictionary(x => x, _ => BigInteger.Zero);

                        foreach (var v in list)
                        {
                            valDic[v]++;
                        }

                        _ruleDic.Add((rule.Key, i), (list, valDic));
                    }
                }
            }
        }

        #region main

        protected override string Part1()
        {
            var current = new List<int>(_startValues);
            for (var i = 0; i < 10; ++i)
            {
                var newList = new List<int> { current[0] };
                for (var j = 0; j < current.Count - 1; ++j)
                {
                    var p = current.Skip(j).Take(2).ToList();
                    var rule = _rules.FirstOrDefault(x => x.Match(p));

                    if (rule != null)
                    {
                        newList.AddRange(rule.Result);
                        newList.Add(p[1]);
                    }
                    else
                    {
                        newList.AddRange(p);
                    }
                }

                current = newList;
            }

            //if (_startValues.AsString() == "3310")
            //{
            //    var debugAssertsAsInts = BuildDebugAsserts(_startValues, 10);

            //    if (debugAssertsAsInts.ContainsKey(10))
            //    {
            //        var expexted = debugAssertsAsInts[10];
            //        Debug.Assert(current.SequenceEqual(expexted));
            //    }
            //}

            var elements = current.Select(x => x).Distinct().ToList();
            var most = elements.OrderByDescending(x => current.Count(y => y == x)).First();
            var least = elements.OrderBy(x => current.Count(y => y == x)).First();

            var mostCount = current.Count(x => x == most);
            var leastCount = current.Count(x => x == least);
            return (mostCount - leastCount).ToString();
        }

        protected override string Part2()
        {
            var n = new LinkListNode<int>(_startValues.First());
            n.AddAfter(_startValues.Skip(1));

            n = ExpandList(n, RuleLevels);

            var resultDic = _values.ToDictionary(x => x, _ => BigInteger.Zero);
            while (n.Next != null)
            {
                var r = _ruleDic[(new List<int> { n.Value, n.Next.Value }, RuleLevels)].Item2;
                foreach (var c in _values)
                {
                    resultDic[c] += r[c];
                }

                n = n.Next;
            }
            resultDic[n.Value]++;

            //if (_startValues.AsString() == "3310")
            //{
            //    var da = BuildDebugAsserts(_startValues, RuleLevels * 2);
            //    var n2 = new LinkListNode<int>(_startValues.First());
            //    n2.AddAfter(_startValues.Skip(1));
            //    n2 = ExpandList(n2, RuleLevels * 2);

            //    Debug.Assert(n2.GetValues().ToList().SequenceEqual(da[RuleLevels * 2]));

            //    var resultDic2 = _values.ToDictionary(x => x, _ => BigInteger.Zero);
            //    while (n2.Next != null)
            //    {
            //        resultDic2[n2.Value]++;

            //        n2 = n2.Next;
            //    }
            //    resultDic2[n2.Value]++;

            //    foreach (var v in resultDic2.Keys)
            //    {
            //        Debug.Assert(resultDic[v] == resultDic2[v]);
            //    }
            //}

            resultDic = resultDic.Keys.Where(x => resultDic[x] != 0).ToDictionary(x => x, x => resultDic[x]);

            var most = resultDic.Keys.OrderByDescending(x => resultDic[x]).First();
            var least = resultDic.Keys.OrderBy(x => resultDic[x]).First();

            var mostCount = resultDic[most];
            var leastCount = resultDic[least];

            var diff = mostCount - leastCount;

            return diff.ToString();
        }
        #endregion

        Dictionary<int, List<int>> BuildDebugAsserts(List<int> startStringAsInts, int toLevel)
        {
            var result = new Dictionary<int, List<int>>();
            var current = startStringAsInts;
            for (var i = 0; i < toLevel; ++i)
            {
                var newList = new[] { current[0] }.ToList();
                for (var j = 0; j < current.Count - 1; ++j)
                {
                    var p = current.Skip(j).Take(2).ToList();
                    var rule = _rules.FirstOrDefault(x => x.Match(p));

                    if (rule != null)
                    {
                        newList.AddRange(rule.Result);
                        newList.Add(p[1]);
                    }
                    else
                    {
                        newList.AddRange(p);
                    }
                }

                current = newList;
                result.Add(i + 1, newList);
            }

            return result;
        }

        #region expand_tools
        public IEnumerable<int> ExpandRule(Rule rule, int levels)
        {
            var startList = rule.Key;

            var n = new LinkListNode<int>(startList.First());
            foreach (var v in startList.Skip(1))
            {
                n.AddAfter(v);
                n = n.Next;
            }

            n = n.GetFirst();
            while (n.Next != null)
            {
                for (var i = n.Level; i < levels; ++i)
                {
                    var r = _rules.First(x => x.Match(n.Value, n.Next.Value));
                    n.AddAfter(r.Result[0]);
                    n.Next.Level = i + 1;
                }

                do
                {
                    n = n.Next;
                } while (n.Level == levels);
            }

            return n.GetValues();
        }

        public LinkListNode<int> ExpandList(LinkListNode<int> firstNode, int levels, Dictionary<(List<int>, int), (List<int>, Dictionary<int, BigInteger>)> ruleDic = null)
        {
            ruleDic ??= _ruleDic;

            if (levels > RuleLevels)
            {
                firstNode = ExpandList(firstNode, RuleLevels);
                levels -= RuleLevels;
                return ExpandList(firstNode, levels);
            }

            var n = firstNode.GetFirst();
            while (n.Next != null)
            {
                var comb = new List<int> { n.Value, n.Next.Value };

                var nNext = n.Next;
                if (ruleDic.ContainsKey((comb, levels)))
                {
                    var newComb = ruleDic[(comb, levels)].Item1;

                    n.AddAfter(newComb.Skip(1));
                }

                n = nNext;
            }

            return n.GetFirst();
        }
        #endregion
    }
}
