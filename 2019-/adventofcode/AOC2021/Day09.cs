﻿using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2021
{
    public class Day09 : Day
    {
        //Globals
        private readonly List<List<int>> _input;

        //Constructor
        public Day09()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\PATH\day.txt");
            var file = ToolsFile.ReadFile(DataPathHemma + @"2021\day09.txt");

            //Test case
            var test1 = @"2199943210
3987894921
9856789892
8767896789
9899965678";

            _input = file.GetIntMap();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var lows = new List<int>();
            for (var y = 0; y < _input.Count; ++y)
            {
                for (var x = 0; x < _input[y].Count; ++x)
                {
                    var v = _input[y][x];
                    var n = new List<int>();
                    if (x > 0) n.Add(_input[y][x - 1]);
                    if (y > 0) n.Add(_input[y - 1][x]);
                    if (x < _input[y].Count - 1) n.Add(_input[y][x + 1]);
                    if (y < _input.Count - 1) n.Add(_input[y + 1][x]);

                    if (v < n.Min())
                    {
                        lows.Add(v);
                    }
                }
            }

            return lows.Sum(x => x + 1).ToString();
        }

        protected override string Part2()
        {
            var lows = new List<(int x, int y)>();
            for (var y = 0; y < _input.Count; ++y)
            {
                for (var x = 0; x < _input[y].Count; ++x)
                {
                    var v = _input[y][x];
                    var n = new List<int>();
                    if (x > 0) n.Add(_input[y][x - 1]);
                    if (y > 0) n.Add(_input[y - 1][x]);
                    if (x < _input[y].Count - 1) n.Add(_input[y][x + 1]);
                    if (y < _input.Count - 1) n.Add(_input[y + 1][x]);

                    if (v < n.Min())
                    {
                        lows.Add((x, y));
                    }
                }
            }

            var basins = new List<List<(int x, int y)>>();
            foreach (var low in lows)
            {
                var basin = new List<(int x, int y)> { low };
                foreach (var n in _input.GetNeighbours(low))
                {
                    CheckB(_input, basin, n);
                }
                basins.Add(basin);
            }

            var biggestBasins = basins.Select(x => x.Count).OrderByDescending(x => x).Take(3);
            var result = biggestBasins.Aggregate(1, (current, b) => current * b);

            return result.ToString();
        }

        private static void CheckB(List<List<int>> map, List<(int x, int y)> basin, (int x, int y) pos)
        {
            var posValue = map.GetValue(pos);
            if (basin.Contains(pos) || posValue == 9)
            {
                return;
            }
            basin.Add(pos);

            var ns = map.GetNeighbours(pos);
            foreach (var n in ns)
            {
                CheckB(map, basin, n);
            }
        }
    }
}
