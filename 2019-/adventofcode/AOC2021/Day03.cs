﻿using System;
using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2021
{
    internal class Day03 : Day
    {
        //Globals
        private readonly List<string> _input;

        //Constructor
        public Day03()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\PATH\day.txt");
            var file = ToolsFile.ReadFile(DataPath + @"2021\day03.txt");

            //Test case
            var test1 = @"00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010";

            var chosenInput = file;
            _input = chosenInput.Replace("\r", "\n").Split('\n').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var gammaString = string.Empty;
            var epsilonString = string.Empty;

            for (var i = 0; i < _input[0].Length; i++)
            {
                var ones = 0;
                var zeros = 0;
                foreach (var row in _input)
                {
                    var v = int.Parse(row[i].ToString());
                    if (v == 0)
                    {
                        zeros++;
                    }
                    else
                    {
                        ones++;
                    }
                }

                gammaString += ones > zeros ? "1" : "0";
                epsilonString += ones > zeros ? "0" : "1";
            }

            var gamma = Convert.ToInt32(gammaString, 2);
            var epsilon = Convert.ToInt32(epsilonString, 2);
            return (gamma * epsilon).ToString();
        }

        protected override string Part2()
        {
            var oxygenString = string.Empty;
            var co2String = string.Empty;

            var rowsLeft = new List<string>(_input);
            for (var i = 0; i < _input[0].Length; i++)
            {
                rowsLeft = GetRowsLeft(rowsLeft, i, 1);
                if (rowsLeft.Count == 1)
                {
                    oxygenString = rowsLeft.First();
                }
            }

            rowsLeft = new List<string>(_input);
            for (var i = 0; i < _input[0].Length; i++)
            {
                rowsLeft = GetRowsLeft(rowsLeft, i, 0);
                if (rowsLeft.Count == 1)
                {
                    co2String = rowsLeft.First();
                }
            }

            var oxygen = Convert.ToInt32(oxygenString, 2);
            var co2 = Convert.ToInt32(co2String, 2);
            return (oxygen * co2).ToString();
        }

        public List<string> GetRowsLeft(List<string> rows, int position, int toKeep)
        {
            var ones = 0;
            var zeros = 0;
            foreach (var row in rows)
            {
                var v = int.Parse(row[position].ToString());
                if (v == 0)
                {
                    zeros++;
                }
                else
                {
                    ones++;
                }
            }

            if (toKeep == 0)
            {
                if (ones > zeros || zeros == ones)
                {
                    return rows.Where(x => x[position] == '0').ToList();
                }

                return rows.Where(x => x[position] == '1').ToList();
            }

            if (zeros > ones)
            {
                return rows.Where(x => x[position] == '0').ToList();
            }

            return rows.Where(x => x[position] == '1').ToList();
        }
    }
}
