﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using adventofcode.Utilities;

namespace adventofcode.AOC2021
{
    public class Day19 : Day
    {
        //Globals
        private readonly List<string> _input;

        private readonly List<Scanner> _scanners;
        //Constructor
        public Day19()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\PATH\day.txt");
            var file = ToolsFile.ReadFile(DataPathHemma + @"2021\day19.txt");

            var test0 = @"--- scanner 0 ---
0,2,0
4,1,0
3,3,0

--- scanner 1 ---
-1,-1,0
-5,0,0
-2,1,0";

            //Test case
            var test1 = @"--- scanner 0 ---
404,-588,-901
528,-643,409
-838,591,734
390,-675,-793
-537,-823,-458
-485,-357,347
-345,-311,381
-661,-816,-575
-876,649,763
-618,-824,-621
553,345,-567
474,580,667
-447,-329,318
-584,868,-557
544,-627,-890
564,392,-477
455,729,728
-892,524,684
-689,845,-530
423,-701,434
7,-33,-71
630,319,-379
443,580,662
-789,900,-551
459,-707,401

--- scanner 1 ---
686,422,578
605,423,415
515,917,-361
-336,658,858
95,138,22
-476,619,847
-340,-569,-846
567,-361,727
-460,603,-452
669,-402,600
729,430,532
-500,-761,534
-322,571,750
-466,-666,-811
-429,-592,574
-355,545,-477
703,-491,-529
-328,-685,520
413,935,-424
-391,539,-444
586,-435,557
-364,-763,-893
807,-499,-711
755,-354,-619
553,889,-390

--- scanner 2 ---
649,640,665
682,-795,504
-784,533,-524
-644,584,-595
-588,-843,648
-30,6,44
-674,560,763
500,723,-460
609,671,-379
-555,-800,653
-675,-892,-343
697,-426,-610
578,704,681
493,664,-388
-671,-858,530
-667,343,800
571,-461,-707
-138,-166,112
-889,563,-600
646,-828,498
640,759,510
-630,509,768
-681,-892,-333
673,-379,-804
-742,-814,-386
577,-820,562

--- scanner 3 ---
-589,542,597
605,-692,669
-500,565,-823
-660,373,557
-458,-679,-417
-488,449,543
-626,468,-788
338,-750,-386
528,-832,-391
562,-778,733
-938,-730,414
543,643,-506
-524,371,-870
407,773,750
-104,29,83
378,-903,-323
-778,-728,485
426,699,580
-438,-605,-362
-469,-447,-387
509,732,623
647,635,-688
-868,-804,481
614,-800,639
595,780,-596

--- scanner 4 ---
727,592,562
-293,-554,779
441,611,-461
-714,465,-776
-743,427,-804
-660,-479,-426
832,-632,460
927,-485,-438
408,393,-506
466,436,-512
110,16,151
-258,-428,682
-393,719,612
-211,-452,876
808,-476,-593
-575,615,604
-485,667,467
-680,325,-822
-627,-443,-432
872,-547,-609
833,512,582
807,604,487
839,-516,451
891,-625,532
-652,-548,-490
30,-46,-14";

            _input = file.GetLines();

            _scanners = new List<Scanner>();
            var pos = 0;
            while (pos < _input.Count)
            {
                var lines = new List<string> { _input[pos] };
                pos++;

                lines.AddRange(_input.Skip(pos).TakeWhile(x => !x.StartsWith("---")));
                var scanner = new Scanner(lines);
                _scanners.Add(scanner);
                pos += lines.Count - 1;
            }

            //Do Pre-stuff
            _scanners.First(x => x.Id == 0).Pos = (0, 0, 0);
        }

        protected override string Part1()
        {
            var scannersToSearch = _scanners.ToList();

            do
            {
                var scanner = scannersToSearch.First(x => x.SetPosition);

                foreach (var other in _scanners)
                {
                    if (scanner == other || other.Matches.Contains(scanner) || !scanner.SetPosition)
                    {
                        continue;
                    }

                    if (Scanner.Match(scanner, other))
                    {
                        Console.WriteLine($"{scanner.Id} matches {other.Id}");
                        scanner.Matches.Add(other);
                        other.Matches.Add(scanner);
                    }
                }

                scannersToSearch.Remove(scanner);
            } while (scannersToSearch.Count > 0);

            var matchedScanners = _scanners.Where(x => x.SetPosition);

            var beacons = matchedScanners.SelectMany(x => x.Beacons).OrderBy(x => x.X).ThenBy(x => x.Y).ThenBy(x => x.Z).ToList();
            while (ClearMatches(beacons)) ;
            beacons = beacons.OrderBy(x => x.X).ThenBy(x => x.Y).ThenBy(x => x.Z).ToList();

            Console.WriteLine("---");
            beacons.ForEach(Console.WriteLine);
            Console.WriteLine("---");

            DebugAssertsPart1Test1(beacons);

            return beacons.Count.ToString();
        }

        protected override string Part2()
        {
            var scannersToSearch = _scanners.ToList();

            do
            {
                var scanner = scannersToSearch.First(x => x.SetPosition);

                foreach (var other in _scanners)
                {
                    if (scanner == other || other.Matches.Contains(scanner) || !scanner.SetPosition)
                    {
                        continue;
                    }

                    if (Scanner.Match(scanner, other))
                    {
                        Console.WriteLine($"{scanner.Id} matches {other.Id}");
                        scanner.Matches.Add(other);
                        other.Matches.Add(scanner);
                    }
                }

                scannersToSearch.Remove(scanner);
            } while (scannersToSearch.Count > 0);

            var maxdist = int.MinValue;
            foreach (IEnumerable<Scanner> comb in Tools.Combinations(_scanners, 2))
            {
                var f = comb.First();
                var l = comb.Last();
                var x = f.Pos.Value.X - l.Pos.Value.X;
                var y = f.Pos.Value.Y - l.Pos.Value.Y;
                var z = f.Pos.Value.Z - l.Pos.Value.Z;
                var dist = Math.Abs(x) + Math.Abs(y) + Math.Abs(z);
                if (dist > maxdist)
                {
                    maxdist = dist;
                }
            }

            return maxdist.ToString();
        }

        public void DebugAssertsPart1Test1(List<Day19Beacon> beacons)
        {
            var expectedTest = @"-892,524,684
-876,649,763
-838,591,734
-789,900,-551
-739,-1745,668
-706,-3180,-659
-697,-3072,-689
-689,845,-530
-687,-1600,576
-661,-816,-575
-654,-3158,-753
-635,-1737,486
-631,-672,1502
-624,-1620,1868
-620,-3212,371
-618,-824,-621
-612,-1695,1788
-601,-1648,-643
-584,868,-557
-537,-823,-458
-532,-1715,1894
-518,-1681,-600
-499,-1607,-770
-485,-357,347
-470,-3283,303
-456,-621,1527
-447,-329,318
-430,-3130,366
-413,-627,1469
-345,-311,381
-36,-1284,1171
-27,-1108,-65
7,-33,-71
12,-2351,-103
26,-1119,1091
346,-2985,342
366,-3059,397
377,-2827,367
390,-675,-793
396,-1931,-563
404,-588,-901
408,-1815,803
423,-701,434
432,-2009,850
443,580,662
455,729,728
456,-540,1869
459,-707,401
465,-695,1988
474,580,667
496,-1584,1900
497,-1838,-617
527,-524,1933
528,-643,409
534,-1912,768
544,-627,-890
553,345,-567
564,392,-477
568,-2007,-577
605,-1665,1952
612,-1593,1893
630,319,-379
686,-3108,-505
776,-3184,-501
846,-3110,-434
1135,-1161,1235
1243,-1093,1063
1660,-552,429
1693,-557,386
1735,-437,1738
1749,-1800,1813
1772,-405,1572
1776,-675,371
1779,-442,1789
1780,-1548,337
1786,-1538,337
1847,-1591,415
1889,-1729,1762
1994,-1805,1792".GetLines().Select(x => new Day19Beacon(x)).OrderBy(x => x.X).ThenBy(x => x.Y).ThenBy(x => x.Z).ToList();

            Debug.Assert(expectedTest.Count == beacons.Count);
            for (var i = 0; i < expectedTest.Count; ++i)
            {
                Debug.Assert(expectedTest[i].Match(beacons[i]));
            }
        }

        public bool ClearMatches(List<Day19Beacon> beacons)
        {
            var matchingBeacons = beacons.Where(x => beacons.Any(y => x != y && x.Match(y))).OrderBy(x => x.X).ThenBy(x => x.Y).ThenBy(x => x.Z).ToList();
            if (!matchingBeacons.Any())
            {
                return false;
            }

            beacons.RemoveAll(x => matchingBeacons.Contains(x));
            for (var i = 0; i < matchingBeacons.Count;)
            {
                var current = matchingBeacons[i];
                beacons.Add(current);
                while (i < matchingBeacons.Count && current.Match(matchingBeacons[i])) i++;
            }
            return true;
        }
    }















    public class Day19Beacon
    {
        public (int X, int Y, int Z) Orig;
        public int X, Y, Z;

        public Day19Beacon(int x, int y, int z)
        {
            Orig = (x, y, z);
            X = x;
            Y = y;
            Z = z;
        }

        public Day19Beacon(string data)
        {
            var c = data.Split(',');
            X = int.Parse(c[0]);
            Y = int.Parse(c[1]);
            Z = int.Parse(c[2]);
        }

        public override string ToString()
        {
            return $"{X},{Y},{Z}";
        }

        public bool Match(Day19Beacon other)
        {
            return X == other.X && Y == other.Y && Z == other.Z;
        }
    }

    public class Scanner
    {
        public int Id;
        public bool SetPosition => Pos.HasValue;
        public (int X, int Y, int Z)? Pos;

        public List<Scanner> Matches = new List<Scanner>();

        //Relativt till eventuell start
        //Max 1000 units away

        public List<Day19Beacon> Beacons;

        public string DataString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"--- scanner {Id} ---");
            foreach (var b in Beacons)
            {
                sb.AppendLine(b.ToString());
            }
            return sb.ToString();
        }
        public override string ToString()
        {
            return $"{Id}: {string.Join(" | ", Beacons.Select(x => x.ToString()))}";
        }

        public Scanner(IEnumerable<string> beaconString)
        {
            Beacons = new List<Day19Beacon>();
            foreach (var line in beaconString)
            {
                if (line.StartsWith("---"))
                {
                    Id = int.Parse(line.Split(' ')[2]);
                }
                else
                {
                    var b = new Day19Beacon(line);
                    Beacons.Add(b);
                }
            }
        }

        public void RotateX()
        {
            foreach (var beacon in Beacons)
            {
                var bX = beacon.X;
                var bY = beacon.Z;
                var bZ = -beacon.Y;
                beacon.X = bX;
                beacon.Y = bY;
                beacon.Z = bZ;
            }
        }

        public void RotateY()
        {
            foreach (var beacon in Beacons)
            {
                var bX = beacon.Z;
                var bY = beacon.Y;
                var bZ = -beacon.X;
                beacon.X = bX;
                beacon.Y = bY;
                beacon.Z = bZ;
            }
        }

        public void RotateZ()
        {
            foreach (var beacon in Beacons)
            {
                var bX = -beacon.Y;
                var bY = beacon.X;
                var bZ = beacon.Z;
                beacon.X = bX;
                beacon.Y = bY;
                beacon.Z = bZ;
            }
        }

        public void ResetOffset(Day19Beacon b)
        {
            var (x, y, z) = (b.X, b.Y, b.Z);
            foreach (var beacon in Beacons)
            {
                beacon.X -= x;
                beacon.Y -= y;
                beacon.Z -= z;
            }
        }

        public static bool Match(Scanner a, Scanner b)
        {
            if (a.SetPosition && b.SetPosition)
            {
                return false;
            }
            //Debug.Assert(!(a.SetPosition && b.SetPosition));

            var aString = a.DataString().GetLines();
            var bString = b.DataString().GetLines();

            var matchingBeacons = new List<(Day19Beacon BeaconA, Day19Beacon BeaconB, int IndexA, int IndexB)>();

            (int x, int y, int z) rotXyz = (0, 0, 0);
            for (var x = 0; x < 4; ++x)
            {
                for (var y = 0; y < 4; ++y)
                {
                    for (var z = 0; z < 4; ++z)
                    {

                        foreach (var bb in b.Beacons)
                        {
                            var origB = new Scanner(bString);
                            origB.ResetOffset(bb);
                            for (var ix = 0; ix < x; ++ix) origB.RotateX();
                            for (var iy = 0; iy < y; ++iy) origB.RotateY();
                            for (var iz = 0; iz < z; ++iz) origB.RotateZ();

                            foreach (var ba in a.Beacons)
                            {
                                var origA = new Scanner(aString);
                                origA.ResetOffset(ba);

                                var tempMatchingBeacons = origA.Beacons
                                    .Where(beacon => origB.Beacons.Any(beacon1 => beacon1.Match(beacon)))
                                    .Select(beacon =>
                                        (
                                            BeaconA: beacon,
                                            BeaconB: origB.Beacons.First(beacon1 => beacon1.Match(beacon)),
                                            IndexA: origA.Beacons.IndexOf(beacon),
                                            IndexB: origB.Beacons.IndexOf(origB.Beacons.First(beacon1 => beacon1.Match(beacon)))))
                                    .ToList();
                                if (tempMatchingBeacons.Count > matchingBeacons.Count)
                                {
                                    matchingBeacons = tempMatchingBeacons;
                                    rotXyz = (x, y, z);
                                }
                            }
                        }
                    }
                }
            }

            if (matchingBeacons.Count >= 12)
            {
                var origB = new Scanner(bString);
                for (var ix = 0; ix < rotXyz.x; ++ix) origB.RotateX();
                for (var iy = 0; iy < rotXyz.y; ++iy) origB.RotateY();
                for (var iz = 0; iz < rotXyz.z; ++iz) origB.RotateZ();

                var aBeacon = a.Beacons[matchingBeacons.First().IndexA];
                var bBeacon = origB.Beacons[matchingBeacons.First().IndexB];

                var (dx, dy, dz) = (aBeacon.X - bBeacon.X, aBeacon.Y - bBeacon.Y, aBeacon.Z - bBeacon.Z);

                for (var ix = 0; ix < rotXyz.x; ++ix) b.RotateX();
                for (var iy = 0; iy < rotXyz.y; ++iy) b.RotateY();
                for (var iz = 0; iz < rotXyz.z; ++iz) b.RotateZ();

                foreach (var beacon in b.Beacons)
                {
                    beacon.X += dx;
                    beacon.Y += dy;
                    beacon.Z += dz;
                }

                b.Pos = (dx, dy, dz);
                return true;
            }

            return false;
        }
    }

}
