﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using adventofcode.Utilities;

namespace adventofcode.AOC2021
{
    public class Movablefish
    {
        public int Timer = 8;
        public BigInteger Count;

        public void ResetTimer() => Timer = 6;
        public override string ToString() => $"T: {Timer}, C: {Count}";
    }

    public class Day06 : Day
    {
        //Globals
        private readonly List<int> _input;

        private readonly List<Movablefish> _movFish = new List<Movablefish>();
        //Constructor
        public Day06()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\PATH\day.txt");
            var file = ToolsFile.ReadFile(DataPath + @"2021\day06.txt");

            //Test case
            var test1 = @"3,4,3,1,2";

            var chosenInput = file;
            _input = chosenInput.Split(',').Select(int.Parse).OrderBy(x => x).ToList();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            const int maxTick = 80;

            for (var i = 0; i < 9; i++)
            {
                _movFish.Add(new Movablefish { Timer = i, Count = _input.Count(x => x == i) });
            }

            for (var ticks = 0; ticks < maxTick; ++ticks)
            {
                for (var i = 0; i < 8; ++i)
                {
                    var f = _movFish.First(x => x.Timer == 0);
                    f.Timer--;
                }

                _movFish.First(x => x.Timer == 6).Count += _movFish.First(x => x.Timer < 0).Count;
                var newF = new Movablefish { Count = _movFish.First(x => x.Timer < 0).Count };
                _movFish.Add(newF);
                _movFish.RemoveAll(x => x.Timer < 0);
            }

            return _movFish.Aggregate(BigInteger.Zero, (current, f) => current + f.Count).ToString();
        }

        protected override string Part2()
        {
            const int maxTick = 256;

            for (var i = 0; i < 9; i++)
            {
                _movFish.Add(new Movablefish { Timer = i, Count = _input.Count(x => x == i) });
            }

            for (var ticks = 0; ticks < maxTick; ++ticks)
            {
                for (var i = 0; i < 9; ++i)
                {
                    var f = _movFish.First(x => x.Timer == i);
                    f.Timer--;
                }

                _movFish.First(x => x.Timer == 6).Count += _movFish.First(x => x.Timer < 0).Count;
                var newF = new Movablefish { Count = _movFish.First(x => x.Timer < 0).Count };
                _movFish.Add(newF);
                _movFish.RemoveAll(x => x.Timer < 0);
            }

            return _movFish.Aggregate(BigInteger.Zero, (current, f) => current + f.Count).ToString();
        }
    }
}
