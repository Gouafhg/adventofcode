﻿using System;
using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2021
{
    public class Day05 : Day
    {
        //Globals
        private readonly List<string> _input;

        //Constructor
        public Day05()
        {
            //Init globals
            Part = 1;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\PATH\day.txt");
            var file = ToolsFile.ReadFile(DataPath + @"2021\day05.txt");

            //Test case
            var test1 = @"0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2";

            var chosenInput = file;
            _input = chosenInput.Replace("\r", "\n").Split('\n').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var coords = new Dictionary<(int, int), int>();
            foreach (var row in _input)
            {
                var parts = row.Split(' ');
                var from = (x: int.Parse(parts.First().Split(',')[0]), y: int.Parse(parts.First().Split(',')[1]));
                var to = (x: int.Parse(parts.Last().Split(',')[0]), y: int.Parse(parts.Last().Split(',')[1]));

                if (from.x == to.x)
                {
                    var fY = Math.Min(from.y, to.y);
                    var tY = Math.Max(from.y, to.y);

                    for (var y = fY; y <= tY; ++y)
                    {
                        if (!coords.ContainsKey((from.x, y)))
                        {
                            coords.Add((from.x, y), 1);
                        }
                        else
                        {
                            coords[(from.x, y)]++;
                        }
                    }
                }
                else if (from.y == to.y)
                {
                    var fX = Math.Min(from.x, to.x);
                    var tX = Math.Max(from.x, to.x);

                    for (var x = fX; x <= tX; ++x)
                    {
                        if (!coords.ContainsKey((x, from.y)))
                        {
                            coords.Add((x, from.y), 1);
                        }
                        else
                        {
                            coords[(x, from.y)]++;
                        }
                    }
                }
                else
                {
                    var x = from.x;
                    var y = from.y;
                    while (x != to.x)
                    {
                        if (!coords.ContainsKey((x, y)))
                        {
                            coords.Add((x, y), 1);
                        }
                        else
                        {
                            coords[(x, y)]++;
                        }

                        if (from.x < to.x)
                        {
                            x++;
                        }
                        else
                        {
                            x--;
                        }
                        if (from.y < to.y)
                        {
                            y++;
                        }
                        else
                        {
                            y--;
                        }
                    }

                    if (!coords.ContainsKey((x, y)))
                    {
                        coords.Add((x, y), 1);
                    }
                    else
                    {
                        coords[(x, y)]++;
                    }

                }

            }

            return coords.Keys.Count(x => coords[x] > 1).ToString();
        }

        protected override string Part2()
        {
            return "";
        }
    }
}
