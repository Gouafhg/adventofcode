﻿using System;
using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2021
{
    public class Ocotopus
    {
        public int Energy;
        public (int x, int y) Position;
        public bool HasFlashed;

        public Ocotopus((int x, int y) position, int energy)
        {
            Energy = energy;
            Position = position;
            HasFlashed = false;
        }

        public override string ToString()
        {
            return $"{Position.x}, {Position.y}: {Energy}";
        }

        public void Tick()
        {
            Energy++;
            if (Energy > 9)
            {
                Flash();
            }
        }

        public void Reset()
        {
            if (HasFlashed)
            {
                Energy = 0;
            }
            HasFlashed = false;
        }

        public void Flash()
        {
            if (HasFlashed)
            {
                return;
            }

            Day11.Flashes++;
            HasFlashed = true;
            foreach (var n in Neighbours)
            {
                n.Energy++;
                if (n.Energy > 9)
                {
                    n.Flash();
                }
            }
        }

        public List<Ocotopus> Neighbours = new List<Ocotopus>();
    }

    public class Day11 : Day
    {
        //Globals
        private readonly List<List<int>> _input;
        private readonly List<Ocotopus> _ocotopuses = new List<Ocotopus>();

        public static int Flashes;
        //Constructor
        public Day11()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\PATH\day.txt");
            var file = ToolsFile.ReadFile(DataPathHemma + @"2021\day11.txt");

            //Test case
            var test1 = @"5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526";

            var test2 = @"11111
19991
19191
19991
11111";

            _input = file.GetIntMap();

            //Do Pre-stuff
            for (var y = 0; y < _input.Count; ++y)
            {
                for (var x = 0; x < _input[y].Count; ++x)
                {
                    var o = new Ocotopus((x, y), _input.GetValue((x, y)));

                    _ocotopuses.Add(o);
                }
            }

            for (var y = 0; y < _input.Count; ++y)
            {
                for (var x = 0; x < _input[y].Count; ++x)
                {
                    var o = _ocotopuses.First(oc => oc.Position == (x, y));

                    var ns = _input.GetNeighbours((x, y), true);
                    foreach (var (nx, ny) in ns)
                    {
                        var no = _ocotopuses.First(oc => oc.Position == (nx, ny));
                        o.Neighbours.Add(no);
                    }
                }
            }
        }

        protected override string Part1()
        {
            for (var i = 0; i < 100; ++i)
            {
                _ocotopuses.ForEach(x => x.Tick());
                _ocotopuses.Where(x => x.HasFlashed).ToList().ForEach(x => x.Energy = 0);
                _ocotopuses.ForEach(x => x.Reset());
            }

            return Flashes.ToString();
        }

        protected override string Part2()
        {
            var tick = 0;
            while (true)
            {
                tick++;
                _ocotopuses.ForEach(x => x.Tick());
                _ocotopuses.Where(x => x.HasFlashed).ToList().ForEach(x => x.Energy = 0);
                if (_ocotopuses.All(x => x.HasFlashed))
                {
                    break;
                }
                _ocotopuses.ForEach(x => x.Reset());
            }

            return tick.ToString();
        }

        private void Print()
        {
            for (var y = 0; y < _input.Count; ++y)
            {
                for (var x = 0; x < _input[y].Count; ++x)
                {
                    var o = _ocotopuses.First(oc => oc.Position == (x, y));
                    Console.Write(o.Energy.ToString()[0]);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
