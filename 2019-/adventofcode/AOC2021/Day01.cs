﻿using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2021
{
    public class Day01 : Day
    {
        //Globals
        private readonly List<string> _input;

        //Constructor
        public Day01()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\Diverse\aoc\adventofcode\2019-\data\PATH\day.txt");
            var file = ToolsFile.ReadFile(DataPath + @"2021\day01.txt");

            //Test case
            var test1 = @"199
200
208
210
200
207
240
269
260
263";

            var chosenInput = file.Replace("\r", "\n").Split('\n').Where(x => !string.IsNullOrWhiteSpace(x)).ToList(); ;
            _input = chosenInput;

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var intinput = _input.Select(int.Parse).ToList();

            var result = 0;
            for (var i = 1; i < intinput.Count; ++i)
            {
                if (intinput[i] > intinput[i - 1])
                {
                    result++;
                }
            }

            return result.ToString();
        }

        protected override string Part2()
        {
            var intinput = _input.Select(int.Parse).ToList();

            var result = 0;
            for (var i = 3; i < intinput.Count; ++i)
            {
                if (intinput[i] + intinput[i - 1] + intinput[i - 2] > intinput[i - 1] + intinput[i - 2] + intinput[i - 3])
                {
                    result++;
                }
            }

            return result.ToString();
        }
    }
}
