﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2021
{
    internal class Day13 : Day
    {
        //Globals
        private List<List<char>> _grid;
        private readonly List<string> _folds;
        private readonly List<string> _input;

        private const char Empty = ' ';
        private const char Mark = 'x';

        //Constructor
        public Day13()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\PATH\day.txt");
            var file = ToolsFile.ReadFile(DataPath + @"2021\day13.txt");

            //Test case
            var test1 = @"6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5";

            _input = file.GetLines();

            //Do Pre-stuff
            var dotPart = _input.TakeWhile(x => char.IsDigit(x[0])).ToList();
            _folds = _input.Skip(dotPart.Count()).ToList();

            var dots = dotPart.Select(x => (x: int.Parse(x.Split(',')[0]), y: int.Parse(x.Split(',')[1]))).ToList();
            var maxX = dots.Max(x => x.x);
            var maxY = dots.Max(x => x.y);

            _grid = new List<List<char>>();
            for (var y = 0; y < maxY + 1; ++y)
            {
                _grid.Add(new List<char>());
                for (var x = 0; x < maxX + 1; ++x)
                {
                    _grid[y].Add(Empty);
                }
            }

            foreach (var (x, y) in dots)
            {
                _grid[y][x] = Mark;
            }
        }

        public void Print()
        {
            foreach (var row in _grid)
            {
                var rowString = new string(row.ToArray());
                Console.WriteLine(rowString);
            }
            Console.WriteLine();

        }

        protected override string Part1()
        {
            foreach (var fold in _folds.Take(1))
            {
                var xy = fold.Split('=')[0].Last().ToString();
                var coord = int.Parse(fold.Split('=')[1]);
                if (xy == "x")
                {
                    for (var y = 0; y < _grid.Count; ++y)
                    {
                        for (var x = 0; x < coord; ++x)
                        {
                            var toBe = _grid[y][_grid[y].Count - 1 - x];
                            if (toBe == Mark)
                            {
                                _grid[y][x] = toBe;
                            }
                        }
                    }

                    _grid = _grid.Select(x => x.Take(coord).ToList()).ToList();
                }
                else
                {
                    for (var y = 0; y < coord; ++y)
                    {
                        for (var x = 0; x < _grid[y].Count; ++x)
                        {
                            var toBe = _grid[_grid.Count - 1 - y][x];
                            if (toBe == Mark)
                            {
                                _grid[y][x] = toBe;
                            }
                        }
                    }

                    _grid = _grid.Take(coord).ToList();
                }
            }

            Print();

            return _grid.SelectMany(x => x).Count(x => x == '#').ToString();
        }

        protected override string Part2()
        {
            foreach (var fold in _folds)
            {
                var xy = fold.Split('=')[0].Last().ToString();
                var coord = int.Parse(fold.Split('=')[1]);

                Debug.Assert(xy == "x" || xy == "y");

                if (xy == "x")
                {
                    var resY = _grid.Count;
                    for (var y = 0; y < resY; ++y)
                    {
                        //Debug.Assert(_grid[y][coord] == Empty);

                        var resX = _grid[y].Count;
                        for (var x = coord + 1; x < resX; ++x)
                        {
                            var toBe = _grid[y][x];
                            if (toBe == Mark)
                            {
                                var diff = Math.Abs(x - coord);
                                _grid[y][coord - diff] = toBe;
                            }
                        }
                        //Debug.Assert(_grid[y][coord] == Empty);
                    }

                    _grid = _grid.Select(x => x.Take(coord).ToList()).ToList();
                }
                else
                {
                    //Debug.Assert(_grid[coord].All(x => x == Empty));

                    var resY = _grid.Count;
                    for (var y = coord + 1; y < resY; ++y)
                    {
                        var resX = _grid[y].Count;
                        for (var x = 0; x < resX; ++x)
                        {
                            var toBe = _grid[y][x];
                            if (toBe == Mark)
                            {
                                var diff = Math.Abs(y - coord);

                                _grid[coord - diff][x] = toBe;
                            }
                        }
                    }

                    //Debug.Assert(_grid[coord].All(x => x == Empty));

                    _grid = _grid.Take(coord).ToList();
                }
            }

            Print();

            return "";
        }
    }
}
