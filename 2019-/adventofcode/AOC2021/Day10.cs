﻿using adventofcode.Utilities;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace adventofcode.AOC2021
{
    public class Day10 : Day
    {
        //Globals
        private readonly List<string> _input;
        private readonly List<string> _pairs = new List<string> { "()", "{}", "[]", "<>" };
        private readonly List<char> _starters;
        private readonly List<char> _finishers;
        //Constructor
        public Day10()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\PATH\day.txt");
            var file = ToolsFile.ReadFile(DataPathHemma + @"2021\day10.txt");

            //Test case
            var test1 = @"[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]";

            _input = file.Replace("\r", "\n").Split('\n').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

            //Do Pre-stuff
            _starters = _pairs.Select(x => x[0]).ToList();
            _finishers = _pairs.Select(x => x[1]).ToList();
        }

        protected override string Part1()
        {
            var sum = 0;
            foreach (var row in _input)
            {
                if (!IsComplete(row, out var ill))
                {
                    var p = ill switch
                    {
                        ')' => 3,
                        ']' => 57,
                        '}' => 1197,
                        _ => 25137,
                    };
                    sum += p;
                }
            }
            return sum.ToString();
        }

        protected override string Part2()
        {
            var completeLines = new List<string>();

            foreach (var row in _input)
            {
                if (IsComplete(row, out _))
                {
                    completeLines.Add(row);
                }
            }

            var points = new List<BigInteger>();
            foreach (var line in completeLines)
            {
                points.Add(CompleteLine(line));
            }
            points = points.OrderBy(x => x).ToList();

            var middle = points[points.Count / 2];
            return middle.ToString();
        }

        private bool IsComplete(string row, out char firstIllegal)
        {
            var current = new Stack<char>();
            for (var i = 0; i < row.Length; ++i)
            {
                if (_starters.Contains(row[i]))
                {
                    current.Push(row[i]);
                    continue;
                }
                if (_finishers.Contains(row[i]))
                {
                    var c = current.Pop();
                    var correctFinisher = _pairs.First(x => x.Contains(c))[1];
                    if (row[i] != correctFinisher)
                    {
                        firstIllegal = row[i];
                        return false;
                    }
                }
            }

            firstIllegal = ' ';
            return true;
        }

        private BigInteger CompleteLine(string row)
        {
            var current = new Stack<char>();
            foreach (var c in row)
            {
                if (_starters.Contains(c))
                {
                    current.Push(c);
                    continue;
                }
                if (_finishers.Contains(c))
                {
                    current.Pop();
                }
            }

            var endString = string.Empty;
            while (current.Count > 0) endString += current.Pop();
            endString = new string(endString.Select(x => _pairs.First(y => y.Contains(x))[1]).ToArray());

            var stringSum = BigInteger.Zero;

            foreach (var c in endString)
            {
                stringSum *= 5;
                stringSum += GetCompletePoint(c);
            }

            return stringSum;
        }

        private int GetCompletePoint(char c)
        {
            return c switch
            {
                ')' => 1,
                ']' => 2,
                '}' => 3,
                _ => 4,
            };
        }
    }
}
