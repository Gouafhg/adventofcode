﻿using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2021
{
    public class Day02 : Day
    {
        //Globals
        private readonly List<string> _input;

        //Constructor
        public Day02()
        {
            //Init globals
            Part = 1;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\PATH\day.txt");
            var file = ToolsFile.ReadFile(DataPath + @"2021\day02.txt");

            //Test case
            var test1 = @"";

            var chosenInput = test1;
            _input = chosenInput.Replace("\r", "\n").Split('\n').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            return "";
        }

        protected override string Part2()
        {
            return "";
        }
    }
}
