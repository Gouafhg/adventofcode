﻿using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2021
{
    public class Node
    {
        public string Name;

        public bool Start;
        public bool End;

        public bool SmallCave;
        public List<Node> Neighbours;

        public Node(string name)
        {
            Name = name;
            Start = name == "start";
            End = name == "end";

            SmallCave = !Start && !End && name == name.ToLower();
            Neighbours = new List<Node>();
        }

        public override string ToString() => Name;
    }

    public class Day12 : Day
    {
        //Globals
        private readonly List<Node> _cave;
        private readonly List<string> _input;

        //Constructor
        public Day12()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\PATH\day.txt");
            var file = ToolsFile.ReadFile(DataPath + @"2021\day12.txt");

            //Test case
            var test1 = @"start-A
start-b
A-c
A-b
b-d
A-end
b-end";
            var test2 = @"dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc";

            var test3 = @"fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW";

            _input = file.GetLines();

            //Do Pre-stuff
            _cave = new List<Node>();
            foreach (var line in _input)
            {
                var fromNode = line.Split('-')[0];
                var toNode = line.Split('-')[1];
                if (_cave.All(x => x.Name != fromNode))
                {
                    _cave.Add(new Node(fromNode));
                }

                if (_cave.All(x => x.Name != toNode))
                {
                    _cave.Add(new Node(toNode));
                }
            }

            var caveLookup = _cave.ToLookup(x => x.Name);

            foreach (var line in _input)
            {
                var fromNode = caveLookup[line.Split('-')[0]].Single();
                var toNode = caveLookup[line.Split('-')[1]].Single();

                fromNode.Neighbours.Add(toNode);
                toNode.Neighbours.Add(fromNode);
            }
        }

        protected override string Part1()
        {
            var paths = new List<List<Node>>();
            var q = new Queue<List<Node>>();

            var start = _cave.First(x => x.Start);
            q.Enqueue(new List<Node> { start });
            while (q.Count > 0)
            {
                var path = q.Dequeue();
                if (path.Last().End)
                {
                    paths.Add(path);
                    continue;
                }

                foreach (var n in path.Last().Neighbours)
                {
                    if (n.Start)
                    {
                        continue;
                    }

                    if (n.SmallCave && path.Any(x => x.Name == n.Name))
                    {
                        continue;
                    }

                    var newPath = new List<Node>(path) { n };
                    q.Enqueue(newPath);
                }
            }

            //foreach (var path in paths)
            //{
            //    Console.WriteLine(string.Join(", ", path.Select(x => x.ToString())));
            //}

            return paths.Count.ToString();
        }

        protected override string Part2()
        {
            var paths = new List<List<Node>>();
            var q = new Queue<List<Node>>();

            var start = _cave.First(x => x.Start);
            q.Enqueue(new List<Node> { start });
            while (q.Count > 0)
            {
                var path = q.Dequeue();
                if (path.Last().End)
                {
                    paths.Add(path);
                    continue;
                }

                foreach (var n in path.Last().Neighbours)
                {
                    if (n.Start)
                    {
                        continue;
                    }

                    var nodeCount = n.SmallCave ? path.Distinct().Where(x => x.SmallCave).ToDictionary(x => x.Name, x => path.Count(y => y.Name == x.Name)) : null;
                    if (n.SmallCave && nodeCount.ContainsKey(n.Name) && (nodeCount[n.Name] > 1 || nodeCount.Values.Any(x => x >= 2)))
                    {
                        continue;
                    }

                    var newPath = new List<Node>(path) { n };
                    q.Enqueue(newPath);
                }
            }

            //foreach (var path in paths)
            //{
            //    Console.WriteLine(string.Join(", ", path.Select(x => x.ToString())));
            //}

            return paths.Count.ToString();
        }
    }
}
