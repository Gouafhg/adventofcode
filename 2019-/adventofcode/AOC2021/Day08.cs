﻿using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2021
{
    public class Day08 : Day
    {
        //Globals
        private readonly List<string> _input;

        private readonly List<char> _chars = new List<char> { 'a', 'b', 'c', 'd', 'e', 'f', 'g' };

        //Constructor
        public Day08()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\PATH\day.txt");
            var file = ToolsFile.ReadFile(DataPath + @"2021\day08.txt");

            //Test case
            var test1 = @"be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce";

            var test2 = "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf";

            _input = file.Replace("\r", "\n").Split('\n').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

            //Do Pre-stuff

        }

        protected override string Part1()
        {
            var ones = 0;
            var fours = 0;
            var sevens = 0;
            var eights = 0;
            foreach (var row in _input)
            {
                var secondPartDigits = row.Split('|').Last().Trim().Split(' ').ToList();
                ones += secondPartDigits.Count(x => x.Length == 2);
                fours += secondPartDigits.Count(x => x.Length == 4);
                sevens += secondPartDigits.Count(x => x.Length == 3);
                eights += secondPartDigits.Count(x => x.Length == 7);
            }

            var sum = ones + fours + sevens + eights;

            return sum.ToString();
        }

        protected override string Part2()
        {
            var result = new List<string>();

            foreach (var row in _input)
            {
                var firstPartDigits = row.Split('|').First().Trim().Split(' ').Select(x => new string(x.OrderBy(y => y).ToArray())).ToList();

                var one = string.Empty;
                var four = string.Empty;
                var seven = string.Empty;
                var eigth = string.Empty;
                var fiveLengths = new List<string>();
                var sixLengths = new List<string>();
                foreach (var digit in firstPartDigits)
                {
                    switch (digit.Length)
                    {
                        case 2:
                            one = digit;
                            break;
                        case 4:
                            four = digit;
                            break;
                        case 3:
                            seven = digit;
                            break;
                        case 7:
                            eigth = digit;
                            break;
                        case 5:
                            fiveLengths.Add(digit);
                            break;
                        case 6:
                            sixLengths.Add(digit);
                            break;
                    }
                }

                var doubleSix = _chars.Where(x => sixLengths.Count(y => y.Contains(x)) == 2).ToList();
                var tripeFive = _chars.Where(x => fiveLengths.Count(y => y.Contains(x)) == 3).ToList();

                var middle = doubleSix.Single(x => tripeFive.Contains(x));
                var zero = sixLengths.Single(x => !x.Contains(middle));
                var sixnine = sixLengths.Where(x => x != zero).ToList();
                var nine = sixnine.Single(x => one.All(x.Contains));
                var six = sixnine.Single(x => x != nine);

                var three = fiveLengths.Single(x => one.All(x.Contains));
                var twofive = fiveLengths.Where(x => x != three).ToList();
                var upleft = four.Select(x => x).Except(three.Select(x => x)).Single();
                var two = twofive.Single(x => !x.Contains(upleft));
                var five = twofive.Single(x => x != two);

                var digits = new Dictionary<string, string>()
                {
                    {one, "1"},
                    {two, "2"},
                    {three, "3"},
                    {four, "4"},
                    {five, "5"},
                    {six, "6"},
                    {seven, "7"},
                    {eigth, "8"},
                    {nine, "9"},
                    {zero, "0"},
                };

                var secondPartDigits = row.Split('|').Last().Trim().Split(' ')
                            .Select(x => new string(x.OrderBy(y => y).ToArray())).ToList();

                var number = string.Join("", secondPartDigits.Select(x => digits[x]));
                result.Add(number);
            }

            return result.Select(int.Parse).Sum().ToString();
        }
    }
}
