using adventofcode.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2024
{
    public class Day07 : Day
    {
        //Globals
        private readonly List<string> _input;

        private readonly Dictionary<List<long>, long> _dict;

        //Constructor
        public Day07()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\Utveckling\TheGang\adventofcode\2019-\data\2024\day07.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"2024\day01.txt");

            //Test case
            var test1 = @"190: 10 19
3267: 81 40 27
83: 17 5
156: 15 6
7290: 6 8 6 15
161011: 16 10 13
192: 17 8 14
21037: 9 7 18 13
292: 11 6 16 20";

            _input = file.GetLines();

            //Do Pre-stuff

            _dict = _input.ToDictionary(x => x.Split(':')[1].Trim().Split(' ').Select(long.Parse).ToList(),
                x => long.Parse(x.Split(':')[0]));
        }
        
        private static bool IsPossibleP1(long current, long answer, IEnumerable<long> sequence)
        {
            if (!sequence.Any())
            {
                return current == answer;
            }

            var next = sequence.First();
            var rest = sequence.Skip(1);
            return 
                IsPossibleP1(current + next, answer, rest) ||
                IsPossibleP1(current * next, answer, rest);
        }

        private static bool IsPossibleP2(long current, long answer, IEnumerable<long> sequence)
        {
            if (!sequence.Any())
            {
                return current == answer;
            }

            var next = sequence.First();
            var rest = sequence.Skip(1);
            return
                IsPossibleP2(current + next, answer, rest) ||
                IsPossibleP2(current * next, answer, rest) ||
                IsPossibleP2(long.Parse(current + next.ToString()), answer, rest);
        }

        protected override string Part1()
        {
            long result = 0;

            foreach (var l in _dict)
            {
                var (sequence, answer) = (l.Key, l.Value);

                if (IsPossibleP1(0, answer, sequence))
                {
                    result += answer;
                }
            }

            return result.ToString();
        }

        protected override string Part2()
        {
            long result = 0;

            foreach (var l in _dict)
            {
                var (sequence, answer) = (l.Key, l.Value);

                if (IsPossibleP2(0, answer, sequence))
                {
                    result += answer;
                }
            }

            return result.ToString();
        }
    }
}