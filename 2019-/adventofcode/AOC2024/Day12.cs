using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2024
{
    public class Day12 : Day
    {
        //Globals
        private readonly List<string> _input;
        private readonly char[,] _grid;
        private readonly List<char> _plantTypes;
        private readonly Dictionary<char, List<Dictionary<(int X, int Y), List<(int X, int Y)>>>> _regions;

        //Constructor
        public Day12()
        {
            //Init globals
            Part = 2;
            OnlyOnePart = true;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\Utveckling\TheGang\adventofcode\2019-\data\2024\day12.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"2024\day01.txt");

            //Test case
            var test1 = @"AAAA
BBCD
BBCC
EEEC";

            var test2 = @"OOOOO
OXOXO
OOOOO
OXOXO
OOOOO";

            var test3 = @"RRRRIICCFF
RRRRIICCCF
VVRRRCCFFF
VVRCCCJFFF
VVVVCJJCFE
VVIVCCJJEE
VVIIICJJEE
MIIIIIJJEE
MIIISIJEEE
MMMISSJEEE";

            var test4 = @"EEEEE
EXXXX
EEEEE
EXXXX
EEEEE";

            var test5 = @"AAAAAA
AAABBA
AAABBA
ABBAAA
ABBAAA
AAAAAA";

            _input = file.GetLines();

            //Do Pre-stuff
            _grid = new char[_input[0].Length, _input.Count];
            for (var y = 0; y < _input.Count; y++)
            {
                for (var x = 0; x < _input[y].Length; x++)
                {
                    _grid[x, y] = _input[y][x];
                }
            }

            _plantTypes = _input.Join().Distinct().ToList();

            _regions = GetRegions(_plantTypes);
        }

        protected override string Part1()
        {
            var price = (long)0;
            
            foreach (var plantType in _plantTypes)
            {
                foreach (var region in _regions[plantType])
                {
                    var regionPerimeter = region.Keys.Sum(x => 4 - region[x].Count);

                    price += regionPerimeter * region.Count;
                }
            }

            return price.ToString();
        }

        protected override string Part2()
        {
            var price = (long)0;
            
            foreach (var plantType in _plantTypes)
            {
                foreach (var region in _regions[plantType])
                {
                    var leftFence = new HashSet<(int X, int Y)>();
                    var rightFence = new HashSet<(int X, int Y)>();
                    var upFence = new HashSet<(int X, int Y)>();
                    var downFence = new HashSet<(int X, int Y)>();
                    
                    var sides = 0;
                    
                    foreach (var plant in region.Keys)
                    {
                        var hasLeft = region[plant].Any(x => x.X == plant.X - 1);
                        var hasRight = region[plant].Any(x => x.X == plant.X + 1);
                        var hasUp = region[plant].Any(x => x.Y == plant.Y - 1);
                        var hasDown = region[plant].Any(x => x.Y == plant.Y + 1);

                        //Det ska vara ett leftFence här!
                        if (!hasLeft && leftFence.Add(plant))
                        {
                            //hitta alla på rad uppåt/neråt
                            var position = plant;
                            while (region[position].Any(x => x == x with { Y = position.Y - 1 } && region[x with { Y = position.Y - 1 }].All(y => y.X != position.X - 1)))
                            {
                                position = position with { Y = position.Y - 1 };
                                leftFence.Add(position);
                            }
                            
                            while (region[position].Any(x => x == x with { Y = position.Y + 1 } && region[x with { Y = position.Y + 1 }].All(y => y.X != position.X - 1)))
                            {
                                position = position with { Y = position.Y + 1 };
                                leftFence.Add(position);
                            }

                            sides++;
                        }
                        
                        if (!hasRight && rightFence.Add(plant))
                        {
                            //hitta alla på rad uppåt/neråt
                            var position = plant;
                            while (region[position].Any(x => x == x with { Y = position.Y - 1 } && region[x with { Y = position.Y - 1 }].All(y => y.X != position.X + 1)))
                            {
                                position = position with { Y = position.Y - 1 };
                                rightFence.Add(position);
                            }
                            
                            while (region[position].Any(x => x == x with { Y = position.Y + 1 } && region[x with { Y = position.Y + 1 }].All(y => y.X != position.X + 1)))
                            {
                                position = position with { Y = position.Y + 1 };
                                rightFence.Add(position);
                            }

                            sides++;
                        }
                        
                        if (!hasUp && upFence.Add(plant))
                        {
                            //hitta alla på rad uppåt/neråt
                            var position = plant;
                            while (region[position].Any(x => x == x with { X = position.X - 1 } && region[x with { X = position.X - 1 }].All(y => y.Y != position.Y - 1)))
                            {
                                position = position with { X = position.X - 1 };
                                upFence.Add(position);
                            }
                            
                            while (region[position].Any(x => x == x with { X = position.X + 1 } && region[x with { X = position.X + 1 }].All(y => y.Y != position.Y - 1)))
                            {
                                position = position with { X = position.X + 1 };
                                upFence.Add(position);
                            }

                            sides++;
                        }
                        
                        if (!hasDown && downFence.Add(plant))
                        {
                            //hitta alla på rad uppåt/neråt
                            var position = plant;
                            while (region[position].Any(x => x == x with { X = position.X - 1 } && region[x with { X = position.X - 1 }].All(y => y.Y != position.Y + 1)))
                            {
                                position = position with { X = position.X - 1 };
                                downFence.Add(position);
                            }
                            
                            while (region[position].Any(x => x == x with { X = position.X + 1 } && region[x with { X = position.X + 1 }].All(y => y.Y != position.Y + 1)))
                            {
                                position = position with { X = position.X + 1 };
                                downFence.Add(position);
                            }

                            sides++;
                        }
                    }

                    Console.WriteLine($"{plantType} {region.Count} {sides}");
                    
                    price += region.Keys.Count * sides;
                }
            }

            return price.ToString();
        }

        private Dictionary<char, List<Dictionary<(int X, int Y), List<(int X, int Y)>>>> GetRegions(
            IEnumerable<char> plantTypes)
        {
            var result = new Dictionary<char, List<Dictionary<(int X, int Y), List<(int X, int Y)>>>>();

            foreach (var plant in plantTypes)
            {
                var positions = new List<(int X, int Y)>();
                for (var y = 0; y < _grid.GetLength(1); y++)
                {
                    for (var x = 0; x < _grid.GetLength(0); x++)
                    {
                        if (_grid[x, y] == plant)
                        {
                            positions.Add((x, y));
                        }
                    }
                }

                var graph = new Dictionary<(int X, int Y), List<(int X, int Y)>>();
                foreach (var position in positions)
                {
                    var left = position with { X = position.X - 1 };
                    var right = position with { X = position.X + 1 };
                    var up = position with { Y = position.Y - 1 };
                    var down = position with { Y = position.Y + 1 };

                    graph.Add(position, new List<(int X, int Y)> { left, right, up, down }.Where(x => positions.Contains(x)).ToList());
                }

                var regions = new List<List<(int X, int Y)>>();
                while (positions.Any())
                {
                    var currentRegion = new List<(int X, int Y)>();

                    var q = new Queue<(int X, int Y)>();
                    q.Enqueue(positions.First());
                    positions.RemoveAt(0);

                    while (q.Any())
                    {
                        var current = q.Dequeue();
                        currentRegion.Add(current);
                        foreach (var neighbour in graph[current])
                        {
                            if (!positions.Contains(neighbour)) continue;

                            positions.Remove(neighbour);
                            q.Enqueue(neighbour);
                        }
                    }

                    regions.Add(currentRegion);
                }

                result[plant] = regions.Select(x => x.ToDictionary(y => y, y => graph[y])).ToList();
            }

            return result;
        }
    }
}