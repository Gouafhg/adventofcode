using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using adventofcode.Utilities;
using adventofcode.Utilities.Extensions;

namespace adventofcode.AOC2024
{
    public class Day19 : Day
    {
        //Globals
        private List<string> _input;
        private List<string> _patterns;
        private List<string> _designs;

        public Day19()
        {
            //Init globals
            Part = 1;
            OnlyOnePart = false;
        }

        //Reset data
        public override void Reset()
        {
            //Setup file
            var file = ToolsFile.ReadFile(@"C:\Utveckling\TheGang\adventofcode\2019-\data\2024\day19.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test1 = @"r, wr, b, g, bwu, rb, gb, br

brwrr
bggr
gbbr
rrbgbr
ubwu
bwurrg
brgr
bbrgwb";

            _input = file.GetLines();

            //Do Pre-stuff
            _patterns = _input.First().Split(",").Select(x => x.Trim()).ToList();
            _designs = _input.Skip(1).ToList();
        }

        protected override string Part1()
        {
            var result = _designs.Count(design => FindByPatterns(design, []) != 0);

            return result.ToString();
        }

        protected override string Part2()
        {
            var result = (BigInteger)0;
            result = _designs.Aggregate(result, (current, design) => current + FindByPatterns(design, []));

            return result.ToString();
        }

        private BigInteger FindByPatterns(string design, Dictionary<string, BigInteger> cache)
        {
            if (cache.TryGetValue(design, out var resultCount))
            {
                return resultCount;
            }
            
            if (design.IsEmpty())
            {
                return 1;
            }

            var result = (BigInteger)0;
            foreach (var pattern in _patterns)
            {
                if (!design.StartsWith(pattern, StringComparison.Ordinal)) continue;

                var newDesign = design[pattern.Length..];
                result += FindByPatterns(newDesign, cache);
            }

            cache[design] = result;
            return result;
        }
    }
}