using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;

namespace adventofcode.AOC2024
{
    public class Day11 : Day
    {
        //Globals
        private List<long> _input;

        //Constructor
        public Day11()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = "8793800 1629 65 5 960 0 138983 85629";
            // var file = ToolsFile.ReadFile(@"C:\Utveckling\TheGang\adventofcode\2019-\data\2024\day11.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"2024\day01.txt");

            //Test case
            var test1 = @"0 1 10 99 999";
            var test2 = "125 17";

            _input = file.Split(' ').Select(long.Parse).ToList();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var numbers = _input.ToList();

            var count = 0;
            foreach (var number in numbers)
            {
                var startQ = new List<long> { number };

                for (var i = 0; i < 25; i++)
                {
                    List<long> newQ = [];
                    foreach (var n in startQ)
                    {
                        var (a, b) = Step(n);
                        newQ.Add(a);
                    
                        if (!b.HasValue) continue;
                    
                        newQ.Add(b.Value);
                    }

                    startQ = newQ;
                }

                count += startQ.Count;
            }

            return count.ToString();
        }

        protected override string Part2()
        {
            var numbers = _input.ToList();

            BigInteger count = 0;
            foreach (var number in numbers)
            {
                var startQ = new Dictionary<long, long>() { { number, 1 }};

                for (var i = 0; i < 75; i++)
                {
                    var newQ = new Dictionary<long, long>();
                    foreach (var n in startQ.Keys)
                    {
                        var (a, b) = Step(n);
                        if (!newQ.TryAdd(a, startQ[n]))
                        {
                            newQ[a] += startQ[n];
                        }
                    
                        if (!b.HasValue) continue;

                        if (!newQ.TryAdd(b.Value, startQ[n]))
                        {
                            newQ[b.Value] += startQ[n];
                        }
                    }

                    startQ = newQ;
                }

                count += startQ.Aggregate(BigInteger.Zero, (c, n) => c + n.Value);
            }

            return count.ToString();
        }

        private static (long A, long? B) Step(long number)
        {
            if (number == 0)
            {
                return (1, null);
            }

            var numberString = number.ToString();
            if (numberString.Length % 2 == 0)
            {
                var half = numberString.Length / 2;
                var firstHalf = long.Parse(numberString[..half]);
                var secondHalf = long.Parse(numberString[half..]);

                return (firstHalf, secondHalf);
            }

            return (number * 2024, null);
        }
    }
}