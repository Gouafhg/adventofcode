﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2024
{
    public class Day01 : Day
    {
        //Globals
        private readonly List<(int Left, int Right)> _input;
        private List<int> left, right;

        //Constructor
        public Day01()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\Users\hanna\source\repos\adventofcode\2019-\data\2024\day01.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"2024\day01.txt");

            //Test case
            var test1 = @"3   4
4   3
2   5
1   3
3   9
3   3";

            while (file.Contains("  ")) file = file.Replace("  ", " ");
            while (test1.Contains("  ")) test1= test1.Replace("  ", " ");

            _input = file
                .GetLines()
                .Select(x => x.Split(' '))
                .Select(x => (int.Parse(x[0]), int.Parse(x[1])))
                .ToList();

            left = _input.Select(x => x.Left).ToList();
            right = _input.Select (x => x.Right).ToList();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            left = [.. left.OrderBy(x => x)];
            right = [.. right.OrderBy(x => x)];

            long diff = 0;
            for (var i = 0; i < left.Count; ++i)
            {
                diff += Math.Abs(right[i] - left[i]);
            }

            return diff.ToString();
        }

        protected override string Part2()
        {
            long sim = 0;
            for (var i = 0; i < left.Count; ++i)
            {
                var value = left[i];
                sim += value * right.Count(x=> x == value);
            }

            return sim.ToString();
        }
    }
}
