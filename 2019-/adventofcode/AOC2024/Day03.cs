﻿using adventofcode.Utilities;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace adventofcode.AOC2024
{
    public class Day03 : Day
    {
        //Globals
        private readonly string _input;
        private readonly string _input2;

        //Constructor
        public Day03()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\Users\hanna\source\repos\adventofcode\2019-\data\2024\day03.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"2024\day01.txt");

            //Test case
            var test1 = @"xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))";
            var test2 = @"xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))";

            _input = file;

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var regexString = "mul\\((\\d*\\,\\d*)\\)";
            var arr = Regex
                .Matches(_input, regexString)
                .Cast<Match>()
                .Select(m => m.Value)
                .Select(x => x.Split("mul(")[1].Split(')')[0].Split(',').Select(int.Parse).ToList())
                .ToArray();

            long result = 0;
            foreach (var item in arr) {
                result += item[0] * item[1];
            }

            return result.ToString();
        }

        protected override string Part2()
        {
            var regexString = "mul\\((\\d*\\,\\d*)\\)";
            var dontsplt = new string(
                [.. _input.GetLines()
                    .Join("")
                    .Replace("don't()", Environment.NewLine + "don't() - ")
                    .Replace("do()", Environment.NewLine + "do() - ")
                    .GetLines()
                    .Where(x => !x.StartsWith("don't()"))
                    .Join("")]);

            var arr = Regex
                .Matches(dontsplt, regexString)
                .Cast<Match>()
                .Select(m => m.Value)
                .Select(x => x.Split("mul(")[1].Split(')')[0].Split(',').Select(int.Parse).ToList())
                .ToArray();

            long result = 0;
            foreach (var item in arr)
            {
                result += item[0] * item[1];
            }

            return result.ToString();
        }
    }
}

