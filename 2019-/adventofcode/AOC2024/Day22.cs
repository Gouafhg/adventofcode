﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Security.AccessControl;
using adventofcode.Utilities;
using adventofcode.Utilities.Extensions;

namespace adventofcode.AOC2024
{
    public class Day22 : Day
    {
        //Globals
        private List<string> _input;

        public Day22()
        {
            //Init globals
            Part = 1;
            OnlyOnePart = false;
        }

        //Reset data
        public override void Reset()
        {
            //Setup file
            var file = ToolsFile.ReadFile(@"C:\Utveckling\TheGang\adventofcode\2019-\data\2024\day22.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test0 = @"123";

            var test1 = @"1
10
100
2024";
            var test2 = @"1
2
3
2024";

            _input = file.GetLines();

            //Do Pre-stuff
        }

        private static BigInteger MixAndPrune(BigInteger a, BigInteger b)
        {
            return (a ^ b) % 16777216;
        }

        private static BigInteger Next(BigInteger prev)
        {
            var a = MixAndPrune(prev, prev * 64);
            a = MixAndPrune(a, a / 32);
            a = MixAndPrune(a, a * 2048);
            return a;
        }

        protected override string Part1()
        {
            BigInteger result = 0;
            foreach (var x in _input)
            {
                var secret = BigInteger.Parse(x);
                for (var j = 1; j < 2000; j++)
                {
                    secret = Next(secret);
                }

                result += secret;
            }

            return result.ToString();
        }

        protected override string Part2()
        {
            var buyerChanges = _input.ToDictionary(x => x.ToBigInt(), _ => new List<(int Price, int Change)>());
            foreach (var x in _input)
            {
                var originalSecret = BigInteger.Parse(x);
                var currentSecret = originalSecret;

                var last = (int)(currentSecret % 10);

                while (buyerChanges[originalSecret].Count < 2000)
                {
                    currentSecret = Next(currentSecret);
                    var current = (int)(currentSecret % 10);
                    buyerChanges[originalSecret].Add((current, current - last));
                    last = current;
                }
            }

            var buyerChangesAsString = buyerChanges.ToDictionary(x => x.Key, x => x.Value.Select(y => y.Change >= 0 ? $"+{y.Change}" : y.Change.ToString()).Join());

            Dictionary<string, BigInteger> patternValues = [];

            foreach (var buyer in buyerChangesAsString.Keys)
            {
                for (var i = 0; i < buyerChangesAsString[buyer].Length - 8; i += 2)
                {
                    var pattern = buyerChangesAsString[buyer].Substring(i, 8);

                    if (patternValues.ContainsKey(pattern))
                    {
                        continue;
                    }

                    ConcurrentDictionary<BigInteger, int> buyerPatternValues = new();

                    buyerChangesAsString.Keys.AsParallel().ForAll(otherBuyer =>
                    {
                        var firstIndexOfPatternInString = buyerChangesAsString[otherBuyer].IndexOf(pattern, StringComparison.Ordinal);
                        if (firstIndexOfPatternInString == -1) return;

                        var intIndex = (firstIndexOfPatternInString + 6) / 2;
                        var value = buyerChanges[otherBuyer][intIndex].Price;
                        buyerPatternValues[otherBuyer] = value;
                    });

                    patternValues[pattern] = buyerPatternValues.Values.Sum();
                }
            }

            var maxPattern = patternValues.OrderByDescending(x => x.Value).First();

            return maxPattern.Value.ToString();
        }
    }
}