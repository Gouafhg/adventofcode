using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using adventofcode.Utilities;
using adventofcode.Utilities.Extensions;

namespace adventofcode.AOC2024
{
    public class Day17 : Day
    {
        //Globals
        private List<string> _input;
        private OpcodeComputer _computer;

        public Day17()
        {
            //Init globals
            Part = 1;
            OnlyOnePart = false;
        }

        //Reset data
        public override void Reset()
        {
            //Setup file
            var file = ToolsFile.ReadFile(@"C:\Utveckling\TheGang\adventofcode\2019-\data\2024\day17.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test1 = @"Register A: 729
Register B: 0
Register C: 0

Program: 0,1,5,4,3,0";

            var test2 = @"Register A: 2024
Register B: 0
Register C: 0

Program: 0,3,5,4,3,0";

            _input = file.GetLines();

            //Do Pre-stuff
            var regA = _input[0].Split(":")[1].ToBigInt();
            var regB = _input[1].Split(":")[1].ToBigInt();
            var regC = _input[2].Split(":")[1].ToBigInt();
            var instructions = _input.Skip(3).First().Split(':')[1].Split(',').Select(y => y.ToInt()).ToArray();

            _computer = new OpcodeComputer(regA, regB, regC, instructions);
        }

        protected override string Part1()
        {
            _computer.Execute();
            return _computer.Outputs.Select(x => x.ToString()).Join(",");
        }

        protected override string Part2()
        {
            var originalInstructions = _computer.Instructions.ToList();

            var cycle = new List<int>();
            var a = BigInteger.MinusOne;
            do
            {
                a += 1;
                _computer.ClearOutputs();
                _computer.SetA(a);
                _computer.Execute();
                
                cycle.Add(_computer.Outputs.First());
                if (cycle.Count <= 10) continue;
                
                var rev = cycle.AsEnumerable().Reverse().Take(10).Reverse().ToList();
                if (rev.SequenceEqual(cycle.Take(10)))
                {
                    break;
                }

            } while (!_computer.Outputs.SequenceEqual(originalInstructions));

            var result = BigInteger.Pow(2, (originalInstructions.Count - 1) * 3);

            _computer.SetA(result);
            _computer.Execute();

            do
            {
                for (var j = _computer.Outputs.Count - 1; j >= 0; j--)
                {
                    if (originalInstructions[j] == _computer.Outputs[j]) continue;
                    
                    result += BigInteger.Pow(2, j * 3);
                    break;
                }
                
                _computer.SetA(result);
                _computer.Execute();
            } while (!originalInstructions.SequenceEqual(_computer.Outputs));

            return result.ToString();
        }

        private class OpcodeComputer(BigInteger a, BigInteger b, BigInteger c, IEnumerable<int> instructions)
        {
            private BigInteger A { get; set; } = a;
            private BigInteger B { get; set; } = b;
            private BigInteger C { get; set; } = c;
            public int[] Instructions { get; } = instructions.ToArray();

            public List<int> Outputs = [];

            public void ClearOutputs() => Outputs = [];
            public void SetA(BigInteger a) => A = a;

            private BigInteger GetCombo(int v)
            {
                return v switch
                {
                    0 => 0,
                    1 => 1,
                    2 => 2,
                    3 => 3,
                    4 => A,
                    5 => B,
                    6 => C,
                    7 => long.MaxValue,
                    _ => throw new ArgumentOutOfRangeException(nameof(v), v, null)
                };
            }

            public void Execute()
            {
                ClearOutputs();
                
                for (var position = 0; position < Instructions.Length; position += 2)
                {
                    var opCode = Instructions[position];
                    var op = Instructions[position + 1];
                    switch (opCode)
                    {
                        case 0:
                        {
                            //adv
                            var n = A;
                            var d = BigInteger.Pow(2, (int)GetCombo(op));
                            var v = n / d;
                            A = v;
                        }
                            break;
                        case 1:
                        {
                            //bxl
                            var v = B ^ op;
                            B = v;
                        }
                            break;
                        case 2:
                        {
                            //bst
                            var v = GetCombo(op) % 8;
                            B = v;
                        }
                            break;
                        case 3:
                        {
                            //jnz
                            if (A == 0) continue;
                            position = op - 2;
                        }
                            break;
                        case 4:
                        {
                            //bxc
                            var v = B ^ C;
                            B = v;
                        }
                            break;
                        case 5:
                        {
                            //out
                            var v = (int)(GetCombo(op) % 8);
                            Outputs.Add(v);
                        }
                            break;
                        case 6:
                        {
                            //bdv
                            var n = A;
                            var d = BigInteger.Pow(2, (int)GetCombo(op));
                            var v = n / d;
                            B = v;
                        }
                            break;
                        case 7:
                        {
                            //cdv
                            var n = A;
                            var d = BigInteger.Pow(2, (int)GetCombo(op));
                            var v = n / d;
                            C = v;
                        }
                            break;
                        default:
                            throw new Exception($"Unknown opcode: {opCode}");
                    }
                }
            }
        }
    }
}