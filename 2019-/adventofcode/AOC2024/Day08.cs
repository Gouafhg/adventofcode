using adventofcode.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2024
{
    public class Day08 : Day
    {
        //Globals
        private readonly List<string> _input;

        private int resX, resY;
        private readonly char[,] _grid;
        
        //Constructor
        public Day08()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\Utveckling\TheGang\adventofcode\2019-\data\2024\day08.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"2024\day01.txt");

            //Test case
            var test1 = @"............
........0...
.....0......
.......0....
....0.......
......A.....
............
............
........A...
.........A..
............
............";

            _input = file.GetLines();

            //Do Pre-stuff
            _grid = new char[_input[0].Length, _input.Count];
            for (var y = 0; y < _input.Count; y++)
            {
                for (var x = 0; x < _input[0].Length; x++)
                {
                    _grid[x, y] = _input[y][x];
                }
            }
            
            resX = _grid.GetLength(0);
            resY = _grid.GetLength(1);
        }

        private List<(int X, int Y)> GetPoints(char c)
        {
            var result = new List<(int, int)>();
            for (var y = 0; y < resY; y++)
            {
                for (var x = 0; x < resX; x++)
                {
                    if (_grid[x, y] == c)
                    {
                        result.Add((x, y));
                    }
                }
            }

            return result;
        }

        private (int X, int Y) GetAntinode((int X, int Y) a, (int X, int Y) b)
        {
            var diffX = b.X - a.X;
            var diffY = b.Y - a.Y;

            return (b.X + diffX, b.Y + diffY);
        }
        
        private List<(int X, int Y)> GetAntinodes((int X, int Y) a, (int X, int Y) b)
        {
            var result = new List<(int, int)>();
            
            var diffX = b.X - a.X;
            var diffY = b.Y - a.Y;
            
            var antinode = (X: b.X + diffX, Y: b.Y + diffY);
            while (antinode.X >= 0 && antinode.X < resX && antinode.Y >= 0 && antinode.Y < resY)
            {
                result.Add(antinode);
                antinode = (antinode.X + diffX, antinode.Y + diffY);
            }

            return result;
        }
        
        protected override string Part1()
        {
            var antennas = _input.Join().Distinct().ToList();
            antennas.Remove('.');

            var antinodes = new Dictionary<(int X, int Y), List<char>>();
            foreach (var antenna in antennas)
            {
                var antennaPoints = GetPoints(antenna);
                foreach (var pair in antennaPoints.Permute(2))
                {
                    var antinode = GetAntinode(pair.First(), pair.Last());
                    if (antinode.X < 0 || antinode.X >= resX || antinode.Y < 0 || antinode.Y >= resY)
                    {
                        continue;
                    }

                    if (antinodes.TryGetValue((antinode.X, antinode.Y), out var antinodeList))
                    {
                        antinodeList.Add(antenna);
                    }
                    else
                    {
                        antinodes[(antinode.X, antinode.Y)] = [antenna];
                    }
                }
            }
            
            return antinodes.Keys.Count.ToString();
        }

        protected override string Part2()
        {
            var antennas = _input.Join().Distinct().ToList();
            antennas.Remove('.');

            var antinodes = new Dictionary<(int X, int Y), List<char>>();
            foreach (var antenna in antennas)
            {
                var antennaPoints = GetPoints(antenna);
                if (antennaPoints.Count > 1)
                {
                    foreach (var antinode in antennaPoints)
                    {
                        if (antinodes.TryGetValue((antinode.X, antinode.Y), out var antinodeList))
                        {
                            antinodeList.Add(antenna);
                        }
                        else
                        {
                            antinodes[(antinode.X, antinode.Y)] = [antenna];
                        }
                    }
                }
                
                foreach (var pair in antennaPoints.Permute(2))
                {
                    var antennaPairAntinodes = GetAntinodes(pair.First(), pair.Last());

                    foreach (var antinode in antennaPairAntinodes)
                    {
                        if (antinodes.TryGetValue((antinode.X, antinode.Y), out var antinodeList))
                        {
                            antinodeList.Add(antenna);
                        }
                        else
                        {
                            antinodes[(antinode.X, antinode.Y)] = [antenna];
                        }
                    }
                }
            }
            
            return antinodes.Keys.Count.ToString();
        }
    }
}