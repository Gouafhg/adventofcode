using System;
using adventofcode.Utilities;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace adventofcode.AOC2024
{
    public class Day15 : Day
    {
        //Globals
        private List<string> _input;

        private LanternfishMap _map;
        private char[] _moves;

        //Constructor
        public Day15()
        {
            //Init globals
            Part = 1;
            OnlyOnePart = false;
        }
        
        public override void Reset()
        {
            //Setup file
            var file = ToolsFile.ReadFile(@"C:\Utveckling\TheGang\adventofcode\2019-\data\2024\day15.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"2024\day01.txt");

            //Test case
            var test1 = @"##########
#..O..O.O#
#......O.#
#.OO..O.O#
#..O@..O.#
#O#..O...#
#O..O..O.#
#.OO.O.OO#
#....O...#
##########

<vv>^<v^>v>^vv^v>v<>v^v<v<^vv<<<^><<><>>v<vvv<>^v^>^<<<><<v<<<v^vv^v>^
vvv<<^>^v^^><<>>><>^<<><^vv^^<>vvv<>><^^v>^>vv<>v<<<<v<^v>^<^^>>>^<v<v
><>vv>v^v^<>><>>>><^^>vv>v<^^^>>v^v^<^^>v^^>v^<^v>v<>>v^v^<v>v^^<^^vv<
<<v<^>>^^^^>>>v^<>vvv^><v<<<>^^^vv^<vvv>^>v<^^^^v<>^>vvvv><>>v^<<^^^^^
^><^><>>><>^^<<^^v>>><^<v>^<vv>>v>>>^v><>^v><<<<v>>v<v<v>vvv>^<><<>^><
^>><>^v<><^vvv<^^<><v<<<<<><^v<<<><<<^^<v<^^^><^>>^<v^><<<^>>^v<v^v<v^
>^>>^v>vv>^<<^v<>><<><<v<<v><>v<^vv<<<>^^v^>^^>>><<^v>>v^v><^^>>^<>vv^
<><^^>^^^<><vvvvv^v<v<<>^v<v>v<<^><<><<><<<^^<<<^<<>><<><^^^>^^<>^>v<>
^^>vv<^v^v<vv>^<><v<^v>^^^>>>^^vvv^>vvv<>>>^<^>>>>>^<<^v>^vvv<>^<><<v>
v^^>>><<^^<>>^v^<v^vv<>v^<<>^<^v^v><^<<<><<^<v><v<>vv>>v><v^<vv<>v^<<^";

            var test2 = @"########
#..O.O.#
##@.O..#
#...O..#
#.#.O..#
#...O..#
#......#
########

<^^>>>vv<v>>v<<";

            var test3 = @"#######
#...#.#
#.....#
#..OO@#
#..O..#
#.....#
#######

<vv<<^^<<^^";

            _input = file.GetLines();

            //Do Pre-stuff
            _moves = _input.SkipWhile(x => x.StartsWith('#')).Join().ToArray();
        }

        protected override string Part1()
        {
            _map = new LanternfishMap(_input.TakeWhile(x => x.StartsWith('#')).ToArray(), 1);
            foreach (var m in _moves)
            {
                var direction = GetDirection(m);
                _map.Robot.Move(direction, null);
            }

            var p = _map.Boxes.Aggregate((long)0, (current, box) => current + (box.Position.Y * 100 + box.Position.X));

            return p.ToString();
        }

        protected override string Part2()
        {
            _map = new LanternfishMap(_input.TakeWhile(x => x.StartsWith('#')).ToArray(), 2);
            foreach (var m in _moves)
            {
                var direction = GetDirection(m);
                _map.Robot.Move(direction, null);
            }

            var p = (long)0;
            var done = new HashSet<LanternfishMap.Box>();
            foreach (var box in _map.Boxes)
            {
                if (!done.Add(box))
                {
                    continue;
                }

                done.Add(box.LinkedItem as LanternfishMap.Box);

                if (box.LinkedItem.Position.X < box.Position.X)
                {
                    p += box.LinkedItem.Position.Y * 100 + box.LinkedItem.Position.X;
                    continue;
                }

                p += box.Position.Y * 100 + box.Position.X;
            }

            // 1470041 too low
            return p.ToString();
        }

        private static (int X, int Y) GetDirection(char c)
        {
            return c switch
            {
                '^' => (0, -1),
                'v' => (0, 1),
                '<' => (-1, 0),
                '>' => (1, 0),
                _ => throw new Exception("Invalid direction")
            };
        }

        private class LanternfishMap
        {
            private List<ThingWithPosition> Things { get; } = [];
            public IEnumerable<Box> Boxes => Things.OfType<Box>();
            public LanternfishRobot Robot { get; }

            [DebuggerDisplay("Position = {Position}, LinkedItem = {LinkedItem != null}")]
            public abstract class ThingWithPosition
            {
                public (int X, int Y) Position { get; set; }
                public LanternfishMap Map { get; init; }
                public ThingWithPosition LinkedItem { get; set; }

                private void GetAllConnected(Box box, (int X, int Y) direction, HashSet<Box> connected)
                {
                    connected.Add(box);
                    while (true)
                    {
                        var newPosition = (box.Position.X + direction.X, box.Position.Y + direction.Y);
                        var blockingThing = Map.Things.FirstOrDefault(x => x.Position == newPosition);
                        if (blockingThing is Box blockingBox)
                        {
                            GetAllConnected(blockingBox, direction, connected);
                        }

                        if (box.LinkedItem is not null && connected.Add(box.LinkedItem as Box))
                        {
                            box = box.LinkedItem as Box;
                            continue;
                        }

                        break;
                    }
                }

                public void Move((int X, int Y) direction, List<ThingWithPosition> evaluating)
                {
                    var newPosition = (Position.X + direction.X, Position.Y + direction.Y);

                    var blockingThing = Map.Things.FirstOrDefault(x => x.Position == newPosition);
                    switch (blockingThing)
                    {
                        case Wall:
                            return;
                        case Box box:
                            var allBlocking = new HashSet<Box>();
                            GetAllConnected(box, direction, allBlocking);

                            if (allBlocking.Any(x => !x.CanMove(direction)))
                            {
                                return;
                            }

                            foreach (var blockingBox in allBlocking)
                            {
                                blockingBox.Position = (blockingBox.Position.X + direction.X,
                                    blockingBox.Position.Y + direction.Y);
                            }

                            Position = newPosition;
                            return;
                        case LanternfishRobot:
                            throw new Exception("FLERA LANTERNFISH");
                        default:

                            Position = newPosition;
                            return;
                    }
                }

                private bool CanMove((int X, int Y) direction)
                {
                    var newPosition = (Position.X + direction.X, Position.Y + direction.Y);
                    var thing = Map.Things.FirstOrDefault(x => x.Position == newPosition);
                    return thing switch
                    {
                        Wall => false,
                        Box => thing.CanMove(direction),
                        LanternfishRobot => throw new Exception("FLERA LANTERNFISH"),
                        _ => true
                    };
                }
            }

            public class LanternfishRobot : ThingWithPosition { }

            public class Box : ThingWithPosition { }

            private class Wall : ThingWithPosition { }

            public LanternfishMap(string[] lines, int part)
            {
                for (var y = 0; y < lines.Length; y++)
                {
                    for (var x = 0; x < lines[y].Length; x++)
                    {
                        switch (lines[y][x])
                        {
                            case '#':
                                if (part == 1)
                                {
                                    Things.Add(new Wall { Position = (x, y), Map = this });
                                    break;
                                }

                                var w1 = new Wall { Position = (x * 2, y), Map = this };
                                var w2 = new Wall { Position = (x * 2 + 1, y), Map = this };
                                Things.AddRange([w1, w2]);
                                break;
                            case '.':
                                break;
                            case 'O':
                                if (part == 1)
                                {
                                    Things.Add(new Box { Position = (x, y), Map = this });
                                    break;
                                }

                                var b1 = new Box { Position = (x * 2, y), Map = this };
                                var b2 = new Box { Position = (x * 2 + 1, y), Map = this };
                                b1.LinkedItem = b2;
                                b2.LinkedItem = b1;
                                Things.AddRange([b1, b2]);
                                break;
                            case '@':
                                if (part == 1)
                                {
                                    Robot = new LanternfishRobot { Position = (x, y), Map = this };
                                    Things.Add(Robot);
                                    break;
                                }

                                Robot = new LanternfishRobot { Position = (x * 2, y), Map = this };
                                Things.Add(Robot);
                                break;
                        }
                    }
                }
            }
        }
    }
}