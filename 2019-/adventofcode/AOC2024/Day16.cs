using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.JavaScript;
using System.Threading.Tasks;
using adventofcode.Utilities;

namespace adventofcode.AOC2024
{
    public class Day16 : Day
    {
        //Globals
        private List<string> _input;
        private char[,] _grid;
        private int _resX, _resY;

        private enum Direction
        {
            Up,
            Down,
            Left,
            Right
        }

        private (int X, int Y) _start, _end;

        public Day16()
        {
            //Init globals
            Part = 1;
            OnlyOnePart = false;
        }

        //Reset data
        public override void Reset()
        {
            //Setup file
            var file = ToolsFile.ReadFile(@"C:\Utveckling\TheGang\adventofcode\2019-\data\2024\day16.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test1 = @"###############
#.......#....E#
#.#.###.#.###.#
#.....#.#...#.#
#.###.#####.#.#
#.#.#.......#.#
#.#.#####.###.#
#...........#.#
###.#.#####.#.#
#...#.....#.#.#
#.#.#.###.#.#.#
#.....#...#.#.#
#.###.#.#.#.#.#
#S..#.....#...#
###############";

            var test2 = @"#################
#...#...#...#..E#
#.#.#.#.#.#.#.#.#
#.#.#.#...#...#.#
#.#.#.#.###.#.#.#
#...#.#.#.....#.#
#.#.#.#.#.#####.#
#.#...#.#.#.....#
#.#.#####.#.###.#
#.#.#.......#...#
#.#.###.#####.###
#.#.#...#.....#.#
#.#.#.#####.###.#
#.#.#.........#.#
#.#.#.#########.#
#S#.............#
#################";

            _input = file.GetLines();

            //Do Pre-stuff
            _resX = _input[0].Length;
            _resY = _input.Count;
            _grid = new char[_resX, _resY];
            for (var y = 0; y < _resY; y++)
            {
                for (var x = 0; x < _resX; x++)
                {
                    _grid[x, y] = _input[y][x];
                    if (_input[y][x] == 'S')
                    {
                        _start = (x, y);
                    }

                    if (_input[y][x] == 'E')
                    {
                        _end = (x, y);
                    }
                }
            }
        }

        private long GetSteps(int startX, int startY, Direction startDir, out Dictionary<(int X, int Y, Direction), long> cache)
        {
            cache = new Dictionary<(int X, int Y, Direction), long>();
            var q = new Queue<(int x, int y, Direction dir, int steps)>();
            
            q.Enqueue((startX, startY, startDir, 0));
            while (q.Any())
            {
                var (x, y, dir, steps) = q.Dequeue();
                
                if (x < 0 || x >= _resX || y < 0 || y >= _resY || _grid[x, y] == '#')
                {
                    continue;
                }

                if (cache.TryGetValue((x, y, dir), out var s) && s <= steps)
                {
                    continue;
                }

                cache[(x, y, dir)] = steps;

                switch (dir)
                {
                    case Direction.Left:
                    {
                        q.Enqueue((x - 1, y, Direction.Left, steps + 1));
                        q.Enqueue((x, y, Direction.Up, steps + 1000));
                        q.Enqueue((x, y, Direction.Down, steps + 1000));
                        break;
                    }
                    case Direction.Right:
                    {
                        q.Enqueue((x + 1, y, Direction.Right, steps + 1));
                        q.Enqueue((x, y, Direction.Up, steps + 1000));
                        q.Enqueue((x, y, Direction.Down, steps + 1000));
                        break;
                    }
                    case Direction.Up:
                    {
                        q.Enqueue((x, y - 1, Direction.Up, steps + 1));
                        q.Enqueue((x, y, Direction.Left, steps + 1000));
                        q.Enqueue((x, y, Direction.Right, steps + 1000));
                        break;
                    }
                    case Direction.Down:
                    {
                        q.Enqueue((x, y + 1, Direction.Down, steps + 1));
                        q.Enqueue((x, y, Direction.Left, steps + 1000));
                        q.Enqueue((x, y, Direction.Right, steps + 1000));
                        break;
                    }
                }
            }
            return cache.Where(x => (x.Key.X, x.Key.Y) == _end).Min(x => x.Value);
        }
        
        protected override string Part1()
        {
            var s = GetSteps(_start.X, _start.Y, Direction.Right, out _);
            return s.ToString();
        }

        protected override string Part2()
        {
            var s = GetSteps(_start.X, _start.Y, Direction.Right, out var cache);

            var end = cache.Single(x => (x.Key.X, x.Key.Y) == _end && x.Value == s);

            var partOfShortestPaths = new HashSet<(int X, int Y)>();
            
            var q = new Queue<(int X, int Y, long Steps)>();
            
            q.Enqueue((end.Key.X, end.Key.Y, end.Value));
            while (q.Any())
            {
                var (x, y, steps) = q.Dequeue();
                if (x < 0 || x >= _resX || y < 0 || y >= _resY || _grid[x, y] == '#')
                {
                    continue;
                }

                partOfShortestPaths.Add((x, y));

                var left = (X: x - 1, Y: y);
                var right = (X: x + 1, Y: y);
                var up = (X: x, Y: y - 1);
                var down = (X: x, Y: y + 1);
                
                var leftValue = cache.GetValueOrDefault((left.X, left.Y, Direction.Right), long.MaxValue);
                var rightValue = cache.GetValueOrDefault((right.X, right.Y, Direction.Left), long.MaxValue);
                var upValue = cache.GetValueOrDefault((up.X, up.Y, Direction.Down), long.MaxValue);
                var downValue = cache.GetValueOrDefault((down.X, down.Y, Direction.Up), long.MaxValue);

                if (leftValue == steps - 1)
                {
                    q.Enqueue((left.X, left.Y, leftValue));
                }
                if (rightValue == steps - 1)
                {
                    q.Enqueue((right.X, right.Y, rightValue));
                }
                if (upValue == steps - 1)
                {
                    q.Enqueue((up.X, up.Y, upValue));
                }
                if (downValue == steps - 1)
                {
                    q.Enqueue((down.X, down.Y, downValue));
                }
                
                var thisRotLeft = cache.GetValueOrDefault((x, y, Direction.Left), long.MaxValue);
                var thisRotRight = cache.GetValueOrDefault((x, y, Direction.Right), long.MaxValue);
                var thisRotUp = cache.GetValueOrDefault((x, y, Direction.Up), long.MaxValue);
                var thisRotDown = cache.GetValueOrDefault((x, y, Direction.Down), long.MaxValue);
                
                if (thisRotLeft == steps - 1000)
                {
                    q.Enqueue((x, y, thisRotLeft));
                }
                if (thisRotRight == steps - 1000)
                {
                    q.Enqueue((x, y, thisRotRight));
                }
                if (thisRotUp == steps - 1000)
                {
                    q.Enqueue((x, y, thisRotUp));
                }
                if (thisRotDown == steps - 1000)
                {
                    q.Enqueue((x, y, thisRotDown));
                }
            }

            return partOfShortestPaths.Count.ToString();
        }
    }
}