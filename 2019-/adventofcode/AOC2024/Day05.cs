using System;
using adventofcode.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2024
{
    public class Day05 : Day
    {
        //Globals
        private readonly List<string> _input;
        private readonly List<List<int>> _updates;

        private readonly ILookup<int, int> _sortingRulesBefore;
        private readonly UpdateComparer _comparer;

        //Constructor
        public Day05()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\Utveckling\TheGang\adventofcode\2019-\data\2024\day05.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"2024\day01.txt");

            //Test case
            var test1 = @"47|53
97|13
97|61
97|47
75|29
61|13
75|53
29|13
97|29
53|29
61|53
97|53
61|29
47|13
75|47
97|75
47|61
75|61
47|29
75|13
53|13

75,47,61,53,29
97,61,53,29,13
75,29,13
75,97,47,61,53
61,13,29
97,13,75,29,47";

            _input = file.Split(Environment.NewLine).ToList();
            //Do Pre-stuff

            _sortingRulesBefore = _input
                .TakeWhile(x => !string.IsNullOrWhiteSpace(x))
                .Select(x => x.Split('|'))
                .ToLookup(x => int.Parse(x[0]), x => int.Parse(x[1]));
            
            _comparer = new UpdateComparer(_sortingRulesBefore);

            _updates = _input.SkipWhile(x => !string.IsNullOrWhiteSpace(x)).Skip(1)
                .Select(x => x.Split(',').Select(int.Parse).ToList()).ToList();
        }

        protected override string Part1()
        {
            var correctUpdates = new List<List<int>>();

            foreach (var update in _updates)
            {
                var isCorrect = true;
                foreach (var x in update)
                {
                    var shouldBeAfter = _sortingRulesBefore[x].Where(update.Contains).ToList();
                    if (shouldBeAfter.Any(y => update.IndexOf(y) < update.IndexOf(x)))
                    {
                        isCorrect = false;
                    }
                }

                if (isCorrect)
                {
                    correctUpdates.Add(update);
                }
            }

            var middleNumbers = correctUpdates.Select(x => x[x.Count / 2]).ToList();

            return middleNumbers.Sum().ToString();
        }

        protected override string Part2()
        {
            var incorrectUpdates = _updates.Where(x => !IsCorrect(x)).ToList();
            var correctedUpdate = incorrectUpdates.Select(SortUpdate).ToList();

            var middleNumbers = correctedUpdate.Select(x => x[x.Count / 2]).ToList();
            return middleNumbers.Sum().ToString();
        }

        private bool IsCorrect(List<int> update)
        {
            foreach (var x in update)
            {
                var shouldBeAfter = _sortingRulesBefore[x].Where(update.Contains).ToList();
                if (shouldBeAfter.Any(y => update.IndexOf(y) < update.IndexOf(x)))
                {
                    return false;
                }
            }

            return true;
        }

        private List<int> SortUpdate(List<int> update)
        {
            return update.OrderBy(x => x, _comparer).ToList();
        }

        private class UpdateComparer(ILookup<int, int> sortingRulesBefore) : IComparer<int>
        {
            public int Compare(int a, int b)
            {
                if (sortingRulesBefore[a].Contains(b))
                {
                    return -1;
                }
                
                return sortingRulesBefore[b].Contains(a) ? 1 : 0;
            }
        }
    }
}