using System;
using adventofcode.Utilities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace adventofcode.AOC2024
{
    public class Day14 : Day
    {
        //Globals
        private List<string> _input;
        private readonly List<Robot> _robots = [];

        private const int W = 101;
        private const int H = 103;
        // private const int W = 11;
        // private const int H = 7;

        public Day14()
        {
            //Init globals
            Part = 1;
            OnlyOnePart = false;
        }
        
        //Constructor
        public override void Reset()
        {

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\Utveckling\TheGang\adventofcode\2019-\data\2024\day14.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"2024\day01.txt");

            //Test case
            var test1 = @"p=0,4 v=3,-3
p=6,3 v=-1,-3
p=10,3 v=-1,2
p=2,0 v=2,-1
p=0,0 v=1,3
p=3,0 v=-2,-2
p=7,6 v=-1,-3
p=3,0 v=-1,-2
p=9,3 v=2,3
p=7,3 v=-1,2
p=2,4 v=2,-3
p=9,5 v=-3,-3";

            _input = file.GetLines();

            //Do Pre-stuff
            _robots.Clear();
            foreach (var l in _input)
            {
                var parts = l.Split(' ');
                var p = parts[0].Split(',');
                var v = parts[1].Split(',');

                var x = int.Parse(p[0][2..]);
                var y = int.Parse(p[1]);

                var xv = int.Parse(v[0][2..]);
                var yv = int.Parse(v[1]);

                var r = new Robot { X = x, Y = y, Xv = xv, Yv = yv };
                _robots.Add(r);
            }
        }

        protected override string Part1()
        {
            for (var i = 0; i < 100; i++)
            {
                foreach (var r in _robots)
                {
                    r.Step();
                }
            }

            var positionCount = _robots.GroupBy(x => (x.X, x.Y)).Select(x => (Position: x.Key, Count: x.Count()))
                .ToList();

            var q1 = 0;
            var q2 = 0;
            var q3 = 0;
            var q4 = 0;

            foreach (var r in positionCount)
            {
                switch (r.Position)
                {
                    case { X: < W / 2, Y: < H / 2 }:
                        q1 += r.Count;
                        break;
                    case { X: > W / 2, Y: < H / 2 }:
                        q2 += r.Count;
                        break;
                    case { X: < W / 2, Y: > H / 2 }:
                        q3 += r.Count;
                        break;
                    case { X: > W / 2, Y: > H / 2 }:
                        q4 += r.Count;
                        break;
                }
            }

            long s = q1 * q2 * q3 * q4;

            return s.ToString();
        }

        protected override string Part2()
        {
            for (var steps = 0; steps < W * H; steps++)
            {
                if (CheckChristmasTree(steps))
                {
                    return steps.ToString();
                }

                _robots.ForEach(x => x.Step());
            }

            return "Ingen lösning!";
        }

        private void PrintRobots(int steps)
        {
            // Thread.Sleep(50);
            for (var y = 0; y < H; y++)
            {
                for (var x = 0; x < W; x++)
                {
                    Console.Write(_robots.Any(r => (r.X, r.Y) == (x, y)) ? "#" : ".");
                }

                Console.WriteLine();
            }

            Console.WriteLine(steps);
        }

        private class Robot
        {
            public int X { get; set; }
            public int Y { get; set; }
            public int Xv { get; init; }
            public int Yv { get; init; }

            public void Step()
            {
                X += Xv;
                Y += Yv;

                X = (X + W) % W;
                Y = (Y + H) % H;
            }
        }

        private bool CheckChristmasTree(int steps)
        {
            var robotLookup = _robots.ToLookup(x => (x.X, x.Y));
            for (var y = 10; y < H; y++)
            {
                for (var x = 0; x < W / 2; x++)
                {
                    if (robotLookup[(x, y)].Any() &&
                        robotLookup[(x, y - 1)].Any() &&
                        robotLookup[(x, y - 2)].Any() &&
                        robotLookup[(x, y - 3)].Any() &&
                        robotLookup[(x, y - 4)].Any() &&
                        robotLookup[(x, y - 5)].Any() &&
                        robotLookup[(x, y - 6)].Any() &&
                        robotLookup[(x, y - 7)].Any() &&
                        robotLookup[(x, y - 8)].Any() &&
                        robotLookup[(x, y - 9)].Any() &&
                        robotLookup[(x, y - 10)].Any())
                    {
                        PrintRobots(steps);
                        Console.WriteLine();
                        Console.WriteLine();
                        return true;
                    }
                }
            }

            return false;
        }
    }
}