using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using adventofcode.Utilities;

namespace adventofcode.AOC2024
{
    public class Day20 : Day
    {
        //Globals
        private List<string> _input;
        private char[,] _grid;
        private int _resX, _resY;
        private (int X, int Y) _start, _end;
        private List<(int X, int Y)> _path;

        public Day20()
        {
            //Init globals
            Part = 1;
            OnlyOnePart = false;
        }

        //Reset data
        public override void Reset()
        {
            //Setup file
            var file = ToolsFile.ReadFile(@"C:\Utveckling\TheGang\adventofcode\2019-\data\2024\day20.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test1 = @"###############
#...#...#.....#
#.#.#.#.#.###.#
#S#...#.#.#...#
#######.#.#.###
#######.#.#...#
#######.#.###.#
###..E#...#...#
###.#######.###
#...###...#...#
#.#####.#.###.#
#.#...#.#.#...#
#.#.#.#.#.#.###
#...#...#...###
###############";

            _input = file.GetLines();

            //Do Pre-stuff
            _resX = _input[0].Length;
            _resY = _input.Count;
            _grid = new char[_resX, _resY];
            for (var y = 0; y < _resY; y++)
            {
                var line = _input[y];
                for (var x = 0; x < _resX; x++)
                {
                    _grid[x, y] = line[x];
                    if (_grid[x, y] == 'S')
                    {
                        _grid[x, y] = '.';
                        _start = (x, y);
                    }

                    if (_grid[x, y] == 'E')
                    {
                        _grid[x, y] = '.';
                        _end = (x, y);
                    }
                }
            }

            _path = StepsRequired(_start, _end);
            Debug.Assert(_path.Count == _input.Join("").Count(x => x == '.') + 2);
        }

        private List<(int X, int Y)> StepsRequired((int X, int Y) start, (int X, int Y) end)
        {
            var cameFrom = new Dictionary<(int X, int Y), (int X, int Y)?>();

            var visited = new Dictionary<(int X, int Y), int>();
            var q = new Queue<((int X, int Y)? CameFrom, (int X, int Y) Position, int CurrentSteps)>();

            q.Enqueue((null, start, 0));
            while (q.Any())
            {
                var (cameFromPosition, pos, currentSteps) = q.Dequeue();

                if (pos.X < 0 || pos.X >= _resX || pos.Y < 0 || pos.Y >= _resY)
                {
                    continue;
                }

                if (_grid[pos.X, pos.Y] == '#')
                {
                    continue;
                }

                if (visited.TryGetValue(pos, out var previousSteps) && previousSteps <= currentSteps)
                {
                    continue;
                }

                visited[pos] = currentSteps;
                cameFrom[pos] = cameFromPosition;

                if (pos == end)
                {
                    var path = new List<(int X, int Y)>();
                    (int X, int Y)? current = pos;
                    while (current != start && current is not null)
                    {
                        path.Add(current.Value);
                        current = cameFrom[current.Value];
                    }

                    path.Add(_start);
                    path.Reverse();
                    return path;
                }

                foreach (var dir in new[] { (-1, 0), (1, 0), (0, -1), (0, 1) })
                {
                    var newPos = (pos.X + dir.Item1, pos.Y + dir.Item2);
                    q.Enqueue((pos, newPos, currentSteps + 1));
                }
            }

            return null;
        }

        protected override string Part1()
        {
            BigInteger result = 0;

            var cacheDistanceEnd = new Dictionary<(int, int), int>();
            _path = StepsRequired(_start, _end);
            var cheatUsed = _path.ToDictionary(x => x, _ => new HashSet<(int, int)>());
            for (var i = 0; i < _path.Count; i++)
            {
                var tile = _path[i];

                foreach (var cheatTile in _path.Where(x => Tools.ManhattanDistance(x, tile) <= 2))
                {
                    if (cheatUsed[tile].Contains(cheatTile))
                    {
                        continue;
                    }

                    cheatUsed[tile].Add(cheatTile);

                    var tileCheatDistance = Tools.ManhattanDistance(tile, cheatTile);

                    if (!cacheDistanceEnd.TryGetValue(cheatTile, out var distanceCheatEnd))
                    {
                        distanceCheatEnd = StepsRequired(cheatTile, _end).Count - 1;
                        cacheDistanceEnd[cheatTile] = distanceCheatEnd;
                    }

                    if (!cacheDistanceEnd.TryGetValue(tile, out var distanceTileEnd))
                    {
                        distanceTileEnd = StepsRequired(tile, _end).Count - 1;
                        cacheDistanceEnd[tile] = distanceTileEnd;
                    }

                    var totalDistance = i + tileCheatDistance + distanceCheatEnd;

                    var savedSteps = (_path.Count - 1) - totalDistance;

                    if (savedSteps >= 100)
                    {
                        result++;
                    }
                }
            }

            return result.ToString();
        }

        protected override string Part2()
        {
            BigInteger result = 0;

            var cacheDistanceEnd = new Dictionary<(int, int), int>();
            _path = StepsRequired(_start, _end);
            var cheatUsed = _path.ToDictionary(x => x, _ => new HashSet<(int, int)>());
            for (var i = 0; i < _path.Count; i++)
            {
                var tile = _path[i];

                foreach (var cheatTile in _path.Where(x => Tools.ManhattanDistance(x, tile) <= 20))
                {
                    if (cheatUsed[tile].Contains(cheatTile))
                    {
                        continue;
                    }

                    cheatUsed[tile].Add(cheatTile);

                    var tileCheatDistance = Tools.ManhattanDistance(tile, cheatTile);

                    if (!cacheDistanceEnd.TryGetValue(cheatTile, out var distanceCheatEnd))
                    {
                        distanceCheatEnd = StepsRequired(cheatTile, _end).Count - 1;
                        cacheDistanceEnd[cheatTile] = distanceCheatEnd;
                    }

                    if (!cacheDistanceEnd.TryGetValue(tile, out var distanceTileEnd))
                    {
                        distanceTileEnd = StepsRequired(tile, _end).Count - 1;
                        cacheDistanceEnd[tile] = distanceTileEnd;
                    }

                    var totalDistance = i + tileCheatDistance + distanceCheatEnd;

                    var savedSteps = (_path.Count - 1) - totalDistance;

                    if (savedSteps >= 100)
                    {
                        result++;
                    }
                }
            }

            return result.ToString();
        }
    }
}