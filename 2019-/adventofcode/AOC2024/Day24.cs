using System.Collections.Generic;
using adventofcode.Utilities;

namespace adventofcode.AOC2024
{
    public class Day24 : Day
    {
        //Globals
        private List<string> _input;

        public Day24()
        {
            //Init globals
            Part = 1;
            OnlyOnePart = false;
        }
        
        //Reset data
        public override void Reset()
        {
            //Setup file
            var file = ToolsFile.ReadFile(@"C:\Utveckling\TheGang\adventofcode\2019-\data\2024\day23.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test1 = @"";

            _input = test1.GetLines();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            return "p1";
        }

        protected override string Part2()
        {
            return "p2";
        }
    }
}