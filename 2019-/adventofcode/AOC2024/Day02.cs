﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2024
{
    public class Day02 : Day
    {
        //Globals
        private readonly List<List<int>> _input;

        //Constructor
        public Day02()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\Users\hanna\source\repos\adventofcode\2019-\data\2024\day02.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"2024\day01.txt");

            //Test case
            var test1 = @"7 6 4 2 1
1 2 7 8 9
9 7 6 2 1
1 3 2 4 5
8 6 4 4 1
1 3 6 7 9";

            _input = file.GetLines().Select(x => x.Split(' ').Select(x => int.Parse(x)).ToList()).ToList();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            long safeCount = 0;
            foreach (var report in _input)
            {
                var safeReport = true;
                var inc = report[1] > report[0];

                for (var i = 1; safeReport && i < report.Count; i++)
                {
                    safeReport = safeReport && CompareLevel(report, i, i - 1, inc);
                }

                if (safeReport) safeCount++;
            }

            return safeCount.ToString();
        }

        protected override string Part2()
        {
            long safeCount = 0;
            foreach (var report in _input)
            {
                var safeReport = CompareReport(report);
                
                if (safeReport) safeCount++;
            }

            return safeCount.ToString();
        }

        private bool CompareReport(List<int> report, int skipIndex = -1)
        {
            List<int> reportToTest;
            if (skipIndex >= 0)
            {
                reportToTest = report.Where((x, i) => i != skipIndex).ToList();
            }
            else
            {
                reportToTest = report;
            }

            var safeReport = true;
            var inc = reportToTest[1] > reportToTest[0];
            for (var i = 1; safeReport && i < reportToTest.Count; ++i)
            {
                safeReport = safeReport && CompareLevel(reportToTest, i, i - 1, inc);
            }

            if (!safeReport && skipIndex < report.Count - 1)
            {
                return CompareReport(report, skipIndex + 1);
            }

            return safeReport;
        }

        private bool CompareLevel(IList<int> report, int a, int b, bool inc)
        {
            var diff = report[a] - report[b];

            if (Math.Abs(diff) > 3)
            {
                return false;
            }

            if (inc)
            {
                return diff > 0;
            }
            else
            {
                return diff < 0;
            }
        }
    }
}

