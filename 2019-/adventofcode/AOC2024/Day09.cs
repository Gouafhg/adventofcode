using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using adventofcode.Utilities;

namespace adventofcode.AOC2024
{
    public class Day09 : Day
    {
        private class FilePart
        {
            public int Id { get; init; }
            public int Length { get; set; }
            public bool FreeSpace => Id == -1;
        }

        //Globals
        private readonly string _input;
        private readonly List<FilePart> _fileParts;

        //Constructor
        public Day09()
        {
            //Init globals
            Part = 2;
            OnlyOnePart = true;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\Utveckling\TheGang\adventofcode\2019-\data\2024\day09.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"2024\day01.txt");

            //Test case
            var test1 = @"2333133121414131402";

            _input = file;

            //Do Pre-stuff
            var id = 0;
            
            _fileParts = _input.Select((x, i) => new FilePart
            {
                Id = i % 2 == 0 ? id++ : -1,
                Length = int.Parse(x.ToString()),
            }).Where(x => x.Length > 0).ToList();
        }

        protected override string Part1()
        {
            var id = 0;
            List<int> blocks = [];
            for (var i = 0; i < _input.Length; ++i)
            {
                var freeSpace = i % 2 == 1;
                var length = int.Parse(_input[i].ToString());

                blocks.AddRange(Enumerable.Range(0, length).Select(_ => freeSpace ? -1 : id));
                if (freeSpace)
                {
                    id++;
                }
            }
            while (blocks.Any(x => x == -1))
            {
                var firstFreeSpaceIndex = blocks.IndexOf(-1);
                var lastBlock = blocks.Last(x => x != -1);
                var lastBlockIndex = blocks.LastIndexOf(lastBlock);
                blocks[firstFreeSpaceIndex] = lastBlock;
                blocks = blocks.Take(lastBlockIndex).ToList();
            }

            long check = 0;
            for (var i = 0; i < blocks.Count; i++)
            {
                check += i * blocks[i];
            }

            return check.ToString();
        }

        protected override string Part2()
        {
            for (var currentId = _fileParts.Max(x => x.Id); currentId >= 0; currentId--)
            {
                var filePart = _fileParts.First(x => x.Id == currentId);
                var firstFreeSpace = _fileParts.FirstOrDefault(x => x.FreeSpace && x.Length >= filePart.Length);

                if (firstFreeSpace is null)
                {
                    continue;
                }

                var indexOfFilePart = _fileParts.IndexOf(filePart);
                var indexOfFirstFreeSpace = _fileParts.IndexOf(firstFreeSpace);
                if (indexOfFilePart < indexOfFirstFreeSpace)
                {
                    continue;
                }
                
                FilePart freeSpaceBefore = null;
                FilePart freeSpaceAfter = null;
                if (indexOfFilePart > 0)
                {
                    freeSpaceBefore = _fileParts[indexOfFilePart - 1].FreeSpace ? _fileParts[indexOfFilePart - 1] : null;
                }
                if (indexOfFilePart < _fileParts.Count - 1)
                {
                    freeSpaceAfter = _fileParts[indexOfFilePart + 1].FreeSpace ? _fileParts[indexOfFilePart + 1] : null;
                }
                
                if (freeSpaceBefore is not null && freeSpaceAfter is not null)
                {
                    freeSpaceBefore.Length += filePart.Length + freeSpaceAfter.Length;
                    _fileParts.Remove(freeSpaceAfter);
                }
                else if (freeSpaceBefore is not null)
                {
                    freeSpaceBefore.Length += filePart.Length;
                }
                else if (freeSpaceAfter is not null)
                {
                    freeSpaceAfter.Length += filePart.Length;
                }
                else
                {
                    var newFreeSpace = new FilePart { Id = -1, Length = filePart.Length };
                    _fileParts.Insert(indexOfFilePart, newFreeSpace);
                }
                
                _fileParts.Remove(filePart);
                _fileParts.Insert(_fileParts.IndexOf(firstFreeSpace), filePart);

                firstFreeSpace.Length -= filePart.Length;
                if (firstFreeSpace.Length == 0)
                {
                    _fileParts.Remove(firstFreeSpace);
                }
            }
            
            var checksum = BigInteger.Zero;
            var driveSize = _fileParts.Sum(x => x.Length);
            
            var currentFileIndex = 0;
            var currentFile = _fileParts[currentFileIndex];
            var currentFileEnd = _fileParts.First().Length;

            for (var drivePosition = 0; drivePosition < driveSize; drivePosition++)
            {
                if (drivePosition >= currentFileEnd)
                {
                    currentFileIndex++;
                    currentFile = _fileParts[currentFileIndex];
                    currentFileEnd += currentFile.Length;
                }

                if (currentFile.FreeSpace)
                {
                    continue;
                }
                
                checksum += drivePosition * currentFile.Id;
            }
            
            return checksum.ToString();
        }
    }
}