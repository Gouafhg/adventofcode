using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Numerics;
using System.Xml.Schema;
using adventofcode.Utilities;
using Microsoft.VisualBasic;

namespace adventofcode.AOC2024
{
    public class Day21 : Day
    {
        //Globals
        private List<string> _input;
        private char[,] _keypad1Grid;
        private char[,] _keypad2Grid;

        public Day21()
        {
            //Init globals
            Part = 2;
            OnlyOnePart = true;
        }

        //Reset data
        public override void Reset()
        {
            //Setup file
            var file = """
                       869A
                       170A
                       319A
                       349A
                       489A
                       """;
            // var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test1 = """
                        029A
                        980A
                        179A
                        456A
                        379A
                        """;

            _input = test1.GetLines();

            //Do Pre-stuff
            var keypad1 = """
                          #####
                          #789#
                          #456#
                          #123#
                          ##0A#
                          #####
                          """;

            _keypad1Grid = new char[5, 5];
            for (var y = 0; y < 5; y++)
            {
                var line = keypad1.GetLines()[y];
                for (var x = 0; x < 5; x++)
                {
                    _keypad1Grid[x, y] = line[x];
                }
            }

            var keypad2 = """
                          #####
                          ##^A#
                          #<v>#
                          #####
                          """;

            _keypad2Grid = new char[5, 4];
            for (var y = 0; y < 4; y++)
            {
                var line = keypad2.GetLines()[y];
                for (var x = 0; x < 5; x++)
                {
                    _keypad2Grid[x, y] = line[x];
                }
            }
        }

        private static bool TryGetCharAtPosition(char[,] grid, (int X, int Y) position, out char result)
        {
            if (!position.IsWithinRange(grid.GetLength(0), grid.GetLength(1)))
            {
                result = default;
                return false;
            }

            result = grid[position.X, position.Y];
            return true;
        }

        private static List<string> GetPaths(char[,] grid, string prevPath, (int X, int Y) currentPosition, (int X, int Y) end, ConcurrentDictionary<(string Path, (int X, int Y) Current, (int X, int Y) End), List<string>> cache)
        {
            if (cache.ContainsKey((prevPath, currentPosition, end)))
            {
                return cache[(prevPath, currentPosition, end)];
            }

            var result = new List<string>();

            if (TryGetCharAtPosition(grid, currentPosition, out var currentChar) && prevPath.Contains(currentChar) || currentChar == '#')
            {
                cache[(prevPath, currentPosition, end)] = null;
                return null;
            }

            var nextPath = prevPath + currentChar;

            if (currentPosition == end)
            {
                result.Add(nextPath);
                cache[(prevPath, currentPosition, end)] = result;
                return result;
            }

            var left = currentPosition with { X = currentPosition.X - 1 };
            var right = currentPosition with { X = currentPosition.X + 1 };
            var up = currentPosition with { Y = currentPosition.Y - 1 };
            var down = currentPosition with { Y = currentPosition.Y + 1 };

            if (TryGetCharAtPosition(grid, left, out var leftChar) && !prevPath.Contains(leftChar))
            {
                var pathsLeft = GetPaths(grid, nextPath, left, end, cache);
                if (pathsLeft is not null)
                {
                    result.AddRange(pathsLeft);
                }
            }

            if (TryGetCharAtPosition(grid, right, out var rightChar) && !prevPath.Contains(rightChar))
            {
                var pathsRight = GetPaths(grid, nextPath, right, end, cache);
                if (pathsRight is not null)
                {
                    result.AddRange(pathsRight);
                }
            }

            if (TryGetCharAtPosition(grid, up, out var upChar) && !prevPath.Contains(upChar))
            {
                var pathsUp = GetPaths(grid, nextPath, up, end, cache);
                if (pathsUp is not null)
                {
                    result.AddRange(pathsUp);
                }
            }

            if (TryGetCharAtPosition(grid, down, out var downChar) && !prevPath.Contains(downChar))
            {
                var pathsDown = GetPaths(grid, nextPath, down, end, cache);
                if (pathsDown is not null)
                {
                    result.AddRange(pathsDown);
                }
            }

            if (!result.Any())
            {
                cache[(prevPath, currentPosition, end)] = null;
                return null;
            }

            var shortest = result.Min(x => x.Length);
            result = result.Where(x => x.Length == shortest).Distinct().ToList();

            cache[(prevPath, currentPosition, end)] = result;
            return result;
        }

        private static (int X, int Y) GetCoordinate(char[,] grid, char target)
        {
            for (var x = 0; x < grid.GetLength(0); x++)
            {
                for (var y = 0; y < grid.GetLength(1); y++)
                {
                    if (grid[x, y] == target)
                    {
                        return (x, y);
                    }
                }
            }

            return (-1, -1);
        }

        private static string TranslatePathToCode(char[,] prevKeymap, string prevKeypadCode)
        {
            var keyPad2Code = string.Empty;
            var currentKeypad1Coordinate = GetCoordinate(prevKeymap, 'A');
            foreach (var prevKeypadChar in prevKeypadCode.Skip(1))
            {
                char keypad2Char;
                if (prevKeypadChar == ' ')
                {
                    keypad2Char = 'A';
                }
                else
                {
                    var nextKeypad1Coordinate = GetCoordinate(prevKeymap, prevKeypadChar);
                    if (nextKeypad1Coordinate.X < currentKeypad1Coordinate.X)
                    {
                        keypad2Char = '<';
                    }
                    else if (nextKeypad1Coordinate.X > currentKeypad1Coordinate.X)
                    {
                        keypad2Char = '>';
                    }
                    else if (nextKeypad1Coordinate.Y < currentKeypad1Coordinate.Y)
                    {
                        keypad2Char = '^';
                    }
                    else if (nextKeypad1Coordinate.Y > currentKeypad1Coordinate.Y)
                    {
                        keypad2Char = 'v';
                    }
                    else
                    {
                        keypad2Char = 'A';
                    }
                }

                if (prevKeypadChar != ' ')
                {
                    currentKeypad1Coordinate = GetCoordinate(prevKeymap, prevKeypadChar);
                }

                keyPad2Code += keypad2Char;
            }

            return keyPad2Code;
        }

        private static List<string> GetPossiblePathsFromCode(char[,] keymap, string code, ConcurrentDictionary<(string Path, (int X, int Y) Position, (int X, int Y) End), List<string>> cache)
        {
            var possiblePaths = new List<string> { string.Empty };

            var currentChar = 'A';
            foreach (var entry in code)
            {
                var currentCoordinates = GetCoordinate(keymap, currentChar);
                var endCoordinates = GetCoordinate(keymap, entry);

                var paths = GetPaths(keymap, string.Empty, currentCoordinates, endCoordinates, cache);
                possiblePaths = paths.SelectMany(newPart => possiblePaths.Select(oldPart => oldPart + newPart[1..] + ' ')).ToList();
                if (entry != ' ')
                {
                    currentChar = entry;
                }
            }

            return possiblePaths.Distinct().Select(x => 'A' + x).ToList();
        }

        protected override string Part1()
        {
            var complexities = new ConcurrentDictionary<string, int>();
            ConcurrentDictionary<(string Path, (int X, int Y) Position, (int X, int Y) End), List<string>> keypad1Cache = [];
            ConcurrentDictionary<(string Path, (int X, int Y) Position, (int X, int Y) End), List<string>> keypad2Cache = [];

            var keypad1Codes = _input;
            keypad1Codes.AsParallel().ForAll(code =>
            {
                var shortest = int.MaxValue;
                var keypad1Paths = GetPossiblePathsFromCode(_keypad1Grid, code, keypad1Cache);
                keypad1Paths.AsParallel().ForAll(keypad1Path =>
                {
                    var keypad2Code = TranslatePathToCode(_keypad1Grid, keypad1Path);
                    var keypad2Paths = GetPossiblePathsFromCode(_keypad2Grid, keypad2Code, keypad2Cache);
                    keypad2Paths.AsParallel().ForAll(keypad2Path =>
                    {
                        var keypad3Code = TranslatePathToCode(_keypad2Grid, keypad2Path);
                        var keypad3Paths = GetPossiblePathsFromCode(_keypad2Grid, keypad3Code, keypad2Cache);

                        var lastPaths = keypad3Paths.Select(x => TranslatePathToCode(_keypad2Grid, x)).ToList();
                        shortest = Math.Min(shortest, lastPaths.Min(x => x.Length));
                    });
                });

                var complexity = int.Parse(code[..^1]) * shortest;
                complexities[code] = complexity;
                Console.WriteLine(".");
            });

            var result = complexities.Values.Aggregate<int, BigInteger>(0, (current, complexity) => current + complexity);

            return result.ToString();
        }

        protected override string Part2()
        {
            Dictionary<(char, char), (int dx, int dy)> pathCache = [];

            foreach (var i in _keypad1Grid)
            {
                foreach (var j in _keypad1Grid)
                {
                    if (i == '#' || j == '#')
                    {
                        continue;
                    }

                    var ic = GetCoordinate(_keypad1Grid, i);
                    var jc = GetCoordinate(_keypad1Grid, j);

                    var dx = jc.X - ic.X;
                    var dy = jc.Y - ic.Y;

                    pathCache[(i, j)] = (dx, dy);
                }
            }

            foreach (var i in _keypad2Grid)
            {
                foreach (var j in _keypad2Grid)
                {
                    if (i == '#' || j == '#')
                    {
                        continue;
                    }

                    var ic = GetCoordinate(_keypad2Grid, i);
                    var jc = GetCoordinate(_keypad2Grid, j);

                    var dx = jc.X - ic.X;
                    var dy = jc.Y - ic.Y;

                    pathCache[(i, j)] = (dx, dy);
                }
            }

            BigInteger result = 0;


            //Kanske rekursiv
            //Kanske kö med koll på hur många robotar?
            foreach (var code in _input)
            {
                var pairs = SplitToPairs("A" + code).ToList();
                var pairPaths = pairs.Select(x => pathCache[x]);

                var cache = new Dictionary<(int, int, int, int), List<(char, char)>>();

                for (var i = 0; i < 2; ++i)
                {
                    pairs = [];

                    //var path = 'A' + new string('<', leftCount) + new string('>', rightCount) + new string('^', upCount) + new string('v', downCount) + 'A';
                    //foreach (var p in SplitToPairs(path))
                    //{
                    //    foreach (var (dx, dy) in pairPaths)
                    //    {
                    //        var leftCount = dx > 0 ? 0 : -dx;
                    //        var rightCount = dx < 0 ? 0 : dx;
                    //        var upCount = dy > 0 ? 0 : -dy;
                    //        var downCount = dy < 0 ? 0 : dy;

                    //    }
                    //}

                    //pairPaths = pairs.Select(x => pathCache[x]).ToList();
                }
                return pairPaths.Sum(x => Math.Abs(x.dx) + Math.Abs(x.dy) + 1).ToString();
            }

            return result.ToString();
        }

        private static IEnumerable<(char A, char B)> SplitToPairs(string code)
        {
            for (var i = 1; i < code.Length; i++)
            {
                yield return (code[i - 1], code[i]);
            }
        }
    }
}