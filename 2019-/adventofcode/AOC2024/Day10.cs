using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2024
{
    public class Day10 : Day
    {
        //Globals
        private readonly List<string> _input;
        
        private int _resX, _resY;
        private int[,] _grid;

        //Constructor
        public Day10()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\Utveckling\TheGang\adventofcode\2019-\data\2024\day10.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"2024\day01.txt");

            //Test case
            var test1 = @"0123
1234
8765
9876";

            var test2 = @"...0...
...1...
...2...
6543456
7.....7
8.....8
9.....9";

            var test3 = @"..90..9
...1.98
...2..7
6543456
765.987
876....
987....";

            var test4 = @"10..9..
2...8..
3...7..
4567654
...8..3
...9..2
.....01
Here's a ";

            var test5 = @"89010123
78121874
87430965
96549874
45678903
32019012
01329801
10456732";

            var test6 = @".....0.
..4321.
..5..2.
..6543.
..7..4.
..8765.
..9....";
            
            _input = file.GetLines();

            //Do Pre-stuff
            
            _grid = new int[_input[0].Length, _input.Count];
            for (var y = 0; y < _input.Count; ++y)
            {
                for (var x = 0; x < _input[0].Length; ++x)
                {
                    _grid[x, y] = _input[y][x] == '.' ? -1 : int.Parse(_input[y][x].ToString());
                }
            }
            
            _resX = _input[0].Length;
            _resY = _input.Count;
        }
        
        private void GetTopsP1((int X, int Y) currentPosition, List<(int X, int Y)> visited, List<(int X, int Y)> tops)
        {
            if (visited.Contains(currentPosition))
            {
                return;
            }
            
            visited.Add(currentPosition);
            
            if (_grid[currentPosition.X, currentPosition.Y] == 9)
            {
                tops.Add(currentPosition);
                return;
            }
            
            var left = currentPosition with { X = currentPosition.X - 1 };
            var right = currentPosition with { X = currentPosition.X + 1 };
            var up = currentPosition with { Y = currentPosition.Y - 1 };
            var down = currentPosition with { Y = currentPosition.Y + 1 };
            
            if (left.X >= 0 && _grid[left.X, left.Y] == _grid[currentPosition.X, currentPosition.Y] + 1 && !visited.Contains(left))
            {
                GetTopsP1(left, visited, tops);
            }
            if (right.X < _resX && _grid[right.X, right.Y] == _grid[currentPosition.X, currentPosition.Y] + 1 && !visited.Contains(right))
            {
                GetTopsP1(right, visited, tops);
            }
            if (up.Y >= 0 && _grid[up.X, up.Y] == _grid[currentPosition.X, currentPosition.Y] + 1 && !visited.Contains(up))
            {
                GetTopsP1(up, visited, tops);
            }
            if (down.Y < _resY && _grid[down.X, down.Y] == _grid[currentPosition.X, currentPosition.Y] + 1 && !visited.Contains(down))
            {
                GetTopsP1(down, visited, tops);
            }
        }

        private void GetTopsP2((int X, int Y) currentPosition, List<(int X, int Y)> visited, List<(int X, int Y)> tops)
        {
            if (visited.Contains(currentPosition))
            {
                return;
            }
            
            visited.Add(currentPosition);
            
            if (_grid[currentPosition.X, currentPosition.Y] == 9)
            {
                tops.Add(currentPosition);
                visited.Clear();
                return;
            }
            
            var left = currentPosition with { X = currentPosition.X - 1 };
            var right = currentPosition with { X = currentPosition.X + 1 };
            var up = currentPosition with { Y = currentPosition.Y - 1 };
            var down = currentPosition with { Y = currentPosition.Y + 1 };
            
            if (left.X >= 0 && _grid[left.X, left.Y] == _grid[currentPosition.X, currentPosition.Y] + 1 && !visited.Contains(left))
            {
                GetTopsP2(left, visited, tops);
            }
            if (right.X < _resX && _grid[right.X, right.Y] == _grid[currentPosition.X, currentPosition.Y] + 1 && !visited.Contains(right))
            {
                GetTopsP2(right, visited, tops);
            }
            if (up.Y >= 0 && _grid[up.X, up.Y] == _grid[currentPosition.X, currentPosition.Y] + 1 && !visited.Contains(up))
            {
                GetTopsP2(up, visited, tops);
            }
            if (down.Y < _resY && _grid[down.X, down.Y] == _grid[currentPosition.X, currentPosition.Y] + 1 && !visited.Contains(down))
            {
                GetTopsP2(down, visited, tops);
            }
        }

        private List<(int X, int Y)> GetTrailHeads()
        {
            var trailheads = new List<(int X, int Y)>();
            
            for (var x = 0; x < _resX; ++x)
            {
                for (var y = 0; y < _resY; ++y)
                {
                    if (_grid[x, y] == 0)
                    {
                        trailheads.Add((x, y));
                    }
                }
            }

            return trailheads;
        }
        
        protected override string Part1()
        {
            var trailheads = GetTrailHeads();
            
            var result = 0;
            foreach (var trailhead in trailheads)
            {
                var visited = new List<(int X, int Y)>();
                var tops = new List<(int X, int Y)>();
                GetTopsP1(trailhead, visited, tops);
                result += tops.Count;
            }
            
            return result.ToString();
        }

        protected override string Part2()
        {
            var trailheads = GetTrailHeads();
            
            var result = 0;
            foreach (var trailhead in trailheads)
            {
                var visited = new List<(int X, int Y)>();
                var tops = new List<(int X, int Y)>();
                GetTopsP2(trailhead, visited, tops);
                result += tops.Count;
            }
            
            return result.ToString();
        }
    }
}

