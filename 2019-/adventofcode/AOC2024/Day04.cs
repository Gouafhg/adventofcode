﻿using adventofcode.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2024
{
    public class Day04 : Day
    {
        //Globals
        private readonly List<string> _input;

        private readonly int _resX;
        private readonly int _resY;
        private readonly char[,] _grid; 

        //Constructor
        public Day04()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\Utveckling\TheGang\adventofcode\2019-\data\2024\day04.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"2024\day01.txt");

            //Test case
            var test1 = @"MMMSXXMASM
MSAMXMSMSA
AMXSXMAAMM
MSAMASMSMX
XMASAMXAMM
XXAMMXXAMA
SMSMSASXSS
SAXAMASAAA
MAMMMXMMMM
MXMXAXMASX";

            _input = file.GetLines();

            //Do Pre-stuff
            _resX = _input[0].Length;
            _resY = _input.Count;
            _grid = new char[_resX, _resY];
            for (var y = 0; y < _resY; y++)
            {
                for (var x = 0; x < _resX; x++)
                {
                    _grid[x, y] = _input[y][x];
                }
            }
        }

        protected override string Part1()
        {
            var c = 0;
            for (var x = 0; x < _resX; ++x)
            {
                for (var y = 0; y < _resY; ++y)
                {
                    var words = GetWordsPart1(x, y);

                    c += words.Count(s => s == "XMAS");
                }
            }
            return c.ToString();
        }

        protected override string Part2()
        {
            var c = 0;
            for (var x = 0; x < _resX; ++x)
            {
                for (var y = 0; y < _resY; ++y)
                {
                    if (IsMas(x, y))
                    {
                        c++;
                    }
                }
            }
            return c.ToString();
        }

        private bool IsMas(int x, int y)
        {
            if (x < 1 || y < 1)
            {
                return false;
            }
            
            if (x >= _resX - 1 || y >= _resY - 1)
            {
                return false;
            }

            if (_grid[x, y] != 'A')
            {
                return false;
            }

            var count = 0;
            
            var upLeft = _grid[x - 1, y - 1];
            
            if (upLeft != 'M' && upLeft != 'S')
            {
                return false;
            }
            var downRight = _grid[x + 1, y + 1];
            switch (upLeft)
            {
                case 'M' when downRight == 'S':
                case 'S' when downRight == 'M':
                    count++;
                    break;
            }

            var upRight = _grid[x + 1, y - 1];
            
            if (upRight != 'M' && upRight != 'S')
            {
                return false;
            }
            
            var downLeft = _grid[x - 1, y + 1];
            switch (upRight)
            {
                case 'M' when downLeft == 'S':
                case 'S' when downLeft == 'M':
                    count++;
                    break;
            }

            return count == 2;
        }
        
        private List<string> GetWordsPart1(int x, int y)
        {
            List<string> result = [null, null, null, null, null, null, null, null];
            
            //horizontal backward
            if (x >= 3)
            {
                result[0] = "";
                for (var i = 0; i < 4; i++)
                {
                    result[0] += _grid[x - i, y];
                }
            }
            //horizontal forward
            if (x <= _resX - 4)
            {
                result[1] = "";
                for (var i = 0; i < 4; i++)
                {
                    result[1] += _grid[x + i, y];
                }
            }
            //vertical backward
            if (y >= 3)
            {
                result[2] = "";
                for (var i = 0; i < 4; i++)
                {
                    result[2] += _grid[x, y - i];
                }
            }
            //vertical forward
            if (y <= _resY - 4)
            {
                result[3] = "";
                for (var i = 0; i < 4; i++)
                {
                    result[3] += _grid[x, y + i];
                }
            }
            //diagonal up left
            if (x >= 3 && y >= 3)
            {
                result[4] = "";
                for (var i = 0; i < 4; i++)
                {
                    result[4] += _grid[x - i, y - i];
                }
            }
            //diagonal down right
            if (x <= _resX - 4 && y <= _resY - 4)
            {
                result[5] = "";
                for (var i = 0; i < 4; i++)
                {
                    result[5] += _grid[x + i, y + i];
                }
            }
            
            //diagonal up right
            if (x <= _resX - 4 && y >= 3)
            {
                result[6] = "";
                for (var i = 0; i < 4; i++)
                {
                    result[6] += _grid[x + i, y - i];
                }
            }
            
            //diagonal down left
            if (x >= 3 && y <= _resY - 4)
            {
                result[7] = "";
                for (var i = 0; i < 4; i++)
                {
                    result[7] += _grid[x - i, y + i];
                }
            }

            return result.Where(s => s is not null).ToList();
        }
    }
}

