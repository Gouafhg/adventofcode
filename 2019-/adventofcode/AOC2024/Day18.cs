using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2024
{
    public class Day18 : Day
    {
        //Globals
        private List<string> _input;

        private int _resX, _resY;
        private (int X, int Y) _start;
        private (int X, int Y) _end;
        private List<(int X, int Y)> _corruptedMemory;

        public Day18()
        {
            //Init globals
            Part = 1;
            OnlyOnePart = false;
        }

        //Reset data
        public override void Reset()
        {
            //Setup file
            var file = ToolsFile.ReadFile(@"C:\Utveckling\TheGang\adventofcode\2019-\data\2024\day18.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test1 = @"5,4
4,2
4,5
3,0
2,1
6,3
2,4
1,5
0,6
3,3
2,6
5,1
1,2
5,5
2,5
6,5
1,4
0,4
6,4
1,1
6,1
1,0
0,5
1,6
2,0";

            _input = file.GetLines();

            //Do Pre-stuff
            _resX = 71;
            _resY = 71;
            _start = (0, 0);
            _end = (_resX - 1, _resY - 1);
            _corruptedMemory = [];
            foreach (var line in _input)
            {
                var split = line.Split(',');
                _corruptedMemory.Add((int.Parse(split[0]), int.Parse(split[1])));
            }
        }

        private int? GetShortestPathP1((int X, int Y) current, Dictionary<(int X, int Y), int> visited,
            IList<(int X, int Y)> corruptedMemory, int steps)
        {
            if (visited.TryGetValue(current, out var stepsAtCurrent) && stepsAtCurrent <= steps)
            {
                return null;
            }

            if (current == _end)
            {
                visited[current] = steps;
                return steps;
            }

            if (current.X < 0 || current.X >= _resX || current.Y < 0 || current.Y >= _resY)
            {
                visited[current] = -1;
                return null;
            }

            if (corruptedMemory.Contains(current))
            {
                visited[current] = -1;
                return null;
            }

            visited[current] = steps;

            var left = current with { X = current.X - 1 };
            var right = current with { X = current.X + 1 };
            var up = current with { Y = current.Y - 1 };
            var down = current with { Y = current.Y + 1 };

            var leftPath = GetShortestPathP1(left, visited, corruptedMemory, steps + 1);
            var rightPath = GetShortestPathP1(right, visited, corruptedMemory, steps + 1);
            var upPath = GetShortestPathP1(up, visited, corruptedMemory, steps + 1);
            var downPath = GetShortestPathP1(down, visited, corruptedMemory, steps + 1);

            return new List<int?> { leftPath, rightPath, upPath, downPath }.Where(x => x.HasValue).Min();
        }

        protected override string Part1()
        {
            var fallenBytes = 1024;
            var corruptedMemory = _corruptedMemory.Take(fallenBytes).ToList();
            var steps = GetShortestPathP1(_start, new Dictionary<(int X, int Y), int>(), corruptedMemory, 0);
            return steps.ToString();
        }

        protected override string Part2()
        {
            for (var fallenBytes = 0; fallenBytes < _corruptedMemory.Count; fallenBytes++)
            {
                var corruptedMemory = _corruptedMemory.Take(fallenBytes).ToList();

                var q = new Queue<((int X, int Y) Position, int Steps)>();
                var visited = new Dictionary<(int X, int Y), int>();
                q.Enqueue((_start, 0));

                while (q.Any())
                {
                    var (current, steps) = q.Dequeue();

                    if (current == _end)
                    {
                        visited[current] = steps;
                        break;
                    }

                    if (visited.TryGetValue(current, out var stepsAtCurrent) && stepsAtCurrent <= steps)
                    {
                        continue;
                    }

                    if (current.X < 0 || current.X >= _resX || current.Y < 0 || current.Y >= _resY)
                    {
                        visited[current] = -1;
                        continue;
                    }

                    if (corruptedMemory.Contains(current))
                    {
                        visited[current] = -1;
                        continue;
                    }

                    visited[current] = steps;

                    var left = current with { X = current.X - 1 };
                    var right = current with { X = current.X + 1 };
                    var up = current with { Y = current.Y - 1 };
                    var down = current with { Y = current.Y + 1 };

                    q.Enqueue((left, steps + 1));
                    q.Enqueue((right, steps + 1));
                    q.Enqueue((up, steps + 1));
                    q.Enqueue((down, steps + 1));
                }

                if (!visited.ContainsKey(_end))
                {
                    return corruptedMemory.Last().ToString();
                }
            }

            return "ALDRIG!";
        }
    }
}