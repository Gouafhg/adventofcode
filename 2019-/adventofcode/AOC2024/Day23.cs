using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using adventofcode.Utilities;

namespace adventofcode.AOC2024
{
    public class Day23 : Day
    {
        //Globals
        private List<string> _input;
        private List<string> _computers;
        private Dictionary<string, HashSet<string>> _connections;

        public Day23()
        {
            //Init globals
            Part = 1;
            OnlyOnePart = false;
        }

        //Reset data
        public override void Reset()
        {
            //Setup file
            var file = ToolsFile.ReadFile(@"C:\Utveckling\TheGang\adventofcode\2019-\data\2024\day23.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test1 = @"kh-tc
qp-kh
de-cg
ka-co
yn-aq
qp-ub
cg-tb
vc-aq
tb-ka
wh-tc
yn-cg
kh-ub
ta-co
de-co
tc-td
tb-wq
wh-td
ta-ka
td-qp
aq-cg
wq-ub
ub-vc
de-ta
wq-aq
wq-vc
wh-yn
ka-de
kh-ta
co-tc
wh-qp
tb-vc
td-yn";

            _input = file.GetLines();

            _computers = [];
            //Do Pre-stuff
            foreach (var line in _input)
            {
                var c1 = line.Split('-')[0];
                var c2 = line.Split('-')[1];

                _computers.Add(c1);
                _computers.Add(c2);
            }

            _computers = _computers.Distinct().ToList();
            _connections = [];

            foreach (var line in _input)
            {
                var c1 = line.Split('-')[0];
                var c2 = line.Split('-')[1];

                if (!_connections.TryAdd(c1, [c2]))
                {
                    _connections[c1].Add(c2);
                }

                if (!_connections.TryAdd(c2, [c1]))
                {
                    _connections[c2].Add(c1);
                }
            }
        }

        protected override string Part1()
        {
            var l = new List<List<string>>();
            for (var i = 0; i < _computers.Count; ++i)
            {
                for (var j = 0; j < _computers.Count; ++j)
                {
                    for (var k = 0; k < _computers.Count; ++k)
                    {
                        if (i == j || i == k || j == k)
                        {
                            continue;
                        }

                        var c1 = _computers[i];
                        var c2 = _computers[j];
                        var c3 = _computers[k];

                        if (_connections[c1].Contains(c2) && _connections[c1].Contains(c3) && _connections[c2].Contains(c3))
                        {
                            l.Add(new List<string> { c1, c2, c3 }.OrderBy(x => x).ToList());
                        }
                    }
                }
            }

            l = l.DistinctBy(x => x.Join()).OrderBy(x => x.Join()).ToList();
            var withT = l.Where(x => x.Any(y => y.StartsWith("t"))).ToList();
            return withT.Count.ToString();
        }

        private string GetC(string computer, HashSet<string> visited, Dictionary<string, string> cache)
        {
            if (visited.Any())
            {
                var computerVisitedAsString = new[] { computer, visited.First() }.OrderBy(x => x).Join();
                if (cache.TryGetValue(computerVisitedAsString, out var value))
                {
                    return value;
                }
            }

            var newVisited = visited.Concat([computer]).ToHashSet();
            var newVisitedAsString = newVisited.OrderBy(x => x).Join(",");

            var connectionsThatContainsAllVisited = _connections[computer].Where(x => newVisited.All(y => _connections[x].Contains(y))).ToArray();

            if (connectionsThatContainsAllVisited.Any())
            {
                return connectionsThatContainsAllVisited.Select(conn => GetC(conn, newVisited, cache)).Distinct().OrderByDescending(x => x).FirstOrDefault();
            }

            foreach (IEnumerable<string> x in Tools.Combinations(newVisited, 2))
            {
                var combinationAsString = new[] { x.First(), x.Last() }.OrderBy(y => y).Join();
                cache[combinationAsString] = newVisitedAsString;
            }

            return newVisitedAsString;
        }

        protected override string Part2()
        {
            Dictionary<string, string> connectedComputers = [];

            Dictionary<string, string> cache = [];
            foreach (var c in _computers)
            {
                HashSet<string> visited = [];
                connectedComputers[c] = GetC(c, visited, cache);
            }

            var passwords = connectedComputers.Select(x => x.Value).Distinct();
            var password = passwords.OrderByDescending(x => x.Length).First();

            return password;
        }
    }
}