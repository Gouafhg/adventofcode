using adventofcode.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2024
{
    public class Day06 : Day
    {
        //Globals
        private readonly List<string> _input;

        private readonly int _resX;
        private readonly int _resY;
        private readonly bool[,] _grid;

        private readonly (int X, int Y) _guardStart;

        private readonly Guard _guard;

        //Constructor
        public Day06()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\Utveckling\TheGang\adventofcode\2019-\data\2024\day06.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"2024\day01.txt");

            //Test case
            var test1 = @"....#.....
.........#
..........
..#.......
.......#..
..........
.#..^.....
........#.
#.........
......#...";

            _input = file.GetLines();

            //Do Pre-stuff
            _resX = _input[0].Length;
            _resY = _input.Count;
            _grid = new bool[_resX, _resY];
            for (var y = 0; y < _resY; y++)
            {
                for (var x = 0; x < _resX; x++)
                {
                    var value = _input[y][x];
                    if (value == '^')
                    {
                        _guard = new Guard
                        {
                            Position = (x, y),
                            Facing = GuardFacing.Up
                        };

                        _guardStart = (x, y);
                        continue;
                    }

                    _grid[x, y] = value == '#' ? true : false;
                }
            }
        }

        protected override string Part1()
        {
            var guardPositions = new List<(GuardFacing Facing, int X, int Y)>();

            while (!_guard.Stopped && !guardPositions.Contains((_guard.Facing, _guard.Position.X, _guard.Position.Y)))
            {
                guardPositions.Add((_guard.Facing, _guard.Position.X, _guard.Position.Y));
                Step();
            }

            var uniquePositions = guardPositions.DistinctBy(x => (x.X, x.Y)).ToList();

            return uniquePositions.Count.ToString();
        }

        protected override string Part2()
        {
            _guard.Stopped = false;
            _guard.Position = _guardStart;
            _guard.Facing = GuardFacing.Up;
            
            var totalGuardPositions = new List<(GuardFacing Facing, int X, int Y)>();

            while (!_guard.Stopped &&
                   !totalGuardPositions.Contains((_guard.Facing, _guard.Position.X, _guard.Position.Y)))
            {
                totalGuardPositions.Add((_guard.Facing, _guard.Position.X, _guard.Position.Y));
                Step();
            }

            var uniquePositions = totalGuardPositions.DistinctBy(x => (x.X, x.Y)).ToList();

            var obs = new List<(int X, int Y)>();

            foreach (var (_, obsX, obsY) in uniquePositions)
            {
                _guard.Stopped = false;
                _guard.Position = _guardStart;
                _guard.Facing = GuardFacing.Up;

                var guardPositions = new List<(GuardFacing Facing, int X, int Y)>();

                while (!_guard.Stopped &&
                       !guardPositions.Contains((_guard.Facing, _guard.Position.X, _guard.Position.Y)))
                {
                    guardPositions.Add((_guard.Facing, _guard.Position.X, _guard.Position.Y));
                    Step(obsX, obsY);
                }

                if (_guard.Stopped)
                {
                    continue;
                }

                obs.Add((obsX, obsY));
            }

            return obs.Count.ToString();
        }

        private class Guard
        {
            public bool Stopped = false;
            public GuardFacing Facing;
            public (int X, int Y) Position;
        }

        private enum GuardFacing
        {
            Up = 0,
            Right = 1,
            Down = 2,
            Left = 3,
        }

        private void Step(int? obsX = null, int? obsY = null)
        {
            var (nextX, nextY) = _guard.Position;
            switch (_guard.Facing)
            {
                case GuardFacing.Up:
                    nextY--;
                    break;
                case GuardFacing.Right:
                    nextX++;
                    break;
                case GuardFacing.Down:
                    nextY++;
                    break;
                case GuardFacing.Left:
                    nextX--;
                    break;
            }

            if (obsX.HasValue && obsY.HasValue)
            {
                if (nextX == obsX && nextY == obsY)
                {
                    _guard.Facing = GetNextFacing(_guard.Facing);
                    return;
                }
            }

            if (nextX < 0 || nextX >= _resX || nextY < 0 || nextY >= _resY)
            {
                _guard.Stopped = true;
                return;
            }

            if (_grid[nextX, nextY])
            {
                _guard.Facing = GetNextFacing(_guard.Facing);
                return;
            }

            _guard.Position = (nextX, nextY);
        }

        private static GuardFacing GetNextFacing(GuardFacing current)
        {
            var next = (int)current;
            next = (next + 1) % 4;
            return (GuardFacing)next;
        }
    }
}