using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace adventofcode.AOC2024
{
    public class Day13 : Day
    {
        //Globals
        private readonly List<string> _input;
        private readonly List<ButtonCase> _buttonCases = [];

        //Constructor
        public Day13()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\Utveckling\TheGang\adventofcode\2019-\data\2024\day13.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"2024\day01.txt");

            //Test case
            var test1 = @"Button A: X+94, Y+34
Button B: X+22, Y+67
Prize: X=8400, Y=5400

Button A: X+26, Y+66
Button B: X+67, Y+21
Prize: X=12748, Y=12176

Button A: X+17, Y+86
Button B: X+84, Y+37
Prize: X=7870, Y=6450

Button A: X+69, Y+23
Button B: X+27, Y+71
Prize: X=18641, Y=10279";

            _input = file.GetLines();

            //Do Pre-stuff
            for (var i = 0; i < _input.Count; i += 3)
            {
                string IntAble(string v)
                {
                    return v.StartsWith('+') ? v[1..] : v;
                }
                
                var aX = IntAble(_input[i].Split(':')[1].Split('X')[1].Split(',')[0]);
                var aY = IntAble(_input[i].Split(':')[1].Split('Y')[1]);
                var bX = IntAble(_input[i + 1].Split(':')[1].Split('X')[1].Split(',')[0]);
                var bY = IntAble(_input[i + 1].Split(':')[1].Split('Y')[1]);
                var prizeX = IntAble(_input[i + 2].Split(':')[1].Split('X')[1].Split(',')[0]).Split('=')[1];
                var prizeY = IntAble(_input[i + 2].Split(':')[1].Split('Y')[1]).Split('=')[1];
                
                _buttonCases.Add(new ButtonCase((long.Parse(aX), long.Parse(aY)), (long.Parse(bX), long.Parse(bY)), (long.Parse(prizeX), long.Parse(prizeY))));
            }
        }

        protected override string Part1()
        {
            var tokens = _buttonCases.Select(c => c.Solve(100)).Where(solution => solution.HasValue).Sum(solution => ButtonCase.TokenA * solution.Value.A + ButtonCase.TokenB * solution.Value.B);
            return tokens.ToString();
        }

        protected override string Part2()
        {
            _buttonCases.ForEach(x => x.PrizePosition = (x.PrizePosition.X + 10000000000000, x.PrizePosition.Y + 10000000000000));
            var tokens = _buttonCases.Select(c => c.Solve(null)).Where(solution => solution.HasValue).Sum(solution => ButtonCase.TokenA * solution.Value.A + ButtonCase.TokenB * solution.Value.B);
            
            return tokens.ToString();
        }

        private class ButtonCase((long X, long Y) buttonA, (long X, long Y) buttonB, (long X, long Y) prizePosition)
        {
            private (long X, long Y) ButtonA { get; set; } = buttonA;
            private (long X, long Y) ButtonB { get; set; } = buttonB;

            public (long X, long Y) PrizePosition { get; set; } = prizePosition;

            public const long TokenA = 3;
            public const long TokenB = 1;

            private long MaxButtonA()
            {
                return Math.Min(PrizePosition.X / ButtonA.X, PrizePosition.Y / ButtonA.Y);
            }

            private long MaxButtonB()
            {
                return Math.Min(PrizePosition.X / ButtonB.X, PrizePosition.Y / ButtonB.Y);
            }

            public (long A, long B)? Solve(int? limit)
            {
                // var s1 = Solve1(limit);
                
                if (ButtonA.Y * ButtonB.X - ButtonA.X * ButtonB.Y == 0)
                {
                    // Debug.Assert(s1 is null);
                    return null;
                }

                if (ButtonA.X == 0)
                {
                    if (PrizePosition.X % ButtonB.X != 0) return null;
                    
                    var tempB = PrizePosition.X / ButtonB.X;
                    if (tempB * ButtonB.Y == PrizePosition.Y)
                    {
                        // Debug.Assert(s1 == (0, tempB));
                        return (0, tempB);
                    }
                    // Debug.Assert(s1 is null);
                    return null;
                }
                
                var b = (PrizePosition.X * ButtonA.Y - PrizePosition.Y * ButtonA.X) / (ButtonA.Y * ButtonB.X - ButtonA.X * ButtonB.Y);
                var a = (PrizePosition.X - b * ButtonB.X) / ButtonA.X;
                
                if (a < 0 || b < 0 || limit.HasValue && (a > limit || b > limit))
                {
                    // Debug.Assert(s1 is null);
                    return null;
                }
                
                if (a * ButtonA.X + b * ButtonB.X != PrizePosition.X || a * ButtonA.Y + b * ButtonB.Y != PrizePosition.Y)
                {
                    // Debug.Assert(s1 is null);
                    return null;
                }
                
                // Debug.Assert(s1 == (a, b));
                return (a, b);
            }

            private (long A, long B)? Solve1(int? limit)
            {
                var maxA = MaxButtonA();
                var maxB = MaxButtonB();
                
                var solutions = new List<(long A, long B)>();
                for (var a = (long)0; a <= maxA && (!limit.HasValue || a <= limit); a++)
                {
                    if (a > limit)
                    {
                        break;
                    }
                    
                    for (var b = (long)0; b <= maxB && (!limit.HasValue || b <= limit); b++)
                    {
                        if (b > limit)
                        {
                            break;
                        }
                        
                        if (a * ButtonA.X + b * ButtonB.X == PrizePosition.X &&
                            a * ButtonA.Y + b * ButtonB.Y == PrizePosition.Y)
                        {
                            solutions.Add((a, b));
                        }
                    }
                }
            
                if (solutions.Count == 0)
                {
                    return null;
                }
                
                return solutions.OrderBy(x => TokenA * x.A + TokenB * x.B).First();
            }
        }
    }
}