﻿using System;
using System.Linq;
using System.Threading;

namespace adventofcode
{
    public abstract class Day
    {
        public static bool OnlyOnePart = false;
        public static int Part;

        private static readonly string BaseDirectory = AppContext.BaseDirectory;

        public static readonly string DataPathHemma = string.Join("\\", BaseDirectory.Split('\\').Take(BaseDirectory.Split('\\').Count() - 5)) + @"\data\";
        public static readonly string DataPath = string.Join("\\", BaseDirectory.Split('\\').Take(BaseDirectory.Split('\\').Count() - 4)) + @"\data\";

        public virtual void Reset() { }
        
        public string Execute()
        {
            Reset();
            if (OnlyOnePart)
            {
                var partResult = Part == 1 ? Part1() : Part2();
                return $"Part {Part}: {partResult}";
            }
            var part1 = Part1();
            Reset();
            var part2 = Part2();

            return $"Part 1:\n{part1}\n------------------------------------------------------------------------\n\nPart 2:\n{part2}";
        }

        protected abstract string Part1();

        protected abstract string Part2();
    }
}
