﻿using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2023
{
    public enum HandStrengthEnum
    {
        HighCard,
        Pair,
        TwoPairs,
        ThreeOfAKind,
        FullHouse,
        FourOfAKind,
        FiveOfAKind,
    }

    public class Day07 : Day
    {
        private static List<string> _cardOrder = new[] { "A", "K", "Q", "J", "T", "9", "8", "7", "6", "5", "4", "3", "2" }.Reverse().ToList();

        private static int GetCardRank(char card)
        {
            return _cardOrder.ToList().IndexOf(card.ToString());
        }

        //Globals
        private readonly List<string> _input;

        //Constructor
        public Day07()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2023\day07.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"2023\day07.txt");

            //Test case
            var test1 = @"32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483";

            _input = file.GetLines();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            return _input
                .Select(x => (Hand: GetHand(x), Bid: GetBid(x)))
                .GroupBy(x => GetHandStrength(x.Hand))
                .OrderBy(x => x.Key)
                .SelectMany(x => x.OrderBy(y => y.Hand, new HandComparer()))
                .Select((x, i) => (i + 1) * x.Bid)
                .Sum()
                .ToString();
        }

        protected override string Part2()
        {
            _cardOrder = new[] { "A", "K", "Q", "T", "9", "8", "7", "6", "5", "4", "3", "2", "J" }.Reverse().ToList();

            return _input
                .Select(x => (Hand: GetHand(x), Bid: GetBid(x)))
                .GroupBy(x => GetHandStrength(ExchangeJoker(x.Hand, GetHandStrength(x.Hand))))
                .OrderBy(x => x.Key)
                .SelectMany(x => x.OrderBy(y => y.Hand, new HandComparer()))
                .Select((x, i) => (i + 1) * x.Bid)
                .Sum()
                .ToString();
        }

        private static HandStrengthEnum GetHandStrength(string hand)
        {
            if (hand.All(x => x == hand[0])) return HandStrengthEnum.FiveOfAKind;
            if (hand.GroupBy(x => x).Any(x => x.Count() == 4)) return HandStrengthEnum.FourOfAKind;
            if (hand.GroupBy(x => x).Any(x => x.Count() == 3) && hand.GroupBy(x => x).Any(x => x.Count() == 2)) return HandStrengthEnum.FullHouse;
            if (hand.GroupBy(x => x).Any(x => x.Count() == 3)) return HandStrengthEnum.ThreeOfAKind;
            if (hand.GroupBy(x => x).Count(x => x.Count() == 2) == 2) return HandStrengthEnum.TwoPairs;
            return hand.GroupBy(x => x).Any(x => x.Count() == 2)
                ? HandStrengthEnum.Pair
                : HandStrengthEnum.HighCard;
        }

        private static string GetHand(string line) => line.Split(' ')[0].Trim();

        private static long GetBid(string line) => long.Parse(line.Split(' ')[1].Trim());

        private static string ExchangeJoker(string hand, HandStrengthEnum handStrength)
        {
            if (!hand.Contains("J"))
            {
                return hand;
            }

            switch (handStrength)
            {
                case HandStrengthEnum.FiveOfAKind:
                {
                    return hand.Replace("J", "A");
                }
                case HandStrengthEnum.FourOfAKind:
                {
                    var exchangeCard = hand.GroupBy(x => x).First(x => x.Count() == 4).Key;
                    var otherCard = hand.First(x => x != exchangeCard);
                    if (exchangeCard == 'J')
                    {
                        exchangeCard = otherCard;
                    }

                    return hand.Replace("J", exchangeCard.ToString());
                }
                case HandStrengthEnum.FullHouse:
                {
                    var exchangeCard = hand.GroupBy(x => x).First(x => x.Count() == 3).Key;
                    if (exchangeCard == 'J')
                    {
                        exchangeCard = hand.GroupBy(x => x).First(x => x.Count() == 2).Key;
                    }

                    return hand.Replace("J", exchangeCard.ToString());
                }
                case HandStrengthEnum.ThreeOfAKind:
                {
                    var exchangeCard = hand.GroupBy(x => x).First(x => x.Count() == 3).Key.ToString();
                    if (exchangeCard == "J")
                    {
                        exchangeCard = hand.GroupBy(x => x).Where(x => x.Count() != 3).Select(x => x.Key).OrderByDescending(GetCardRank).First().ToString();
                    }

                    return hand.Replace("J", exchangeCard);
                }
                case HandStrengthEnum.TwoPairs:
                {
                    var exchangeCard = hand.GroupBy(x => x).Where(x => x.Count() == 2 && x.Key != 'J').Select(x => x.Key).OrderByDescending(GetCardRank).First().ToString();
                    return hand.Replace("J", exchangeCard);
                }
                case HandStrengthEnum.Pair:
                {
                    var exchangeCard = hand.GroupBy(x => x).First(x => x.Count() == 2).Key.ToString();
                    if (exchangeCard == "J")
                    {
                        exchangeCard = hand.GroupBy(x => x).Where(x => x.Count() != 2).Select(x => x.Key).OrderByDescending(GetCardRank).First().ToString();
                    }

                    return hand.Replace("J", exchangeCard);
                }
                case HandStrengthEnum.HighCard:
                default:
                {
                    var exchangeCard = hand.GroupBy(x => x).Select(x => x.Key).OrderByDescending(GetCardRank).First().ToString();
                    return hand.Replace("J", exchangeCard);
                }
            }
        }

        private class HandComparer : IComparer<string>
        {
            public int Compare(string a, string b)
            {
                if (a == b)
                {
                    return 0;
                }

                for (var i = 0; i < a.Length; ++i)
                {
                    if (GetCardRank(a[i]) > GetCardRank(b[i])) return 1;
                    if (GetCardRank(a[i]) < GetCardRank(b[i])) return -1;
                }

                return 0;
            }
        }
    }
}