﻿using System;
using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;
using adventofcode.Utilities.Extensions;

namespace adventofcode.AOC2023
{
    public class Day10Node
    {
        public bool IsStart { get; set; }
        public (int X, int Y) Pos { get; set; }
        public Day10Node Prev { get; set; }
        public Day10Node Next { get; set; }
        public int DistanceFromStart { get; set; }
        public char Char { get; set; }
    }

    public class Day10 : Day
    {
        //Globals
        private readonly List<string> _input;
        private readonly List<List<char>> _charGrid;
        private List<Day10Node> Nodes { get; set; }

        //Constructor
        public Day10()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2023\day10.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"2023\day10.txt");

            //Test case
            var test1 = @"..F7.
.FJ|.
SJ.L7
|F--J
LJ...";

            var test2 = @"...........
.S-------7.
.|F-----7|.
.||.....||.
.||.....||.
.|L-7.F-J|.
.|..|.|..|.
.L--J.L--J.
...........";

            var test3 = @"..........
.S------7.
.|F----7|.
.||....||.
.||....||.
.|L-7F-J|.
.|..||..|.
.L--JL--J.
..........";

            var test4 = @".F----7F7F7F7F-7....
.|F--7||||||||FJ....
.||.FJ||||||||L7....
FJL7L7LJLJ||LJ.L-7..
L--J.L7...LJS7F-7L7.
....F-J..F7FJ|L7L7L7
....L7.F7||L7|.L7L7|
.....|FJLJ|FJ|F7|.LJ
....FJL-7.||.||||...
....L---J.LJ.LJLJ...";
            _input = file.GetLines();

            //Do Pre-stuff
            _charGrid = _input.Select(line => line.ToList()).ToList();

            var width = _charGrid.First().Count;
            var height = _charGrid.Count;

            // Day10Pos startPos = null;
            (int X, int Y) startPos = (-1, -1);
            var foundStartPos = false;
            Nodes = new List<Day10Node>();
            for (var y = 0; y < _charGrid.Count; y++)
            {
                var line = _charGrid[y];
                for (var x = 0; x < line.Count; x++)
                {
                    var c = line[x];
                    if (c != 'S') continue;
                    startPos = (x, y);
                    foundStartPos = true;
                    break;
                }

                if (foundStartPos)
                {
                    break;
                }
            }

            var lastNode = new Day10Node
            {
                IsStart = true,
                Pos = startPos,
                Prev = null,
                Next = null,
            };
            var startNode = lastNode;
            Nodes.Add(lastNode);
            var currentPos = (startPos.X, startPos.Y);
            var visited = new List<(int X, int Y)>();
            do
            {
                char? s = null;
                var currChar = _charGrid[currentPos.Y][currentPos.X];
                visited.Add(currentPos);

                var newPos = new List<(int X, int Y)>();
                if (currChar == 'S')
                {
                    var connectsToLeft = new[] { '-', 'J', '7' };
                    var connectsToRight = new[] { '-', 'L', 'F' };
                    var connectsToBelow = new[] { '|', 'F', '7' };
                    var connectsToAbove = new[] { '|', 'J', 'L' };

                    switch (currChar)
                    {
                        case 'S' when currentPos.Y > 0 && connectsToBelow.Contains(_charGrid[currentPos.Y - 1][currentPos.X]) && currentPos.Y < height - 1 && connectsToAbove.Contains(_charGrid[currentPos.Y + 1][currentPos.X]):
                            s = '|';
                            break;
                        case 'S' when currentPos.X > 0 && connectsToRight.Contains(_charGrid[currentPos.Y][currentPos.X - 1]) && currentPos.X < width - 1 && connectsToLeft.Contains(_charGrid[currentPos.Y][currentPos.X + 1]):
                            s = '-';
                            break;
                        case 'S' when currentPos.Y > 0 && connectsToBelow.Contains(_charGrid[currentPos.Y - 1][currentPos.X]) && currentPos.X < width - 1 && connectsToLeft.Contains(_charGrid[currentPos.Y][currentPos.X + 1]):
                            s = 'L';
                            break;
                        case 'S' when currentPos.Y > 0 && connectsToBelow.Contains(_charGrid[currentPos.Y - 1][currentPos.X]) && currentPos.X > 0 && connectsToRight.Contains(_charGrid[currentPos.Y][currentPos.X - 1]):
                            s = 'J';
                            break;
                        case 'S' when currentPos.X > 0 && connectsToRight.Contains(_charGrid[currentPos.Y][currentPos.X - 1]) && currentPos.Y < height - 1 && connectsToAbove.Contains(_charGrid[currentPos.Y + 1][currentPos.X]):
                            s = '7';
                            break;
                        case 'S' when currentPos.X < width - 1 && connectsToLeft.Contains(_charGrid[currentPos.Y][currentPos.X + 1]) && currentPos.Y < height - 1 && connectsToAbove.Contains(_charGrid[currentPos.Y + 1][currentPos.X]):
                            s = 'F';
                            break;
                    }
                }

                if (s == '7' || currChar == '7')
                {
                    newPos.Add((currentPos.X, currentPos.Y + 1));
                    newPos.Add((currentPos.X - 1, currentPos.Y));
                }
                else if (s == 'F' || currChar == 'F')
                {
                    newPos.Add((currentPos.X, currentPos.Y + 1));
                    newPos.Add((currentPos.X + 1, currentPos.Y));
                }
                else if (s == 'J' || currChar == 'J')
                {
                    newPos.Add((currentPos.X, currentPos.Y - 1));
                    newPos.Add((currentPos.X - 1, currentPos.Y));
                }
                else if (s == 'L' || currChar == 'L')
                {
                    newPos.Add((currentPos.X, currentPos.Y - 1));
                    newPos.Add((currentPos.X + 1, currentPos.Y));
                }
                else if (s == '|' || currChar == '|')
                {
                    newPos.Add((currentPos.X, currentPos.Y - 1));
                    newPos.Add((currentPos.X, currentPos.Y + 1));
                }
                else if (s == '-' || currChar == '-')
                {
                    newPos.Add((currentPos.X - 1, currentPos.Y));
                    newPos.Add((currentPos.X + 1, currentPos.Y));
                }
                else if (currChar == '.')
                {
                    //Do nothing
                }
                else
                {
                    throw new Exception("Glömde ngt");
                    //Do nothing
                }

                if (lastNode.IsStart && s != null)
                {
                    lastNode.Char = s.Value;
                }

                newPos.RemoveAll(x => visited.Contains(x));

                if (newPos.IsEmpty())
                {
                    lastNode.Next = startNode;

                    startNode.Prev = lastNode;
                }
                else
                {
                    var nextPos = newPos.First();
                    var nextChar = _charGrid[nextPos.Y][nextPos.X];
                    var node = new Day10Node
                    {
                        Pos = nextPos,
                        Char = nextChar,
                        Prev = lastNode,
                        Next = null,
                    };

                    lastNode.Next = node;
                    Nodes.Add(node);
                    lastNode = node;
                    currentPos = nextPos;
                }
            } while (lastNode.Next != startNode);
        }

        protected override string Part1()
        {
            var startNode = Nodes.First(x => x.IsStart);
            var currentNode = startNode;
            do
            {
                currentNode = currentNode.Next;
                currentNode.DistanceFromStart = currentNode.Prev.DistanceFromStart + 1;
            } while (currentNode != startNode);

            startNode.DistanceFromStart = 0;
            do
            {
                currentNode = currentNode.Prev;
                var newDistanceFromStart = currentNode.Next.DistanceFromStart + 1;
                if (newDistanceFromStart < currentNode.DistanceFromStart)
                {
                    currentNode.DistanceFromStart = newDistanceFromStart;
                }
            } while (currentNode != startNode);

            return Nodes.Max(x => x.DistanceFromStart).ToString();
        }

        protected override string Part2()
        {
            var width = _charGrid.First().Count;
            var height = _charGrid.Count;

            var reverseOrder = Nodes.Sum(x => (x.Next.Pos.X - x.Pos.X) * (x.Next.Pos.Y - x.Pos.Y)) < 0;

            if (reverseOrder)
            {
                foreach (var node in Nodes)
                {
                    var tempNext = node.Next;
                    var tempPrev = node.Prev;
                    node.Next = tempPrev;
                    node.Prev = tempNext;
                }
            }

            var dotPositions = new List<(int X, int Y)>();
            for (var y = 0; y < height; ++y)
            {
                for (var x = 0; x < width; ++x)
                {
                    var nodeInPos = Nodes.FirstOrDefault(node => node.Pos.X == x && node.Pos.Y == y);
                    _charGrid[y][x] = nodeInPos?.Char ?? '.';
                    if (_charGrid[y][x] == '.')
                    {
                        dotPositions.Add((x, y));
                    }
                }
            }

            return dotPositions.Count(InsidePolygon).ToString();
        }

        private bool InsidePolygon((int X, int Y) p)
        {
            return Math.Abs(Nodes.Sum(node => Angle2D((node.Pos.X - p.X, node.Pos.Y - p.Y), (node.Next.Pos.X - p.X, node.Next.Pos.Y - p.Y)))) >= Math.PI;
        }

        private static float Angle2D((int X, int Y) p1, (int X, int Y) p2)
        {
            var dtheta = Math.Atan2(p2.Y, p2.X) - Math.Atan2(p1.Y, p1.X);
            while (dtheta > Math.PI)
                dtheta -= Math.PI * 2;
            while (dtheta < -Math.PI)
                dtheta += Math.PI * 2;

            return (float)dtheta;
        }
    }
}