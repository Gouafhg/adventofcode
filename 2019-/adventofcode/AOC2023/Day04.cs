﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using adventofcode.Utilities;

namespace adventofcode.AOC2023
{
    public class Day04 : Day
    {
        //Globals
        private readonly List<string> _input;

        //Constructor
        public Day04()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2023\day04.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"2023\day04.txt");

            //Test case
            var test1 = @"Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11";

            _input = file.GetLines();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var sum = BigInteger.Zero;
            foreach (var line in _input)
            {
                var correct = line.Split('|')[0].Split(':')[1].Split(' ').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                var guess = line.Split('|')[1].Split(' ').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

                var winning = guess.Where(correct.Contains).ToList();

                if (!winning.Any()) continue;
                
                var p = BigInteger.Pow(2, winning.Count() - 1);
                sum += p;
            }

            return sum.ToString();
        }

        protected override string Part2()
        {
            var cards = _input.Select(GetGameNumber).ToDictionary(gn => gn, _ => 1);

            foreach (var line in _input)
            {
                var gn = GetGameNumber(line);

                var correct = line.Split('|')[0].Split(':')[1].Split(' ').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                var guess = line.Split('|')[1].Split(' ').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

                var winning = guess.Where(correct.Contains).ToList();

                var winCount = winning.Count;
                var cardCount = cards[gn];

                for (var i = 0; i < winCount; ++i)
                {
                    var nextGn = gn + 1 + i;
                    cards[nextGn] += cardCount;
                }
            }

            return cards.Values.Sum().ToString();
        }
        
        private static int GetGameNumber(string line)
        {
            return int.Parse(line.Split(':')[0].Split(' ').Where(x => !string.IsNullOrWhiteSpace(x)).Skip(1).First());
        }
    }
}