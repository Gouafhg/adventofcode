﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using adventofcode.Utilities;

namespace adventofcode.AOC2023
{
    public class Day08Node
    {
        public string Name { get; set; }
        public Day08Node Left { get; set; }
        public Day08Node Right { get; set; }
    }

    public class Day08 : Day
    {
        //Globals
        private readonly List<string> _input;
        private readonly List<Day08Node> _nodes = new List<Day08Node>();
        private readonly string _movement;

        //Constructor
        public Day08()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2023\day08.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"2023\day08.txt");

            //Test case
            var test1 = @"RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)
";

            var test2 = @"LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)";

            var test3 = @"LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)";

            _input = file.GetLines();

            //Do Pre-stuff
            _movement = _input.First();

            foreach (var line in _input.Skip(1))
            {
                var name = line.Split('=')[0].Trim();
                _nodes.Add(new Day08Node { Name = name });
            }

            foreach (var line in _input.Skip(1))
            {
                var name = line.Split('=')[0].Trim();
                var left = line.Split('=')[1].Split(',')[0].Replace("(", string.Empty).Trim();
                var right = line.Split('=')[1].Split(',')[1].Replace(")", string.Empty).Trim();

                var node = _nodes.First(x => x.Name == name);
                node.Left = _nodes.First(x => x.Name == left);
                node.Right = _nodes.First(x => x.Name == right);
            }
        }

        protected override string Part1()
        {
            try
            {
                var start = _nodes.First(x => x.Name == "AAA");
                var end = _nodes.First(x => x.Name == "ZZZ");

                var steps = 0;
                var pos = 0;
                var current = start;
                while (current != end)
                {
                    steps++;
                    current = _movement[pos] == 'L' ? current.Left : current.Right;

                    pos++;
                    pos %= _movement.Length;
                }

                return steps.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        protected override string Part2()
        {
            var nodeSteps = new Dictionary<Day08Node, long>();

            var startNodes = _nodes.Where(x => x.Name.EndsWith("A")).ToList();
            foreach (var node in startNodes)
            {
                long nodeStep = 0;
                var pos = 0;
                var current = node;

                while (!current.Name.EndsWith("Z"))
                {
                    nodeStep++;
                    current = _movement[pos] == 'L' ? current.Left : current.Right;

                    pos++;
                    pos %= _movement.Length;

                    if (current.Name.EndsWith("Z"))
                    {
                        nodeSteps.Add(node, nodeStep);
                    }
                }
            }

            return nodeSteps
                .SelectMany(x => GetPrimes((int)x.Value))
                .Distinct()
                .Aggregate(BigInteger.One, (current1, prime) => current1 * prime)
                .ToString();
        }

        private static IEnumerable<int> GetPrimes(int a)
        {
            switch (a)
            {
                case 1:
                    yield break;
                case 2:
                    yield return 2;
                    yield break;
            }

            //Oj, Range funkar inte så nej. Men OK!
            foreach (var x in Enumerable.Range(2, a / 2).Where(x => a % x == 0)) yield return x;
        }
    }
}