﻿using System;
using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2023
{
    public class Day11Galaxy
    {
        public int Index { get; set; }
        public (int X, int Y) Position { get; set; }
        public Dictionary<Day11Galaxy, (int gridDistance, int emptyVoidDistance)> Distances { get; set; } = new Dictionary<Day11Galaxy, (int, int)>();
    }

    public class Day11 : Day
    {
        //Globals
        private readonly List<string> _input;

        private readonly List<Day11Galaxy> _galaxies;

        //Constructor
        public Day11()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2023\day11.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"2023\day11.txt");

            //Test case
            var test1 = @"...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....";

            _input = file.GetLines();

            //Do Pre-stuff
            var grid = _input.Select(x => x.ToList()).ToList();
            var width = grid[0].Count;
            var height = grid.Count;

            _galaxies = new List<Day11Galaxy>();
            for (var r = 0; r < height; ++r)
            {
                for (var c = 0; c < width; ++c)
                {
                    if (grid[r][c] == '#')
                    {
                        var galaxy = new Day11Galaxy { Position = (c, r), Index = _galaxies.Count };
                        _galaxies.Add(galaxy);
                    }
                }
            }
            
            var emptyColumns = Enumerable.Range(0, width).Where(c => grid.Select(x => x[c]).ToList().All(x => x == '.')).ToList();
            var emptyRows = Enumerable.Range(0, height).Where(r => grid[r].All(x => x == '.')).ToList();
            
            foreach (var galaxy in _galaxies)
            {
                foreach (var otherGalaxy in _galaxies)
                {
                    if (galaxy == otherGalaxy || galaxy.Distances.ContainsKey(otherGalaxy))
                    {
                        continue;
                    }

                    var distance = Math.Abs(galaxy.Position.X - otherGalaxy.Position.X) + Math.Abs(galaxy.Position.Y - otherGalaxy.Position.Y);
                    var minColumn = Math.Min(galaxy.Position.X, otherGalaxy.Position.X);
                    var maxColumn = Math.Max(galaxy.Position.X, otherGalaxy.Position.X);
                    var minRow = Math.Min(galaxy.Position.Y, otherGalaxy.Position.Y);
                    var maxRow = Math.Max(galaxy.Position.Y, otherGalaxy.Position.Y);

                    var columns = minColumn == maxColumn ? new List<int>() : Enumerable.Range(minColumn + 1, maxColumn - minColumn - 1);
                    var rows = minRow == maxRow ? new List<int>() : Enumerable.Range(minRow + 1, maxRow - minRow - 1);

                    galaxy.Distances[otherGalaxy] = (distance, columns.Count(x => emptyColumns.Contains(x)) + rows.Count(x => emptyRows.Contains(x)));
                    otherGalaxy.Distances[galaxy] = galaxy.Distances[otherGalaxy];
                }
            }

        }

        protected override string Part1()
        {
            long sum = 0;
            for (var i = 0; i < _galaxies.Count; ++i)
            {
                for (var j = i + 1; j < _galaxies.Count; ++j)
                {
                    var distance = _galaxies[i].Distances[_galaxies[j]]; 
                    sum += distance.gridDistance + distance.emptyVoidDistance;
                }
            }

            return sum.ToString();
        }

        protected override string Part2()
        {
            long sum = 0;
            for (var i = 0; i < _galaxies.Count; ++i)
            {
                for (var j = i + 1; j < _galaxies.Count; ++j)
                {
                    var distance = _galaxies[i].Distances[_galaxies[j]]; 
                    sum += distance.gridDistance + distance.emptyVoidDistance * 1000000;
                }
            }

            return sum.ToString();
        }
    }
}