﻿using System;
using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2023
{
    public class Day06Race
    {
        public int Time { get; set; }
        public int Distance { get; set; }
        public int Ways { get; set; }
    }

    public class Day06 : Day
    {
        //Globals
        private readonly List<string> _input;

        //Constructor
        public Day06()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = @"Time:        62     64     91     90
Distance:   553   1010   1473   1074";

            //Test case
            var test1 = @"Time:      7  15   30
Distance:  9  40  200";

            _input = file.GetLines();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var times = _input[0].Split(':')[1].Split(' ').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
            var distances = _input[1].Split(':')[1].Split(' ').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

            var races = times.Select((t, i) => new Day06Race { Time = int.Parse(t), Distance = int.Parse(distances[i]) }).ToList();

            foreach (var race in races)
            {
                var x1 = (int)(race.Time / 2f + Math.Sqrt(race.Time * race.Time / 4 - race.Distance));
                var x2 = (int)(race.Time / 2f - Math.Sqrt(race.Time * race.Time / 4 - race.Distance));

                var from = Math.Min(x1, x2) - 2;
                var to = Math.Max(x1, x2) + 2;
                while ((race.Time - from) * from <= race.Distance) from++;
                while ((race.Time - to) * to <= race.Distance) to--;

                var ways = Math.Max(from, to) - Math.Min(from, to) + 1;
                race.Ways = ways;
            }

            return races.Aggregate(1, (current, race) => current * race.Ways).ToString();
        }

        protected override string Part2()
        {
            var times = _input[0].Split(':')[1].Split(' ').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
            var distances = _input[1].Split(':')[1].Split(' ').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

            var races = times.Select((t, i) => new Day06Race { Time = int.Parse(t), Distance = int.Parse(distances[i]) }).ToList();

            var t = long.Parse(races.Aggregate(string.Empty, (current, race) => current + race.Time));
            var d = long.Parse(races.Aggregate(string.Empty, (current, race) => current + race.Distance));

            var x1 = (long)(t / 2f + Math.Sqrt(t * t / 4 - d));
            var x2 = (long)(t / 2f - Math.Sqrt(t * t / 4 - d));

            var from = Math.Min(x1, x2) - 2;
            var to = Math.Max(x1, x2) + 2;
            while ((t - from) * from <= d) from++;
            while ((t - to) * to <= d) to--;

            var ways = Math.Max(from, to) - Math.Min(from, to) + 1;
            return ways.ToString();
        }
    }
}