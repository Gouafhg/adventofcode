﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using adventofcode.Utilities;

namespace adventofcode.AOC2023
{
    public class MapRow
    {
        public MapRow(BigInteger sourceStart, BigInteger sourceEnd, BigInteger destinationStart)
        {
            SourceStart = sourceStart;
            SourceEnd = sourceEnd;
            DestinationStart = destinationStart;
        }

        public BigInteger SourceStart { get; set; }
        public BigInteger SourceEnd { get; set; }
        private BigInteger DestinationStart { get; set; }

        public BigInteger Move => DestinationStart - SourceStart;
    }

    public class Day05 : Day
    {
        //Globals
        private readonly List<string> _input;

        private List<BigInteger> _seeds;
        private readonly List<MapRow> _seedToSoilMap;
        private readonly List<MapRow> _soilToFertilizerMap;
        private readonly List<MapRow> _fertilizerToWaterMap;
        private readonly List<MapRow> _waterToLightMap;
        private readonly List<MapRow> _lightToTemperatureMap;
        private readonly List<MapRow> _temperatureToHumidityMap;
        private readonly List<MapRow> _humidityToLocationMap;

        //Constructor
        public Day05()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2023\day05.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"2023\day05.txt");

            //Test case
            var test1 = @"seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4";

            _input = file.GetLines();

            var maplines = _input.Where(x => x.Contains("map")).Select(x => _input.IndexOf(x)).ToList();

            List<MapRow> GetMapRows(int index) => _input.GetRange(maplines[index] + 1,
                index >= maplines.Count - 1
                    ? _input.Count - maplines[index] - 1
                    : maplines[index + 1] - maplines[index] - 1).Select(x =>
            {
                var sourceStart = BigInteger.Parse(x.Split(' ')[1]);
                var destinationStart = BigInteger.Parse(x.Split(' ')[0]);
                var range = BigInteger.Parse(x.Split(' ')[2]);

                return new MapRow(sourceStart, sourceStart + range - 1, destinationStart);
            }).OrderBy(x => x.SourceStart).ToList();

            _seedToSoilMap = GetMapRows(0);
            _soilToFertilizerMap = GetMapRows(1);
            _fertilizerToWaterMap = GetMapRows(2);
            _waterToLightMap = GetMapRows(3);
            _lightToTemperatureMap = GetMapRows(4);
            _temperatureToHumidityMap = GetMapRows(5);
            _humidityToLocationMap = GetMapRows(6);
        }

        protected override string Part1()
        {
            _seeds = _input[0].Split(':')[1].Split(' ').Where(x => !string.IsNullOrWhiteSpace(x))
                .Select(BigInteger.Parse).ToList();

            var locations = new List<BigInteger>();
            foreach (var seed in _seeds)
            {
                var value = seed;

                value += _seedToSoilMap.FirstOrDefault(x => x.SourceStart <= value && x.SourceEnd >= value)?.Move ?? 0;
                value += _soilToFertilizerMap.FirstOrDefault(x => x.SourceStart <= value && x.SourceEnd >= value)?.Move ?? 0;
                value += _fertilizerToWaterMap.FirstOrDefault(x => x.SourceStart <= value && x.SourceEnd >= value)?.Move ?? 0;
                value += _waterToLightMap.FirstOrDefault(x => x.SourceStart <= value && x.SourceEnd >= value)?.Move ?? 0;
                value += _lightToTemperatureMap.FirstOrDefault(x => x.SourceStart <= value && x.SourceEnd >= value)?.Move ?? 0;
                value += _temperatureToHumidityMap.FirstOrDefault(x => x.SourceStart <= value && x.SourceEnd >= value)?.Move ?? 0;
                value += _humidityToLocationMap.FirstOrDefault(x => x.SourceStart <= value && x.SourceEnd >= value)?.Move ?? 0;

                locations.Add(value);
            }

            return locations.Min().ToString();
        }

        protected override string Part2()
        {
            _seeds = _input[0].Split(':')[1].Split(' ').Where(x => !string.IsNullOrWhiteSpace(x)).Select(BigInteger.Parse).ToList();

            var seedRanges = new List<(BigInteger start, BigInteger end)>();

            for (var i = 0; i < _seeds.Count; i += 2)
            {
                var seedStart = _seeds[i];
                var seedEnd = seedStart + _seeds[i + 1];
                seedRanges.Add((seedStart, seedEnd));
            }

            seedRanges = seedRanges.OrderBy(x => x.start).ToList();

            var soilRanges = GetRanges(_seedToSoilMap, seedRanges);
            var fertilizerRanges = GetRanges(_soilToFertilizerMap, soilRanges);
            var waterRanges = GetRanges(_fertilizerToWaterMap, fertilizerRanges);
            var lightRanges = GetRanges(_waterToLightMap, waterRanges);
            var temperatureRanges = GetRanges(_lightToTemperatureMap, lightRanges);
            var humidityRanges = GetRanges(_temperatureToHumidityMap, temperatureRanges);
            var locationRanges = GetRanges(_humidityToLocationMap, humidityRanges);

            return locationRanges.Min(x => x.start).ToString();
        }

        private static List<(BigInteger start, BigInteger end)> GetRanges(IReadOnlyCollection<MapRow> map,
            IList<(BigInteger start, BigInteger end)> ranges)
        {
            var resultingRange = new List<(BigInteger start, BigInteger end)>();
            for (var i = 0; i < ranges.Count; ++i)
            {
                var range = ranges[i];

                var firstSeed = range.start;
                var row = GetMapRow(map, firstSeed);
                if (row != null)
                {
                    var last = BigInteger.Min(range.end, row.SourceEnd);
                    var prevRange = (start: firstSeed, end: last);
                    ranges[i] = prevRange;

                    resultingRange.Add((prevRange.start + row.Move, prevRange.end + row.Move));

                    if (last < range.end)
                    {
                        var nextRange = (last + 1, range.end);
                        ranges.Add(nextRange);
                    }
                }
                else
                {
                    var nextRow = GetNextRow(map, range);
                    if (nextRow != null)
                    {
                        var last = nextRow.SourceStart - 1;

                        var prevRange = (start: firstSeed, end: last);
                        ranges[i] = prevRange;
                        resultingRange.Add((prevRange.start, prevRange.end));

                        var nextRange = (nextRow.SourceStart, range.end);
                        ranges.Add(nextRange);
                    }
                    else
                    {
                        resultingRange.Add(range);
                    }
                }
            }

            return resultingRange;
        }

        private static MapRow GetMapRow(IEnumerable<MapRow> map, BigInteger s) => map.FirstOrDefault(x => x.SourceStart <= s && x.SourceEnd >= s);

        private static MapRow GetNextRow(IEnumerable<MapRow> map, (BigInteger start, BigInteger end) range) => map.FirstOrDefault(x => range.start < x.SourceStart && range.end >= x.SourceStart);

        private static BigInteger GetMin(params BigInteger[] values) => values.Min();
        private static BigInteger GetMax(params BigInteger[] values) => values.Max();
    }
}