﻿using adventofcode.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2023
{
    public class Day02 : Day
    {
        //Globals
        private readonly List<string> _input;

        //Constructorday
        public Day02()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2023\day02.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"2023\day02.txt");

            //Test case
            var test1 = @"Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";

            _input = file.GetLines();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var maxRed = 12;
            var maxGreen = 13;
            var maxBlue = 14;

            var sum = 0;
            foreach (var line in _input.Select(x => x.ToLower()))
            {
                var possibleGame = true;
                var counts = line.Split(' ');

                for (var i = 0; i < counts.Length; ++i)
                {
                    if (!counts[i].Contains("red")) continue;

                    var count = counts[i - 1];
                    if (int.Parse(count) > maxRed)
                    {
                        possibleGame = false;
                    }
                }

                for (var i = 0; i < counts.Length; ++i)
                {
                    if (!counts[i].Contains("green")) continue;

                    var count = counts[i - 1];
                    if (int.Parse(count) > maxGreen)
                    {
                        possibleGame = false;
                    }
                }

                for (var i = 0; i < counts.Length; ++i)
                {
                    if (!counts[i].Contains("blue")) continue;

                    var count = counts[i - 1];
                    if (int.Parse(count) > maxBlue)
                    {
                        possibleGame = false;
                    }
                }

                if (!possibleGame)
                {
                    continue;
                }

                var gameId = line.Split(' ')[1].Replace(":", "");

                sum += int.Parse(gameId);
            }

            return sum.ToString();
        }

        protected override string Part2()
        {
            var sum = 0;
            foreach (var line in _input.Select(x => x.ToLower()))
            {
                var lineMinRed = int.MinValue;
                var lineMinGreen = int.MinValue;
                var lineMinBlue = int.MinValue;

                if (line.Contains("red"))
                {
                    var counts = line.Split(' ');
                    for (var i = 0; i < counts.Length; ++i)
                    {
                        if (!counts[i].Contains("red")) continue;

                        var count = counts[i - 1];
                        if (int.Parse(count) > lineMinRed)
                        {
                            lineMinRed = int.Parse(count);
                        }
                    }
                }

                if (line.Contains("green"))
                {
                    var counts = line.Split(' ');
                    for (var i = 0; i < counts.Length; ++i)
                    {
                        if (!counts[i].Contains("green")) continue;

                        var count = counts[i - 1];
                        if (int.Parse(count) > lineMinGreen)
                        {
                            lineMinGreen = int.Parse(count);
                        }
                    }
                }

                if (line.Contains("blue"))
                {
                    var counts = line.Split(' ');
                    for (var i = 0; i < counts.Length; ++i)
                    {
                        if (!counts[i].Contains("blue")) continue;

                        var count = counts[i - 1];
                        if (int.Parse(count) > lineMinBlue)
                        {
                            lineMinBlue = int.Parse(count);
                        }
                    }
                }

                var power = lineMinRed * lineMinGreen * lineMinBlue;

                sum += power;
            }

            return sum.ToString();
        }
    }
}