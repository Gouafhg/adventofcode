﻿using System;
using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2023
{
    public class Day14Stone
    {
        public char Type { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
    }

    public class Day14 : Day
    {
        //Globals
        private readonly List<string> _input;
        private readonly List<List<char>> _grid = new List<List<char>>();
        private readonly List<Day14Stone> _stones = new List<Day14Stone>();

        private readonly int _height;
        private readonly int _width;

        //Constructor
        public Day14()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2023\day14.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"2023\day14.txt");

            //Test case
            var test1 = @"O....#....
O.OO#....#
.....##...
OO.#O....O
.O.....O#.
O.#..O.#.#
..O..#O..O
.......O..
#....###..
#OO..#....";

            _input = file.GetLines();

            //Do Pre-stuff
            for (var r = 0; r < _input.Count; r++)
            {
                _grid.Add(new List<char>());
                for (var c = 0; c < _input[r].Length; c++)
                {
                    _grid[r].Add(_input[r][c] == 'O' ? '.' : _input[r][c]);
                    if (_input[r][c] == 'O')
                    {
                        _stones.Add(new Day14Stone { Type = 'O', X = c, Y = r });
                    }
                }
            }

            _height = _grid.Count;
            _width = _grid[0].Count;
        }

        protected override string Part1()
        {
            RollNorth();
            return _stones.Sum(x => _height - x.Y).ToString();
        }

        protected override string Part2()
        {
            var results = new Dictionary<long, List<int>>();
            for (var i = 0; i < 1000000000; i++)
            {
                Cycle();

                var sum = _stones.Sum(x => _height - x.Y);

                if (results.ContainsKey(sum))
                {
                    results[sum].Add(i + 1);
                }
                else
                {
                    results.Add(sum, new List<int>() { i + 1 });
                }

                string GetResultString(KeyValuePair<long, List<int>> kvp) => $"[{kvp.Key}]: {kvp.Value.Join(", ")}";
                
                Console.WriteLine(results.Select(GetResultString).Join(Environment.NewLine));
                Console.WriteLine();
            }

            return _stones.Sum(x => _height - x.Y).ToString();
        }

        private void Cycle()
        {
            RollNorth();
            RollWest();
            RollSouth();
            RollEast();
        }

        private void RollNorth()
        {
            while (_stones.Any(x => x.Y != 0 && _grid[x.Y - 1][x.X] == '.' && !_stones.Any(y => x != y && x.Y - 1 == y.Y && x.X == y.X)))
            {
                foreach (var stone in _stones.Where(stone => stone.Y > 0 && _grid[stone.Y - 1][stone.X] == '.' && !_stones.Any(x => x.Y == stone.Y - 1 && x.X == stone.X)))
                {
                    stone.Y--;
                }
            }
        }

        private void RollSouth()
        {
            while (_stones.Any(x => x.Y != _height - 1 && _grid[x.Y + 1][x.X] == '.' && !_stones.Any(y => x != y && x.Y + 1 == y.Y && x.X == y.X)))
            {
                foreach (var stone in _stones.Where(stone => stone.Y < _height - 1 && _grid[stone.Y + 1][stone.X] == '.' && !_stones.Any(x => x.Y == stone.Y + 1 && x.X == stone.X)))
                {
                    stone.Y++;
                }
            }
        }

        private void RollWest()
        {
            while (_stones.Any(x => x.X != 0 && _grid[x.Y][x.X - 1] == '.' && !_stones.Any(y => x != y && x.Y == y.Y && x.X - 1 == y.X)))
            {
                foreach (var stone in _stones.Where(stone => stone.X > 0 && _grid[stone.Y][stone.X - 1] == '.' && !_stones.Any(x => x.Y == stone.Y && x.X == stone.X - 1)))
                {
                    stone.X--;
                }
            }
        }

        private void RollEast()
        {
            while (_stones.Any(x => x.X != _width - 1 && _grid[x.Y][x.X + 1] == '.' && !_stones.Any(y => x != y && x.Y == y.Y && x.X + 1 == y.X)))
            {
                foreach (var stone in _stones.Where(stone => stone.X < _width - 1 && _grid[stone.Y][stone.X + 1] == '.' && !_stones.Any(x => x.Y == stone.Y && x.X == stone.X + 1)))
                {
                    stone.X++;
                }
            }
        }
    }
}