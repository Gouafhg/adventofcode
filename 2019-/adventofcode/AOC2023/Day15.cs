﻿using System;
using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2023
{
    public class Day15 : Day
    {
        //Globals
        private readonly List<string> _input;

        //Constructor
        public Day15()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2023\day15.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"2023\day15.txt");

            //Test case
            var test1 = @"rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7";

            _input = file.GetLines();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            return _input.Join("").Split(',').Aggregate<string, long>(0, (current, input) => current + GetHash(input)).ToString();
        }

        private class Day15Lens
        {
            public Day15Box Box;
            public string Label;
            public int FocalLength;

            public override string ToString()
            {
                return $"{Label} {FocalLength}";
            }
        }

        private class Day15Box
        {
            public int Index;
            public readonly List<Day15Lens> Lenses = new List<Day15Lens>();

            public override string ToString()
            {
                return $"{Index} [{Lenses.Join("] [")}]";
            }
        }

        private static int GetHash(string input)
        {
            var value = 0;
            foreach (var asciiValue in input.Select(c => (int)c))
            {
                value += asciiValue;
                value *= 17;
                value %= 256;
            }

            return value;
        }

        protected override string Part2()
        {
            var commaSeparatedInput = _input.Join("").Split(',');

            var boxes = new Day15Box[256];
            for (var i = 0; i < 256; ++i)
            {
                boxes[i] = new Day15Box { Index = i };
            }

            foreach (var input in commaSeparatedInput)
            {
                if (input.Contains('-'))
                {
                    var label = input.Split('-')[0];
                    var hash = GetHash(label);
                    var box = boxes[hash];

                    box.Lenses.RemoveAll(x => x.Label == label);
                }
                else if (input.Contains('='))
                {
                    var label = input.Split('=')[0];
                    var hash = GetHash(label);
                    var box = boxes[hash];

                    var focalLength = input.Split('=')[1];
                    if (box.Lenses.Any(x => x.Label == label))
                    {
                        var lens = box.Lenses.First(x => x.Label == label);
                        var index = box.Lenses.IndexOf(lens);
                        box.Lenses.Insert(index, new Day15Lens { Box = box, Label = label, FocalLength = int.Parse(focalLength) });
                        box.Lenses.RemoveAt(index + 1);
                    }
                    else
                    {
                        box.Lenses.Add(new Day15Lens { Box = box, Label = label, FocalLength = int.Parse(focalLength) });
                    }

                }
                else
                {
                    throw new Exception();
                }
            }


            var focusingPower = boxes.Sum(box => box.Lenses.Select(GetFocalPower).Sum());

            return focusingPower.ToString();
        }

        private long GetFocalPower(Day15Lens lens)
        {
            var value = lens.Box.Index + 1;
            value *= lens.Box.Lenses.IndexOf(lens) + 1;
            value *= lens.FocalLength;
            return value;
        }
    }
}