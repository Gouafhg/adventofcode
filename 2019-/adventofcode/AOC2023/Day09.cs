﻿using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;
using adventofcode.Utilities.Extensions;

namespace adventofcode.AOC2023
{
    public class Day09 : Day
    {
        //Globals
        private readonly List<string> _input;

        //Constructor
        public Day09()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2023\day09.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"2023\day09.txt");

            //Test case
            var test1 = @"0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45";

            _input = file.GetLines();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var linesAsNumbers = _input.Select(x => x.Split(' ').Select(y => y.ToInt()).ToList()).ToList();
            foreach (var line in linesAsNumbers)
            {
                var linesInLine = new List<List<int>>() { line.ToList() };
                while (linesInLine.Last().Any(x => x != 0))
                {
                    var oldLine = linesInLine.Last();
                    var newLine = new List<int>();
                    for (var i = 1; i < oldLine.Count; i++)
                    {
                        var newNumber = oldLine[i]  -oldLine[i - 1];
                        newLine.Add(newNumber);
                    }
                    linesInLine.Add(newLine);
                }
                
                for (var i = linesInLine.Count - 1; i >= 0; i--)
                {
                    if (i == linesInLine.Count - 1)
                    {
                        linesInLine[i].Add(0);
                    }
                    else
                    {
                        var lineInLine = linesInLine[i];
                        var nextLineInLine = linesInLine[i + 1];
                        lineInLine.Add(lineInLine.Last() + nextLineInLine.Last());
                    }
                }

                line.Add(linesInLine.First().Last());
            }
            
            return linesAsNumbers.Sum(x => x.Last()).ToString();
        }

        protected override string Part2()
        {
            var linesAsNumbers = _input.Select(x => x.Split(' ').Select(y => y.ToInt()).ToList()).ToList();
            foreach (var line in linesAsNumbers)
            {
                var linesInLine = new List<List<int>>() { line.ToList() };
                while (linesInLine.Last().Any(x => x != 0))
                {
                    var oldLine = linesInLine.Last();
                    var newLine = new List<int>();
                    for (var i = 1; i < oldLine.Count; i++)
                    {
                        var newNumber = oldLine[i]  -oldLine[i - 1];
                        newLine.Add(newNumber);
                    }
                    linesInLine.Add(newLine);
                }
                
                for (var i = linesInLine.Count - 1; i >= 0; i--)
                {
                    if (i == linesInLine.Count - 1)
                    {
                        linesInLine[i].Add(0);
                    }
                    else
                    {
                        var lineInLine = linesInLine[i];
                        var nextLineInLine = linesInLine[i + 1];
                        lineInLine.Insert(0, lineInLine.First() - nextLineInLine.First());
                    }
                }

                line.Add(linesInLine.First().First());
            }
            
            return linesAsNumbers.Sum(x => x.Last()).ToString();
        }
    }
}