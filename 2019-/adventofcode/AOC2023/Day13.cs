﻿using System;
using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2023
{
    public enum ReflectionTypeEnum
    {
        Horizontal,
        Vertical,
    }

    public class Day13Grid
    {
        public List<List<char>> Grid { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
    }

    public class Day13 : Day
    {
        //Globals
        private readonly List<string> _input;
        private readonly List<Day13Grid> _grids;

        //Constructor
        public Day13()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2023\day13.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"2023\day13.txt");

            //Test case
            var test1 = @"#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.

#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#";

            _input = file.GetLines(true);

            //Do Pre-stuff
            _grids = new List<Day13Grid>();
            var rowIndex = 0;
            while (rowIndex < _input.Count)
            {
                var gridRows = _input.Skip(rowIndex).TakeWhile(x => !string.IsNullOrWhiteSpace(x)).ToList();
                rowIndex += gridRows.Count + 1;

                var grid = new List<List<char>>();
                for (var r = 0; r < gridRows.Count; r++)
                {
                    grid.Add(new List<char>());
                    for (var c = 0; c < gridRows[r].Length; c++)
                    {
                        grid[r].Add(gridRows[r][c]);
                    }
                }

                var height = grid.Count;
                var width = grid[0].Count;
                _grids.Add(new Day13Grid { Grid = grid, Height = height, Width = width });
            }
        }

        private static List<int> FindHorizontalReflection(Day13Grid grid)
        {
            //r == 1 betyder att strecket går efter 0, innan 1
            var result = new List<int>();
            for (var r = 1; r < grid.Height; r++)
            {
                var rowsAbove = grid.Grid.Take(r).Select(x => x.Join()).ToList();
                var rowsBelow = grid.Grid.Skip(r).Select(x => x.Join()).ToList();

                rowsAbove.Reverse();

                var length = Math.Min(rowsAbove.Count, rowsBelow.Count);
                if (rowsAbove.Take(length).SequenceEqual(rowsBelow.Take(length)))
                {
                    result.Add(r);
                }
            }

            return result;
        }

        private static List<int> FindVerticalReflection(Day13Grid grid)
        {
            var result = new List<int>();
            //c == 1 betyder att strecket går efter 0, innan 1
            for (var c = 1; c < grid.Width; c++)
            {
                var columnsLeft = Enumerable.Range(0, grid.Width).Take(c).Select(i => grid.Grid.Select(x => x[i]).Join()).ToList();
                var columnsRight = Enumerable.Range(0, grid.Width).Skip(c).Select(i => grid.Grid.Select(x => x[i]).Join()).ToList();

                columnsLeft.Reverse();

                var length = Math.Min(columnsLeft.Count, columnsRight.Count);
                if (columnsLeft.Take(length).SequenceEqual(columnsRight.Take(length)))
                {
                    result.Add(c);
                }
            }

            return result;
        }

        protected override string Part1()
        {
            var sum = 0;
            foreach (var grid in _grids)
            {
                var horizontalReflection = FindHorizontalReflection(grid);

                if (horizontalReflection.Any())
                {
                    sum += 100 * horizontalReflection.First();
                }
                else
                {
                    var verticalReflection = FindVerticalReflection(grid);
                    if (verticalReflection.Any())
                    {
                        sum += verticalReflection.First();
                    }
                }
            }

            return sum.ToString();
        }

        protected override string Part2()
        {
            var dict = new Dictionary<Day13Grid, (ReflectionTypeEnum type, int index)>();

            foreach (var grid in _grids)
            {
                var horizontalReflection = FindHorizontalReflection(grid);

                if (horizontalReflection.Any())
                {
                    dict.Add(grid, (ReflectionTypeEnum.Horizontal, horizontalReflection.First()));
                }
                else
                {
                    var verticalReflection = FindVerticalReflection(grid);
                    if (verticalReflection.Any())
                    {
                        dict.Add(grid, (ReflectionTypeEnum.Vertical, verticalReflection.First()));
                    }
                }
            }

            var sum = 0;
            foreach (var grid in _grids)
            {
                var found = false;
                for (var r = 0; r < grid.Height; ++r)
                {
                    for (var c = 0; c < grid.Width; ++c)
                    {
                        var pos = (c, r);
                        SwitchPos(grid, pos);

                        var value = 0;
                        var horizontalReflection = FindHorizontalReflection(grid);
                        if (dict[grid].type == ReflectionTypeEnum.Horizontal)
                        {
                            horizontalReflection.Remove(dict[grid].index);
                        }
                        if (horizontalReflection.Any())
                        {
                            value += 100 * horizontalReflection.First();
                        }
                        
                        var verticalReflection = value != 0 ? new List<int>() : FindVerticalReflection(grid);
                        if (dict[grid].type == ReflectionTypeEnum.Vertical)
                        {
                            verticalReflection.Remove(dict[grid].index);
                        }
                        if (verticalReflection.Any())
                        {
                            value += verticalReflection.First();
                        }

                        sum += value;

                        if (value != 0)
                        {
                            // Console.WriteLine(pos);
                            found = true;
                            c += grid.Width;
                            r += grid.Height;
                        }

                        SwitchPos(grid, pos);
                    }
                }

                if (found) continue;
                
                Console.WriteLine(dict[grid].type + ", " + dict[grid].index);
                Console.WriteLine(grid.Grid.Select(x => x.Join()).Join(Environment.NewLine));
                Console.WriteLine();
            }

            return sum.ToString();
        }

        private static void SwitchPos(Day13Grid grid, (int c, int r) pos)
        {
            grid.Grid[pos.r][pos.c] = grid.Grid[pos.r][pos.c] switch
            {
                '#' => '.',
                '.' => '#',
                _ => grid.Grid[pos.r][pos.c]
            };
        }
    }
}