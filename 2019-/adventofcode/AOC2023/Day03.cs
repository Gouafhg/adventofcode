﻿using System.Collections.Generic;
using System.Linq;
using adventofcode.Utilities;

namespace adventofcode.AOC2023
{
    public class Day03 : Day
    {
        //Globals
        private readonly List<string> _input;

        private readonly int _width;
        private readonly int _height;

        //Constructor
        public Day03()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2023\day03.txt");
            // var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day.txt");

            //Test case
            var test1 = @"467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..";

            _input = file.GetLines();

            //Do Pre-stuff

            _width = _input.First().Length;
            _height = _input.Count;
        }

        protected override string Part1()
        {
            var newInput = _input.ToList();

            var symbols = new List<(int x, int y)>();
            var numbers = new List<string>();
            for (var x = 0; x < _width; ++x)
            {
                for (var y = 0; y < _height; ++y)
                {
                    if (!char.IsDigit(newInput[y][x]) && newInput[y][x] != '.')
                    {
                        symbols.Add((x, y));
                    }
                }
            }

            foreach (var symbol in symbols)
            {
                var aboveLeft = (x: symbol.x - 1, y: symbol.y - 1);
                var above = (x: symbol.x, y: symbol.y - 1);
                var aboveRight = (x: symbol.x + 1, y: symbol.y - 1);
                var left = (x: symbol.x - 1, y: symbol.y);
                var right = (x: symbol.x + 1, y: symbol.y);
                var belowLeft = (x: symbol.x - 1, y: symbol.y + 1);
                var below = (x: symbol.x, y: symbol.y + 1);
                var belowRight = (x: symbol.x + 1, y: symbol.y + 1);

                FixNumber(ref newInput, aboveLeft);
                FixNumber(ref newInput, above);
                FixNumber(ref newInput, aboveRight);
                FixNumber(ref newInput, left);
                FixNumber(ref newInput, right);
                FixNumber(ref newInput, belowLeft);
                FixNumber(ref newInput, below);
                FixNumber(ref newInput, belowRight);
            }

            var otherInput = _input.ToList();

            for (var y = 0; y < _height; ++y)
            {
                for (var x = 0; x < _width; ++x)
                {
                    if (!char.IsDigit(otherInput[y][x])) continue;
                    if (char.IsDigit(newInput[y][x])) continue;

                    var number = string.Empty;
                    while (x < _width && char.IsDigit(otherInput[y][x]))
                    {
                        number += otherInput[y][x];
                        ++x;
                    }

                    numbers.Add(number);
                }
            }

            return numbers.Sum(int.Parse).ToString();
        }

        protected override string Part2()
        {
            var newInput = _input.ToList();

            var gears = new List<(int x, int y)>();
            var numbers = new List<int>();

            for (var x = 0; x < _width; ++x)
            {
                for (var y = 0; y < _height; ++y)
                {
                    var isGear = newInput[y][x] == '*';
                    if (isGear)
                    {
                        gears.Add((x, y));
                    }
                }
            }

            foreach (var gear in gears)
            {
                var aboveLeft = (x: gear.x - 1, y: gear.y - 1);
                var above = (x: gear.x, y: gear.y - 1);
                var aboveRight = (x: gear.x + 1, y: gear.y - 1);
                var left = (x: gear.x - 1, y: gear.y);
                var right = (x: gear.x + 1, y: gear.y);
                var belowLeft = (x: gear.x - 1, y: gear.y + 1);
                var below = (x: gear.x, y: gear.y + 1);
                var belowRight = (x: gear.x + 1, y: gear.y + 1);

                var aboveLeftIsNumber = IsNumber(newInput, aboveLeft);
                var aboveIsNumber = IsNumber(newInput, above);
                var aboveRightIsNumber = IsNumber(newInput, aboveRight);
                var leftIsNumber = IsNumber(newInput, left);
                var rightIsNumber = IsNumber(newInput, right);
                var belowLeftIsNumber = IsNumber(newInput, belowLeft);
                var belowIsNumber = IsNumber(newInput, below);
                var belowRightIsNumber = IsNumber(newInput, belowRight);

                var numberCount = 0;
                if (aboveLeftIsNumber) numberCount++;
                if (aboveIsNumber) numberCount++;
                if (aboveRightIsNumber) numberCount++;
                if (leftIsNumber) numberCount++;
                if (rightIsNumber) numberCount++;
                if (belowLeftIsNumber) numberCount++;
                if (belowIsNumber) numberCount++;
                if (belowRightIsNumber) numberCount++;

                if (aboveLeftIsNumber && aboveIsNumber)
                {
                    numberCount--;
                    aboveLeftIsNumber = false;
                }

                if (aboveIsNumber && aboveRightIsNumber)
                {
                    numberCount--;
                    aboveIsNumber = false;
                }

                if (belowLeftIsNumber && belowIsNumber)
                {
                    numberCount--;
                    belowLeftIsNumber = false;
                }

                if (belowIsNumber && belowRightIsNumber)
                {
                    numberCount--;
                    belowIsNumber = false;
                }
                
                if (numberCount != 2 ) continue;
                
                var gearNumbers = new List<string>();
                if (aboveLeftIsNumber) gearNumbers.Add(GetNumber(newInput, aboveLeft));
                if (aboveIsNumber) gearNumbers.Add(GetNumber(newInput, above));
                if (aboveRightIsNumber) gearNumbers.Add(GetNumber(newInput, aboveRight));
                if (leftIsNumber) gearNumbers.Add(GetNumber(newInput, left));
                if (rightIsNumber) gearNumbers.Add(GetNumber(newInput, right));
                if (belowLeftIsNumber) gearNumbers.Add(GetNumber(newInput, belowLeft));
                if (belowIsNumber) gearNumbers.Add(GetNumber(newInput, below));
                if (belowRightIsNumber) gearNumbers.Add(GetNumber(newInput, belowRight));
                

                var gearRatio = 1;
                gearNumbers.ForEach(x => gearRatio *= int.Parse(x));
                numbers.Add(gearRatio);
            }

            return numbers.Sum().ToString();
        }
        
        private void FixNumber(ref List<string> input, (int x, int y) pos)
        {
            if (pos.x < 0 || pos.y < 0 || pos.x >= _width || pos.y >= _height || !char.IsDigit(input[pos.y][pos.x]))
            {
                return;
            }

            var p = pos.x;

            do
            {
                p--;
            } while (p >= 0 && char.IsDigit(input[pos.y][p]));

            p++;

            var row = input[pos.y].Substring(0, p);
            do
            {
                row += ".";
                ++p;
            } while (p < _width && char.IsDigit(input[pos.y][p]));

            row += input[pos.y].Substring(p);
            input[pos.y] = row;
        }

        private bool IsNumber(List<string> input, (int x, int y) pos)
        {
            return pos is { x: >= 0, y: >= 0 } && pos.x < _width && pos.y < _height && char.IsDigit(input[pos.y][pos.x]);
        }
        
        private string GetNumber(List<string> input, (int x, int y) pos)
        {
            if (pos.x < 0 || pos.y < 0 || pos.x >= _width || pos.y >= _height ||
                !char.IsDigit(input[pos.y][pos.x]))
            {
                return "1";
            }

            var number = string.Empty;
            var p = pos.x;

            do
            {
                p--;
            } while (p >= 0 && char.IsDigit(input[pos.y][p]));

            p++;

            do
            {
                number += input[pos.y][p];
                ++p;
            } while (p < _width && char.IsDigit(input[pos.y][p]));

            return number;
        }

    }
}