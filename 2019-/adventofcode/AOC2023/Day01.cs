﻿using adventofcode.Utilities;
using adventofcode.Utilities.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2023
{
    public class Day01 : Day
    {
        //Globals
        private readonly List<string> _input;

        //Constructor
        public Day01()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2023\day01.txt");
            //var file = ToolsFile.ReadFile(DataPathHemma + @"PATH\day01.txt");

            //Test case
            var test1 = @"1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet";

            var test2 = @"two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen";

            _input = file.GetLines();

            //Do Pre-stuff
        }

        Dictionary<string, int> numberToIntMap = new Dictionary<string, int>()
        {
            { "1", 1 },
            { "2", 2 },
            { "3", 3 },
            { "4", 4 },
            { "5", 5 },
            { "6", 6 },
            { "7", 7 },
            { "8", 8 },
            { "9", 9 },
            { "one", 1 },
            { "two", 2 },
            { "three", 3 },
            { "four", 4 },
            { "five", 5 },
            { "six", 6 },
            { "seven", 7 },
            { "eight", 8 },
            { "nine", 9 },
        };

        protected override string Part1()
        {
            var sum = 0;
            foreach (var line in _input)
            {
                var digits = line.Where(char.IsDigit).ToList();

                var first = digits.First();
                var last = digits.Last();

                var s = string.Empty + first + last;
                sum += int.Parse(s);
            }

            return sum.ToString();
        }

        protected override string Part2()
        {
            var sum = 0;
            foreach (var line in _input)
            {
                var numbers = numberToIntMap.Keys.ToList();

                var occurences = new List<(string number, List<int> indices)>();
                foreach (var number in numbers)
                {
                    var occurence = Occurences(line, number);
                    if (occurence.IsEmpty()) continue;
                    occurences.Add((number, Occurences(line, number)));
                }

                var firstDigit = occurences.OrderBy(x => x.indices.Min()).First();
                var lastDigit = occurences.OrderBy(x => x.indices.Max()).Last();

                var combinedNumber = string.Empty + numberToIntMap[firstDigit.number] + numberToIntMap[lastDigit.number];
                sum += int.Parse(combinedNumber);
            }

            return sum.ToString();
        }

        private static List<int> Occurences(string s, string test)
        {
            var result = new List<int>();
            for (var i = 0; i < s.Length; ++i)
            {
                var occurs = !test.Where((t, j) => i + j >= s.Length || s[i + j] != t).Any();

                if (occurs)
                {
                    result.Add(i);
                }
            }

            return result;
        }
    }
}

