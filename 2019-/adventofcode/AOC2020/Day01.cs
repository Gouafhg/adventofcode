﻿using adventofcode.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2020
{
    internal class Day01 : Day
    {
        private readonly List<int> _input;

        public Day01()
        {
            Part = 1;
            var file = ToolsFile.ReadFileLines(DataPath + @"2020\day01.txt");
            _input = file.Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => int.Parse(x)).ToList();
        }

        protected override string Part1()
        {
            for (var i = 0; i < _input.Count; ++i)
            {
                for (var j = 0; j < _input.Count; ++j)
                {
                    if (i == j) continue;
                    if (_input[i] + _input[j] == 2020)
                    {
                        return (_input[i] * _input[j]).ToString();
                    }
                }
            }
            return "ERROR";
        }

        protected override string Part2()
        {
            for (var i = 0; i < _input.Count; ++i)
            {
                for (var j = 0; j < _input.Count; ++j)
                {
                    for (var k = 0; k < _input.Count; ++k)
                    {
                        if (i == j || i == k || j == k) continue;
                        if (_input[i] + _input[j] + _input[k] == 2020)
                        {
                            return (_input[i] * _input[j] * _input[k]).ToString();
                        }
                    }
                }
            }
            return "ERROR";
        }
    }
}
