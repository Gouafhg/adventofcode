﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace adventofcode.AOC2020
{
    public class Day10 : Day
    {
        //Globals
        private readonly List<int> _input;

        private readonly Dictionary<int, List<int>> _adjacent;
        private readonly Dictionary<int, int> _points;

        //Constructor
        public Day10()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2020\day10.txt");
            var file = ToolsFile.ReadFile(DataPath + @"2020\day10.txt");

            //Test case
            var test1 = @"16
10
15
5
1
11
7
19
6
12
4";

            var test2 = @"28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3";

            _points = new Dictionary<int, int>
            {
                { 5, 7 },
                { 4, 4 },
                { 3, 2 },
                { 2, 1 },
                { 1, 1 },
            };

            _input = file.Split(Environment.NewLine.ToArray()).Where(x => x != "").Select(int.Parse).OrderBy(x => x).ToList();
            _input.Insert(0, 0);
            _input.Add(_input.Max(x => x) + 3);

            //Do Pre-stuff
            _adjacent = new Dictionary<int, List<int>>();
            for (var i = 0; i < _input.Count; ++i)
            {
                _adjacent.Add(_input[i], new List<int>());
                for (var j = i + 1; j < _input.Count && _input[j] <= _input[i] + 3; ++j)
                {
                    _adjacent[_input[i]].Add(_input[j]);
                }
            }
        }

        protected override string Part1()
        {
            var distances = new Dictionary<int, int?>();
            foreach (var n in _input)
            {
                distances.Add(n, null);
            }
            var q = new Queue<int>();
            q.Enqueue(_input[0]);
            distances[_input[0]] = 0;
            var cameFrom = new Dictionary<int, int?> { { _input[0], null } };

            while (q.Count > 0)
            {
                var currentnode = q.Dequeue();
                foreach (var n in _adjacent[currentnode])
                {
                    if (distances[n] >= distances[currentnode] + 1)
                        continue;
                    cameFrom[n] = currentnode;
                    distances[n] = distances[currentnode] + 1;
                    q.Enqueue(n);
                }
            }


            var path = new List<int>();
            var pathNode = _input.Last();
            while (!path.Contains(_input.First()))
            {
                path.Add(pathNode);
                pathNode = cameFrom[pathNode] ?? -1;
            }

            path.Reverse();

            var ones = BigInteger.Zero;
            var threes = BigInteger.Zero;
            for (var i = 1; i < _input.Count; ++i)
            {
                var diff = _input[i] - _input[i - 1];
                if (diff == 1) ones++;
                if (diff == 3) threes++;
            }
            return ones + ", " + threes + " = " + (ones * threes) + " :  " + string.Join(", ", path);
        }

        protected override string Part2()
        {
            var value = BigInteger.One;
            var start = -1;
            for (var i = 0; i < _input.Count - 1; ++i)
            {
                var current = _input[i];
                var next = _input[i + 1];
                if (next - current == 3)
                {
                    var diff = i - start;
                    value *= _points[diff];
                    start = i;
                }
            }
            return value.ToString();
        }
    }
}
