﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2020
{
    public class Day11 : Day
    {
        //Globals
        private readonly List<string> _input;
        private readonly char[] _parsedInput;
        private readonly int _resX;
        private readonly int _resY;

        //Constructor
        public Day11()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(DataPath + @"2020\day11.txt");

            //Test case
            var test1 = @"L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL";

            var chosenInput = file;
            _parsedInput = chosenInput.Replace('\r', '\n').Replace("\n", "").ToCharArray();
            _input = chosenInput.Split(Environment.NewLine.ToArray()).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

            //Do Pre-stuff
            _resX = _input[0].Length;
            _resY = _input.Count;
        }

        public char GetXy(int x, int y, char[] array)
        {
            return array[y * _resX + x];
        }

        public void SetXy(int x, int y, char[] array, char value)
        {
            array[y * _resX + x] = value;
        }

        public char[] CopyFloor(char[] pre)
        {
            var copy = new char[pre.Length];
            Array.Copy(pre, copy, pre.Length);
            return copy;
        }

        public char[] Tick1(char[] pre)
        {
            var after = CopyFloor(pre);

            for (var y = 0; y < _resY; ++y)
            {
                for (var x = 0; x < _resX; ++x)
                {
                    var current = GetXy(x, y, pre);
                    if (current == '.')
                        continue;

                    var fillCount = 0;
                    for (var dx = -1; dx < 2; ++dx)
                    {
                        for (var dy = -1; dy < 2; ++dy)
                        {
                            if (dx == 0 && dy == 0) continue;
                            var nx = x + dx;
                            var ny = y + dy;
                            if (nx < 0 || nx >= _resX) continue;
                            if (ny < 0 || ny >= _resY) continue;
                            var n = GetXy(nx, ny, pre);
                            if (n == '#')
                                fillCount++;
                            else
                            {
                            }
                        }
                    }

                    if (current == 'L' && fillCount == 0)
                    {
                        SetXy(x, y, after, '#');
                    }
                    else if (current == '#' && fillCount >= 4)
                    {
                        SetXy(x, y, after, 'L');
                    }
                }
            }

            return after;
        }

        public char[] Tick2(char[] pre)
        {
            var after = CopyFloor(pre);

            for (var y = 0; y < _resY; ++y)
            {
                for (var x = 0; x < _resX; ++x)
                {
                    var current = GetXy(x, y, pre);
                    if (current == '.')
                        continue;

                    var fillCount = 0;
                    for (var dirX = -1; dirX < 2; ++dirX)
                    {
                        for (var dirY = -1; dirY < 2; ++dirY)
                        {
                            if (dirX == 0 && dirY == 0) continue;
                            var nx = x;
                            var ny = y;
                            while (true)
                            {
                                nx = nx + dirX;
                                ny = ny + dirY;
                                if (nx < 0 || ny < 0 || nx >= _resX || ny >= _resY) break;
                                var n = GetXy(nx, ny, pre);
                                if (n == '#')
                                {
                                    fillCount++;
                                    break;
                                }
                                else if (n == 'L')
                                {
                                    break;
                                }
                            }
                        }
                    }

                    if (current == 'L' && fillCount == 0)
                    {
                        SetXy(x, y, after, '#');
                    }
                    else if (current == '#' && fillCount >= 5)
                    {
                        SetXy(x, y, after, 'L');
                    }
                }
            }

            return after;
        }

        public void PrintArray(char[] array)
        {
            Console.Clear();
            for (var y = 0; y < _resY; ++y)
            {
                var line = string.Empty;
                for (var x = 0; x < _resX; ++x)
                {
                    line += GetXy(x, y, array);
                }
                Console.WriteLine(line);
            }
            Console.WriteLine();
        }

        protected override string Part1()
        {
            var ticks = 0;
            char[] current;
            var after = CopyFloor(_parsedInput);
            do
            {
                current = after;
                after = Tick1(current);
                ticks++;
                if (ticks % 2 == 1) PrintArray(after);
            } while (new string(current) != new string(after));

            return after.Count(x => x == '#').ToString();
        }

        protected override string Part2()
        {
            char[] current;
            var after = CopyFloor(_parsedInput);
            do
            {
                current = after;
                after = Tick2(current);
                PrintArray(after);
            } while (new string(current) != new string(after));

            return after.Count(x => x == '#').ToString();
        }
    }
}
