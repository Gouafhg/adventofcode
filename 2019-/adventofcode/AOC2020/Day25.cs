﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace adventofcode.AOC2020
{
    internal class Day25 : Day
    {
        //Globals
        private readonly List<string> _input;

        private readonly BigInteger _cardPublicKey;
        private readonly BigInteger _doorPublicKey;

        //Constructor
        public Day25()
        {
            //Init globals
            Part = 1;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\PATH\day.txt");
            var file = @"16915772
18447943";

            //Test case
            var test1 = @"5764801
17807724";

            var chosenInput = file;
            _input = chosenInput.Replace("\r", "\n").Split('\n').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

            _cardPublicKey = BigInteger.Parse(_input[0]);
            _doorPublicKey = BigInteger.Parse(_input[1]);
            //Do Pre-stuff
        }

        private BigInteger CryptographicHandshake(BigInteger cardLoopSize, BigInteger doorLoopSize)
        {
            var cardPublicKey = BigInteger.ModPow(7, cardLoopSize, 20201227);
            var doorPublicKey = BigInteger.ModPow(7, doorLoopSize, 20201227);
            if (cardPublicKey != _cardPublicKey)
            {
                throw new Exception();
            }
            if (doorPublicKey != _doorPublicKey)
            {
                throw new Exception();
            }

            var cardEncryptionKey = BigInteger.ModPow(doorPublicKey, cardLoopSize, 20201227);
            var doorEncryptionKey = BigInteger.ModPow(cardPublicKey, doorLoopSize, 20201227);
            if (cardEncryptionKey != doorEncryptionKey)
            {
                throw new Exception();
            }
            return cardEncryptionKey;
        }

        protected override string Part1()
        {
            var findValues = false;

            var cardLoopSize = BigInteger.One;
            var doorLoopSize = BigInteger.One;
            if (findValues)
            {
                while (BigInteger.ModPow(7, cardLoopSize, 20201227) != _cardPublicKey) cardLoopSize++;
                while (BigInteger.ModPow(7, doorLoopSize, 20201227) != _doorPublicKey) doorLoopSize++;
            }
            else
            {
                cardLoopSize = 4618530;
                doorLoopSize = 6662323;
            }

            var encryptionKey = CryptographicHandshake(cardLoopSize, doorLoopSize);
            return encryptionKey.ToString();
        }

        protected override string Part2()
        {
            return "WELL";
        }
    }
}
