﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace adventofcode.AOC2020
{
    internal class Day05 : Day
    {
        //Globals
        private readonly List<string> _input;

        //Constructor
        public Day05()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(DataPath + @"2020\day05.txt");

            //Test case
            var test1 = @"FBFBBFFRLR";

            _input = file.Split(Environment.NewLine.ToArray()).Where(x => x != "").ToList();

            //Do Pre-stuff
        }

        public (int row, int column, int id) ParsePass(string pass)
        {
            var row = 0;
            var column = 0;

            var rowSize = 127;
            var columnSize = 7;
            var rowString = pass.Substring(0, 7);
            var columnString = pass.Substring(7, 3);
            foreach (var c in rowString)
            {
                row += c == 'F' ? 0 : (rowSize / 2 + 1);
                rowSize /= 2;
            }
            foreach (var c in columnString)
            {
                column += c == 'L' ? 0 : (columnSize / 2 + 1);
                columnSize /= 2;
            }
            var id = row * 8 + column;
            return (row, column, id);
        }

        protected override string Part1()
        {
            var parsedPasses = new List<(int row, int column, int id)>();
            foreach (var pass in _input)
            {
                parsedPasses.Add(ParsePass(pass));
            }

            return parsedPasses.Max(x => x.id).ToString();
        }

        protected override string Part2()
        {
            var parsedPasses = new List<(int row, int column, int id)>();
            foreach (var pass in _input)
            {
                parsedPasses.Add(ParsePass(pass));
            }
            var missingIds = new List<int>();
            for (var row = 0; row < 128; ++row)
            {
                if (row == 0 || row == 127) continue;

                for (var column = 0; column < 8; ++column)
                {
                    missingIds.Add(row * 8 + column);
                }
            }
            for (var i = 0; i < 128 * 8; ++i)
            {
                if (parsedPasses.Any(x => x.id == i))
                {
                    break;
                }
                missingIds.Remove(i);
            }
            for (var i = 128 * 8 - 1; i >= 0; --i)
            {
                if (parsedPasses.Any(x => x.id == i))
                {
                    break;
                }
                missingIds.Remove(i);
            }
            missingIds.RemoveAll(x => parsedPasses.Any(y => y.id == x));

            var sb = new StringBuilder();
            foreach (var missingId in missingIds)
            {
                sb.AppendLine(missingId.ToString());
            }
            return sb.ToString();
        }
    }
}
