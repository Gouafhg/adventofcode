﻿using adventofcode.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2020
{
    public class Day06 : Day
    {
        //Globals
        private readonly List<string> _inputPart1;
        private readonly List<List<string>> _inputPart2;

        //Constructor
        public Day06()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(DataPath + @"2020\day06.txt");

            //Test case
            var test1 = @"abc

a
b
c

ab
ac

a
a
a
a

b";

            _inputPart1 = new List<string>();
            var preInput1 = test1.Replace("\r\n", "\n").Split('\n');
            var s = string.Empty;
            foreach (var match in preInput1)
            {
                if (string.IsNullOrEmpty(match))
                {
                    _inputPart1.Add(s);
                    s = string.Empty;
                    continue;
                }
                s += match;
            }
            _inputPart1.Add(s);
            _inputPart1 = _inputPart1.Select(x => x.Replace("\n", "")).Select(x => new string(x.Distinct().ToArray())).ToList();

            _inputPart2 = new List<List<string>>();
            var preInput2 = file.Replace("\r\n", "\n").Replace("\n\n", "0").Split('0');
            s = string.Empty;
            foreach (var group in preInput2)
            {
                _inputPart2.Add(group.Split('\n').Where(x => !string.IsNullOrEmpty(x)).ToList());
            }

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var sum = _inputPart1.Sum(x => x.Length);
            return sum.ToString();
        }

        protected override string Part2()
        {
            var chars = new List<char>();
            for (var i = (int)'a'; i <= (int)'z'; ++i)
            {
                chars.Add((char)i);
            }

            var commonAnswers = new List<int>();
            foreach (var group in _inputPart2)
            {
                var commonAnswer = 0;
                foreach (var c in chars)
                {
                    if (group.All(x => x.Contains(c)))
                    {
                        commonAnswer++;
                    }
                }
                commonAnswers.Add(commonAnswer);
            }

            return commonAnswers.Sum(x => x).ToString();
        }
    }
}
