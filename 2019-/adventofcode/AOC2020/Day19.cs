﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace adventofcode.AOC2020
{
    public class Day19 : Day
    {
        //Globals
        private readonly List<string> _input;

        private readonly int _maxDepth = 3;
        private readonly Dictionary<int, string> _rules;
        private readonly List<string> _messages;

        private readonly List<string> _test2Results;

        //Constructor
        public Day19()
        {
            //Init globals
            Part = 1;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2020\day19.txt");
            var file = ToolsFile.ReadFile(DataPath + @"2020\day19.txt").Replace('\"', '{').Replace("{", string.Empty);

            //Test case
            var test1 = @"0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: a
5: b

ababbb
bababa
abbbab
aaabbb
aaaabbb";

            var test2 = @"42: 9 14 | 10 1
9: 14 27 | 1 26
10: 23 14 | 28 1
1: a
11: 42 31
5: 1 14 | 15 1
19: 14 1 | 14 14
12: 24 14 | 19 1
16: 15 1 | 14 14
31: 14 17 | 1 13
6: 14 14 | 1 14
2: 1 24 | 14 4
0: 8 11
13: 14 3 | 1 12
15: 1 | 14
17: 14 2 | 1 7
23: 25 1 | 22 14
28: 16 1
4: 1 1
20: 14 14 | 1 15
3: 5 14 | 16 1
27: 1 6 | 14 18
14: b
21: 14 1 | 1 14
25: 1 1 | 1 14
22: 14 14
8: 42
26: 14 22 | 1 20
18: 15 15
7: 14 5 | 1 21
24: 14 1

abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa
bbabbbbaabaabba
babbbbaabbbbbabbbbbbaabaaabaaa
aaabbbbbbaaaabaababaabababbabaaabbababababaaa
bbbbbbbaaaabbbbaaabbabaaa
bbbababbbbaaaaaaaabbababaaababaabab
ababaaaaaabaaab
ababaaaaabbbaba
baabbaaaabbaaaababbaababb
abbbbabbbbaaaababbbbbbaaaababb
aaaaabbaabaaaaababaa
aaaabbaaaabbaaa
aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
babaaabbbaaabaababbaabababaaab
aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba";

            var test2ResultString = @"
    bbabbbbaabaabba
    babbbbaabbbbbabbbbbbaabaaabaaa
    aaabbbbbbaaaabaababaabababbabaaabbababababaaa
    bbbbbbbaaaabbbbaaabbabaaa
    bbbababbbbaaaaaaaabbababaaababaabab
    ababaaaaaabaaab
    ababaaaaabbbaba
    baabbaaaabbaaaababbaababb
    abbbbabbbbaaaababbbbbbaaaababb
    aaaaabbaabaaaaababaa
    aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
    aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba";
            _test2Results = test2ResultString.Replace("\r", "\n").Replace(" ", string.Empty).Split('\n').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

            var chosenInput = file;
            _input = chosenInput.Replace("\r", "\n").Replace("\"", string.Empty).Split('\n').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

            _rules = _input.TakeWhile(x => char.IsDigit(x[0])).OrderBy(x => int.Parse(Regex.Split(x, ": ")[0])).ToDictionary(x => int.Parse(Regex.Split(x, ": ")[0]), x => Regex.Split(x, ": ")[1]);// OrderBy(x => int.Parse(Regex.Split(x, ": ")[0])).Select(x => Regex.Split(x, ": ")[1]).ToList();
            _messages = _input.SkipWhile(x => char.IsDigit(x[0])).ToList();
        }

        public string ParseStringVerbose(Dictionary<int, int> ruleList, string s, Dictionary<int, string> rules)
        {
            var result = s;
            do
            {
                var nextIteration = string.Empty;
                for (var i = 0; i < result.Length; ++i)
                {
                    if (result[i] == '(')
                    {
                        var ss = Tools.GetParantheses(result.Substring(i, result.Length - i));
                        nextIteration += "(" + ParseStringVerbose(ruleList, ss, rules);
                        i += ss.Length;
                    }
                    else if (char.IsDigit(result[i]))
                    {
                        var number = int.Parse(new string(result.Skip(i).TakeWhile(char.IsDigit).ToArray()));

                        if (number == 8 || number == 11)
                        {
                            ruleList = new Dictionary<int, int>(ruleList);
                            if (ruleList.ContainsKey(number)) ruleList[number]++;
                            else ruleList.Add(number, 1);
                            if (ruleList[number] > _maxDepth)
                            {
                                return string.Empty;
                            }
                        }

                        nextIteration += "(" + rules[number] + ")";
                        i += number.ToString().Length - 1;
                    }
                    else
                    {
                        nextIteration += result[i];
                    }
                }
                result = nextIteration;
            } while (result.Any(char.IsDigit));
            return result;
        }

        public string ParseString(Dictionary<int, int> ruleList, string s, Dictionary<int, string> rules)
        {
            return ParseStringVerbose(ruleList, s, rules)
                .Replace(" | ", "|");
        }

        private string ClearParentheses(string v)
        {
            if (string.IsNullOrWhiteSpace(v) || v.First() != '(' || v.Last() != ')' || GetParts(v).Count != 1 || GetDisjunctions(v).Count != 1)
                return v.Trim();

            return new string(v.Skip(1).Take(v.Length - 2).ToArray()).Trim();
        }

        private List<string> FixDisjunctions(string v)
        {
            v = ClearParentheses(v);
            var disjunctions = GetDisjunctions(v);
            if (disjunctions.Count == 1)
            {
                return new List<string> { v };
            }
            for (var i = 0; i < disjunctions.Count; ++i)
            {
                var parts = GetParts(disjunctions[i]);
                for (var j = 0; j < parts.Count; ++j)
                {
                    parts[j] = "(" + string.Join(")|(", FixDisjunctions(parts[j]).Select(ClearParentheses)) + ")";
                }
                disjunctions[i] = "(" + string.Join(") (", parts.Select(ClearParentheses)) + ")";
            }
            return disjunctions;
        }

        private List<string> GetDisjunctions(string v)
        {
            var level = 0;
            var i = 0;
            while (i < v.Length)
            {
                if (v[i] == '(') level++;
                else if (v[i] == ')') level--;
                else if (level == 0 && v[i] == '|')
                {
                    break;
                }
                i++;
            }

            if (i == v.Length)
            {
                return new List<string> { v.Trim() };
            }
            return new List<string> { v.Substring(0, i), v.Substring(i + 1, v.Length - i - 1).Trim() }.Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
        }

        private List<string> GetParts(string v)
        {
            if (string.IsNullOrWhiteSpace(v))
            {
                return new List<string>();
            }
            var result = new List<string>();
            var lastPosition = 0;
            var level = 0;
            var i = 0;
            while (i < v.Length)
            {
                if (v[i] == '(') level++;
                else if (v[i] == ')') level--;
                else if (level == 0 && v[i] == ' ')
                {
                    result.Add(v.Substring(lastPosition, i - lastPosition).Trim());
                    lastPosition = i + 1;
                }
                i++;
            }
            result.Add(v.Substring(lastPosition, i - lastPosition).Trim());
            return result.Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
        }

        private void MoveParts(int index, List<string> parts, Dictionary<int, List<int>> fromTo)
        {
            var part = ClearParentheses(parts[index]);
            var a = GetParts(part);
            if (a.Count > 1)
            {
                var prev = fromTo.Where(kp => kp.Value.Contains(index)).ToList();
                foreach (var p in prev)
                {
                    p.Value.Remove(index);
                    p.Value.Add(parts.Count);
                }
                var firstPart = ClearParentheses(a.First());
                parts.Add(firstPart);
                fromTo.Add(parts.Count - 1, new List<int>());
                for (var i = 1; i < a.Count - 1; ++i)
                {
                    var newPart = ClearParentheses(a[i]);
                    parts.Add(newPart);
                    fromTo[parts.Count - 2].Add(parts.Count - 1);
                    fromTo.Add(parts.Count - 1, new List<int>());
                }
                var lastPart = ClearParentheses(a.Last());
                parts.Add(lastPart);
                fromTo[parts.Count - 2].Add(parts.Count - 1);
                fromTo.Add(parts.Count - 1, new List<int>());
                fromTo[parts.Count - 1].AddRange(fromTo[index]);
                return;
            }
            a = GetDisjunctions(part);
            if (a.Count == 2)
            {
                var prev = fromTo.Where(kp => kp.Value.Contains(index)).ToList();
                foreach (var p in prev)
                {
                    p.Value.Remove(index);
                }
                foreach (var x in a)
                {
                    var newPart = ClearParentheses(x);
                    parts.Add(newPart);
                    fromTo.Add(parts.Count - 1, new List<int>());
                    fromTo[parts.Count - 1].AddRange(fromTo[index]);
                    foreach (var p in prev)
                    {
                        p.Value.Add(parts.Count - 1);
                    }
                }
                return;
            }
        }

        private List<List<int>> GetPositions(List<int> index, List<List<int>> lists, List<List<int>> result)
        {
            if (index.Count == lists.Count)
            {
                return result;
            }

            for (var i = 0; i < lists[index.Count].Count; ++i)
            {
                var newIndex = new List<int>(index)
                {
                    i,
                };
                result.AddRange(GetPositions(newIndex, lists, result));
            }
            return result;
        }

        private List<List<int>> GotoNext(int index, Dictionary<int, List<int>> fromTo, List<int> path)
        {
            var result = new List<List<int>>();
            path.Add(index);
            if (fromTo[index].Count == 0)
            {
                result.Add(path);
            }
            foreach (var i in fromTo[index])
            {
                result.AddRange(GotoNext(i, fromTo, new List<int>(path)));
            }
            return result;
        }


        protected override string Part1()
        {
            var tempRules = new Dictionary<int, string>(_rules);
            var keys = tempRules.Keys.ToList();
            foreach (var key in keys)
            {
                var rule = tempRules[key];
                if (rule.Contains('|'))
                {
                    var ruleParts = Regex.Split(rule, " \\| ");
                    tempRules[key] = "(" + string.Join(")|(", ruleParts) + ")";
                }
            }
            for (var i = keys.Count - 1; i >= 0; --i)
            {
                var keyi = keys[i];
                var pattern = $@"\b{keyi}\b";
                foreach (var keyj in keys)
                {
                    tempRules[keyj] = Regex.Replace(tempRules[keyj], pattern, $"({tempRules[keyi]})");
                }
            }
            foreach (var key in keys)
            {
                var newValue = tempRules[key].Replace("(a)", "a").Replace("(b)", "b");
                while (newValue != tempRules[key])
                {
                    tempRules[key] = newValue;
                    newValue = tempRules[key].Replace("(a)", "a").Replace("(b)", "b");
                }
            }

            var parts = new List<string>();
            parts.AddRange(GetParts(tempRules[0]));
            for (var i = 0; i < parts.Count; ++i)
            {
                parts[i] = ClearParentheses(parts[i]);
            }
            var fromTo = new Dictionary<int, List<int>>
            {
                { -1, new List<int> { 0 } },
            };
            for (var i = 0; i < parts.Count - 1; ++i)
            {
                fromTo.Add(i, new List<int> { i + 1 });
            }
            fromTo.Add(parts.Count - 1, new List<int>());

            for (var i = 0; i < parts.Count; ++i)
            {
                MoveParts(i, parts, fromTo);
            }

            var paths = GotoNext(-1, fromTo, new List<int>());

            var legalStrings = paths.Select(x => string.Join(string.Empty, x.Skip(1).Select(y => parts[y]))).ToList();

            return _messages.Count(x => legalStrings.Contains(x)).ToString();
        }

        private List<string> GetStrings(string rule)
        {
            var parts = new List<string>();
            parts.AddRange(GetParts(rule));
            var fromTo = new Dictionary<int, List<int>>
            {
                { -1, new List<int> { 0 } },
            };
            for (var i = 0; i < parts.Count - 1; ++i)
            {
                fromTo.Add(i, new List<int> { i + 1 });
            }
            fromTo.Add(parts.Count - 1, new List<int>());

            for (var i = 0; i < parts.Count; ++i)
            {
                MoveParts(i, parts, fromTo);
            }
            for (var i = 0; i < parts.Count; ++i)
            {
                parts[i] = parts[i] == "(a)" ? "a" : parts[i] == "(b)" ? "b" : parts[i];
            }

            var ps = GotoNext(-1, fromTo, new List<int>());

            return ps.Select(x => string.Join(string.Empty, x.Skip(1).Select(y => ClearParentheses(parts[y])))).ToList();
        }

        private List<string> GetStrings(string rule, Dictionary<int, List<string>> ruleStrings)
        {
            var parts = new List<string>();
            parts.AddRange(GetParts(rule));
            var fromTo = new Dictionary<int, List<int>>
            {
                { -1, new List<int> { 0 } },
            };
            for (var i = 0; i < parts.Count - 1; ++i)
            {
                fromTo.Add(i, new List<int> { i + 1 });
            }
            fromTo.Add(parts.Count - 1, new List<int>());

            for (var i = 0; i < parts.Count; ++i)
            {
                MoveParts(i, parts, fromTo);
            }

            var ps = GotoNext(-1, fromTo, new List<int>());
            var pathStrings = ps.Select(x => x.Skip(1)).Select(x => x.Select(y => ruleStrings[int.Parse(parts[y])]).ToList()).ToList();

            var strings = ps.Select(x => string.Join(" ", x.Skip(1).Select(y => int.Parse(parts[y])))).ToList();

            return ps.Select(x => string.Join(string.Empty, x.Skip(1).Select(y => ruleStrings[int.Parse(parts[y])]))).ToList();
        }

        private List<string> GetStrings(int ruleIndex, Dictionary<int, string> ruleChanges)
        {
            var tempRules = new Dictionary<int, string>(_rules);
            foreach (var ruleChange in ruleChanges)
            {
                tempRules[ruleChange.Key] = ruleChange.Value;
            }

            var rule = ParseString(new Dictionary<int, int>(),
                                   tempRules[ruleIndex],
                                   tempRules)
                .Replace(" | ", "|");

            var parts = new List<string>();
            parts.AddRange(GetParts(rule));
            var fromTo = new Dictionary<int, List<int>>
            {
                { -1, new List<int> { 0 } },
            };
            for (var i = 0; i < parts.Count - 1; ++i)
            {
                fromTo.Add(i, new List<int> { i + 1 });
            }
            fromTo.Add(parts.Count - 1, new List<int>());

            for (var i = 0; i < parts.Count; ++i)
            {
                MoveParts(i, parts, fromTo);
            }

            var paths = GotoNext(-1, fromTo, new List<int>());

            return paths.Select(x => string.Join(string.Empty, x.Skip(1).Select(y => parts[y]))).ToList();
        }

        protected override string Part2()
        {
            var tempRules = new Dictionary<int, string>(_rules);
            var keys = tempRules.Keys.ToList();
            foreach (var key in keys)
            {
                var rule = tempRules[key];
                if (rule.Contains('|'))
                {
                    var ruleParts = Regex.Split(rule, " \\| ");
                    tempRules[key] = "(" + string.Join(")|(", ruleParts) + ")";
                }
            }
            for (var i = keys.Count - 1; i >= 0; --i)
            {
                var keyi = keys[i];
                var pattern = $@"\b{keyi}\b";
                foreach (var keyj in keys)
                {
                    tempRules[keyj] = Regex.Replace(tempRules[keyj], pattern, $"({tempRules[keyi]})");
                }
            }
            foreach (var key in keys)
            {
                var newValue = tempRules[key].Replace("(a)", "a").Replace("(b)", "b");
                while (newValue != tempRules[key])
                {
                    tempRules[key] = newValue;
                    newValue = tempRules[key].Replace("(a)", "a").Replace("(b)", "b");
                }
            }

            var assert31 = "(b ((b ((a (b a))|(b (a a))))|(a ((b ((a b)|((a|b) a)))|(a ((b a)|(a b)))))))|(a ((b ((((a b)|((a|b) a)) b)|((((a|b) a)|(b b)) a)))|(a (((b a) b)|(((b a)|(b b)) a)))))";
            var assert42 = "(((b ((a ((b b)|(a b)))|(b ((a|b) (a|b)))))|(a ((b (b b))|(a ((b b)|(a (a|b))))))) b)|(((((((a a)|(a b)) a)|((b b) b)) b)|(((((a|b) a)|(b b)) a) a)) a)";
            if (tempRules[31] != assert31)
            {
                Console.WriteLine("--");
                Console.WriteLine("ERROR 31:");
                Console.WriteLine(tempRules[31]);
                Console.WriteLine(assert31);
                Console.WriteLine();
            }
            if (tempRules[42] != assert42)
            {
                Console.WriteLine("--");
                Console.WriteLine("ERROR 42:");
                Console.WriteLine(tempRules[42]);
                Console.WriteLine(assert42);
                Console.WriteLine();
            }

            var ruleStrings = new Dictionary<int, List<string>>
            {
                {31, GetStrings(tempRules[31])}, 
                {42, GetStrings(tempRules[42])},
            };

            var rev31S = new List<string>();
            foreach (var s in ruleStrings[31])
            {
                rev31S.Add(new string(s.Reverse().ToArray()));
            }
            var rests = new List<string>();
            var messages = new List<string>();
            foreach (var m in _messages)
            {
                var tempMessage = new string(m.Reverse().ToArray());
                var hit31Count = 0;
                while (rev31S.Any(tempMessage.StartsWith))
                {
                    var r = rev31S.First(tempMessage.StartsWith);
                    tempMessage = new string(tempMessage.Skip(r.Length).ToArray());
                    hit31Count++;
                }
                if (hit31Count == 0)
                {
                    continue;
                }

                tempMessage = new string(tempMessage.Reverse().ToArray());
                var hit42Count = 0;
                while (ruleStrings[42].Any(tempMessage.StartsWith))
                {
                    var r = ruleStrings[42].First(tempMessage.StartsWith);
                    tempMessage = new string(tempMessage.Skip(r.Length).ToArray());
                    hit42Count++;
                }

                var leftTo8 = hit42Count - hit31Count;

                if (leftTo8 > 0 && tempMessage.Length == 0)
                {
                    rests.Add(tempMessage);
                    messages.Add(m);
                }
            }

            var newMessages = new List<string>();
            for (var i = 0; i < messages.Count; ++i)
            {
                newMessages.Add(rests[i] + ": " + messages[i]);
            }
            Console.WriteLine("--");
            Console.WriteLine(string.Join(Environment.NewLine, newMessages));
            Console.WriteLine("!");
            Console.WriteLine(string.Join(Environment.NewLine, _test2Results.Where(x => !messages.Contains(x))));
            Console.WriteLine("?");
            Console.WriteLine(string.Join(Environment.NewLine, messages.Where(x => !_test2Results.Contains(x))));
            Console.WriteLine();
            return messages.Count().ToString();
        }
    }
}
