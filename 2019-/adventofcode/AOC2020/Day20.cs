﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace adventofcode.AOC2020
{
    public enum Side
    {
        Left,
        Top,
        Right,
        Bottom,
        FlipLeft,
        FlipTop,
        FlipRight,
        FlipBottom,
    }
    public class Tile
    {
        public int Id;
        public char[,] Grid;
        public Dictionary<Side, string> Sides;
        public List<(Side mine, Side other, Tile tile)> Connections;
        public Tile()
        {
            Sides = new Dictionary<Side, string>();
            Connections = new List<(Side mine, Side other, Tile t)>();
        }

        public override string ToString()
        {
            return Id.ToString();
        }

        public void RebuildConnections()
        {
            var connectedTiles = Connections.Select(x => x.tile).ToList();
            Connections.Clear();
            foreach (var t in connectedTiles)
            {
                Connections = Connections.Where(x => x.tile.Id != t.Id).ToList();
                foreach (Side s1 in Enum.GetValues(typeof(Side)))
                {
                    foreach (Side s2 in Enum.GetValues(typeof(Side)))
                    {
                        if (Sides[s1] == t.Sides[s2])
                        {
                            Connections.Add((s1, s2, t));
                            var otherC = t.Connections.First(x => x.mine == s2);
                            t.Connections.Remove(otherC);
                            t.Connections.Add((s2, s1, this));
                        }
                    }
                }
            }
        }

        public void FlipTopBottom()
        {
            Grid = FlipTopBottom(Grid);

            Sides = new Dictionary<Side, string>()
                    {
                        { Side.Left, GetLeft(Grid) },
                        { Side.FlipLeft, FlipSide(GetLeft(Grid)) },
                        { Side.Right, GetRight(Grid) },
                        { Side.FlipRight, FlipSide(GetRight(Grid)) },
                        { Side.Top, GetTop(Grid) },
                        { Side.FlipTop, FlipSide(GetTop(Grid)) },
                        { Side.Bottom, GetBottom(Grid) },
                        { Side.FlipBottom, FlipSide(GetBottom(Grid)) },
                    };

            RebuildConnections();
        }

        public static char[,] FlipTopBottom(char[,] grid)
        {
            var rX = grid.GetLength(0);
            var rY = grid.GetLength(1);
            var newGrid = new char[rX, rY];
            for (var x = 0; x < rX; ++x)
            {
                for (var y = 0; y < rY; ++y)
                {
                    newGrid[x, y] = grid[x, rY - 1 - y];
                }
            }
            return newGrid;
        }

        public void FlipLeftRight()
        {
            Grid = FlipLeftRight(Grid);

            Sides = new Dictionary<Side, string>()
                    {
                        { Side.Left, GetLeft(Grid) },
                        { Side.FlipLeft, FlipSide(GetLeft(Grid)) },
                        { Side.Right, GetRight(Grid) },
                        { Side.FlipRight, FlipSide(GetRight(Grid)) },
                        { Side.Top, GetTop(Grid) },
                        { Side.FlipTop, FlipSide(GetTop(Grid)) },
                        { Side.Bottom, GetBottom(Grid) },
                        { Side.FlipBottom, FlipSide(GetBottom(Grid)) },
                    };

            RebuildConnections();
        }

        public static char[,] FlipLeftRight(char[,] grid)
        {
            var rX = grid.GetLength(0);
            var rY = grid.GetLength(1);
            var newGrid = new char[rX, rY];
            for (var x = 0; x < rX; ++x)
            {
                for (var y = 0; y < rY; ++y)
                {
                    newGrid[x, y] = grid[rX - 1 - x, y];
                }
            }
            return newGrid;
        }

        public void RotateRight()
        {
            Grid = RotateRight(Grid);

            Sides = new Dictionary<Side, string>()
                    {
                        { Side.Left, GetLeft(Grid) },
                        { Side.FlipLeft, FlipSide(GetLeft(Grid)) },
                        { Side.Right, GetRight(Grid) },
                        { Side.FlipRight, FlipSide(GetRight(Grid)) },
                        { Side.Top, GetTop(Grid) },
                        { Side.FlipTop, FlipSide(GetTop(Grid)) },
                        { Side.Bottom, GetBottom(Grid) },
                        { Side.FlipBottom, FlipSide(GetBottom(Grid)) },
                    };

            RebuildConnections();
        }

        public static char[,] RotateRight(char[,] grid)
        {
            var n = grid.GetLength(0);
            var newGrid = new char[n, n];

            for (var i = 0; i < n; ++i)
            {
                for (var j = 0; j < n; ++j)
                {
                    newGrid[i, j] = grid[j, n - i - 1];
                }
            }
            return newGrid;
        }

        public void RotateLeft()
        {
            Grid = RotateLeft(Grid);

            Sides = new Dictionary<Side, string>()
                    {
                        { Side.Left, GetLeft(Grid) },
                        { Side.FlipLeft, FlipSide(GetLeft(Grid)) },
                        { Side.Right, GetRight(Grid) },
                        { Side.FlipRight, FlipSide(GetRight(Grid)) },
                        { Side.Top, GetTop(Grid) },
                        { Side.FlipTop, FlipSide(GetTop(Grid)) },
                        { Side.Bottom, GetBottom(Grid) },
                        { Side.FlipBottom, FlipSide(GetBottom(Grid)) },
                    };

            RebuildConnections();
        }

        public static char[,] RotateLeft(char[,] grid)
        {
            var n = grid.GetLength(0);
            var newGrid = new char[n, n];

            for (var i = 0; i < n; ++i)
            {
                for (var j = 0; j < n; ++j)
                {
                    newGrid[i, j] = grid[n - j - 1, i];
                }
            }
            return newGrid;
        }

        public static string FlipSide(string v)
        {
            return new string(v.Reverse().ToArray());
        }

        public static string GetLeft(char[,] tile)
        {
            var result = new List<char>();
            for (var y = 0; y < tile.GetLength(1); y++)
            {
                result.Add(tile[0, y]);
            }
            return new string(result.ToArray());
        }

        public static string GetRight(char[,] tile)
        {
            var result = new List<char>();
            for (var y = 0; y < tile.GetLength(1); y++)
            {
                result.Add(tile[tile.GetLength(0) - 1, y]);
            }
            return new string(result.ToArray());
        }

        public static string GetTop(char[,] tile)
        {
            var result = new List<char>();
            for (var x = 0; x < tile.GetLength(0); x++)
            {
                result.Add(tile[x, 0]);
            }
            return new string(result.ToArray());
        }

        public static string GetBottom(char[,] tile)
        {
            var result = new List<char>();
            for (var x = 0; x < tile.GetLength(0); x++)
            {
                result.Add(tile[x, tile.GetLength(1) - 1]);
            }
            return new string(result.ToArray());
        }

        public void Shrink()
        {
            var newGrid = new char[Grid.GetLength(0) - 2, Grid.GetLength(1) - 2];
            for (var y = 1; y < Grid.GetLength(1) - 1; y++)
            {
                for (var x = 1; x < Grid.GetLength(0) - 1; x++)
                {
                    newGrid[x - 1, y - 1] = Grid[x, y];
                }
            }
            Grid = newGrid;
        }
    }

    public class Day20 : Day
    {
        //Globals
        private readonly List<string> _input;

        private readonly List<Tile> _tiles;

        //Constructor
        public Day20()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2020\day20.txt");
            var file = ToolsFile.ReadFile(DataPath + @"2020\day20.txt");

            //Test case
            var test1 = @"Tile 2311:
..##.#..#.
##..#.....
#...##..#.
####.#...#
##.##.###.
##...#.###
.#.#.#..##
..#....#..
###...#.#.
..###..###

Tile 1951:
#.##...##.
#.####...#
.....#..##
#...######
.##.#....#
.###.#####
###.##.##.
.###....#.
..#.#..#.#
#...##.#..

Tile 1171:
####...##.
#..##.#..#
##.#..#.#.
.###.####.
..###.####
.##....##.
.#...####.
#.##.####.
####..#...
.....##...

Tile 1427:
###.##.#..
.#..#.##..
.#.##.#..#
#.#.#.##.#
....#...##
...##..##.
...#.#####
.#.####.#.
..#..###.#
..##.#..#.

Tile 1489:
##.#.#....
..##...#..
.##..##...
..#...#...
#####...#.
#..#.#.#.#
...#.#.#..
##.#...##.
..##.##.##
###.##.#..

Tile 2473:
#....####.
#..#.##...
#.##..#...
######.#.#
.#...#.#.#
.#########
.###.#..#.
########.#
##...##.#.
..###.#.#.

Tile 2971:
..#.#....#
#...###...
#.#.###...
##.##..#..
.#####..##
.#..####.#
#..#.#..#.
..####.###
..#.#.###.
...#.#.#.#

Tile 2729:
...#.#.#.#
####.#....
..#.#.....
....#..#.#
.##..##.#.
.#.####...
####.#.#..
##.####...
##..#.##..
#.##...##.

Tile 3079:
#.#.#####.
.#..######
..#.......
######....
####.#..#.
.#...#.##.
#.#####.##
..#.###...
..#.......
..#.###...";

            var chosenInput = file;
            _input = chosenInput.Replace("\r", "\n").Split('\n').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
            _tiles = new List<Tile>();

            for (var i = 0; i < _input.Count; ++i)
            {
                if (string.IsNullOrWhiteSpace(_input[i]) || _input[i][0] != 'T')
                {
                    continue;
                }
                var line = _input[i];

                var id = int.Parse(line.Split(' ')[1].Split(':')[0]);
                var tileInInput = _input.Skip(i + 1).TakeWhile(x => !string.IsNullOrWhiteSpace(x) && x[0] != 'T').ToList();
                var tileGrid = new char[tileInInput[0].Length, tileInInput.Count];

                for (var y = 0; y < tileInInput.Count; ++y)
                {
                    for (var x = 0; x < tileInInput[y].Length; ++x)
                    {
                        tileGrid[x, y] = tileInInput[y][x];
                    }
                }

                var tile = new Tile()
                {
                    Id = id,
                    Grid = tileGrid,
                    Sides = new Dictionary<Side, string>()
                    {
                        { Side.Left, Tile.GetLeft(tileGrid) },
                        { Side.FlipLeft, Tile.FlipSide(Tile.GetLeft(tileGrid)) },
                        { Side.Right, Tile.GetRight(tileGrid) },
                        { Side.FlipRight, Tile.FlipSide(Tile.GetRight(tileGrid)) },
                        { Side.Top, Tile.GetTop(tileGrid) },
                        { Side.FlipTop, Tile.FlipSide(Tile.GetTop(tileGrid)) },
                        { Side.Bottom, Tile.GetBottom(tileGrid) },
                        { Side.FlipBottom, Tile.FlipSide(Tile.GetBottom(tileGrid)) },
                    },
                };
                _tiles.Add(tile);
            }
            //Do Pre-stuff
        }

        public void BuildConnections()
        {
            foreach (var t1 in _tiles)
            {
                foreach (var t2 in _tiles)
                {
                    if (t1.Id == t2.Id) continue;
                    foreach (Side s1 in Enum.GetValues(typeof(Side)))
                    {
                        foreach (Side s2 in Enum.GetValues(typeof(Side)))
                        {
                            if (t1.Sides[s1] == t2.Sides[s2])
                            {
                                t1.Connections.Add((s1, s2, t2));
                            }
                        }
                    }
                }
            }
        }

        private List<(int x, int y)> SeamonsterPositions(char[,] grid)
        {
            var result = new List<(int x, int y)>();
            for (var x = 1; x < grid.GetLength(0) - 18; ++x)
            {
                for (var y = 2; y < grid.GetLength(1); ++y)
                {
                    if ((grid[x, y] == '#' || grid[x, y] == 'O')
                        && (grid[x + 3, y] == '#' || grid[x + 3, y] == 'O')
                        && (grid[x + 6, y] == '#' || grid[x + 6, y] == 'O')
                        && (grid[x + 9, y] == '#' || grid[x + 9, y] == 'O')
                        && (grid[x + 12, y] == '#' || grid[x + 12, y] == 'O')
                        && (grid[x + 15, y] == '#' || grid[x + 15, y] == 'O')
                        && (grid[x - 1, y - 1] == '#' || grid[x - 1, y - 1] == 'O')
                        && (grid[x + 4, y - 1] == '#' || grid[x + 4, y - 1] == 'O')
                        && (grid[x + 5, y - 1] == '#' || grid[x + 5, y - 1] == 'O')
                        && (grid[x + 10, y - 1] == '#' || grid[x + 10, y - 1] == 'O')
                        && (grid[x + 11, y - 1] == '#' || grid[x + 11, y - 1] == 'O')
                        && (grid[x + 16, y - 1] == '#' || grid[x + 16, y - 1] == 'O')
                        && (grid[x + 17, y - 1] == '#' || grid[x + 17, y - 1] == 'O')
                        && (grid[x + 18, y - 1] == '#' || grid[x + 18, y - 1] == 'O')
                        && (grid[x + 17, y - 2] == '#' || grid[x + 17, y - 2] == 'O')
                        )
                    {
                        result.Add((x, y));
                        result.Add((x + 3, y));
                        result.Add((x + 6, y));
                        result.Add((x + 9, y));
                        result.Add((x + 12, y));
                        result.Add((x + 15, y));
                        result.Add((x - 1, y - 1));
                        result.Add((x + 4, y - 1));
                        result.Add((x + 5, y - 1));
                        result.Add((x + 10, y - 1));
                        result.Add((x + 11, y - 1));
                        result.Add((x + 16, y - 1));
                        result.Add((x + 17, y - 1));
                        result.Add((x + 18, y - 1));
                        result.Add((x + 17, y - 2));
                    }
                }
            }
            return result;
        }

        public void PrintGrid(char[,] grid)
        {
            for (var y = 0; y < grid.GetLength(1); ++y)
            {
                var line = string.Empty;
                for (var x = 0; x < grid.GetLength(0); ++x) line += grid[x, y];
                Console.WriteLine(line);
            }
        }

        protected override string Part1()
        {
            BuildConnections();
            var cornertiles = _tiles.Where(x => x.Connections.Select(y => y.tile).Distinct().Count() == 2).ToList();
            var v = BigInteger.One;
            foreach (var t in cornertiles) v *= t.Id;
            return v.ToString();
        }

        protected override string Part2()
        {
            BuildConnections();
            var gridLength = (int)Math.Sqrt(_tiles.Count);
            var grid = new Tile[gridLength, gridLength];
            var cornerTiles = _tiles.Where(x => x.Connections.Select(y => y.tile).Distinct().Count() == 2).ToList();

            var topLeft = cornerTiles.First(x => x.Connections.Any(y => y.mine == Side.Right) && x.Connections.Any(y => y.mine == Side.Bottom));
            grid[0, 0] = topLeft;
            for (var y = 1; y < gridLength; ++y)
            {
                var (_, other, tile) = grid[0, y - 1].Connections.First(x => x.mine == Side.Bottom);
                grid[0, y] = tile;
                while (other != Side.Top)
                {
                    switch (other)
                    {
                        case Side.FlipBottom:
                        case Side.FlipTop:
                            tile.FlipLeftRight();
                            break;
                        case Side.FlipLeft:
                        case Side.FlipRight:
                            tile.FlipTopBottom();
                            break;
                        case Side.Bottom:
                            tile.RotateLeft(); tile.RotateLeft();
                            break;
                        case Side.Left:
                            tile.RotateRight();
                            break;
                        case Side.Right:
                            tile.RotateLeft();
                            break;
                    }

                    (_, other, tile) = grid[0, y - 1].Connections.First(x => x.mine == Side.Bottom);
                }
            }

            for (var y = 0; y < gridLength; ++y)
            {
                for (var x = 1; x < gridLength; ++x)
                {
                    var (_, other, tile) = grid[x - 1, y].Connections.First(c => c.mine == Side.Right);
                    grid[x, y] = tile;
                    while (other != Side.Left)
                    {
                        switch (other)
                        {
                            case Side.FlipBottom:
                            case Side.FlipTop:
                                tile.FlipLeftRight();
                                break;
                            case Side.FlipLeft:
                            case Side.FlipRight:
                                tile.FlipTopBottom();
                                break;
                            case Side.Right:
                                tile.RotateLeft(); tile.RotateLeft();
                                break;
                            case Side.Top:
                                tile.RotateRight();
                                break;
                            case Side.Bottom:
                                tile.RotateLeft();
                                break;
                        }

                        (_, other, tile) = grid[x - 1, y].Connections.First(c => c.mine == Side.Right);
                    }
                }
            }

            foreach (var tile in _tiles)
            {
                tile.Shrink();
            }

            var bigGrid = new char[gridLength * _tiles[0].Grid.GetLength(0), gridLength * _tiles[0].Grid.GetLength(1)];
            for (var x = 0; x < gridLength; ++x)
            {
                for (var y = 0; y < gridLength; ++y)
                {
                    for (var xx = 0; xx < grid[x, y].Grid.GetLength(0); ++xx)
                    {
                        for (var yy = 0; yy < grid[x, y].Grid.GetLength(1); ++yy)
                        {
                            var nx = x * grid[x, y].Grid.GetLength(0) + xx;
                            var ny = y * grid[x, y].Grid.GetLength(1) + yy;
                            bigGrid[nx, ny] = grid[x, y].Grid[xx, yy];
                        }
                    }
                }
            }

            for (var rotation = 0; rotation < 8; ++rotation)
            {
                bigGrid = Tile.RotateLeft(bigGrid);
                var newCoos = SeamonsterPositions(bigGrid);
                foreach (var (x, y) in newCoos)
                {
                    bigGrid[x, y] = 'O';
                }

                bigGrid = Tile.FlipLeftRight(bigGrid);
                newCoos = SeamonsterPositions(bigGrid);
                foreach (var (x, y) in newCoos)
                {
                    bigGrid[x, y] = 'O';
                }

                bigGrid = Tile.FlipLeftRight(bigGrid);
                bigGrid = Tile.FlipTopBottom(bigGrid);
                newCoos = SeamonsterPositions(bigGrid);
                foreach (var (x, y) in newCoos)
                {
                    bigGrid[x, y] = 'O';
                }

                bigGrid = Tile.FlipTopBottom(bigGrid);
                bigGrid = Tile.FlipLeftRight(bigGrid);
                newCoos = SeamonsterPositions(bigGrid);
                foreach (var (x, y) in newCoos)
                {
                    bigGrid[x, y] = 'O';
                }
            }

            var otherCount = 0;
            for (var x = 0; x < bigGrid.GetLength(0); ++x)
            {
                for (var y = 0; y < bigGrid.GetLength(1); ++y)
                {
                    if (bigGrid[x, y] == '#') otherCount++;
                }
            }

            return otherCount.ToString();
        }
    }
}
