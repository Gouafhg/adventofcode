﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2020
{
    public class Day07 : Day
    {
        //Globals
        private readonly List<string> _input;
        private readonly Dictionary<string, List<(int count, string type)>> _bags;

        //Constructor
        public Day07()
        {
            //Init globals
            Part = 2;
            _bags = new Dictionary<string, List<(int count, string type)>>();

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2020\day07.txt");

            //Test case
            var test1 = @"light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.";

            var test2 = @"shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.";

            _input = file.Replace(".", "").Split(Environment.NewLine.ToArray()).Where(x => x != "").ToList();

            foreach (var line in _input)
            {
                var firstBag = string.Join(" ", line.Split(' ').TakeWhile(x => x != "contain")).Replace("bags", "bag");
                var rest = string.Join(" ", line.Split(' ').SkipWhile(x => x != "contain").Skip(1)).Replace("bags", "bag");
                _bags.Add(firstBag, new List<(int count, string type)>());
                foreach (var x in rest.Split(','))
                {
                    var y = new string(x.SkipWhile(z => z == ' ').ToArray());
                    if (y == "no other bag") continue;

                    var count = new string(y.TakeWhile(z => z != ' ').ToArray());
                    var type = new string(y.SkipWhile(z => z != ' ').Skip(1).ToArray());
                    var item = (int.Parse(count), type);
                    _bags[firstBag].Add(item);
                }
            }
            //Do Pre-stuff
        }

        private List<string> GetBagsContaining(string bag)
        {
            return _bags.Keys.Where(x => _bags[x].Any(y => y.type == bag)).ToList();
        }


        protected override string Part1()
        {
            var containsShinyGoldBag = new List<string>();
            while (true)
            {
                var newContains = GetBagsContaining("shiny gold bag");
                foreach (var bag in containsShinyGoldBag)
                {
                    newContains.AddRange(GetBagsContaining((bag)));
                }

                newContains = newContains.Distinct().ToList();

                if (newContains.Count == containsShinyGoldBag.Count)
                {
                    break;
                }
                containsShinyGoldBag = newContains;
            }

            return containsShinyGoldBag.Count.ToString();
        }

        private List<(int count, string type)> BagContains(string bag)
        {
            var bagList = new List<(int count, string type)>();
            foreach (var x in _bags[bag])
            {
                bagList.Add(x);
                bagList.AddRange(BagContains(x.type).Select(y => (y.count * x.count, y.type)));
            }

            return bagList;
        }

        protected override string Part2()
        {
            var bagsToHold = BagContains("shiny gold bag");
            return bagsToHold.Sum(x => x.count) + ":" + string.Join(Environment.NewLine, bagsToHold);
        }
    }
}
