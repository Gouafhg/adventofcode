﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2020
{
    public class Day02 : Day
    {
        //Globals
        private readonly List<(string, string)> _input;

        //Constructor
        public Day02()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFileLines(DataPath + @"2020\day02.txt");

            //Test case
            var test1 = @"1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc".Replace("r", string.Empty).Split('\n').ToList();

            _input = file.Select(x => (x.Split(':')[0], x.Split(':')[1])).ToList();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var possiblePasswords = new List<string>();

            foreach (var line in _input)
            {
                var rules = line.Item1;
                var letter = rules.Split(' ')[1][0];
                var min = int.Parse(rules.Split(' ')[0].Split('-')[0]);
                var max = int.Parse(rules.Split(' ')[0].Split('-')[1]);

                var count = 0;
                for (var i = 0; i < line.Item2.Length; ++i)
                {
                    if (line.Item2[i] == letter)
                    {
                        count++;
                    }
                }

                if (count >= min && count <= max)
                {
                    possiblePasswords.Add(line.Item2);
                }
            }

            return possiblePasswords.Count.ToString();
        }

        protected override string Part2()
        {
            var possiblePasswords = new List<string>();

            foreach (var line in _input)
            {
                var rules = line.Item1;
                var letter = rules.Split(' ')[1][0];
                var first = int.Parse(rules.Split(' ')[0].Split('-')[0]);
                var second = int.Parse(rules.Split(' ')[0].Split('-')[1]);

                var matches = new List<bool>();
                try
                {
                    matches.Add(line.Item2[first] == letter);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
                try
                {
                    matches.Add(line.Item2[second] == letter);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

                if (matches.Count(x => x) == 1)
                {
                    possiblePasswords.Add(line.Item2);
                }
            }

            return possiblePasswords.Count.ToString();
        }
    }
}
