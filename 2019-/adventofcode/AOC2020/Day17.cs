﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2020
{
    public class Day17 : Day
    {
        //Globals
        private readonly List<string> _input;
        private List<(int x, int y, int z)> _activeCellsPart1 = new List<(int x, int y, int z)>();
        private List<(int x, int y, int z, int w)> _activeCellsPart2 = new List<(int x, int y, int z, int w)>();

        //Constructor
        public Day17()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2020\day17.txt");
            var file = ToolsFile.ReadFile(DataPath + @"2020\day17.txt");

            //Test case
            var test1 = @".#.
..#
###";

            var chosenInput = file;
            _input = chosenInput.Split(Environment.NewLine.ToArray()).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
            var z = 0;
            var w = 0;
            for (var y = 0; y < _input.Count; ++y)
            {
                for (var x = 0; x < _input[y].Length; ++x)
                {
                    if (_input[y][x] == '#')
                    {
                        _activeCellsPart1.Add((x, y, z));
                        _activeCellsPart2.Add((x, y, z, w));
                    }
                }
            }

            //Do Pre-stuff
        }

        public List<(int x, int y, int z)> CyclePart1(List<(int x, int y, int z)> pre)
        {
            var result = new List<(int x, int y, int z)>();

            if (!pre.Any())
            {
                return result;
            }

            var minX = pre.Min(x => x.x) - 1;
            var maxX = pre.Max(x => x.x) + 1;
            var minY = pre.Min(x => x.y) - 1;
            var maxY = pre.Max(x => x.y) + 1;
            var minZ = pre.Min(x => x.z) - 1;
            var maxZ = pre.Max(x => x.z) + 1;

            for (var x = minX; x <= maxX; ++x)
            {
                for (var y = minY; y <= maxY; ++y)
                {
                    for (var z = minZ; z <= maxZ; ++z)
                    {
                        var current = (x, y, z);
                        var cell = pre.Any(c => c == current);
                        var neighbours = pre.Where(c =>
                        Math.Abs(c.x - x) <= 1
                        && Math.Abs(c.y - y) <= 1
                        && Math.Abs(c.z - z) <= 1).ToList();

                        if (cell) neighbours.Remove(current);

                        var neighbourCount = neighbours.Count();
                        if (cell && neighbourCount == 2 || neighbourCount == 3)
                        {
                            result.Add(current);
                        }
                        else if (!cell && neighbourCount == 3)
                        {
                            result.Add(current);
                        }
                    }
                }
            }

            return result;
        }

        public List<(int x, int y, int z, int w)> CyclePart2(List<(int x, int y, int z, int w)> pre)
        {
            var result = new List<(int x, int y, int z, int w)>();

            if (!pre.Any())
            {
                return result;
            }

            var minX = pre.Min(x => x.x) - 1;
            var maxX = pre.Max(x => x.x) + 1;
            var minY = pre.Min(x => x.y) - 1;
            var maxY = pre.Max(x => x.y) + 1;
            var minZ = pre.Min(x => x.z) - 1;
            var maxZ = pre.Max(x => x.z) + 1;
            var minW = pre.Min(x => x.w) - 1;
            var maxW = pre.Max(x => x.w) + 1;

            for (var x = minX; x <= maxX; ++x)
            {
                for (var y = minY; y <= maxY; ++y)
                {
                    for (var z = minZ; z <= maxZ; ++z)
                    {
                        for (var w = minW; w <= maxW; ++w)
                        {
                            var current = (x, y, z, w);
                            var cell = pre.Any(c => c == current);
                            var neighbours = pre.Where(c =>
                            Math.Abs(c.x - x) <= 1
                            && Math.Abs(c.y - y) <= 1
                            && Math.Abs(c.z - z) <= 1
                            && Math.Abs(c.w - w) <= 1).ToList();

                            if (cell) neighbours.Remove(current);

                            var neighbourCount = neighbours.Count();
                            if (cell && neighbourCount == 2 || neighbourCount == 3)
                            {
                                result.Add(current);
                            }
                            else if (!cell && neighbourCount == 3)
                            {
                                result.Add(current);
                            }
                        }
                    }
                }
            }

            return result;
        }

        protected override string Part1()
        {
            for (var i = 0; i < 6; ++i)
            {
                _activeCellsPart1 = CyclePart1(_activeCellsPart1);
            }
            return _activeCellsPart1.Count.ToString();
        }

        protected override string Part2()
        {
            for (var i = 0; i < 6; ++i)
            {
                _activeCellsPart2 = CyclePart2(_activeCellsPart2);
            }
            return _activeCellsPart2.Count.ToString();
        }
    }
}
