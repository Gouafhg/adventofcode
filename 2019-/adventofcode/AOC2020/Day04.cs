﻿using adventofcode.Utilities;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace adventofcode.AOC2020
{
    internal class Day04 : Day
    {
        //Globals
        private readonly List<Dictionary<string, string>> _cards;

        //Constructor
        public Day04()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2020\day04.txt");

            //Test case
            var test1 = @"ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in";

            var invalidTest = @"eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007";

            var validTest = @"pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719";

            var input = file.Replace("\r\n", "\n").Split('\n');

            _cards = new List<Dictionary<string, string>>();
            var currentCard = new Dictionary<string, string>();
            foreach (var line in input)
            {
                if (string.IsNullOrWhiteSpace(line))
                {
                    _cards.Add(currentCard);
                    currentCard = new Dictionary<string, string>();
                    continue;
                }

                var data = line.Split(' ');
                foreach (var x in data)
                {
                    var fields = x.Split(':');
                    if (fields[0] != "cid")
                        currentCard.Add(fields[0], fields[1]);
                }
            }
            _cards.Add(currentCard);

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var validCards =
                _cards.Where(card => card.Count == 7);

            return validCards.Count().ToString();
        }

        protected override string Part2()
        {
            var validCards =
                _cards.Where(card => card.Count == 7);
            var rStart = "^";
            var rDigit = "[0-9]";
            var rHexaDigit = "[0-9a-f]";
            var rOr = "|";
            var rEnd = "$";
            var hgtRegex = new Regex(rStart + rDigit + rDigit + rDigit + "cm" + rEnd + rOr + rStart + rDigit + rDigit + "in" + rEnd);
            var hclRegex = new Regex(rStart + "#" + rHexaDigit + rHexaDigit + rHexaDigit + rHexaDigit + rHexaDigit + rHexaDigit + rEnd);
            var eclRegex = new Regex("^(amb|blu|brn|gry|grn|hzl|oth)$");
            var pidRegex = new Regex(rStart + rDigit + rDigit + rDigit + rDigit + rDigit + rDigit + rDigit + rDigit + rDigit + rEnd);

            var validCardCount = 0;
            foreach (var card in validCards)
            {
                if (!int.TryParse(card["byr"], out var byr) || byr < 1920 || byr > 2002)
                {
                    continue;
                }
                if (!int.TryParse(card["iyr"], out var iyr) || iyr < 2010 || iyr > 2020)
                {
                    continue;
                }
                if (!int.TryParse(card["eyr"], out var eyr) || eyr < 2020 || eyr > 2030)
                {
                    continue;
                }

                if (!hgtRegex.IsMatch(card["hgt"]))
                {
                    continue;
                }

                var hgtUnit = card["hgt"].Contains("cm") ? "cm" : "in";
                var hgt = int.Parse(card["hgt"].Substring(0, card["hgt"].Length - 2));
                if (hgtUnit == "cm")
                {
                    if (hgt < 150 || hgt > 193)
                    {
                        continue;
                    }
                }
                else
                {
                    if (hgt < 59 || hgt > 76)
                    {
                        continue;
                    }
                }

                if (!hclRegex.IsMatch(card["hcl"]))
                {
                    continue;
                }

                if (!eclRegex.IsMatch(card["ecl"]))
                {
                    continue;
                }

                if (!pidRegex.IsMatch(card["pid"]))
                {
                    continue;
                }

                validCardCount++;
            }

            return validCardCount.ToString();
        }
    }
}
