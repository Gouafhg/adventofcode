﻿using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2020
{
    public class Day15 : Day
    {
        //Globals
        private readonly List<int> _input;

        //Constructor
        public Day15()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = "18,8,0,5,4,1,20";
            //Test case
            var test1 = @"0,3,6";

            _input = file.Split(',').Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => int.Parse(x)).ToList();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var listOfNumbers = new List<int>(_input);
            while (listOfNumbers.Count < 2020)
            {
                var lastNumber = listOfNumbers.Last();
                if (listOfNumbers.Count(x => x == lastNumber) == 1)
                {
                    listOfNumbers.Add(0);
                }
                else
                {
                    var newNumber = (listOfNumbers as IEnumerable<int>).Reverse().Skip(1).TakeWhile(x => x != lastNumber).Count() + 1;
                    listOfNumbers.Add(newNumber);
                }
            }
            return listOfNumbers.Count + ": " + listOfNumbers.Last();
        }

        protected override string Part2()
        {
            var dicOfNumbers = new Dictionary<int, int>();
            for (var i = 0; i < _input.Count; ++i)
            {
                dicOfNumbers.Add(_input[i], i);
            }

            var listOfNumbers = new List<int>(_input)
            {
                0,
            };

            for (var i = listOfNumbers.Count; i < 30000000; ++i)
            {
                var lastNumber = listOfNumbers.Last();
                int newNumber;
                if (dicOfNumbers.ContainsKey(lastNumber))
                {
                    newNumber = i - dicOfNumbers[lastNumber] - 1;
                }
                else
                {
                    newNumber = 0;
                }

                if (dicOfNumbers.ContainsKey(lastNumber))
                {
                    dicOfNumbers[lastNumber] = i - 1;
                }
                else
                {
                    dicOfNumbers.Add(lastNumber, i - 1);
                }

                listOfNumbers.Add(newNumber);
            }

            return listOfNumbers.Last().ToString();
        }
    }
}
