﻿using adventofcode.Utilities;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace adventofcode.AOC2020
{
    public enum CombatPlayer { Player1, Player2 }
    public class Day22 : Day
    {
        //Globals
        private readonly List<string> _input;

        private readonly Queue<int> _p1Deck;
        private readonly Queue<int> _p2Deck;

        //Constructor
        public Day22()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2020\day22.txt");
            var file = ToolsFile.ReadFile(DataPath + @"2020\day22.txt");

            //Test case
            var test1 = @"Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10";

            var test2 = @"Player 1:
43
19

Player 2:
2
29
14";

            var chosenInput = file;
            _input = chosenInput.Replace("\r", "\n").Split('\n').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

            var p1Cards = _input.Skip(1).TakeWhile(x => x != "Player 2:").ToList();
            _p1Deck = new Queue<int>();
            foreach (var card in p1Cards)
            {
                _p1Deck.Enqueue(int.Parse(card));
            }
            var p2Cards = _input.SkipWhile(x => x != "Player 2:").Skip(1).ToList();
            _p2Deck = new Queue<int>();
            foreach (var card in p2Cards)
            {
                _p2Deck.Enqueue(int.Parse(card));
            }


            //Do Pre-stuff
        }

        public void CombatRound()
        {
            var p1Card = _p1Deck.Dequeue();
            var p2Card = _p2Deck.Dequeue();

            if (p1Card > p2Card)
            {
                _p1Deck.Enqueue(p1Card);
                _p1Deck.Enqueue(p2Card);
            }
            else
            {
                _p2Deck.Enqueue(p2Card);
                _p2Deck.Enqueue(p1Card);
            }
        }

        public (CombatPlayer player, BigInteger score) RecursiveCombat(IEnumerable<int> p1DeckEnumerable, IEnumerable<int> p2DeckEnumerable, int level)
        {
            var p1Deck = new Queue<int>(p1DeckEnumerable);
            var p2Deck = new Queue<int>(p2DeckEnumerable);
            var p1Decklist = new List<string>();
            var p2Decklist = new List<string>();
            //var round = 0;
            while (p1Deck.Any() && p2Deck.Any())
            {
                //Console.WriteLine($"-- Round {++round} (Game {level}) --");
                //Console.WriteLine("Player 1's deck: " + string.Join(", ", P1Deck));
                //Console.WriteLine("Player 2's deck: " + string.Join(", ", P2Deck));
                var p1deckshot = string.Join(",", p1Deck);
                var p2deckshot = string.Join(",", p2Deck);
                if (p1Decklist.Any(x => x == p1deckshot) || p2Decklist.Any(x => x == p2deckshot))
                {
                    var r = BigInteger.Zero;
                    var place = 1;
                    foreach (var c in p1Deck.Reverse())
                    {
                        r += place * c;
                        place++;
                    }

                    return (CombatPlayer.Player1, r);
                }
                p1Decklist.Add(p1deckshot);
                p1Decklist.Add(p2deckshot);

                var p1Card = p1Deck.Dequeue();
                var p2Card = p2Deck.Dequeue();
                //Console.WriteLine("Player 1 plays: " + p1Card);
                //Console.WriteLine("Player 2 plays: " + p2Card);

                CombatPlayer winnerOfRound;
                if (p1Deck.Count >= p1Card && p2Deck.Count >= p2Card)
                {
                    winnerOfRound = RecursiveCombat(p1Deck.Take(p1Card), p2Deck.Take(p2Card), level + 1).player;
                }
                else
                {
                    winnerOfRound = p1Card > p2Card ? CombatPlayer.Player1 : CombatPlayer.Player2;
                }

                if (winnerOfRound == CombatPlayer.Player1)
                {
                    p1Deck.Enqueue(p1Card);
                    p1Deck.Enqueue(p2Card);
                }
                else
                {
                    p2Deck.Enqueue(p2Card);
                    p2Deck.Enqueue(p1Card);
                }
                //Console.WriteLine($"{winnerOfRound} wins round {round} of game {level}!");
                //Console.WriteLine();
                //Console.ReadKey();
            }

            var winner = p1Deck.Any() ? CombatPlayer.Player1 : CombatPlayer.Player2;
            var winnerDeck = winner == CombatPlayer.Player1 ? p1Deck : p2Deck;

            var score = BigInteger.Zero;
            var i = 1;
            foreach (var c in winnerDeck.Reverse())
            {
                score += i * c;
                i++;
            }

            return (winner, score);
        }

        protected override string Part1()
        {
            var r = BigInteger.Zero;

            while (_p1Deck.Any() && _p2Deck.Any()) CombatRound();
            var winnerDeck = _p1Deck.Any() ? _p1Deck : _p2Deck;
            var i = 1;
            foreach (var c in winnerDeck.Reverse())
            {
                r += i * c;
                i++;
            }

            return r.ToString();
        }

        protected override string Part2()
        {
            return RecursiveCombat(_p1Deck, _p2Deck, 1).score.ToString();
        }
    }
}
