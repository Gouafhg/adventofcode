﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace adventofcode.AOC2020
{
    public class Day23 : Day
    {
        //Globals
        private readonly string _input;

        private readonly int _part2CupCount = 1000000;

        //Constructor
        public Day23()
        {
            //Init globals
            Part = 2;

            var realInput = "167248359";

            //Test cases
            var test1 = "32415";
            var test2 = "389125467";


            _input = realInput;
            //Do Pre-stuff
        }

        private int IntParse(char c)
        {
            return int.Parse(string.Empty + c);
        }

        private char Subtract(char c)
        {
            return (IntParse(c) - 1).ToString()[0];
        }

        private bool LessThan(char a, char b)
        {
            return IntParse(a) < IntParse(b);
        }

        public (List<char> next, char NextCurrent) RoundPart1(List<char> prev, char currentCup)
        {
            var rest = new List<char>(prev);
            var indexOfCurrent = rest.IndexOf(currentCup);
            var firstThree = new List<char>();
            var indexOfFirstThree = (indexOfCurrent + 1) % rest.Count;
            while (firstThree.Count < 3)
            {
                firstThree.Add(rest[indexOfFirstThree]);
                indexOfFirstThree = (indexOfFirstThree + 1) % rest.Count;
            }
            rest.RemoveAll(x => firstThree.Contains(x));
            var dest = Subtract(currentCup);
            if (LessThan(dest, '1'))
            {
                dest = '9';
            }
            while (firstThree.Any(x => x == dest))
            {
                dest = Subtract(dest);
                if (LessThan(dest, '1'))
                {
                    dest = '9';
                }
            }
            var indexOfDest = rest.IndexOf(dest);
            rest.InsertRange(indexOfDest + 1, firstThree);

            indexOfCurrent = rest.IndexOf(currentCup);
            var newCurrent = rest[(indexOfCurrent + 1) % rest.Count];
            return (rest, newCurrent);
        }

        private readonly List<int> _ft = new List<int>()
        {
            Capacity = 3,
        };

        public int RoundPart2(List<int> cups, int currentCupIndex)
        {
            var currentCup = cups[currentCupIndex];
            //Console.WriteLine("CurrentCup: " + currentCup);
            _ft.Clear();
            var indexOfFirstThree = (currentCupIndex + 1) % cups.Count;
            while (_ft.Count < 3)
            {
                _ft.Add(cups[indexOfFirstThree]);
                indexOfFirstThree = (indexOfFirstThree + 1) % cups.Count;
            }
            //Console.WriteLine("Three: " + string.Join(", ", FT.Select(x => x.ToString())));
            //Console.WriteLine();
            var removeIndex = (currentCupIndex + 1) % cups.Count;
            //Console.WriteLine(cups[removeIndex]);
            cups.RemoveAt(removeIndex);
            if (removeIndex < currentCupIndex) currentCupIndex--;
            removeIndex = (currentCupIndex + 1) % cups.Count;
            //Console.WriteLine(cups[removeIndex]);
            cups.RemoveAt(removeIndex);
            if (removeIndex < currentCupIndex) currentCupIndex--;
            removeIndex = (currentCupIndex + 1) % cups.Count;
            //Console.WriteLine(cups[removeIndex]);
            cups.RemoveAt(removeIndex);
            if (removeIndex < currentCupIndex) currentCupIndex--;
            //Console.WriteLine();
            var dest = currentCup - 1;
            if (dest < 1)
            {
                dest = _part2CupCount;
            }
            while (_ft.Any(x => x == dest))
            {
                dest -= 1;
                if (dest < 1)
                {
                    dest = _part2CupCount;
                }
            }
            //Console.WriteLine("Destination: " + dest);
            //Console.WriteLine();
            //Console.WriteLine();
            var indexOfDest = cups.IndexOf(dest);
            if (indexOfDest < currentCupIndex) currentCupIndex += 3;
            cups.InsertRange(indexOfDest + 1, _ft);

            currentCupIndex = (currentCupIndex + 1) % cups.Count;
            return currentCupIndex;
        }

        protected override string Part1()
        {
            var cups = _input.ToList();
            var currentCup = cups.First();

            for (var i = 0; i < 100; ++i)
            {
                Console.WriteLine(string.Join(" ", cups.Select(x => string.Empty + x)));
                (cups, currentCup) = RoundPart1(cups, currentCup);
            }

            var order = new List<char>();
            var index = (cups.IndexOf('1') + 1) % cups.Count;
            while (order.Count < cups.Count - 1)
            {
                order.Add(cups[index]);
                index = (index + 1) % cups.Count;
            }

            return new string(order.ToArray());
        }

        protected override string Part2()
        {
            var cups = _input.Select(x => IntParse(x)).ToList();
            var startLabel = 9;
            while (cups.Count < _part2CupCount)
            {
                startLabel++;
                cups.Add(startLabel);
            }
            var currentCupIndex = 0;

            for (var i = 0; i < 10000000; ++i)
            {
                if (i % 1000 == 0)
                {
                    Console.WriteLine(i);
                }
                //Console.WriteLine(string.Join(" ", cups.Select(x => (string.Empty + x).PadRight(2))));
                //Thread.Sleep(500);
                currentCupIndex = RoundPart2(cups, currentCupIndex);
            }

            var indexOfOne = cups.IndexOf(1);
            var first = cups[(indexOfOne + 1) % cups.Count];
            var second = cups[(indexOfOne + 2) % cups.Count];

            var mult = BigInteger.One;
            mult *= first;
            mult *= second;
            return mult.ToString();
        }
    }
}
