﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace adventofcode.AOC2020
{
    public class Day08 : Day
    {
        //Globals

        private readonly List<(InstructionEnum, int)> _register;

        private readonly StateMachine2020 _sm;
        //Constructor
        public Day08()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2020\day08.txt");

            //Test case
            var test1 = @"nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6";

            var input = Regex.Split(file.Replace("\r\n", "\n"), "\n").ToList();
            _register = new List<(InstructionEnum, int)>();
            foreach (var line in input)
            {
                var instruction = (NoOp: InstructionEnum.Nop, 0);
                var ins = line.Split(' ')[0];
                var arg = int.Parse(line.Split(' ')[1]);
                switch (ins)
                {
                    case "nop":
                        instruction = (InstructionEnum.Nop, arg);
                        break;
                    case "acc":
                        instruction = (InstructionEnum.Acc, arg);
                        break;
                    case "jmp":
                        instruction = (InstructionEnum.Jmp, arg);
                        break;
                }
                _register.Add(instruction);
            }

            //Do Pre-stuff

            _sm = new StateMachine2020(_register);
        }

        protected override string Part1()
        {
            _sm.Execute();
            return _sm.Accumulator.ToString();
        }

        protected override string Part2()
        {
            var nopExchange = new Queue<int>();
            var jmpExchange = new Queue<int>();
            for (var i = 0; i < _register.Count; ++i)
            {
                if (_register[i].Item1 == InstructionEnum.Nop)
                {
                    nopExchange.Enqueue(i);
                }
                if (_register[i].Item1 == InstructionEnum.Jmp)
                {
                    jmpExchange.Enqueue(i);
                }
            }

            bool halted;
            do
            {
                _sm.Execute();
                halted = _sm.Halt;
                if (!halted) continue;

                if (nopExchange.Count > 0)
                {
                    _sm.Reset();
                    var pos = nopExchange.Dequeue();
                    var ins = _sm.Get(pos);
                    ins.instruction = InstructionEnum.Jmp;
                    _sm.Put(ins, pos);
                }
                else if (jmpExchange.Count > 0)
                {
                    _sm.Reset();
                    var pos = jmpExchange.Dequeue();
                    var ins = _sm.Get(pos);
                    ins.instruction = InstructionEnum.Nop;
                    _sm.Put(ins, pos);
                }
                else
                {
                    throw new ApplicationException();
                }
            } while (halted);
            return _sm.Accumulator.ToString();
        }
    }
}
