﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2020
{
    public enum FlipSide { White, Black }
    public enum MoveCommand { East, West, NorthEast, NorthWest, SouthEast, SouthWest }
    public class Day24 : Day
    {
        //Globals
        private readonly List<string> _input;

        private readonly List<List<MoveCommand>> _commandLines;
        //Constructor
        public Day24()
        {
            //Init globals
            Part = 2;

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2020\day24.txt");
            //var file = ToolsFile.ReadFile(@"D:\GIT\adventofcode\2019-\data\2020\day24.txt");
            var file = ToolsFile.ReadFile(DataPath + @"2020\day24.txt");

            //Test case
            var test1 = @"sesenwnenenewseeswwswswwnenewsewsw
neeenesenwnwwswnenewnwwsewnenwseswesw
seswneswswsenwwnwse
nwnwneseeswswnenewneswwnewseswneseene
swweswneswnenwsewnwneneseenw
eesenwseswswnenwswnwnwsewwnwsene
sewnenenenesenwsewnenwwwse
wenwwweseeeweswwwnwwe
wsweesenenewnwwnwsenewsenwwsesesenwne
neeswseenwwswnwswswnw
nenwswwsewswnenenewsenwsenwnesesenew
enewnwewneswsewnwswenweswnenwsenwsw
sweneswneswneneenwnewenewwneswswnese
swwesenesewenwneswnwwneseswwne
enesenwswwswneneswsenwnewswseenwsese
wnwnesenesenenwwnenwsewesewsesesew
nenewswnwewswnenesenwnesewesw
eneswnwswnwsenenwnwnwwseeswneewsenese
neswnwewnwnwseenwseesewsenwsweewe
wseweeenwnesenwwwswnew";

            var chosenInput = file;
            _input = chosenInput.Replace("\r", "\n").Split('\n').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
            _commandLines = new List<List<MoveCommand>>();
            foreach (var line in _input)
            {
                var commandLine = new List<MoveCommand>();
                var i = 0;
                while (i < line.Length)
                {
                    switch (line[i])
                    {
                        case 'e':
                            commandLine.Add(MoveCommand.East);
                            i++;
                            break;
                        case 'w':
                            commandLine.Add(MoveCommand.West);
                            i++;
                            break;
                        case 'n':
                            commandLine.Add(line[i + 1] == 'w' ? MoveCommand.NorthWest : MoveCommand.NorthEast);
                            i += 2;
                            break;
                        case 's':
                            commandLine.Add(line[i + 1] == 'w' ? MoveCommand.SouthWest : MoveCommand.SouthEast);
                            i += 2;
                            break;
                    }
                }
                _commandLines.Add(commandLine);
            }

            //Do Pre-stuff
        }

        private (int x, int y) Add((int x, int y) a, (int x, int y) b) => (a.x + b.x, a.y + b.y);
        private (int x, int y) Subtract((int x, int y) a, (int x, int y) b) => (a.x - b.x, a.y - b.y);

        private (int x, int y) ParseCommand((int x, int y) pos, MoveCommand command)
        {
            switch (command)
            {
                case MoveCommand.East:
                    pos = Add(pos, (1, 0));
                    break;
                case MoveCommand.West:
                    pos = Add(pos, (-1, 0));
                    break;
                case MoveCommand.NorthEast:
                    if (pos.y % 2 == 0)
                    {
                        pos = Add(pos, (0, -1));
                    }
                    else
                    {
                        pos = Add(pos, (1, -1));
                    }
                    break;
                case MoveCommand.NorthWest:
                    if (pos.y % 2 == 0)
                    {
                        pos = Add(pos, (-1, -1));
                    }
                    else
                    {
                        pos = Add(pos, (0, -1));
                    }
                    break;
                case MoveCommand.SouthEast:
                    if (pos.y % 2 == 0)
                    {
                        pos = Add(pos, (0, 1));
                    }
                    else
                    {
                        pos = Add(pos, (1, 1));
                    }
                    break;
                case MoveCommand.SouthWest:
                    if (pos.y % 2 == 0)
                    {
                        pos = Add(pos, (-1, 1));
                    }
                    else
                    {
                        pos = Add(pos, (0, 1));
                    }
                    break;
            }
            return pos;
        }

        private List<(int x, int y)> GetAdjacent((int x, int y) pos)
        {
            var result = new List<(int x, int y)>();
            foreach (MoveCommand c in Enum.GetValues(typeof(MoveCommand)))
            {
                result.Add(ParseCommand(pos, c));
            }
            return result;
        }

        public FlipSide Flip(FlipSide prev)
        {
            return prev == FlipSide.White ? FlipSide.Black : FlipSide.White;
        }

        protected override string Part1()
        {
            var flips = new List<(int x, int y)>();
            foreach (var commandLine in _commandLines)
            {
                var pos = (x: 0, y: 0);
                foreach (var command in commandLine)
                {
                    pos = ParseCommand(pos, command);
                }
                flips.Add(pos);
            }

            var sidesUp = new Dictionary<(int x, int y), FlipSide>();

            foreach (var pos in flips.Distinct())
            {
                sidesUp.Add(pos, FlipSide.White);
            }

            foreach (var pos in flips)
            {
                sidesUp[pos] = Flip(sidesUp[pos]);
            }

            return sidesUp.Count(kp => kp.Value == FlipSide.Black).ToString();
        }

        public List<(int x, int y)> Tick(List<(int x, int y)> state)
        {
            var result = new List<(int x, int y)>(state);
            var totalChecks = new List<(int x, int y)>(state);
            foreach (var pos in state)
            {
                totalChecks.AddRange(GetAdjacent(pos));
            }
            totalChecks = totalChecks.Distinct().ToList();

            foreach (var pos in totalChecks)
            {
                //was black
                if (state.Contains(pos))
                {
                    var blackCount = GetAdjacent(pos).Count(state.Contains);
                    if (blackCount == 0 || blackCount > 2)
                    {
                        result.Remove(pos);
                    }
                }
                //was white
                else
                {
                    var blackCount = GetAdjacent(pos).Count(state.Contains);
                    if (blackCount == 2)
                    {
                        result.Add(pos);
                    }
                }
            }
            return result;
        }

        protected override string Part2()
        {
            var flips = new List<(int x, int y)>();
            foreach (var commandLine in _commandLines)
            {
                var pos = (x: 0, y: 0);
                foreach (var command in commandLine)
                {
                    pos = ParseCommand(pos, command);
                }
                flips.Add(pos);
            }

            var sidesUp = new Dictionary<(int x, int y), FlipSide>();

            foreach (var pos in flips.Distinct())
            {
                sidesUp.Add(pos, FlipSide.White);
            }

            foreach (var pos in flips)
            {
                sidesUp[pos] = Flip(sidesUp[pos]);
            }

            var blackTiles = sidesUp.Where(kp => kp.Value == FlipSide.Black).Select(x => x.Key).ToList();

            for (var i = 0; i < 100; ++i)
            {
                if (
                    i == 1
                    || i == 2
                    || i == 3
                    || i == 4
                    || i == 5
                    || i == 6
                    || i == 7
                    || i == 8
                    || i == 9
                    || i == 10
                    || i == 20
                    || i == 30
                    || i == 40
                    || i == 50
                    || i == 60
                    || i == 70
                    || i == 80
                    || i == 90
                    )
                {
                    Console.WriteLine(i + ": " + blackTiles.Count);
                }
                blackTiles = Tick(blackTiles);

            }

            return blackTiles.Count.ToString(); ;
        }
    }
}
