﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text.RegularExpressions;
using adventofcode.Utilities;

namespace adventofcode.AOC2020
{
    public class Day16 : Day
    {
        //Globals
        private readonly List<string> _input;

        private readonly List<(int low, int high)> _intervals;
        private readonly List<(string name, List<(int low, int high)> intervals)> _namedIntervals;
        private readonly int[] _myTicket;
        private readonly List<int[]> _othertickets;


        //Constructor
        public Day16()
        {
            //Init globals
            Part = 2;

            //Setup file
            var dataPath = @"C:\THEGANGGIT\adventofcode\2019-\data\";
            var file = ToolsFile.ReadFile(dataPath + @"2020\day16.txt");

            //Test case
            var test1 = @"class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12";

            var test2 = @"class: 0-1 or 4-19
row: 0-5 or 8-19
seat: 0-13 or 16-19

your ticket:
11,12,13

nearby tickets:
3,9,18
15,1,5
5,14,9";

            var chosenInput = file;
            _input = chosenInput.Replace("\r\n", "\n").Split('\n').ToList();

            _intervals = _input.TakeWhile(x => !string.IsNullOrWhiteSpace(x)).Select(x => Regex.Split(x, ": ")[1])
                .SelectMany(x => Regex.Split(x, " or ")).Select(x =>
                    (low: int.Parse(x.Split('-')[0]), high: int.Parse(x.Split('-')[1]))).ToList();

            var names = _input.TakeWhile(x => !string.IsNullOrWhiteSpace(x)).Select(x => Regex.Split(x, ": ")[0])
                .ToList();
            _namedIntervals = new List<(string name, List<(int low, int high)>)>();
            for (var i = 0; i < names.Count; ++i)
            {
                _namedIntervals.Add((names[i], new List<(int low, int high)>
                {
                    (_intervals[i*2].low, _intervals[i*2].high),
                    (_intervals[i*2 + 1].low, _intervals[i*2 + 1].high),
                }));
            }
            _myTicket = _input.SkipWhile(x => !string.IsNullOrWhiteSpace(x)).Skip(2).Take(1).ToList()[0].Split(',')
                .Select(int.Parse).ToArray();
            _othertickets = _input.SkipWhile(x => !string.IsNullOrWhiteSpace(x)).Skip(05)
                .Select(x => x.Split(',').Select(int.Parse).ToArray()).ToList();
            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var allOtherValues = _othertickets.SelectMany(x => x);
            var otherValuesWithError =
                allOtherValues.Where(x => !_intervals.Any(y => y.low <= x && y.high >= x));
            return otherValuesWithError.Sum(x => x).ToString();
        }

        private static bool InIntervals(int value, IEnumerable<(int low, int high)> intervals)
        {
            return intervals.Any(x => x.low <= value && x.high >= value);
        }

        protected override string Part2()
        {
            var validTickets = _othertickets.Where(x => x.All(y => _intervals.Any(z => z.low <= y && z.high >= y))).ToList();
            var fieldPossibilities = new List<(int field, List<string> names)>();

            for (var i = 0; i < validTickets[0].Length; ++i)
            {
                fieldPossibilities.Add((i, new List<string>()));
            }

            foreach (var (field, names) in fieldPossibilities)
            {
                foreach (var (name, intervals) in _namedIntervals)
                {
                    if (validTickets.All(x => InIntervals(x[field], intervals)))
                    {
                        names.Add(name);
                    }
                }
            }

            while (fieldPossibilities.Any(x => x.names.Count != 1))
            {
                for (var i = 0; i < fieldPossibilities.Count; ++i)
                {
                    if (fieldPossibilities[i].names.Count != 1) continue;
                    for (var j = 0; j < fieldPossibilities.Count; ++j)
                    {
                        if (i == j) continue;
                        fieldPossibilities[j].names.Remove(fieldPossibilities[i].names[0]);
                    }
                }
            }

            var departures = fieldPossibilities.Where(x => x.names.Any(y => y.StartsWith("departure"))).ToList();
            var departureValues = departures.Select(x => x.field).Select(x => _myTicket[x]);

            var agg = departureValues.Aggregate(BigInteger.One, (current, v) => current * v);


            return agg.ToString();
        }
    }
}
