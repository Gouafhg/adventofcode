﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace adventofcode.AOC2020
{
    internal class Day03 : Day
    {
        //Globals
        private readonly List<string> _input;

        //Constructor
        public Day03()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(DataPath + @"2020\day03.txt");

            //Test case
            var test1 = @"..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#";

            _input = file.Split(Environment.NewLine.ToArray()).Where(x => x != "").ToList();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var slopeX = 3;
            var slopeY = 1;
            var treeCount = 0;
            var column = 0;
            var row = 0;
            while (row < _input.Count)
            {
                if (_input[row][column % _input[row].Length] == '#')
                {
                    treeCount++;
                }
                column += slopeX;
                row += slopeY;
            }
            return treeCount.ToString();
        }

        protected override string Part2()
        {
            var slopeList = new List<(int x, int y)>
            {
                (1,1),
                (3,1),
                (5,1),
                (7,1),
                (1,2),
            };
            BigInteger ack = 1;
            foreach (var slope in slopeList)
            {
                var treeCount = 0;
                var column = 0;
                var row = 0;
                while (row < _input.Count)
                {
                    if (_input[row][column % _input[row].Length] == '#')
                    {
                        treeCount++;
                    }
                    column += slope.x;
                    row += slope.y;
                }
                ack *= treeCount;
                Console.WriteLine(".");
            }
            return ack.ToString();
        }
    }
}
