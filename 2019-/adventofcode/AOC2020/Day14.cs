﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text.RegularExpressions;

namespace adventofcode.AOC2020
{
    public class Day14 : Day
    {
        //Globals
        private readonly List<List<string>> _input;
        private readonly Dictionary<ulong, string> _register;

        //Constructor
        public Day14()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(DataPath + @"2020\day14.txt");

            //Test case
            var test1 = @"mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0
mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0";

            var test2 = @"mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1";

            _input = new List<List<string>>();
            _register = new Dictionary<ulong, string>();

            var chosenInput = file;
            var lines = chosenInput.Split(Environment.NewLine.ToArray()).Where(x => x != "").ToList();
            for (var i = 0; i < lines.Count;)
            {
                _input.Add(lines.Skip(i + 1).TakeWhile(x => x.Substring(0, 4) != "mask").ToList());
                _input.Last().Insert(0, lines[i]);
                i += _input.Last().Count;
            }

            //Do Pre-stuff
        }

        private string Mask(string pre, string mask, string value)
        {
            var result = string.Empty;
            var i = 0;
            for (; i < value.Length && i < mask.Length && i < pre.Length; ++i)
            {
                result += mask[i] == 'X' ? value[i] : mask[i];
            }

            for (; i < pre.Length && i < mask.Length; ++i)
            {
                result += mask[i] == 'X' ? pre[i] : mask[i];
            }
            return result;
        }

        protected override string Part1()
        {

            var maskSet = new List<(string mask, List<(ulong adress, string value)> commands)>();
            foreach (var s in _input)
            {
                var mask = new string(Regex.Split(s[0], " = ")[1].Reverse().ToArray());
                maskSet.Add((mask, new List<(ulong, string)>()));
                foreach (var r in s.Skip(1))
                {
                    var parts = Regex.Split(r, " = ");
                    var adress = ulong.Parse(new string(parts[0].Skip(4).TakeWhile(x => x != ']').ToArray()));
                    var value = new string(Convert.ToString(long.Parse(parts[1]), 2).Reverse().ToArray());
                    maskSet.Last().commands.Add((adress, value.PadRight(mask.Length, '0')));
                }
            }

            foreach (var v in maskSet.SelectMany(x => x.commands.Select(y => y.adress)).OrderBy(x => x).Distinct())
            {
                _register.Add(v, new string('0', 36));
            }

            foreach (var s in maskSet)
            {
                var mask = s.mask;
                foreach (var (adress, value) in s.commands)
                {
                    _register[adress] = Mask(_register[adress], mask, value);
                }
            }

            var ulongReg = new Dictionary<ulong, ulong>();
            var adresses = _register.Keys.ToList();
            foreach (var adress in adresses)
            {
                _register[adress] = new string(_register[adress].Reverse().ToArray());
                ulongReg.Add(adress, Convert.ToUInt64(_register[adress], 2));
            }

            return SumUlongs(ulongReg.Values).ToString();
        }

        private static BigInteger SumUlongs(IEnumerable<ulong> list)
        {
            return list.Aggregate(BigInteger.Zero, (current, x) => current + x);
        }

        private List<ulong> GetAdressList(string mask, int adress)
        {
            var adressAsString = new string(Convert.ToString(adress, 2).Reverse().ToArray()).PadRight(36, '0');

            var maskedAdress = new char[mask.Length];
            for (var i = 0; i < mask.Length; ++i)
            {
                maskedAdress[i] = mask[i] == '0' ? adressAsString[i] : mask[i];
            }

            var indicesWithX = new List<int>();
            for (var i = 0; i < mask.Length; ++i)
            {
                if (maskedAdress[i] == 'X') indicesWithX.Add(i);
            }

            var combinations = new List<List<int>>();
            for (var i = 0; i <= indicesWithX.Count; ++i)
            {
                var combinationsOfCount = Tools.Combinations(indicesWithX, i).Cast<IEnumerable<int>>().Select(x => x.ToList()).ToList();
                combinations.AddRange(combinationsOfCount);
            }

            var result = new List<ulong>();
            foreach (var combination in combinations)
            {
                var newArray = new char[mask.Length];
                Array.Copy(maskedAdress, newArray, maskedAdress.Length);
                foreach (var index in indicesWithX)
                {
                    newArray[index] = combination.Contains(index) ? '1' : '0';
                }

                var newArrayAsString = new string(newArray.Reverse().ToArray());
                result.Add(Convert.ToUInt64(newArrayAsString, 2));
            }
            return result;
        }

        protected override string Part2()
        {
            var maskSet = new List<(string mask, List<(int adress, string value)> commands)>();
            foreach (var s in _input)
            {
                var mask = new string(Regex.Split(s[0], " = ")[1].Reverse().ToArray());
                maskSet.Add((mask, new List<(int, string)>()));
                foreach (var r in s.Skip(1))
                {
                    var parts = Regex.Split(r, " = ");
                    var adress = int.Parse(new string(parts[0].Skip(4).TakeWhile(x => x != ']').ToArray()));
                    var value = new string(Convert.ToString(long.Parse(parts[1]), 2).Reverse().ToArray());
                    maskSet.Last().commands.Add((adress, value.PadRight(mask.Length, '0')));
                }
            }

            for (var i = 0; i < maskSet.Count; ++i)
            {
                var mask = maskSet[i].mask;
                for (var j = 0; j < maskSet[i].commands.Count; ++j)
                {
                    var command = maskSet[i].commands[j];
                    var adresses = GetAdressList(mask, command.adress);
                    foreach (var adress in adresses)
                    {
                        if (!_register.Keys.Contains(adress))
                        {
                            _register.Add(adress, command.value);
                            continue;
                        }

                        _register[adress] = command.value;
                    }
                }
            }

            var ulongReg = new Dictionary<ulong, ulong>();
            var asssss = _register.Keys.ToList();
            foreach (var adress in asssss)
            {
                _register[adress] = new string(_register[adress].Reverse().ToArray());
                ulongReg.Add(adress, Convert.ToUInt64(_register[adress], 2));
            }

            return SumUlongs(ulongReg.Values).ToString();
        }
    }
}
