﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode.AOC2020
{
    public class Day12 : Day
    {
        //Globals
        private readonly List<string> _input;

        //Constructor
        public Day12()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(DataPath + @"2020\day12.txt");

            //Test case
            var test1 = @"F10
N3
F7
R90
F11";

            _input = file.Split(Environment.NewLine.ToArray()).Where(x => x != "").ToList();

            //Do Pre-stuff
        }

        private (string dir, int amount) ParseLine(string line)
        {
            var dir = line[0].ToString();
            var amount = int.Parse(new string(line.Skip(1).ToArray()));
            return (dir, amount);
        }

        private (string dir, int amount) ParseMove(string line)
        {
            var move = ParseLine(line);
            switch (move.dir)
            {
                default:
                    return (_facing, move.amount);
                case "N":
                    return move;
                case "E":
                    return move;
                case "S":
                    return move;
                case "W":
                    return move;
                case "L":
                    switch (move.amount)
                    {
                        default:
                            return (_facing, 0);
                        case 90:
                            switch (_facing)
                            {
                                case "N":
                                    _facing = "W";
                                    return ("W", 0);
                                case "E":
                                    _facing = "N";
                                    return ("N", 0);
                                case "S":
                                    _facing = "E";
                                    return ("E", 0);
                                case "W":
                                    _facing = "S";
                                    return ("S", 0);
                            }
                            break;
                        case 180:
                            switch (_facing)
                            {
                                case "N":
                                    _facing = "S";
                                    return ("S", 0);
                                case "E":
                                    _facing = "W";
                                    return ("W", 0);
                                case "S":
                                    _facing = "N";
                                    return ("N", 0);
                                case "W":
                                    _facing = "E";
                                    return ("E", 0);
                            }
                            break;
                        case 270:
                            switch (_facing)
                            {
                                case "N":
                                    _facing = "E";
                                    return ("E", 0);
                                case "E":
                                    _facing = "S";
                                    return ("S", 0);
                                case "S":
                                    _facing = "W";
                                    return ("W", 0);
                                case "W":
                                    _facing = "N";
                                    return ("N", 0);
                            }
                            break;
                    }
                    break;
                case "R":
                    switch (move.amount)
                    {
                        default:
                            return (_facing, 0);
                        case 90:
                            switch (_facing)
                            {
                                case "N":
                                    _facing = "E";
                                    return ("E", 0);
                                case "E":
                                    _facing = "S";
                                    return ("S", 0);
                                case "S":
                                    _facing = "W";
                                    return ("W", 0);
                                case "W":
                                    _facing = "N";
                                    return ("N", 0);
                            }
                            break;
                        case 180:
                            switch (_facing)
                            {
                                case "N":
                                    _facing = "S";
                                    return ("S", 0);
                                case "E":
                                    _facing = "W";
                                    return ("W", 0);
                                case "S":
                                    _facing = "N";
                                    return ("N", 0);
                                case "W":
                                    _facing = "E";
                                    return ("E", 0);
                            }
                            break;
                        case 270:
                            switch (_facing)
                            {
                                case "N":
                                    _facing = "W";
                                    return ("W", 0);
                                case "E":
                                    _facing = "N";
                                    return ("N", 0);
                                case "S":
                                    _facing = "E";
                                    return ("E", 0);
                                case "W":
                                    _facing = "S";
                                    return ("S", 0);
                            }
                            break;
                    }
                    break;
            }
            return ("", 0);
        }

        private (int x, int y) Move((int x, int y) lastPosition, (string dir, int amount) move)
        {
            var newPosition = lastPosition;
            switch (move.dir)
            {
                case "N":
                    newPosition.y += move.amount;
                    break;
                case "E":
                    newPosition.x += move.amount;
                    break;
                case "S":
                    newPosition.y -= move.amount;
                    break;
                case "W":
                    newPosition.x -= move.amount;
                    break;
            }
            return newPosition;
        }

        private (int x, int y) MoveWaypoint((int x, int y) lastPosition, (string dir, int amount) move)
        {
            var newPosition = lastPosition;
            switch (move.dir)
            {
                case "N":
                    newPosition.y += move.amount;
                    break;
                case "E":
                    newPosition.x += move.amount;
                    break;
                case "S":
                    newPosition.y -= move.amount;
                    break;
                case "W":
                    newPosition.x -= move.amount;
                    break;
                case "L":
                    switch (move.amount)
                    {
                        case 0:
                            break;
                        case 90:
                            newPosition = (-lastPosition.y, lastPosition.x);
                            break;
                        case 180:
                            newPosition = (-lastPosition.x, -lastPosition.y);
                            break;
                        case 270:
                            newPosition = (lastPosition.y, -lastPosition.x);
                            break;
                    }
                    break;
                case "R":
                    switch (move.amount)
                    {
                        case 0:
                            break;
                        case 90:
                            newPosition = (lastPosition.y, -lastPosition.x);
                            break;
                        case 180:
                            newPosition = (-lastPosition.x, -lastPosition.y);
                            break;
                        case 270:
                            newPosition = (-lastPosition.y, lastPosition.x);
                            break;
                    }
                    break;
            }
            return newPosition;
        }

        private string _facing = "E";

        protected override string Part1()
        {
            var currentPosition = (x: 0, y: 0);

            foreach (var line in _input)
            {
                var thisMove = ParseMove(line);
                currentPosition = Move(currentPosition, thisMove);
            }
            return (Math.Abs(currentPosition.x) + Math.Abs(currentPosition.y)).ToString();
        }

        private (int x, int y) _waypoint;

        protected override string Part2()
        {
            _waypoint = (10, 1);
            var currentPosition = (x: 0, y: 0);

            foreach (var line in _input)
            {
                var thisMove = ParseLine(line);
                if (thisMove.dir == "F")
                {
                    currentPosition = (currentPosition.x + _waypoint.x * thisMove.amount, currentPosition.y + _waypoint.y * thisMove.amount);
                }
                else
                {
                    _waypoint = MoveWaypoint(_waypoint, thisMove);
                }
            }
            return (Math.Abs(currentPosition.x) + Math.Abs(currentPosition.y)).ToString();
        }
    }
}
