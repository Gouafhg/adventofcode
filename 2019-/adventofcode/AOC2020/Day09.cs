﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace adventofcode.AOC2020
{
    public class Day09 : Day
    {
        //Globals
        private readonly List<BigInteger> _input;

        private readonly int _preamble = 25;
        //Constructor
        public Day09()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(DataPath + @"2020\day09.txt");

            //Test case
            var test1 = @"35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576";

            _input = file.Split(Environment.NewLine.ToArray()).Where(x => x != "").Select(x => BigInteger.Parse(x)).ToList();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var index = -1;
            for (var i = _preamble; i < _input.Count; ++i)
            {
                var found = false;
                for (var a = i - _preamble; a < i && !found; ++a)
                {
                    for (var b = a + 1; b < i && !found; ++b)
                    {
                        if (_input[a] + _input[b] == _input[i])
                        {
                            found = true;
                        }
                    }
                }
                if (!found)
                {
                    index = i;
                    break;
                }
            }
            return $"Fails at {(index + 1)} which is {_input[index]}.";
        }

        protected override string Part2()
        {
            var index = -1;
            for (var i = _preamble; i < _input.Count; ++i)
            {
                var found = false;
                for (var a = i - _preamble; a < i && !found; ++a)
                {
                    for (var b = a + 1; b < i && !found; ++b)
                    {
                        if (_input[a] + _input[b] == _input[i])
                        {
                            found = true;
                        }
                    }
                }
                if (!found)
                {
                    index = i;
                    break;
                }
            }
            var value = _input[index];
            List<BigInteger> currentList;
            var first = -BigInteger.One;
            var last = -BigInteger.One;
            var exit = false;
            for (var a = 0; a < _input.Count && !exit; ++a)
            {
                currentList = new List<BigInteger> { _input[a] };
                var sum = _input[a];
                for (var b = a + 1; b < _input.Count && !exit; ++b)
                {
                    currentList.Add(_input[b]);
                    sum += _input[b];
                    if (sum == value)
                    {
                        first = currentList.Min();
                        last = currentList.Max();
                        exit = true;
                    }
                    if (sum > value)
                    {
                        break;
                    }
                }
            }

            return (first + last).ToString();
        }
    }
}
