﻿using adventofcode.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace adventofcode.AOC2020
{
    public class Day13 : Day
    {
        //Globals
        private readonly int _arrival;

        private readonly List<int> _input;
        private readonly List<string> _cleanInput;

        //Constructor
        public Day13()
        {
            //Init globals
            Part = 1;

            //Setup file
            var file = ToolsFile.ReadFile(DataPath + @"2020\day13.txt");

            //Test case
            var test1 = @"939
7,13,x,x,59,x,31,19";

            var chosenInput = file;
            _arrival = int.Parse(chosenInput.Split(Environment.NewLine.ToArray()).Where(x => !string.IsNullOrWhiteSpace(x)).First());
            _input = chosenInput.Split(Environment.NewLine.ToArray()).Where(x => !string.IsNullOrWhiteSpace(x)).Skip(1).First().Split(',').Where(x => x != "x").Select(x => int.Parse(x)).ToList();
            _cleanInput = chosenInput.Split(Environment.NewLine.ToArray()).Where(x => !string.IsNullOrWhiteSpace(x)).Skip(1).First().Split(',').ToList();

            //Do Pre-stuff
        }

        protected override string Part1()
        {
            return _input.Select(x => (id: x, time: x - _arrival % x)).OrderBy(x => x.time).Select(x => x.id * x.time).First().ToString();
        }

        protected override string Part2()
        {
            var idList = new List<(BigInteger index, BigInteger id)>();
            for (var i = 0; i < _cleanInput.Count; ++i)
            {
                if (_cleanInput[i] != "x")
                {
                    idList.Add((new BigInteger(i), BigInteger.Parse(_cleanInput[i])));
                }
            }
            var modList = idList.Select(x => (a: (x.id - x.index) % x.id, n: x.id)).ToList();

            var current = (a: modList.First().a, n: modList.First().n);
            modList.Remove(current);
            while (modList.Any())
            {
                var next = (a: modList.First().a, n: modList.First().n);
                modList.Remove(next);

                var c = Tools.ExtendedEuclid(current.n, next.n);

                var mult = current.n * next.n;

                var v = (((current.a * c.b * next.n + next.a * c.a * current.n) % (mult)) + mult) % mult;
                current = (v, mult);
            }

            return current.a.ToString();
        }
    }
}
