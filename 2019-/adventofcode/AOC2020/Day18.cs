﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using adventofcode.Utilities;

namespace adventofcode.AOC2020
{
    public class Day18 : Day
    {
        //Globals
        private readonly List<string> _inputPart1;
        private readonly List<string> _inputPart2;

        //Constructor
        public Day18()
        {
            //Init globals
            Part = 2;

            //Setup file
            var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2020\day18.txt");
            //var file = ToolsFile.ReadFile(DataPath + @"2020\day18.txt");

            //Test case
            var test1NoFix = @"2 * 3 + (4 * 5)".Replace(" ", string.Empty);
            var test1 = FixLine(@"2 * 3 + (4 * 5)".Replace(" ", string.Empty));

            var test2NoFix = @"5 + (8 * 3 + 9 + 3 * 4 * 3)".Replace(" ", string.Empty);
            var test2 = FixLine(@"5 + (8 * 3 + 9 + 3 * 4 * 3)".Replace(" ", string.Empty));

            var test3NoFix = @"5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))".Replace(" ", string.Empty);
            var test3 = FixLine(@"5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))".Replace(" ", string.Empty));

            var test4NoFix = @"((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2".Replace(" ", string.Empty);
            var test4 = FixLine(@"((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2".Replace(" ", string.Empty));

            var chosenInput = file;

            _inputPart1 = chosenInput.Replace(" ", string.Empty).Replace("\r", "\n").Split('\n').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
            _inputPart2 = chosenInput.Replace(" ", string.Empty).Replace("\r", "\n").Split('\n').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

            for (var i = 0; i < _inputPart2.Count; ++i)
            {
                _inputPart2[i] = FixLine(_inputPart2[i]);
            }

            //Do Pre-stuff
        }

        private static string FixLine(string line)
        {
            var level = 0;
            for (var i = 0; i < line.Length; ++i)
            {
                if (line[i] == '(') level++;
                if (line[i] == ')') level--;
                if (line[i] != '+') continue;

                var j = i + 1;
                var levelBefore = level;
                while (j < line.Length && (level != levelBefore || (line[j] != ')' && line[j] != '*' && line[j] != '+')))
                {
                    if (line[j] == '(') level++;
                    if (line[j] == ')') level--;
                    j++;
                }
                line = line.Insert(j, ")");

                levelBefore = level;
                j = i - 1;
                while (j >= 0 && (level != levelBefore || (line[j] != '(' && line[j] != '*' && line[j] != '+')))
                {
                    if (line[j] == '(') level--;
                    if (line[j] == ')') level++;
                    j--;
                }

                line = line.Insert(j + 1, "(");
                level++;
                i++;
            }

            return line;
        }

        private static BigInteger ParseLine(ref string line)
        {
            var value = BigInteger.Zero;
            var op = ' ';
            for (var i = 0; i < line.Length;)
            {
                BigInteger posValue;
                if (line[i] == '*')
                {
                    op = line[i];
                    i++;
                }
                else if (line[i] == '+')
                {
                    op = line[i];
                    i++;
                }
                else if (line[i] == '(')
                {
                    var a = new string(line.Skip(i + 1).ToArray());
                    posValue = ParseLine(ref a);
                    switch (op)
                    {
                        case '*':
                            value *= posValue;
                            break;
                        case '+':
                            value += posValue;
                            break;
                        default:
                            value = posValue;
                            break;
                    }
                    i += a.Length + 2;
                }
                else if (line[i] == ')')
                {
                    line = line.Substring(0, i);
                    return value;
                }
                else if (line[i] >= '0' && line[i] <= '9')
                {
                    var a = new string(line.Skip(i).TakeWhile(c => (c >= '0' && c <= '9')).ToArray());
                    posValue = BigInteger.Parse(a);
                    switch (op)
                    {
                        case '*':
                            value *= posValue;
                            break;
                        case '+':
                            value += posValue;
                            break;
                        default:
                            value = posValue;
                            break;
                    }
                    i += a.Length;
                }
                else
                {
                    i++;
                }
            }

            return value;
        }

        public static BigInteger CalculateSum(string line)
        {
            var l = line;
            return ParseLine(ref l);
        }

        public BigInteger CalculateSum(List<string> lines)
        {
            var totalSum = BigInteger.Zero;
            foreach (var line in lines)
            {
                totalSum += CalculateSum(line);
            }
            return totalSum;
        }

        protected override string Part1()
        {
            return CalculateSum(_inputPart1).ToString();
        }

        protected override string Part2()
        {
            return CalculateSum(_inputPart2).ToString();
        }
    }
}
