﻿using adventofcode.Utilities;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace adventofcode.AOC2020
{
    public class Day21 : Day
    {
        //Globals
        private readonly List<string> _input;

        private readonly List<string> _ingredients;

        private readonly Dictionary<string, HashSet<string>> _ingredientsInAllergen;
        //Constructor
        public Day21()
        {
            //Init globals
            Part = 2;

            _ingredients = new List<string>();
            _ingredientsInAllergen = new Dictionary<string, HashSet<string>>();

            //Setup file
            //var file = ToolsFile.ReadFile(@"C:\THEGANGGIT\adventofcode\2019-\data\2020\day21.txt");
            var file = ToolsFile.ReadFile(DataPath + @"2020\day21.txt");

            //Test case
            var test1 = @"mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)";

            var chosenInput = file;
            _input = chosenInput.Replace("\r", "\n").Split('\n').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
            foreach (var line in _input)
            {
                var parts = Regex.Split(line, "\\(contains ");
                parts[0] = parts[0].Trim();
                parts[1] = new string(parts[1].TakeWhile(x => x != ')').ToArray());
                var ingredients = parts[0].Split(' ').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                var allergens = Regex.Split(parts[1], ", ").ToHashSet();
                foreach (var allergen in allergens)
                {
                    if (_ingredientsInAllergen.ContainsKey(allergen))
                    {
                        _ingredientsInAllergen[allergen].IntersectWith(ingredients);
                    }
                    else
                    {
                        _ingredientsInAllergen.Add(allergen, new HashSet<string>(ingredients));
                    }
                }
                _ingredients.AddRange(ingredients);
            }

            while (_ingredientsInAllergen.Values.Any(x => x.Count > 1))
            {
                foreach (var kpi in _ingredientsInAllergen)
                {
                    if (kpi.Value.Count > 1) continue;
                    foreach (var kpj in _ingredientsInAllergen)
                    {
                        if (kpi.Key == kpj.Key) continue;
                        kpj.Value.ExceptWith(kpi.Value);
                    }
                }
            }
            //Do Pre-stuff
        }

        protected override string Part1()
        {
            var nonAllergenIngredients = _ingredients.Where(x => !_ingredientsInAllergen.Values.Any(y => y.Contains(x))).ToList();

            return nonAllergenIngredients.Count.ToString();
        }

        protected override string Part2()
        {
            var ingredients = _ingredientsInAllergen.OrderBy(x => x.Key).Select(x => x.Value.First()).ToList();
            var r = string.Join(",", ingredients);
            return r;
        }
    }
}
