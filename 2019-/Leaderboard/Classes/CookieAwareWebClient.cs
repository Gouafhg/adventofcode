﻿using System;
using System.Net;

namespace Leaderboard.Classes
{
    public class CookieAwareWebClient : WebClient
    {
        public CookieContainer CookieContainer { get; set; }

        public CookieAwareWebClient()
          : base()
        {
            CookieContainer = new CookieContainer();
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest request = base.GetWebRequest(address);

            HttpWebRequest webRequest = request as HttpWebRequest;
            if (webRequest != null)
            {
                webRequest.CookieContainer = CookieContainer;
            }

            return request;
        }

        internal void AddCookie(string name, string value, string path, string domain)
        {
            var cookie = new Cookie(name, value, path, domain);
            CookieContainer.Add(cookie);
        }
    }
}
