﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Leaderboard.Classes;
using Newtonsoft.Json;

namespace Leaderboard
{
    public class TimeStamp
    {
        public long get_star_ts { get; set; }
        public DateTime StarTime => DateTimeOffset.FromUnixTimeSeconds(get_star_ts).DateTime.AddHours(1);
    }

    public class Member
    {
        public string id { get; set; }
        public string last_star_ts { get; set; }
        public string name { get; set; }
        public int stars { get; set; }
        public int local_score { get; set; }
        public int global_score { get; set; }
        public Dictionary<string, Dictionary<string, TimeStamp>> completion_day_level { get; set; }
    }

    public class LeaderBoard
    {
        public string owner_id;
        public string @event;
        public Dictionary<string, Member> members;
    }

    internal class Program
    {
        private static readonly string BaseDirectory = AppContext.BaseDirectory;
        public static string DataPathHemma = string.Join("\\", BaseDirectory.Split('\\').Take(BaseDirectory.Split('\\').Count() - 5)) + @"\data\";
        public static string DataPath = string.Join("\\", BaseDirectory.Split('\\').Take(BaseDirectory.Split('\\').Count() - 4)) + @"\data\";

        private static void Main(string[] args)
        {
            Console.Write("Vilken leaderboard? (1 (Jobb) / 2 (Demoscene)):  ");
            var league = Console.ReadLine().Trim();
            league = string.IsNullOrWhiteSpace(league) ? "1" : league;

            const string jobbLeaderboard = "373568";
            const string demoLeaderboard = "24420";

            if (!int.TryParse(league, out var leagueEntered) || (leagueEntered != 1 && leagueEntered != 2))
            {
                Console.WriteLine($"Bara 1 eller 2, men: {league}");
                Console.ReadKey();
                return;
            }

            league = leagueEntered == 1 ? jobbLeaderboard : demoLeaderboard;

            var url = $"https://adventofcode.com/2021/leaderboard/private/view/{league}.json";

            var cookieFilePath = @"D:\adventofcode-com-cookie.txt";
            var cookies = File.ReadAllLines(cookieFilePath).SkipWhile(x => string.IsNullOrWhiteSpace(x) || x.StartsWith("#"));
            var cookieData = cookies.First().Split('\t');
            (string name, string value, string path, string domain) cookie = (cookieData[5], cookieData[6], cookieData[2], cookieData[0]);

            string json;
            using (var wc = new CookieAwareWebClient())
            {
                wc.AddCookie(cookie.name, cookie.value, cookie.path, cookie.domain);
                wc.Encoding = System.Text.Encoding.UTF8;
                json = wc.DownloadString(url);
            }

            //var json = File.ReadAllText(DataPath + @"2019\373568 20211214 0017.json"); //Jobbet 2019
            //var json = File.ReadAllText(DataPath + @"2021\373568 20211213 2335.json"); //Jobbet 20211213
            //var json = File.ReadAllText(DataPath + @"2021\24420 20211216 1049.json"); //Demoscene 20211216
            var leaderBoard = JsonConvert.DeserializeObject<LeaderBoard>(json);

            var members = leaderBoard.members.Values.OrderByDescending(x => x.local_score).ThenBy(x => x.stars).ToList();
            foreach (var member in members)
            {
                var memberString = $"{member.name} - {member.local_score}";
                Console.WriteLine(memberString);
            }
            Console.WriteLine();
            Console.WriteLine();

            for (var dayNumber = 1; dayNumber <= 25; ++dayNumber)
            {
                var dayString = dayNumber.ToString();

                var twoStar = members
                    .Where(x =>
                        x.completion_day_level.ContainsKey(dayString) &&
                        x.completion_day_level[dayString].ContainsKey("2"))
                    .OrderBy(x => x.completion_day_level[dayString]["2"].get_star_ts)
                    .ToList();

                var oneStar = members
                    .Where(x => 
                        x.completion_day_level.ContainsKey(dayString) && !twoStar.Contains(x))
                    .OrderBy(x => x.completion_day_level[dayString]["1"].get_star_ts)
                    .ToList();

                if (oneStar.Any() || twoStar.Any())
                {
                    Console.WriteLine("Day " + dayString);
                    foreach (var member in twoStar)
                    {
                        var name = member.name;
                        var star1Time = member.completion_day_level[dayString]["1"].StarTime;
                        var star2Time = member.completion_day_level[dayString]["2"].StarTime;
                        var diff = (star2Time - star1Time);

                        var diffString = diff.ToString("%h\\:mm\\:ss");
                        var memberString =
                            $"{name,-20}{star1Time,-10:dd:HH:mm:ss}{star2Time,-20:dd:HH:mm:ss}{diffString}";
                        Console.WriteLine(memberString);
                    }

                    foreach (var member in oneStar)
                    {
                        var name = member.name;
                        var star1Time = member.completion_day_level[dayString]["1"].StarTime;
                        var memberString =
                            $"{name,-20}{star1Time,-10:dd:HH:mm:ss}-";
                        Console.WriteLine(memberString);
                    }

                    Console.WriteLine();
                    Console.WriteLine();
                }
            }

            Console.ReadKey();
        }
    }
}
