﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace _21
{
    internal class InstructionList
    {
        public int posRegIndex;
        public List<Instruction> instrList;
        public uint[] regBackup;

        public InstructionList()
        {
            regBackup = new uint[Program.reg.Length];
            instrList = new List<Instruction>();
        }

        public void Execute()
        {
            Program.reg[posRegIndex] = 0;
            //Program.CopyMem(ref Program.reg, ref regBackup);
            while (true)
            {
                try
                {
                    if (Program.reg[posRegIndex] == 18)
                    {
                        //Program.reg[1] = (Program.reg[2]) / 256;
                        //Program.reg[2] = Program.reg[3];
                    }
                    if (Program.reg[posRegIndex] == 12)
                    {
                        //Program.reg[5] = Program.reg[3];
                    }
                    //Program.CopyMem(ref Program.reg, ref regBackup);
                    //Console.WriteLine("Regs before: ".PadRight(13) + Program.reg[0].ToString().PadRight(10) + Program.reg[1].ToString().PadRight(10) + Program.reg[2].ToString().PadRight(10) + Program.reg[3].ToString().PadRight(10) + Program.reg[4].ToString().PadRight(10) + Program.reg[5].ToString().PadRight(10));
                    //Console.WriteLine(instrList[Program.reg[posRegIndex]].instr.ToString().PadRight(13) + instrList[Program.reg[posRegIndex]].a.ToString().PadRight(10) + instrList[Program.reg[posRegIndex]].b.ToString().PadRight(10) + instrList[Program.reg[posRegIndex]].c.ToString().PadRight(10));
                    instrList[(int)Program.reg[posRegIndex]].Execute();
                    //Console.WriteLine("Regs after: ".PadRight(13) + Program.reg[0].ToString().PadRight(10) + Program.reg[1].ToString().PadRight(10) + Program.reg[2].ToString().PadRight(10) + Program.reg[3].ToString().PadRight(10) + Program.reg[4].ToString().PadRight(10) + Program.reg[5].ToString().PadRight(10));
                    //Console.WriteLine();
                    //Thread.Sleep(20);

                    if (Program.reg[posRegIndex] == 26)
                    {
                    }
                    if (Program.reg[posRegIndex] == 28)
                    {
                        /*
                        Console.Clear();
                        Console.WriteLine("reg 2:" + Program.reg[2].ToString().PadRight(16) + Convert.ToString(Program.reg[2], 2).PadLeft(24, '0'));
                        Console.WriteLine("reg 4:" + Program.reg[4].ToString().PadRight(16) + Convert.ToString(Program.reg[4], 2).PadLeft(24, '0'));
                        //Thread.Sleep(25);
                        Console.ReadKey();
                        //Thread.Sleep(100);
                        */
                        if (Program.reg4.Contains(Program.reg[4]))
                        {
                            Console.Clear();
                            Console.WriteLine("reg 2:" + Program.reg[2].ToString().PadRight(16) + Convert.ToString(Program.reg[2], 2).PadLeft(24, '0'));
                            Console.WriteLine("reg 4:" + Program.reg[4].ToString().PadRight(16) + Convert.ToString(Program.reg[4], 2).PadLeft(24, '0'));
                            //Thread.Sleep(25);

                            Console.WriteLine();
                            Console.WriteLine("Last was " + Program.reg4[Program.reg4.Count - 1]);
                            Console.ReadKey();
                            //Thread.Sleep(100);
                        }
                        else
                        {
                            Program.reg4.Add(Program.reg[4]);
                            Console.WriteLine(Program.reg[4].ToString().PadRight(15) + ", " + Program.reg4.Count);
                        }
                    }
                    //Console.ReadKey();
                    Program.reg[posRegIndex]++;
                }
                catch
                {
                    //Program.CopyMem(ref regBackup, ref Program.reg);
                    break;
                }
            }
        }
    }
}
