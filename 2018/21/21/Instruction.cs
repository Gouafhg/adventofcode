﻿namespace _21
{
    internal class Instruction
    {
        public InstructionSet instr;
        public int a, b, c;

        public Instruction()
        {
            instr = (InstructionSet)0;
            a = b = c = 0;
        }

        public Instruction(InstructionSet i, int a, int b, int c)
        {
            this.instr = i;
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public void Execute()
        {
            Program.ExecuteInstruction(instr, a, b, c);
        }
    }
}
