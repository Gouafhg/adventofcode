﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _21
{
    enum InstructionSet { addr, addi, mulr, muli, banr, bani, borr, bori, setr, seti, gtir, gtri, gtrr, eqir, eqri, eqrr }

    class Program
    {
        public static int[] reg;
        public static List<int> reg4;

        static Program()
        {
            reg = new int[6];
            reg4 = new List<int>();
        }

        public static void Reset()
        {
            for (int i = 0; i < Program.reg.Length; ++i) Program.reg[i] = 0;
        }

        public static void CopyMem(ref int[] source, ref int[] destination)
        {
            for (int i = 0; i < source.Length; ++i) destination[i] = source[i];
        }

        public static void ExecuteInstruction(InstructionSet instruction, int a, int b, int c)
        {
            switch (instruction)
            {
                case InstructionSet.addr:
                    reg[c] = reg[a] + reg[b];
                    break;
                case InstructionSet.addi:
                    reg[c] = reg[a] + b;
                    break;
                case InstructionSet.mulr:
                    reg[c] = reg[a] * reg[b];
                    break;
                case InstructionSet.muli:
                    reg[c] = reg[a] * b;
                    break;
                case InstructionSet.banr:
                    reg[c] = reg[a] & reg[b];
                    break;
                case InstructionSet.bani:
                    reg[c] = reg[a] & b;
                    break;
                case InstructionSet.borr:
                    reg[c] = reg[a] | reg[b];
                    break;
                case InstructionSet.bori:
                    reg[c] = reg[a] | b;
                    break;
                case InstructionSet.setr:
                    reg[c] = reg[a];
                    break;
                case InstructionSet.seti:
                    reg[c] = a;
                    break;
                case InstructionSet.gtir:
                    reg[c] = a > reg[b] ? 1 : 0;
                    break;
                case InstructionSet.gtri:
                    reg[c] = reg[a] > b ? 1 : 0;
                    break;
                case InstructionSet.gtrr:
                    reg[c] = reg[a] > reg[b] ? 1 : 0;
                    break;
                case InstructionSet.eqir:
                    reg[c] = a == reg[b] ? 1 : 0;
                    break;
                case InstructionSet.eqri:
                    reg[c] = reg[a] == b ? 1 : 0;
                    break;
                case InstructionSet.eqrr:
                    reg[c] = reg[a] == reg[b] ? 1 : 0;
                    break;
            }
        }

        static void Main(string[] args)
        {
            /*
            var low = int.MaxValue;
            var r4 = 6152285;
            var r2 = 0;
            for (int i = 0; i < 256; ++i)
            {
                r2 = i;
                r4 = (((r4 + r2 % 255) & 16777215) * 65899) & 16777215;
                if (r4 > 0) low = Math.Min(r4, low);
                Console.WriteLine("[2]: " + r2.ToString().PadRight(10) + "[4]: " + r4);
                Console.ReadKey();
            }
            Console.WriteLine("Done");
            Console.WriteLine("Lowest: " + low);
            Console.ReadKey();
            return;
            */
            var input = "3!seti,123,0,4!bani,4,456,4!eqri,4,72,4!addr,4,3,3!seti,0,0,3!seti,0,9,4!bori,4,65536,2!seti,6152285,4,4!bani,2,255,1!addr,4,1,4!bani,4,16777215,4!muli,4,65899,4!bani,4,16777215,4!gtir,256,2,1!addr,1,3,3!addi,3,1,3!seti,27,4,3!seti,0,3,1!addi,1,1,5!muli,5,256,5!gtrr,5,2,5!addr,5,3,3!addi,3,1,3!seti,25,9,3!addi,1,1,1!seti,17,4,3!setr,1,9,2!seti,7,4,3!eqrr,4,0,1!addr,1,3,3!seti,5,6,3";


            var program = new InstructionList();
            {
                var rows = input.Split('!');
                program.posRegIndex = int.Parse(rows[0]);
                for (int i = 1; i < rows.Length; ++i)
                {
                    var v = rows[i].Split(',');
                    var instr = (InstructionSet)Enum.Parse(typeof(InstructionSet), v[0]);
                    var a = int.Parse(v[1]);
                    var b = int.Parse(v[2]);
                    var c = int.Parse(v[3]);
                    var instruction = new Instruction(instr, a, b, c);
                    program.instrList.Add(instruction);
                }
            }

            Reset();
            reg[0] = 0;
            program.Execute();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine(
                "Regs: ".PadRight(13) +
                reg[0].ToString().PadRight(10) +
                reg[1].ToString().PadRight(10) +
                reg[2].ToString().PadRight(10) +
                reg[3].ToString().PadRight(10) +
                reg[4].ToString().PadRight(10) +
                reg[5].ToString().PadRight(10));
            Console.ReadKey();
        }
    }
}
