﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11
{

    class Program
    {
        static int[,,] sqValue;
        static FuelCell[,] fuelGrid;
        static readonly bool test1 = false;
        static readonly bool test2 = false;
        static readonly bool test3 = false;
        public static readonly int serialNumber = test1 ? 8 : test2 ? 18 : test3 ? 42 : 2694;

        /*
        static int FindGreatestDivisor(int N)
        {
            var d = 1;
            var sqrt = (int)Math.Sqrt(N);
            bool even = N % 2 == 0;
            if (even)
            {
                d = 2;
            }
            else if (N > 1)
            {
                d = 3;
                while (d != sqrt && N % d != 0) d += 2;
            }
            if (d != N && N % d == 0)
            {
                return N / d;
            }
            else
            {
                return N;
            }
        }
        */

        static Program()
        {
            fuelGrid = new FuelCell[300, 300];
            sqValue = new int[300, 300, 301];
        }

        static bool isPrime(int N)
        {
            return false;
        }

        static void Main(string[] args)
        {
            for (int size = 0; size <= 300; ++size)
            {
                for (int y = 0; y < fuelGrid.GetLength(1); ++y)
                {
                    for (int x = 0; x < fuelGrid.GetLength(0); ++x)
                    {
                        var cell = new FuelCell(x, y);
                        cell.CalculateValue();
                        fuelGrid[x, y] = cell;

                        //Just init
                        sqValue[x, y, size] = 0;
                    }
                }
            }

            var maxAtX = 1;
            var maxAtY = 1;
            var maxAtSize = 1;
            var maxValue = sqValue[1, 1, 1];
            for (int size = 1; size <= 300; ++size)
            {
                Console.WriteLine("Size: " + size);

                for (int y = 0; y < fuelGrid.GetLength(1) - size; ++y)
                {
                    for (int x = 0; x < fuelGrid.GetLength(0) - size; ++x)
                    {
                        int sum;
                        if (size < 3)
                        {
                            sum = 0;
                            for (int xx = 0; xx < size; ++xx)
                            {
                                for (int yy = 0; yy < size; ++yy)
                                {
                                    sum += fuelGrid[x + xx, y + yy].value;
                                }
                            }
                        }
                        else
                        {
                            sum =  sqValue[x    , y    , size - 1];
                            for (int xx = 0; xx < size; ++xx)
                            {
                                sum += fuelGrid[x + xx, y + size - 1].value;
                            }
                            for (int yy = 0; yy < size - 1;  ++yy)
                            {
                                sum += fuelGrid[x + size - 1, y + yy].value;
                            }
                        }

                        sqValue[x, y, size] = sum;
                        if (sqValue[x, y, size] > maxValue)
                        {
                            maxAtX = x;
                            maxAtY = y;
                            maxAtSize = size;
                            maxValue = sqValue[maxAtX, maxAtY, maxAtSize];
                        }
                    }
                }
            }

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();

            if (test1)
            {
                Console.WriteLine("Value of (3,5) is " + fuelGrid[3, 5].value);
            }
            else if (test2)
            {
                for (int y = 0; y < 5; ++y)
                {
                    for (int x = 0; x < 5; ++x)
                    {
                        Console.Write(fuelGrid[32 + x, 44 + y].value + " ");
                    }
                    Console.WriteLine();
                }
                Console.WriteLine("Value of (33,45,3) @18 is " + sqValue[33, 45, 3]);
                Console.WriteLine("Value of (33,45,4) @18 is " + sqValue[33, 45, 4]);
                Console.WriteLine("Value of (32,44,5) @18 is " + sqValue[32, 44, 5]);
            }
            else if (test3)
            {
                for (int y = 0; y < 5; ++y)
                {
                    for (int x = 0; x < 5; ++x)
                    {
                        Console.Write(fuelGrid[20 + x, 60 + y].value + " ");
                    }
                    Console.WriteLine();
                }
                Console.WriteLine("Value of (21,61,3) @42 is " + sqValue[21, 61, 3]);
                Console.WriteLine("Value of (21,61,4) @42 is " + sqValue[21, 61, 3]);
                Console.WriteLine("Value of (20,60,5) @42 is " + sqValue[20, 60, 3]);
            }
            else
            {
                Console.WriteLine("Max value: " + maxValue);
                Console.WriteLine("Reached at (" + maxAtX + "," + maxAtY + ") when the squaresize is " + maxAtSize);
            }
            Console.ReadKey();
        }
    }

    class FuelCell
    {
        int x, y;
        public int value;

        public FuelCell()
        {
            x = y = 0;
            value = 0;
        }

        public FuelCell(int x, int y)
        {
            this.x = x;
            this.y = y;
            value = 0;
        }

        public int CalculateValue()
        {
            var rackID = x + 10;
            value = rackID * y;
            value += Program.serialNumber;
            value *= rackID;
            if (value > 99)
            {
                var asString = value.ToString();
                value = int.Parse(asString[asString.Length - 3] + string.Empty);
                value -= 5;
            }
            else
            {
                value = 0;
            }
            return value;
        }
    }
}
