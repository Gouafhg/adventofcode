﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20
{
    class Room
    {
        public bool inPath;
        public bool start;
        public bool goal;
        public Room North;
        public Room West;
        public Room South;
        public Room East;

        public int x, y;
        public HashSet<string> branchesEvaluated;

        public Room()
        {
            x = y = 0;
            branchesEvaluated = new HashSet<string>();
            inPath = false;
            start = goal = false;
            North = West = South = East = null;
        }

        public Room(int x, int y)
        {
            this.x = x;
            this.y = y;
            inPath = false;
            branchesEvaluated = new HashSet<string>();
            start = goal = false;
            North = West = South = East = null;
        }

        public override string ToString()
        {
            return "(" + x + "," + y + ")";
        }

        static int ct = 0;
        public void Print()
        {
            Console.SetCursorPosition(x * 2, y * 2);
            Console.Write(North == null ? "###" : "# #");
            Console.SetCursorPosition(x * 2, y * 2 + 1);
            Console.Write(West == null ? "# " : "  ");
            Console.Write(East == null ? "#" : " ");
            Console.SetCursorPosition(x * 2, y * 2 + 2);
            Console.Write(South == null ? "###" : "# #");

            if (inPath)
            {
                Console.SetCursorPosition(x * 2 + 1, y * 2 + 1);
                Console.Write(".");
            }
            if (start)
            {
                Console.SetCursorPosition(x * 2 + 1, y * 2 + 1);
                Console.Write("X");
            }
            else if (goal)
            {
                Console.SetCursorPosition(x * 2 + 1, y * 2 + 1);
                Console.Write("G");
            }
        }
    }
}
