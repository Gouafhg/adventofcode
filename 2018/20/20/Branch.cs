﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20
{
    class Branch
    {
        public Room Start;
        public string Rest;

        public Branch()
        {
            Rest = "";
            Start = null;
        }

        public Branch(Room start, string rest)
        {
            this.Start = start;
            this.Rest = rest;
        }

        public void Process()
        {
            if (Start.branchesEvaluated.Contains(Rest))
            {
                return;
            }

            Start.branchesEvaluated.Add(Rest);
            var currentRoom = Start;

            for (int i = 0; i < Rest.Length; ++i)
            {
                var c = Rest[i];
                switch (c)
                {
                    case '$':
                        //currentRoom.goal = true;
                        break;
                    case 'N':
                        currentRoom.North = Program.rooms.Any(r => r.x == currentRoom.x && r.y == currentRoom.y - 1) ? Program.rooms.First(r => r.x == currentRoom.x && r.y == currentRoom.y - 1) : Program.AddRoom(currentRoom.x, currentRoom.y - 1);
                        currentRoom.North.South = currentRoom;
                        currentRoom = currentRoom.North;
                        break;
                    case 'W':
                        currentRoom.West = Program.rooms.Any(r => r.x == currentRoom.x - 1 && r.y == currentRoom.y) ? Program.rooms.First(r => r.x == currentRoom.x - 1 && r.y == currentRoom.y) : Program.AddRoom(currentRoom.x - 1, currentRoom.y);
                        currentRoom.West.East = currentRoom;
                        currentRoom = currentRoom.West;
                        break;
                    case 'S':
                        currentRoom.South = Program.rooms.Any(r => r.x == currentRoom.x && r.y == currentRoom.y + 1) ? Program.rooms.First(r => r.x == currentRoom.x && r.y == currentRoom.y + 1) : Program.AddRoom(currentRoom.x, currentRoom.y + 1);
                        currentRoom.South.North = currentRoom;
                        currentRoom = currentRoom.South;
                        break;
                    case 'E':
                        currentRoom.East = Program.rooms.Any(r => r.x == currentRoom.x + 1 && r.y == currentRoom.y) ? Program.rooms.First(r => r.x == currentRoom.x + 1 && r.y == currentRoom.y) : Program.AddRoom(currentRoom.x + 1, currentRoom.y);
                        currentRoom.East.West = currentRoom;
                        currentRoom = currentRoom.East;
                        break;
                    case '(':
                        var after = Rest.Substring(i, Rest.Length - i);
                        var pCount = 0;
                        int continuationIndex;
                        var divisions = new List<int>();
                        for (continuationIndex = 0; continuationIndex < after.Length; ++continuationIndex)
                        {
                            if (after[continuationIndex] == '(') pCount++;
                            if (after[continuationIndex] == ')') pCount--;
                            if (pCount == 1 && after[continuationIndex] == '|') divisions.Add(continuationIndex);
                            if (pCount == 0) break;
                        }
                        var branchDivision = after.Substring(1, continuationIndex - 1);
                        var branchList = new List<string>();
                        var startIndex = 0;
                        foreach (var j in divisions)
                        {
                            branchList.Add(branchDivision.Substring(startIndex, j - startIndex - 1));
                            startIndex = j;
                        }
                        branchList.Add(branchDivision.Substring(startIndex, branchDivision.Length - startIndex));
                        //var b1 = branchDivision.Substring(0, divisionindex - 1);
                        //var b2 = branchDivision.Substring(divisionindex, branchDivision.Length - divisionindex);
                        var end = after.Substring(continuationIndex + 1, after.Length - continuationIndex - 1);
                        foreach (var b in branchList)
                        {
                            Program.branches.Push(new Branch(currentRoom, b + end));
                        }
                        //var first = b1 + end;
                        //var second = b2 + end;
                        //Program.branches.Push(new Branch(currentRoom, first));
                        //Program.branches.Push(new Branch(currentRoom, second));
                        return;
                }
            }
        }
    }
}
