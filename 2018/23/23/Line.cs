﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23
{
    public class Line
    {
        public Vector3i p;
        public Vector3i d;

        public Line() { p = d = Vector3i.Zero; }
        public Line(Vector3i p, Vector3i d)
        {
            this.p = p;
            this.d = d;
        }
    }
}
