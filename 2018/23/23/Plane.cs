﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23
{
    public class Plane
    {
        public Vector3i n;
        public int d;

        public Plane()
        {
            n = Vector3i.Zero;
            d = 0;
        }

        public Plane(Vector3i a, Vector3i b, Vector3i c)
        {
            FromPoints(a, b, c);
        }

        private void FromPoints(Vector3i a, Vector3i b, Vector3i c)
        {
            var v1 = a - b;
            var v2 = c - b;
            n = Vector3i.CrossProduct(v1, v2);
        }

        public Line IntersectPlane(Plane b)
        {
            if (Vector3i.CrossProduct(n, b.n) == Vector3i.Zero) return null;

            Line l = new Line();
            l.d = Vector3i.CrossProduct(n, b.n);
            var x = (n.y * b.d - b.n.y * d) / (n.x * b.n.y - b.n.x * n.y);
            var y = (b.n.x * d - n.x * b.d) / (n.x * b.n.y - b.n.x * n.y);
            l.p = new Vector3i(x, y, 0);
            return l;
        }
    }
}
