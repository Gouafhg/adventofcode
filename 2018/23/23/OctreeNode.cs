﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23
{
    public class OctreeNode
    {
        public static Vector3i globalOffset;
        static OctreeNode()
        {
            globalOffset = Vector3i.Zero;
        }

        public OctreeNode parent;
        public int level;
        public Cube cube;
        public HashSet<NanoRobot> usedBots;
        public HashSet<NanoRobot> bots;
        public OctreeNode[] children;
        public bool built;
        public bool end;

        public OctreeNode()
        {
            parent = null;
            level = 0;
            cube = new Cube(Vector3i.Zero, 1);
            usedBots = new HashSet<NanoRobot>();
            bots = new HashSet<NanoRobot>();
            children = new OctreeNode[8];
            built = false;
            end = true;
        }

        public OctreeNode(OctreeNode parent, Cube cube)
        {
            this.parent = parent;
            level = parent == null ? 0 : parent.level + 1;
            this.cube = cube;
            usedBots = new HashSet<NanoRobot>();
            bots = new HashSet<NanoRobot>();
            children = new OctreeNode[8];
            built = false;
            end = true;
        }

        public override string ToString()
        {
            return "Level " + level + " at " + cube.ToString() + " with " + BotCount;
        }

        public void BuildChildren()
        {
             built = true;
            if (cube.s == 1) return;
            end = false;

            var start1x = cube.minX;
            var start2x = start1x + (cube.maxX - cube.minX) / 2 + 1;
            var start1y = cube.minY;
            var start2y = start1y + (cube.maxY - cube.minY) / 2 + 1;
            var start1z = cube.minZ;
            var start2z = start1z + (cube.maxZ - cube.minZ) / 2 + 1;

            children[0] = new OctreeNode(this, new Cube(new Vector3i(start1x, start1y, start1z), cube.s / 2));
            children[1] = new OctreeNode(this, new Cube(new Vector3i(start2x, start1y, start1z), cube.s / 2));
            children[2] = new OctreeNode(this, new Cube(new Vector3i(start1x, start2y, start1z), cube.s / 2));
            children[3] = new OctreeNode(this, new Cube(new Vector3i(start2x, start2y, start1z), cube.s / 2));
            children[4] = new OctreeNode(this, new Cube(new Vector3i(start1x, start1y, start2z), cube.s / 2));
            children[5] = new OctreeNode(this, new Cube(new Vector3i(start2x, start1y, start2z), cube.s / 2));
            children[6] = new OctreeNode(this, new Cube(new Vector3i(start1x, start2y, start2z), cube.s / 2));
            children[7] = new OctreeNode(this, new Cube(new Vector3i(start2x, start2y, start2z), cube.s / 2));
            foreach (var node in children)
            {
                foreach (var r in bots)
                {
                    if (r.CanSee(node.cube))
                        node.bots.Add(r);
                }
                foreach (var r in usedBots)
                {
                    if (r.CanSee(node.cube))
                        node.usedBots.Add(r);
                }
            }
        }

        public void OffsetChildren(Vector3i v)
        {
            cube.offset += v;
            if (!end) foreach (var c in children) c.OffsetChildren(v);
        }

        public void ResetBots()
        {
            bots.UnionWith(usedBots);
            usedBots.Clear();
            foreach (var c in children.Where(child => child != null)) c.ResetBots();
        }

        public void ClearBots(IEnumerable<NanoRobot> botsToClear)
        {
            usedBots.UnionWith(botsToClear);
            usedBots.IntersectWith(bots);
            bots.ExceptWith(usedBots);
            foreach (var c in children.Where(child => child != null)) c.ClearBots(botsToClear);
        }

        public int BotCount
        {
            get { return usedBots.Count + bots.Count; }
        }

        public int DistancetoZero
        {
            get
            {
                return Math.Abs(cube.offset.x + OctreeNode.globalOffset.x) + Math.Abs(cube.offset.y + OctreeNode.globalOffset.y) + Math.Abs(cube.offset.z + OctreeNode.globalOffset.z);
            }
        }
    }
}
