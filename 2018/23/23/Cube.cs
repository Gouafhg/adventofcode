﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23
{
    public class Cube
    {
        public Vector3i _offset;
        public Vector3i offset
        {
            get
            {
                return _offset;
            }
            set
            {
                _offset = value;
                minX = _offset.x;
                maxX = _offset.x + s - 1;
                minY = _offset.y;
                maxY = _offset.y + s - 1;
                minZ = _offset.z;
                maxZ = _offset.z + s - 1;
            }
        }
        public int s;

        public int minX, maxX, minY, maxY, minZ, maxZ;
        public Cube(Vector3i c, int s)
        {
            this.s = s;
            this.offset = c;
        }

        public override string ToString()
        {
            return offset.ToString() + " : " + s;
        }

        public int distance(Vector3i p)
        {
            if (p.x >= minX && p.x <= maxX && p.y >= minY && p.y <= maxY && p.z >= minZ && p.z <= maxZ)
            {
                return 0;
            }

            var dx = Math.Max(minX - p.x, Math.Max(0, p.x - maxX));
            var dy = Math.Max(minY - p.y, Math.Max(0, p.y - maxY));
            var dz = Math.Max(minZ - p.z, Math.Max(0, p.z - maxZ));
            return dx + dy + dz;
        }
    }
}
