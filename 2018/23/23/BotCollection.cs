﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23
{
    class BotCollection
    {
        public List<NanoRobot> bots;
        public BotCollection()
        {
            bots = new List<NanoRobot>();
        }

        public void ReadCollection(string input)
        {
            var dataArray = input.Split('!');
            foreach (var data in dataArray)
            {
                var values = data.Split(',').Select(s => int.Parse(s)).ToList();
                var bot = new NanoRobot(values[0], values[1], values[2], values[3]);
                bots.Add(bot);
            }
        }

        public List<NanoRobot> BotsReaching(Vector3i p)
        {
            return bots.FindAll(b => b.CanSee(p));
        }
    }
}
