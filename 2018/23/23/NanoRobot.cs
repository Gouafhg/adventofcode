﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23
{
    public class NanoRobot
    {
        public Vector3i center;
        public int signalRadius;
        //public List<Vector3i> corners;

        public NanoRobot()
        {
            //corners = new List<Vector3i>();
        }

        public NanoRobot(int x, int y, int z, int signalRadius)
        {
            center.x = x;
            center.y = y;
            center.z = z;
            this.signalRadius = signalRadius;
            /*
            corners = new List<Vector3i>();
            Vector3i a, b, c, d, e, f;
            a.x = center.x + signalRadius;
            a.y = center.y;
            a.z = center.z;
            corners.Add(a);
            b.x = center.x;
            b.y = center.y + signalRadius;
            b.z = center.z;
            corners.Add(b);
            c.x = center.x;
            c.y = center.y;
            c.z = center.z + signalRadius;
            corners.Add(c);
            d.x = center.x - signalRadius;
            d.y = center.y;
            d.z = center.z;
            corners.Add(a);
            e.x = center.x;
            e.y = center.y - signalRadius;
            e.z = center.z;
            corners.Add(b);
            f.x = center.x;
            f.y = center.y;
            f.z = center.z - signalRadius;
            corners.Add(c);
            */
        }

        public int Distance(NanoRobot a)
        {
            return center.Distance(a.center);
        }

        public int Distance(Cube c)
        {
            return c.distance(center);
        }

        public bool CanSee(Cube c)
        {
            return Distance(c) <= signalRadius;
        }

        public override string ToString()
        {
            return "(" + center.x + ", " + center.y + ", " + center.z + ")" + ": " + signalRadius;
        }

        public bool CanSee(Vector3i p)
        {
            return center.Distance(p) <= signalRadius;
        }
    }
}
