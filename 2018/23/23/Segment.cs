﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23
{
    public class Segment
    {
        public Vector3i start;
        public Vector3i end;

        public Segment() { start = end = Vector3i.Zero; }
        public Segment(Vector3i start, Vector3i end)
        {
            this.start = start;
            this.end = end;
        }

        public Vector3i Intersection(Plane p)
        {
            if (Vector3i.dotProduct(p.n, end - start) == 0)
            {
                throw new NotImplementedException();
            }
            var pointInPlane = p.n.z != 0 ? new Vector3i(0, 0, -p.d / p.n.z) : p.n.y != 0 ? new Vector3i(0, -p.d / p.n.y, 0) : new Vector3i(-p.d / p.n.x, 0, 0);
            var r = Vector3i.dotProduct(p.n, pointInPlane - start) / Vector3i.dotProduct(p.n, end - start);
            if (r > 1)
            {
                throw new NotImplementedException();
            }
            return start + r * (end - start);
        }
    }
}
