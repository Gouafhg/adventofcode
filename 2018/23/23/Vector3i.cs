﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23
{
    public struct Vector3i
    {
        public int x;
        public int y;
        public int z;

        public static readonly Vector3i Zero = new Vector3i(0, 0, 0);

        public Vector3i(int v)
        {
            x = y = z = v;
        }

        public Vector3i(int x, int y, int z)
        {
            this.x = x; this.y = y; this.z = z;
        }

        public static int Distance(Vector3i a, Vector3i b)
        {
            return Math.Abs(a.x - b.x) + Math.Abs(a.y - b.y) + Math.Abs(a.z - b.z);
        }

        public int Distance(Vector3i b)
        {
            return Vector3i.Distance(this, b);
        }

        public static int dotProduct(Vector3i a, Vector3i b)
        {
            return a.x * b.x + a.y * b.y + a.z * b.z;
        }

        public static Vector3i CrossProduct(Vector3i a, Vector3i b)
        {
            return new Vector3i(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x);
        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash = hash * 7 + x.GetHashCode();
            hash = hash * 7 + y.GetHashCode();
            hash = hash * 7 + z.GetHashCode();
            return hash;
        }

        public override bool Equals(object obj)
        {
            if (obj is Vector3i)
            {
                return (Vector3i)obj == this;
            }
            return false;
        }

        public static bool operator != (Vector3i a, Vector3i b)
        {
            return (a.x != b.x) || (a.y != b.y) || (a.z != b.z);
        }
        public static bool operator == (Vector3i a, Vector3i b)
        {
            return (a.x == b.x) && (a.y == b.y) && (a.z == b.z);
        }

        public static Vector3i operator + (Vector3i a, Vector3i b)
        {
            return new Vector3i(a.x + b.x, a.y + b.y, a.z + b.z);
        }
        public static Vector3i operator - (Vector3i a, Vector3i b)
        {
            return new Vector3i(a.x - b.x, a.y - b.y, a.z - b.z);
        }
        public static Vector3i operator *(float a, Vector3i b)
        {
            return new Vector3i((int)(a * b.x), (int)(a * b.y), (int)(a * b.z));
        }
        public static Vector3i operator *(Vector3i b, float a)
        {
            return new Vector3i((int)(a * b.x), (int)(a * b.y), (int)(a * b.z));
        }

        public override string ToString()
        {
            return "(" + x + ", " + y + ", " + z + ")";
        }

        public List<Vector3i> Neighbours()
        {
            var result = new List<Vector3i>();
            for (int i = -1; i < 2; ++i)
            {
                for (int j = -1; j < 2; ++j)
                {
                    for (int k = -1; k < 2; ++k)
                    {
                        if (i == 0 && j == 0 && k == 0) continue;
                        result.Add(new Vector3i(x + i, y + j, z + k));
                    }
                }
            }
            return result;
        }
    }
}
