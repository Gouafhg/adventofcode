﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23
{
    class Triangle
    {
        public Vector3i a, b, c;

        public Triangle(Vector3i a, Vector3i b, Vector3i c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public bool Intersects(Plane p)
        {
            var aS = Vector3i.dotProduct(p.n, a) + p.d;
            var bS = Vector3i.dotProduct(p.n, b) + p.d;
            var cS = Vector3i.dotProduct(p.n, c) + p.d;
            return (aS == 0 || bS == 0 || cS == 0 || Math.Sign(aS) == Math.Sign(bS)) || (Math.Sign(aS) == Math.Sign(cS)) || (Math.Sign(bS) == Math.Sign(cS));
        }

        public Segment Intersection(Plane p)
        {
            if (!Intersects(p)) return null;
            var aS = Vector3i.dotProduct(p.n, a) + p.d;
            var bS = Vector3i.dotProduct(p.n, b) + p.d;
            var cS = Vector3i.dotProduct(p.n, c) + p.d;
            if (aS == 0 || bS == 0 || bS == 0) return null; //point/Triangle
            Segment s1, s2;
            if (Math.Sign(aS) == Math.Sign(bS))
            {
                s1 = new Segment(a, c);
                s2 = new Segment(b, c);
            }
            else if (Math.Sign(aS) == Math.Sign(cS))
            {
                s1 = new Segment(a, b);
                s2 = new Segment(c, b);
            }
            else
            {
                s1 = new Segment(b, a);
                s2 = new Segment(c, a);
            }

            Vector3i p0 = s1.Intersection(p);
            Vector3i p1 = s2.Intersection(p);
            return new Segment(p0, p1);
        }

        public Segment Intersection(Triangle t)
        {
            Plane P2 = new Plane(t.a, t.b, t.c);
            if (!Intersects(P2)) throw new NotImplementedException();
            Plane P1 = new Plane(a, b, c);
            var s1 = Intersection(P2);
            var s2 = t.Intersection(P1);

            throw new NotFiniteNumberException();
        }
    }
}
