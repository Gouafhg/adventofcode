﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19
{
    enum InstructionSet { addr, addi, mulr, muli, banr, bani, borr, bori, setr, seti, gtir, gtri, gtrr, eqir, eqri, eqrr }

    class Program
    {
        public static int[] reg;

        static Program()
        {
            reg = new int[6];
        }

        public static void Reset()
        {
            for (int i = 0; i < Program.reg.Length; ++i) Program.reg[i] = 0;
        }

        public static void CopyMem(ref int[] source, ref int[] destination)
        {
            for (int i = 0; i < source.Length; ++i) destination[i] = source[i];
        }

        public static void ExecuteInstruction(InstructionSet instruction, int a, int b, int c)
        {
            switch (instruction)
            {
                case InstructionSet.addr:
                    reg[c] = reg[a] + reg[b];
                    break;
                case InstructionSet.addi:
                    reg[c] = reg[a] + b;
                    break;
                case InstructionSet.mulr:
                    reg[c] = reg[a] * reg[b];
                    break;
                case InstructionSet.muli:
                    reg[c] = reg[a] * b;
                    break;
                case InstructionSet.banr:
                    reg[c] = reg[a] & reg[b];
                    break;
                case InstructionSet.bani:
                    reg[c] = reg[a] & b;
                    break;
                case InstructionSet.borr:
                    reg[c] = reg[a] | reg[b];
                    break;
                case InstructionSet.bori:
                    reg[c] = reg[a] | b;
                    break;
                case InstructionSet.setr:
                    reg[c] = reg[a];
                    break;
                case InstructionSet.seti:
                    reg[c] = a;
                    break;
                case InstructionSet.gtir:
                    reg[c] = a > reg[b] ? 1 : 0;
                    break;
                case InstructionSet.gtri:
                    reg[c] = reg[a] > b ? 1 : 0;
                    break;
                case InstructionSet.gtrr:
                    reg[c] = reg[a] > reg[b] ? 1 : 0;
                    break;
                case InstructionSet.eqir:
                    reg[c] = a == reg[b] ? 1 : 0;
                    break;
                case InstructionSet.eqri:
                    reg[c] = reg[a] == b ? 1 : 0;
                    break;
                case InstructionSet.eqrr:
                    reg[c] = reg[a] == reg[b] ? 1 : 0;
                    break;
            }
        }

        static void Main(string[] args)
        {
            var inputTest = "0!seti,5,0,1!seti,6,0,2!addi,0,1,0!addr,1,2,3!setr,1,0,0!seti,8,0,4!seti,9,0,5";
            var inputReal = "1!addi,1,16,1!seti,1,4,5!seti,1,4,2!mulr,5,2,4!eqrr,4,3,4!addr,4,1,1!addi,1,1,1!addr,5,0,0!addi,2,1,2!gtrr,2,3,4!addr,1,4,1!seti,2,6,1!addi,5,1,5!gtrr,5,3,4!addr,4,1,1!seti,1,7,1!mulr,1,1,1!addi,3,2,3!mulr,3,3,3!mulr,1,3,3!muli,3,11,3!addi,4,3,4!mulr,4,1,4!addi,4,18,4!addr,3,4,3!addr,1,0,1!seti,0,7,1!setr,1,4,4!mulr,4,1,4!addr,1,4,4!mulr,1,4,4!muli,4,14,4!mulr,4,1,4!addr,3,4,3!seti,0,0,0!seti,0,1,1";

            var input = inputReal;

            var program = new InstructionList();
            {
                var rows = input.Split('!');
                program.posRegIndex = int.Parse(rows[0]);
                for (int i = 1; i < rows.Length; ++i)
                {
                    var v = rows[i].Split(',');
                    var instr = (InstructionSet)Enum.Parse(typeof(InstructionSet), v[0]);
                    var a = int.Parse(v[1]);
                    var b = int.Parse(v[2]);
                    var c = int.Parse(v[3]);
                    var instruction = new Instruction(instr, a, b, c);
                    program.instrList.Add(instruction);
                }
            }

            Reset();
            reg[0] = 1;
            program.Execute();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine(
                "Regs: ".PadRight(13) +
                reg[0].ToString().PadRight(10) +
                reg[1].ToString().PadRight(10) +
                reg[2].ToString().PadRight(10) +
                reg[3].ToString().PadRight(10) +
                reg[4].ToString().PadRight(10) +
                reg[5].ToString().PadRight(10));
            Console.ReadKey();
        }
    }

    class InstructionList
    {
        public int posRegIndex;
        public List<Instruction> instrList;
        public int[] regBackup;

        public InstructionList()
        {
            regBackup = new int[Program.reg.Length];
            instrList = new List<Instruction>();
        }

        public void Execute()
        {
            Program.reg[posRegIndex] = 0;
            //Program.CopyMem(ref Program.reg, ref regBackup);
            while (true)
            {
                try
                {
                    if (Program.reg[posRegIndex] == 3)
                    {
                        //Program.reg[2] = Program.reg[3];
                    }
                    if (Program.reg[posRegIndex] == 12)
                    {
                        //Program.reg[5] = Program.reg[3];
                    }
                    //Program.CopyMem(ref Program.reg, ref regBackup);
                    Console.WriteLine(
                        "Regs before: ".PadRight(13) +
                        Program.reg[0].ToString().PadRight(10) +
                        Program.reg[1].ToString().PadRight(10) +
                        Program.reg[2].ToString().PadRight(10) +
                        Program.reg[3].ToString().PadRight(10) +
                        Program.reg[4].ToString().PadRight(10) +
                        Program.reg[5].ToString().PadRight(10));
                    Console.WriteLine(instrList[Program.reg[posRegIndex]].instr.ToString().PadRight(13) + instrList[Program.reg[posRegIndex]].a.ToString().PadRight(10) + instrList[Program.reg[posRegIndex]].b.ToString().PadRight(10) + instrList[Program.reg[posRegIndex]].c.ToString().PadRight(10));
                    instrList[Program.reg[posRegIndex]].Execute();
                    Console.WriteLine(
                        "Regs after: ".PadRight(13) +
                        Program.reg[0].ToString().PadRight(10) +
                        Program.reg[1].ToString().PadRight(10) +
                        Program.reg[2].ToString().PadRight(10) +
                        Program.reg[3].ToString().PadRight(10) +
                        Program.reg[4].ToString().PadRight(10) +
                        Program.reg[5].ToString().PadRight(10));
                    Console.WriteLine();
                    Console.ReadKey();
                    Program.reg[posRegIndex]++;
                }
                catch
                {
                    //Program.CopyMem(ref regBackup, ref Program.reg);
                    break;
                }
            }
        }
    }

    class Instruction
    {
        public InstructionSet instr;
        public int a, b, c;

        public Instruction()
        {
            instr = (InstructionSet)0;
            a = b = c = 0;
        }

        public Instruction(InstructionSet i, int a, int b, int c)
        {
            this.instr = i;
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public void Execute()
        {
            Program.ExecuteInstruction(instr, a, b, c);
        }
    }
}
