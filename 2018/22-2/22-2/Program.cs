﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _22_2
{
    class Program
    {
        static void Main(string[] args)
        {
            //long depth = 510;
            //var tX = 10;
            //var tY = 10;
            long depth = 9171;
            var tX = 7;
            var tY = 721;

            Console.WriteLine("Calculating geologic data ...");
            var cave = new Cave(depth, tX, tY, tX + 50, tY + 100);
            Console.WriteLine("Geologic data calculated ...");
            /*
            Console.Clear();
            cave.Print(Equipment.Torch);
            Console.ReadKey();
            */
            Console.ReadKey();
            Console.Clear();
            var path = cave.Dijkstra(cave.mouth, cave.target);

            /*
            cave.mouth.Print();
            foreach (var t in path) { t.Print(); }
            Console.SetCursorPosition(0, cave.h + 2);
            */

            var prevTile = cave.mouth;
            var minutes = 0;

            Console.SetCursorPosition(0, cave.h + 3);
            Console.WriteLine(minutes.ToString().PadRight(3) + ": " + cave.mouth.ToString());
            foreach (var t in path)
            {
                if (prevTile.layer != t.layer)
                {
                    minutes += 7;
                    Console.WriteLine(minutes.ToString().PadRight(3) + ": " + "Changed equipment");
                }
                else
                {
                    minutes++;
                    Console.WriteLine(minutes.ToString().PadRight(3) + ": " + t.ToString());
                }
                prevTile = t;
            }
            Console.WriteLine();
            Console.WriteLine("Shortest path in " + minutes + " minutes");

            Console.ReadKey();
        }
    }
}
