﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _22_2
{
    public class Cave
    {
        public long Depth;
        public int w, h;
        public CaveTile mouth;
        public CaveTile target;

        public CaveTile[,,] caveMatrix;
        public List<CaveTile> tiles;

        public Cave()
        {
            Depth = 0;
            w = h = 0;
            mouth = null;
            caveMatrix = new CaveTile[3, 0, 0];
        }

        public Cave(long depth, int targetX, int targetY, int w, int h)
        {
            this.Depth = depth;
            this.w = w;
            this.h = h;
            caveMatrix = new CaveTile[3, w, h];

            tiles = new List<CaveTile>();
            for (int y = 0; y < h; ++y)
            {
                for (int x = 0; x < w; ++x)
                {
                    var tile = new CaveTile(this, x, y);
                    if (tile.x <= 0 || tile.y <= 0)
                    {
                        tile.CalculateGeologicIndex();
                        tile.CalculateErosionLevel();
                    }
                    tiles.Add(tile);
                }
            }

            for (int y = 1; y < h; ++y)
            {
                for (int x = 1; x < w; ++x)
                {
                    var tile = tiles.Find(t => t.x == x && t.y == y);
                    if (x == targetX && y == targetY) tile.GeologicIndex = 0;
                    else tile.CalculateGeologicIndex();
                    tile.CalculateErosionLevel();
                }
            }

            for (int y = 0; y < h; ++y)
            {
                for (int x = 0; x < w; ++x)
                {
                    for (int i = 0; i < 3; ++i)
                    {
                        var tile = tiles.Find(t => t.x == x && t.y == y);
                        if (tile.cannotUse != (Equipment)i)
                        {
                            caveMatrix[i, x, y] = tile.Clone();
                            caveMatrix[i, x, y].layer = (Equipment)i;
                        }
                    }
                }
            }

            for (int y = 0; y < h; ++y)
            {
                for (int x = 0; x < w; ++x)
                {
                    for (int i = 0; i < 3; ++i)
                    {
                        var tile = caveMatrix[i, x, y];
                        if (tile != null)
                        {
                            AddNeighbours(tile);
                        }
                    }
                }
            }

            target = GetTile(Equipment.Torch, targetX, targetY);
            target.target = true;
            target.CalculateGeologicIndex();
            target.CalculateErosionLevel();
            mouth = GetTile(Equipment.Torch, 0, 0);
            mouth.mouth = true;
        }

        void AddNeighbours(CaveTile t)
        {
            t.neighbours = new HashSet<CaveTile>();
            int layer = (int)t.layer;
            if (t.y > 0)
            {
                var n = caveMatrix[layer, t.x, t.y - 1];
                if (n != null) t.neighbours.Add(n);
            }
            if (t.x > 0)
            {
                var w = caveMatrix[layer, t.x - 1, t.y];
                if (w != null) t.neighbours.Add(w);
            }
            if (t.y < h - 1)
            {
                var s = caveMatrix[layer, t.x, t.y + 1];
                if (s != null) t.neighbours.Add(s);
            }
            if (t.x < w - 1)
            {
                var e = caveMatrix[layer, t.x + 1, t.y];
                if (e != null) t.neighbours.Add(e);
            }

            t.otherLayerNeighbours = new HashSet<CaveTile>();
            for (int j = 0; j < 3; ++j)
            {
                if (j == layer || j == (int)t.cannotUse) continue;
                var otherTile = caveMatrix[j, t.x, t.y];
                if (otherTile != null) t.otherLayerNeighbours.Add(otherTile);
            }
        }

        public CaveTile GetTile(Equipment eq, int x, int y)
        {
            return caveMatrix[(int)eq, x, y];
        }

        public int CalculateRiskLevel(int topleftX, int topleftY, int w, int h)
        {
            var result = 0;
            var endY = topleftY + h;
            var endX = topleftX + w;
            for (int y = topleftY; y < endY; ++y)
            {
                for (int x = topleftX; x < endX; ++x)
                {
                    result += (int)GetTile(0, x, y).type;
                }
            }
            return result;
        }

        public void Print(Equipment eq)
        {
            for (int y = 0; y < h; ++y)
            {
                for (int x = 0; x < w; ++x)
                {
                    var t = caveMatrix[(int)eq, x, y];
                    if (t != null) t.Print();
                }
            }
        }

        public int tDistance(CaveTile a, CaveTile b)
        {
            return Math.Abs(a.x - b.x) + Math.Abs(a.y - b.y);
        }

        List<CaveTile> reconstructPath(Dictionary<CaveTile, CaveTile> cameFrom, CaveTile current)
        {
            var totalPath = new List<CaveTile>() { current };
            while (cameFrom.Keys.Contains(current) && cameFrom[current] != null)
            {
                current = cameFrom[current];
                totalPath.Add(current);
            }
            totalPath.Reverse();
            totalPath.RemoveAt(0);
            return totalPath;
        }

        public List<CaveTile> AstarFind(CaveTile start, CaveTile destination)
        {
            var closedSet = new HashSet<CaveTile>();
            var openSet = new List<CaveTile>() { start };
            var cameFrom = new Dictionary<CaveTile, CaveTile>();

            var gScore = new Dictionary<CaveTile, int>
            {
                { start, 0 }
            };

            var fScore = new Dictionary<CaveTile, int>
            {
                { start, tDistance(start, destination) }
            };

            while (openSet.Count > 0)
            {
                var current = openSet.OrderBy(t => fScore[t]).ThenBy(t => gScore[t]).First();
                if (current == destination)
                {
                    return reconstructPath(cameFrom, current);
                }
                openSet.Remove(current);
                closedSet.Add(current);

                foreach (var t in current.neighbours)
                {
                    if (closedSet.Contains(t))
                    {
                        continue;
                    }

                    var tentative_gScore = gScore[current]++;

                    if (!openSet.Contains(t)) openSet.Add(t);
                    else if (tentative_gScore >= gScore[t]) continue;

                    if (cameFrom.ContainsKey(t)) cameFrom[t] = current;
                    else cameFrom.Add(t, current);
                    if (gScore.ContainsKey(t)) gScore[t] = tentative_gScore;
                    else gScore.Add(t, tentative_gScore);
                    if (fScore.ContainsKey(t)) fScore[t] = gScore[t] + tDistance(t, destination);
                    else fScore.Add(t, gScore[t] + tDistance(t, destination));
                }

                foreach (var t in current.otherLayerNeighbours)
                {
                    if (closedSet.Contains(t))
                    {
                        continue;
                    }

                    var tentative_gScore = gScore[current] + 7;

                    if (!openSet.Contains(t)) openSet.Add(t);
                    else if (tentative_gScore >= gScore[t]) continue;

                    if (cameFrom.ContainsKey(t)) cameFrom[t] = current;
                    else cameFrom.Add(t, current);
                    if (gScore.ContainsKey(t)) gScore[t] = tentative_gScore;
                    else gScore.Add(t, tentative_gScore);
                    if (fScore.ContainsKey(t)) fScore[t] = gScore[t] + tDistance(t, destination);
                    else fScore.Add(t, gScore[t] + tDistance(t, destination));
                }
            }

            return null;
        }

        static int FindIndex(Dictionary<CaveTile, int> distance, List<CaveTile> openSet, CaveTile t)
        {
            if (openSet.Count == 0)
            {
                return 0;
            }
            if (openSet.Count == 1)
            {
                return distance[openSet[0]] < distance[t] ? 1 : 0;
            }
            else
            {
                var l = 0;
                var h = openSet.Count;
                var T = distance[t];
                while (l < h)
                {
                    var m = (l + h) / 2;
                    var Am = distance[openSet[m]];
                    if (Am < T)
                    {
                        l = m + 1;
                    }
                    else
                    {
                        h = m;
                    }
                }
                return l;
            }
        }

        public List<CaveTile> Dijkstra(CaveTile start, CaveTile destination)
        {
            var reachedDestination = false;
            var openSet = new List<CaveTile>();
            var distance = new Dictionary<CaveTile, int>();
            var cameFrom = new Dictionary<CaveTile, CaveTile>();

            foreach (var t in caveMatrix)
            {
                if (t != null)
                {
                    distance.Add(t, int.MaxValue);
                    cameFrom.Add(t, null);
                    openSet.Add(t);
                }
            }

            distance[start] = 0;
            openSet.Remove(start);
            openSet.Insert(0, start);
            while (openSet.Count > 0)
            {
                if (openSet.Count % 100 == 0)
                    Console.WriteLine(openSet.Count.ToString().PadRight(10) + "left to check");
                //var current = openSet.OrderBy(t => distance[t]).First();
                //openSet = openSet.OrderBy(t => distance[t]).ToList();
                var current = openSet[0];
                if (distance[current] == int.MaxValue && !reachedDestination)
                {
                    return null;
                }

                if (current == destination) reachedDestination = true;
                openSet.Remove(current);

                foreach (var n in current.neighbours)
                {
                    var alt = distance[current] + 1;
                    if (alt < distance[n])
                    {
                        cameFrom[n] = current;
                        distance[n] = alt;
                        openSet.Remove(n);
                        openSet.Insert(FindIndex(distance, openSet, n), n);

                        //Console.SetCursorPosition(3 * n.x + (int)n.layer * (w + 1) * 3, n.y);
                        //Console.Write(distance[n].ToString().PadRight(3));
                    }
                }
                foreach (var n in current.otherLayerNeighbours)
                {
                    var alt = distance[current] + 7;
                    if (alt < distance[n])
                    {
                        cameFrom[n] = current;
                        distance[n] = alt;
                        openSet.Remove(n);
                        openSet.Insert(FindIndex(distance, openSet, n), n);
                        //Console.SetCursorPosition(3 * n.x + (int)n.layer * (w + 1) * 3, n.y);
                        //Console.Write(distance[n].ToString().PadRight(3));
                    }
                }
            }

            if (reachedDestination)
                return reconstructPath(cameFrom, destination);
            else
                return null;
        }
    }
}
