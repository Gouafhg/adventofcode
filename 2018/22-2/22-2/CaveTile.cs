﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _22_2
{
    public enum RegionType { Rocky, Wet, Narrow, SolidRock }
    public enum Equipment { Neither, Torch, ClimbingGear }

    public class CaveTile
    {
        private static Dictionary<RegionType, HashSet<Equipment>> _requirements;
        private static Dictionary<RegionType, Equipment> _cannotUse;

        static CaveTile()
        {
            _requirements = new Dictionary<RegionType, HashSet<Equipment>>
            {
                { RegionType.Rocky, new HashSet<Equipment>() },
                { RegionType.Wet, new HashSet<Equipment>() },
                { RegionType.Narrow, new HashSet<Equipment>() }
            };
            _requirements[RegionType.Rocky].Add(Equipment.ClimbingGear);
            _requirements[RegionType.Rocky].Add(Equipment.Torch);
            _requirements[RegionType.Wet].Add(Equipment.ClimbingGear);
            _requirements[RegionType.Wet].Add(Equipment.Neither);
            _requirements[RegionType.Narrow].Add(Equipment.Torch);
            _requirements[RegionType.Narrow].Add(Equipment.Neither);

            _cannotUse = new Dictionary<RegionType, Equipment>();
            _cannotUse.Add(RegionType.Rocky, Equipment.Neither);
            _cannotUse.Add(RegionType.Wet, Equipment.Torch);
            _cannotUse.Add(RegionType.Narrow, Equipment.ClimbingGear);
        }

        public Cave parent;
        public long GeologicIndex;
        public long ErosionLevel;
        public int x, y;
        public bool mouth;
        public bool target;
        public RegionType type;
        public HashSet<Equipment> requirements { get { return _requirements[type]; } }
        public Equipment cannotUse { get { return _cannotUse[type]; } }
        public Equipment layer;
        public HashSet<CaveTile> neighbours;
        public HashSet<CaveTile> otherLayerNeighbours;

        public CaveTile Clone()
        {
            var clone = new CaveTile()
            {
                x = this.x,
                y = this.y,
                parent = this.parent,
                GeologicIndex = this.GeologicIndex,
                ErosionLevel = this.ErosionLevel,
                mouth = this.mouth,
                target = this.target,
                type = this.type,
            };
            return clone;
        }

        public void CalculateGeologicIndex()
        {
            if (x < 0 || y < 0) GeologicIndex = 0;
            else if ((x == 0 && y == 0) || mouth || target) GeologicIndex = 0;
            else if (y == 0) GeologicIndex = (long)x * 16807;
            else if (x == 0) GeologicIndex = (long)y * 48271;
            else
            {
                var n = parent.tiles.Find(t => t.x == x && t.y == y - 1); //parent.GetTile(cannotUse, x, y - 1);
                var w = parent.tiles.Find(t => t.x == x - 1 && t.y == y); //parent.GetTile(cannotUse, x - 1, y);
                GeologicIndex = w.ErosionLevel * n.ErosionLevel;
            }
        }

        public void CalculateErosionLevel()
        {
            ErosionLevel = (GeologicIndex + parent.Depth) % 20183;
            if (x >= 0 && y >= 0)
            {
                type = (RegionType)(ErosionLevel % 3);
            }
            else
            {
                type = RegionType.SolidRock;
            }
        }

        public CaveTile()
        {
            parent = null;
            x = y = -1;
            mouth = false;
            target = false;
            GeologicIndex = 0;
            ErosionLevel = 0;
        }

        public CaveTile(Cave parent, int x, int y)
        {
            this.parent = parent;
            this.x = x;
            this.y = y;
            mouth = x == 0 && y == 0;
            target = false;
        }

        public override string ToString()
        {
            return layer.ToString().PadRight(12) + " (" + x + ", " + y + "): " + type;
        }

        public void Print()
        {
            var c = type == RegionType.Rocky ? "." : type == RegionType.Wet ? "=" : "|";
            if (mouth) c = "M";
            if (target) c = "T";
            Console.SetCursorPosition(x, y);
            Console.Write(c);
        }
    }
}
