﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9
{
    class Program
    {
        public static HashSet<Marble> marbles;
        public static Queue<Player> players;
        public static Queue<Marble> marblePile;
        public static HashSet<Marble> placedMarbles;

        static Program()
        {
            marbles = new HashSet<Marble>();
            players = new Queue<Player>();
            marblePile = new Queue<Marble>();
            placedMarbles = new HashSet<Marble>();
        }

        public static void RemoveMarble(Marble marble)
        {
            placedMarbles.Remove(marble);
            if (marble.clockwise != null && marble.counterClockwise != null)
            {
                var c = marble.clockwise;
                var cw = marble.counterClockwise;
                marble.clockwise.counterClockwise = cw;
                marble.counterClockwise.clockwise = c;
                marble.clockwise = null;
                marble.counterClockwise = null;
            }
        }

        static void Main(string[] args)
        {
            const int playerCount = 431;
            const int marbleCount = 7095000;

            for (int i = 0; i < playerCount; ++i)
            {
                new Player();
            }
            for (int i = 0; i < marbleCount; ++i)
            {
                marblePile.Enqueue(new Marble());
            }

            int round = 0;
            Player currentPlayer;
            Marble firstMarble = marblePile.Dequeue();
            firstMarble.clockwise = firstMarble;
            firstMarble.counterClockwise = firstMarble;
            Marble currentMarble = firstMarble;
            placedMarbles.Add(currentMarble);

            //Console.WriteLine("[-] (0)");

            while (marblePile.Count > 0)
            {
                round++;
                if (round % (marbleCount / 20) == 0) Console.Write("|");

                currentPlayer = players.Dequeue();
                var nextMarble = marblePile.Dequeue();

                if (nextMarble.id % 23 != 0)
                {
                    var oneStepClockWise = currentMarble.clockwise;
                    var twoStepsClockWise = oneStepClockWise.clockwise;
                    oneStepClockWise.clockwise = nextMarble;
                    twoStepsClockWise.counterClockwise = nextMarble;
                    nextMarble.counterClockwise = oneStepClockWise;
                    nextMarble.clockwise = twoStepsClockWise;
                    placedMarbles.Add(nextMarble);
                    currentMarble = nextMarble;
                }
                else
                {
                    currentPlayer.TakeMarble(nextMarble);
                    Marble nextToTake = currentMarble;
                    for (int i = 0; i < 7; ++i) nextToTake = nextToTake.counterClockwise;
                    currentMarble = nextToTake.clockwise;
                    currentPlayer.TakeMarble(nextToTake);
                }
                players.Enqueue(currentPlayer);
                /*
                Console.Write(currentPlayer.ToString() + " " + firstMarble.ToString() + " ");
                Marble nextToWrite = firstMarble.clockwise;
                while (nextToWrite != firstMarble)
                {
                    Console.Write(nextToWrite.ToString() + " ");
                    nextToWrite = nextToWrite.clockwise;
                }
                Console.WriteLine();
                */
            }

            var playerHighScore = players.OrderByDescending(p => p.score).ToList();
            var winningPlayer = playerHighScore[0];
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine(players.Count + " players; last marble is worth " + marbles.Count + " points: high score is " + winningPlayer.score);
            Console.ReadKey();
        }
    }

    class Player
    {
        public ulong id;
        public ulong score;
        HashSet<Marble> marblesTaken;

        public void TakeMarble(Marble marble)
        {
            marblesTaken.Add(marble);
            Program.RemoveMarble(marble);
            score += marble.id;

            //Console.WriteLine("Player " + ToString() + " picked up marble " + marble.ToString() + " and has a total score of " + score);
        }

        public Player()
        {
            id = (ulong)(Program.players.Count + 1);
            Program.players.Enqueue(this);
            marblesTaken = new HashSet<Marble>();
        }

        public override string ToString()
        {
            return "[" + id + "]";
        }
    }

    class Marble
    {
        public ulong id;
        public Marble clockwise;
        public Marble counterClockwise;

        public Marble()
        {
            id = (ulong)Program.marbles.Count;
            clockwise = null;
            counterClockwise = null;
            Program.marbles.Add(this);
        }

        public override string ToString()
        {
            return "(" + id + ")";
        }
    }
}
