﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _18
{
    class Program
    {
        static string[,] prevState;
        static string[,] currentState;
        static int w;
        static int h;

        static Program()
        {
        }

        static void Render()
        {
            for (int y = 0; y < h; ++y)
            {
                for (int x = 0; x < w; ++x)
                {
                    Console.Write(currentState[x, y]);
                }
                Console.WriteLine();
            }
        }

        static long GetOpens()
        {
            long opens = 0;
            for (int y = 0; y < h; ++y)
            {
                for (int x = 0; x < w; ++x)
                {
                    if (currentState[x, y] == ".") opens++;
                }
            }
            return opens;
        }

        static long GetTrees()
        {
            long trees = 0;
            for (int y = 0; y < h; ++y)
            {
                for (int x = 0; x < w; ++x)
                {
                    if (currentState[x, y] == "|") trees++;
                }
            }
            return trees;
        }

        static long GetLumbers()
        {
            long lumbers = 0;
            for (int y = 0; y < h; ++y)
            {
                for (int x = 0; x < w; ++x)
                {
                    if (currentState[x, y] == "#") lumbers++;
                }
            }
            return lumbers;
        }

        static void CopyState(ref string[,] input, ref string[,] output)
        {
            for (int y = 0; y < h; ++y)
            {
                for (int x = 0; x < w; ++x)
                {
                    output[x, y] = input[x, y];
                }
            }
        }

        static bool CompareState(ref string[,] input, ref string[,] output)
        {
            for (int y = 0; y < h; ++y)
            {
                for (int x = 0; x < w; ++x)
                {
                    if (input[x, y] != output[x, y]) return false;
                }
            }
            return true;
        }

        static void ProcessField()
        {
            CopyState(ref currentState, ref prevState);

            for (int y = 0; y < h; ++y)
            {
                for (int x = 0; x < w; ++x)
                {
                    int opens = 0;
                    int lumbers = 0;
                    int trees = 0;
                    for (int yy = y - 1; yy < y + 2; ++yy)
                    {
                        if (yy < 0 || yy >= currentState.GetLength(1))
                        {
                            continue;
                        }
                        for (int xx = x - 1; xx < x + 2; ++xx)
                        {
                            if (xx < 0 || xx >= currentState.GetLength(0))
                            {
                                continue;
                            }
                            if (xx == x && yy == y) continue;

                            if (prevState[xx, yy] == ".") opens++;
                            else if (prevState[xx, yy] == "#") lumbers++;
                            else if (prevState[xx, yy] == "|") trees++;
                        }
                    }


                    if (prevState[x, y] == "." && trees >= 3)
                    {
                        currentState[x, y] = "|";
                    }
                    if (prevState[x, y] == "|" && lumbers >= 3)
                    {
                        currentState[x, y] = "#";
                    }
                    if (prevState[x, y] == "#")
                    {
                        if (lumbers >= 1 && trees >= 1) currentState[x, y] = "#";
                        else currentState[x, y] = ".";
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            var inputTest = ".#.#...|#.!.....#|##|!.|..|...#.!..|#.....#!#.#|||#|#|!...#.||...!.|....|...!||...#|.#|!|.||||..|.!...#.|..|.";
            var inputReal = ".#...#..|.#||.|.......##.|#|.....#|...#.|.....#.|.!|..|.#.|.#....#.|..#..|#....#|#.|||.....|...#....#!|..|..|.||.....#..#||..#..#..|.#.|..|..||...|#...#!..##|.||.|.#.###.....#....#...#.|.|..||||.....|.|.!.....#.......|..|#.|...|.|#..........#|.|..|.||.##!.#|....|..|..#.#|.|..||#...|....##||..|.||....#.#.!|##......#.#..|.|..##|#...|...|.##|...|#...|...##|!...|...|||.#|..|#.#....#|....|..||..|...|#|..#|.|.!|.|##....#.|.|.#..#...##.#......#.#.|......|...|||!..#.||..#.....#.|##..#..|##...|#|..|##|..|.|#..##.!..||#...#..|...|#.#....#...#...|#.||.|.##|.#....#|!.#...#.#|#|.#....#..|....|.|...|.|.#...#.||....|..!.#...........##.#...#..||...#...|###...|...#.|.#..!....|.|...##|#...#..#|#......##....#...|.#.....|#|!.##..#.#.##.....#......|....#...|.#.#......#...##.!...#.#..|#|#..#.....||.##|#....|.#.|...#||#.|#...|!.#..##||.#|....|.||.||...|..|....|#..#|..##.#|.|.#!.#.||.|##.|..|..#..|#|..###.||#........||#|.||##|.!.|.##|.#....#.#..|..|...#...|.#.|......|##..#.....!..||...|.|##.||..|.|...|#..|.........#.|.#.....|##!#..|..|#.......#.||#.|#..|#|#.##.|||....#.#...|..|!|..|#|...||..|..||...#......##..|.|#..##....#.##.|!#|.#...|..|.....||.|###..#|.|..#.|..|||.#.#....|..!..#.||....##|.##.#.##..#||......#.#.....|.....#...!#.#...||..|..|#...|#..||#.|..|..|..||.|.#...|...#.!..|.#.#.|.#|#...#.||.|#|#..#..#.#|....##||#..|.|.#!.#...|....#...|.||#.|.#...|.......#..|.#........|.!..|.#|#||#....|..|#.#...#....#..|.|.|..||#|..##...!......|........|......||................|...##..#.!#|.......|.|.|.|..####..#.|#....|..|#|#....#....|.!||.||#.||#....|.#..#.....|..#.|#.|.|...|....####..!..........#|..|||..#|..|..|||##|#||#|....||.|...||!....|....#.|......#|....###....|.##|.#||||||..#.|#!.|#|.#.##||......#.#.||.|.##...|.|.|#........#.#|.!.......|...|..|.|.....|.#|.|.#...#...#|...#.||...#!.||.#..|.##.#..##...#....|...|#..##|.|.#..|#|#|..|!.#..|.|##|....#|###.....|...||.|.|||......#|....|.!.|#.#.....|.##.##.|........|...|...|||....#....##.!...##|||.|##|.|..#......#.......|##|.|..|.......|.!...#.....#|.#.||.....|....#.#|......##.....##..|#|!..|.|...|....#|....###.|##..........|#..#|.#|..|#.!..|..#.|.....|...|...#|.||#|..#.#..#.#|..|........!..|..||.#.......|##|..|..|..|..##|.|...#.#..|.....!#.#..#.|###...#...||.#..#|...|.##...|..#|.......|.!..##.|..#|...#...|..#.|..|#.|.....#..#.#..#|#|...#!#......|..|.#.|.|...........#......||..#.||..#....!...#|......#.#|.....#....|..|......|.#.#.|.|......!#|.##...#.##..|#..|...#.....#|.#.............|..|#!.....|.|..|.||.#|.||##...#|.##|#.....#..#..#|.#...!..|..|.#.....#.##|.##|.||..#..#|#.||..||#.###..||.";

            var KeyFollowUp = new Dictionary<ulong, ulong>();

            var input = inputReal;
            {
                var rows = input.Split('!');
                w = rows[0].Length;
                h = rows.Length;
                prevState = new string[w, h];
                currentState = new string[w, h];
                for (int y = 0; y < h; ++y)
                {
                    for (int x = 0; x < w; ++x)
                    {
                        currentState[x, y] = rows[y][x] + string.Empty;
                    }
                }
            }


            /*
            Console.Clear();
            Console.WriteLine("Initial state:");
            Console.WriteLine();
            Render();
            Thread.Sleep(250);
            */

            //long opens = GetOpens();
            long trees = GetTrees();
            long lumbers = GetLumbers();
            long lastResources = 0;
            long resources = trees * lumbers;

            long minutes = 0;
            long totalMinutes = 1000000000;
            while (minutes < totalMinutes)
            {
                ProcessField();
                minutes++;
                //Console.Clear();
                //Console.WriteLine("After " + minutes.ToString().PadRight(10) + " minutes");
                //Console.WriteLine();
                //Render();

                //opens = GetOpens();
                trees = GetTrees();
                lumbers = GetLumbers();
                lastResources = resources;
                resources = trees * lumbers;
                var delta = resources - lastResources;

                //Console.WriteLine();
                //Console.WriteLine();
                Console.WriteLine("(" + minutes + ") ... Last resources: " + lastResources.ToString().PadRight(15) + " Resources: " + resources.ToString().PadRight(15) + " Delta: " + delta);
                //Console.ReadKey();
                //Thread.Sleep(250);
            }



            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Resources: " + resources);
            Console.ReadKey();
        }
    }
}
