﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace _6
{
    class Program
    {
        static readonly int DISTANCE_THRESHOLD = 10000;

        static int ManhattanDistance(Point a, Point b)
        {
            return (Math.Abs(a.X - b.X) + Math.Abs(a.Y - b.Y));
        }

        static void CreateScreen(out List<List<char>> screen, Size resolution)
        {
            screen = new List<List<char>>();
            for (int x = 0; x < resolution.Width; ++x)
            {
                var column = new List<char>();
                for (int y = 0; y < resolution.Height; ++y)
                {
                    column.Add('.');
                }
                screen.Add(column);
            }
        }

        static void ClearScreen(ref List<List<char>> screen)
        {
            Size resolution = new Size(screen[0].Count, screen.Count);
            for (int y = 0; y < resolution.Height; ++y)
            {
                for (int x = 0; x < resolution.Width; ++x)
                {
                    screen[x][y] = '.';
                }
            }
        }

        static void PrintScreen(List<List<char>> screen)
        {
            Size resolution = new Size(screen[0].Count, screen.Count);
            for (int y = 0; y < resolution.Height; ++y)
            {
                var r = string.Empty;
                for (int x = 0; x < resolution.Width; ++x)
                {
                    r += screen[x][y];
                }
                Console.WriteLine(r);
            }
        }

        static void Main(string[] args)
        {
            var input = new List<int>() { 227, 133, 140, 168, 99, 112, 318, 95, 219, 266, 134, 144, 306, 301, 189, 188, 58, 334, 337, 117, 255, 73, 245, 144, 102, 257, 255, 353, 303, 216, 141, 167, 40, 321, 201, 50, 60, 188, 132, 74, 125, 199, 176, 307, 204, 218, 338, 323, 276, 278, 292, 229, 109, 228, 85, 305, 86, 343, 97, 254, 182, 151, 110, 292, 285, 124, 43, 223, 153, 188, 285, 136, 334, 203, 84, 243, 92, 185, 330, 223, 259, 275, 106, 199, 183, 205, 188, 212, 231, 150, 158, 95, 174, 212, 279, 97, 172, 131, 247, 320 };

            var points = new List<Point>();
            for (int i = 0; i < input.Count; i += 2)
            {
                points.Add(new Point(input[i], input[i + 1]));
            }

            var maxX = int.MinValue;
            var maxY = int.MinValue;

            foreach (var p in points)
            {
                maxX = Math.Max(maxX, p.X + 1);
                maxY = Math.Max(maxY, p.Y + 1);
            }
            maxX = Math.Max(maxX, maxY);
            maxY = maxX;

            var origMaxX = maxX;
            var origMaxY = maxY;

            for (int i = 0; i < points.Count; ++i)
            {
                var p = points[i];
                points[i] = new Point(p.X + DISTANCE_THRESHOLD, p.Y + DISTANCE_THRESHOLD);
            }
            maxX += 2 * DISTANCE_THRESHOLD;
            maxY += 2 * DISTANCE_THRESHOLD;

            HashSet<Point> validPoints = new HashSet<Point>();
            for (int y = 0; y < maxY; ++y)
            {
                Console.Clear();
                Console.WriteLine((float)(y * maxX) / (maxY * maxX + maxX));
                for (int x = 0; x < maxX; ++x)
                {
                    var currentPoint = new Point(x, y);
                    var dSum = 0;
                    foreach (var p in points)
                    {
                        dSum += ManhattanDistance(currentPoint, p);
                    }
                    if (dSum < DISTANCE_THRESHOLD)
                    {
                        validPoints.Add(currentPoint);
                    }
                }
            }

            Console.WriteLine("There are " + validPoints.Count + " valid points");

            Console.ReadKey();
            /*
            Dictionary<Point, int> pointIdBefore = new Dictionary<Point, int>();
            var currentId = 0;
            foreach (var p in points)
            {
                pointIdBefore.Add(p, currentId);
                currentId++;
            }

            Dictionary<int, int> pointAreasBefore = new Dictionary<int, int>();
            for (int y = 0; y < maxY; ++y)
            {
                for (int x = 0; x < maxX; ++x)
                {
                    Point currentPoint = new Point(x, y);
                    Point shortestPoint = points[0];
                    int shortestDistance = int.MaxValue;
                    foreach (var p in points)
                    {
                        var d = ManhattanDistance(currentPoint, p);
                        if (d < shortestDistance)
                        {
                            shortestDistance = d;
                            shortestPoint = p;
                        }
                    }
                    int shortestCount = 0;
                    foreach (var p in points)
                    {
                        var d = ManhattanDistance(currentPoint, p);
                        if (d == shortestDistance)
                        {
                            shortestCount++;
                        }
                    }
                    if (shortestCount == 1)
                    {
                        pointAreasBefore.TryAdd(pointIdBefore[shortestPoint], 0);
                        pointAreasBefore[pointIdBefore[shortestPoint]]++;
                    }
                }
            }
            Console.WriteLine("_Before_");
            Console.WriteLine();

            Console.WriteLine();
            foreach (var p in points)
            {
                Console.WriteLine("Point " + pointIdBefore[p] + " has area " + pointAreasBefore[pointIdBefore[p]]);
            }

            Console.WriteLine();
            Console.WriteLine();
            for (int i = 0; i < points.Count; ++i)
            {
                var p = points[i];
                points[i] = new Point(p.X + origMaxX, p.Y + origMaxY);
            }
            maxX += 2 * origMaxX;
            maxY += 2 * origMaxY;

            Dictionary<Point, int> pointIdAfter = new Dictionary<Point, int>();
            currentId = 0;
            foreach (var p in points)
            {
                pointIdAfter.Add(p, currentId);
                currentId++;
            }
            Dictionary<int, int> pointAreasAfter = new Dictionary<int, int>();
            for (int y = 0; y < maxY; ++y)
            {
                for (int x = 0; x < maxX; ++x)
                {
                    Point currentPoint = new Point(x, y);
                    Point shortestPoint = new Point(-1, -1);
                    int shortestDistance = int.MaxValue;
                    foreach (var p in points)
                    {
                        var d = ManhattanDistance(currentPoint, p);
                        if (d < shortestDistance)
                        {
                            shortestDistance = d;
                            shortestPoint = p;
                        }
                    }
                    int shortestCount = 0;
                    foreach(var p in points)
                    {
                        var d = ManhattanDistance(currentPoint, p);
                        if (d == shortestDistance)
                        {
                            shortestCount++;
                        }
                    }
                    if (shortestCount == 1)
                    {
                        pointAreasAfter.TryAdd(pointIdAfter[shortestPoint], 0);
                        pointAreasAfter[pointIdAfter[shortestPoint]]++;
                    }
                }
            }
            Console.WriteLine("_After_");
            Console.WriteLine();

            Console.WriteLine();
            foreach (var p in points)
            {
                Console.WriteLine("Point " + pointIdAfter[p] + " has area " + pointAreasAfter[pointIdAfter[p]]);
            }

            HashSet<int> finiteAreaPoints = new HashSet<int>();
            finiteAreaPoints.UnionWith(pointAreasAfter.Keys.Where(c => pointAreasBefore[c] == pointAreasAfter[c]));
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.Write("Finite points: ");
            foreach (var id in finiteAreaPoints) Console.Write(id + " ");
            Console.WriteLine();

            Dictionary<int, int> finiteAreas = new Dictionary<int, int>();
            foreach (var id in finiteAreaPoints)
            {
                finiteAreas.Add(id, pointAreasAfter[id]);
            }
            var areaHighScore = pointAreasAfter.Keys.Where(c => finiteAreaPoints.Contains(c)).ToList().OrderBy(c => finiteAreas[c]).ToList();
            Console.WriteLine();
            foreach (var id in areaHighScore)
            {
                Console.WriteLine("Point " + id + " has area " + pointAreasAfter[id]);
            }

            var bestAreaName = areaHighScore[areaHighScore.Count - 1];
            var bestAreaSize = pointAreasAfter[bestAreaName];
            Console.WriteLine("Best is: " + bestAreaName + " with " + bestAreaSize);
            Console.ReadKey();
            */
        }
    }
}
