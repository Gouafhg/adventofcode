﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7
{
    class Program
    {
        public static readonly int MIN_TIME = 60;
        public static readonly int WORKER_COUNT = 5;

        public static HashSet<char> stepsDone;
        public static List<StepWorker> workers;
        public static string output;

        static Program()
        {
            stepsDone = new HashSet<char>();
            workers = new List<StepWorker>();
            output = string.Empty;
        }

        static void Main(string[] args)
        {
            var input = new List<char>() { 'G', 'T', 'L', 'V', 'D', 'P', 'J', 'K', 'N', 'B', 'K', 'W', 'T', 'I', 'F', 'E', 'P', 'O', 'X', 'I', 'M', 'S', 'Y', 'O', 'I', 'Z', 'V', 'Z', 'Q', 'Z', 'H', 'C', 'R', 'Z', 'U', 'S', 'E', 'Z', 'O', 'W', 'Z', 'S', 'S', 'C', 'W', 'B', 'A', 'B', 'C', 'B', 'L', 'P', 'J', 'V', 'E', 'W', 'Z', 'W', 'W', 'C', 'S', 'W', 'Q', 'S', 'O', 'B', 'R', 'W', 'D', 'H', 'E', 'O', 'Y', 'H', 'V', 'O', 'O', 'S', 'X', 'V', 'R', 'E', 'S', 'A', 'K', 'Y', 'V', 'W', 'U', 'W', 'H', 'R', 'P', 'I', 'E', 'C', 'H', 'Z', 'N', 'V', 'N', 'W', 'A', 'C', 'V', 'E', 'N', 'Q', 'Y', 'V', 'R', 'O', 'R', 'C', 'L', 'S', 'V', 'R', 'X', 'R', 'Z', 'A', 'O', 'Z', 'U', 'C', 'X', 'W', 'K', 'O', 'O', 'A', 'K', 'T', 'N', 'O', 'X', 'C', 'Z', 'C', 'N', 'X', 'T', 'A', 'D', 'O', 'M', 'Q', 'D', 'C', 'U', 'E', 'N', 'H', 'I', 'U', 'N', 'A', 'M', 'E', 'M', 'V', 'P', 'B', 'K', 'X', 'N', 'S', 'S', 'B', 'Y', 'W', 'K', 'Q', 'V', 'S', 'E', 'S', 'N', 'Z', 'P', 'A', 'T', 'V', 'L', 'D', 'I', 'C', 'Q', 'E', 'Y', 'U', 'J', 'I', 'P', 'H', 'T', 'M', 'T', 'E', 'D', 'F' };

            var steps = new Dictionary<char, Step>();
            for (int i = 0; i < input.Count; ++i)
            {
                if (!steps.ContainsKey(input[i]))
                {
                    steps.Add(input[i], new Step(input[i]));
                }
            }
            for (int i = 0; i < input.Count; i += 2)
            {
                steps[input[i + 1]].dependencies.Add(input[i]);
            }

            for (int i = 0; i < WORKER_COUNT; ++i)
            {
                workers.Add(new StepWorker());
            }


            var timeUnits = 0;
            var todo = steps.Values.Where(s => s.dependencies.Count == 0).OrderBy(s => s.id).ToList();
            while (workers.Any(w => w.busy) || todo.Count > 0)
            {
                var freeWorkers = workers.FindAll(w => !w.busy);
                while (todo.Count > 0 && freeWorkers.Count > 0)
                {
                    freeWorkers[0].AssignWork(todo[0]);
                    freeWorkers.RemoveAt(0);
                    todo.RemoveAt(0);
                }

                var workDone = false;
                workers.ForEach(w => workDone = w.DoWork() | workDone);
                timeUnits++;
                if (workDone)
                {
                    foreach (var s in steps.Values)
                    {
                        if (!stepsDone.Contains(s.id) && !todo.Contains(s) && !workers.Any(w => w.workingOn == s) && s.GoodToGo())
                        {
                            todo.Add(s);
                        }
                    }
                    todo = todo.OrderBy(s => s.id).ToList();
                }
            }

            Console.WriteLine(output);
            Console.WriteLine("Done in " + timeUnits + " timeunits");
            Console.ReadKey();
        }
    }

    public class Step
    {
        private char _id;
        public char id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                TimeLeft = Program.MIN_TIME + (int)(_id - 'A') + 1;
            }
        }
        public HashSet<char> dependencies;
        public int TimeLeft;

        public Step()
        {
            id = ' ';
            TimeLeft = 0;
            dependencies = new HashSet<char>();
        }

        public Step(char id)
        {
            this.id = id;
            dependencies = new HashSet<char>();
        }

        public bool GoodToGo()
        {
            var notDone = dependencies.Where(id => !Program.stepsDone.Contains(id)).ToList();
            return notDone.Count == 0;
        }
    }

    public class StepWorker
    {
        public int id;
        public Step workingOn;
        public string timeReport;

        public StepWorker()
        {
            id = Program.workers.Count;
            timeReport = string.Empty;
        }

        public bool busy
        {
            get
            {
                return workingOn != null;
            }
        }

        public void AssignWork(Step step)
        {
            workingOn = step;
        }

        public bool DoWork()
        {
            if (busy)
            {
                timeReport += workingOn.id;
                workingOn.TimeLeft--;
                if (workingOn.TimeLeft < 1)
                {
                    Program.stepsDone.Add(workingOn.id);
                    Program.output += workingOn.id;
                    workingOn = null;
                    return true;
                }
            }
            else
            {
                timeReport += ".";
            }
            return false;
        }
    }
}
