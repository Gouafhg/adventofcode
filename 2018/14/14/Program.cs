﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _14
{
    class Program
    {
        static bool FindSequenceInList(List<byte> a, List<byte> b)
        {
            if (a.Count < b.Count)
            {
                return false;
            }

            for (int i = 0; i < a.Count - b.Count; ++i)
            {
                bool foundSequence = true;
                for (int j = 0; j < b.Count; ++j)
                {
                    if (a[i + j] != b[j])
                    {
                        foundSequence = false;
                    }
                }
                if (foundSequence)
                {
                    return true;
                }
            }
            return false;
        }

        static void Main(string[] args)
        {
            var inputTest1 = "9";
            var inputTest2 = "59414";
            var inputTest3 = "92510";
            var inputTest4 = "8176111038";
            var inputReal = "890691";

            var input = inputReal;
            var inputAsList = new List<byte>();
            for (int i = 0; i < input.Length; ++i) inputAsList.Add(byte.Parse(input[i] + string.Empty));

            var scores = new List<byte>() { 3, 7 };
            int Elf1Index = 0;
            int Elf2Index = 1;

            var foundSequence = false;
            var foundSequenceAt = -1;
            Console.Clear();

            while (!foundSequence)
            {
                var startIndex = scores.Count;
                var Elf1Score = scores[Elf1Index];
                var Elf2Score = scores[Elf2Index];

                var sumOfScores = (Elf1Score + Elf2Score).ToString();
                for (int j = 0; j < sumOfScores.Length; ++j)
                {
                    scores.Add(byte.Parse(string.Empty + sumOfScores[j]));
                }
                startIndex++;

                Elf1Index = (Elf1Index + 1 + Elf1Score) % scores.Count;
                Elf2Index = (Elf2Index + 1 + Elf2Score) % scores.Count;

                if (scores.Count > inputAsList.Count)
                {
                    for (int i = 0; i <= (scores.Count - startIndex) && !foundSequence; ++i)
                    {
                        foundSequence = true;
                        for (int j = 0; j < inputAsList.Count; ++j)
                        {
                            if (scores[startIndex + i - inputAsList.Count + j] != inputAsList[j])
                            {
                                foundSequence = false;
                                break;
                            }
                        }
                        if (foundSequence)
                        {
                            foundSequenceAt = startIndex + i - inputAsList.Count;
                        }
                    }
                }
            }

            Console.WriteLine("Input found after: " + foundSequenceAt);
            Console.WriteLine();
            Console.ReadKey();
        }
    }
}
