using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextEditor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.Text))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        List<string> SplitToRows(string input)
        {
            var rows = input.Split('\r').ToList();
            var result = new List<string>();
            foreach (var row in rows)
            {
                var newRow = string.Empty;
                foreach (var c in row)
                    if (c != '\r' && c != '\n')
                        newRow += c;
                result.Add(newRow);
            }
            return result;
        }

        string ParseNumbers(string input)
        {
            var output = string.Empty;
            foreach (var c in input)
            {
                switch (c)
                {
                    case ' ':
                        break;
                    case '+':
                        break;
                    case '\r':
                        output += ',';
                        break;
                    case '\n':
                        output += ' ';
                        break;
                    default:
                        output += c;
                        break;
                }
            }
            return output;
        }

        string ParseStrings(string input)
        {
            var output = "\"";
            foreach (var c in input)
            {
                switch (c)
                {
                    case ' ':
                        break;
                    case '+':
                        break;
                    case '\r':
                        output += "\", \"";
                        break;
                    case '\n':
                        break;
                    default:
                        output += c;
                        break;
                }
            }
            return output;
        }

        string ParseRectangles(string input)
        {
            var output = string.Empty;
            for (int i = 0; i < input.Length; ++i)
            {
                var c = input[i];
                switch (c)
                {
                    case ' ':
                        break;
                    case '#':
                        while (input[i] != '@') i++;
                        break;
                    case ':':
                        output += ", ";
                        break;
                    case '\r':
                        output += ", ";
                        break;
                    case '\n':
                        break;
                    case ',':
                        output += ", ";
                        break;
                    case 'x':
                        output += ", ";
                        break;
                    default:
                        output += c;
                        break;
                }
            }
            return output;
        }

        string ParseGuardTimes(string input)
        {
            var output = string.Empty;
            var rows = SplitToRows(input);
            foreach (var row in rows)
            {
                string year = string.Empty;
                string month = string.Empty;
                string day = string.Empty;
                string hour = string.Empty;
                string minute = string.Empty;
                string action = string.Empty;
                for (int i = 1; i < row.Length; ++i)
                {
                    year += new String(new char[] { row[i], row[i + 1], row[i + 2], row[i + 3] });
                    i += 5;
                    month += new String(new char[] { row[i], row[i + 1] });
                    i += 3;
                    day += new String(new char[] { row[i], row[i + 1] });
                    i += 3;
                    hour += new String(new char[] { row[i], row[i + 1] });
                    i += 3;
                    minute += new String(new char[] { row[i], row[i + 1] });
                    i += 4;
                    while (i < row.Length) action += row[i++].ToString();
                }
                if (rows.IndexOf(row) == 0)
                    output += "\"" + year + "\", \"" + month + "\", \"" + day + "\", \"" + hour + "\", \"" + minute + "\", \"" + action;
                else
                    output += "\", \"" + year + "\", \"" + month + "\", \"" + day + "\", \"" + hour + "\", \"" + minute + "\", \"" + action;
            }
            output += "\"";
            return output;
        }

        string ParseSteps(string input)
        {
            var output = string.Empty;
            var rows = SplitToRows(input);
            foreach (var row in rows)
            {
                output += "\'" + row[5] + "\', ";
                output += "\'" + row[36] + "\', ";
            }
            output = output.Remove(output.Length - 2, 2);
            return output;
        }

        string ParseSpaceDividedNumbers(string input)
        {
            var output = input;
            output = output.Replace(" ", ", ");
            return output;
        }

        string ParseCoordinatesAndVelocities(string input)
        {
            var removed = input.Replace("position=", string.Empty);
            removed = removed.Replace("velocity=", string.Empty);
            removed = removed.Replace("\r\n", string.Empty);
            string output = string.Empty;
            foreach (var c in removed)
            {
                switch (c)
                {
                    case '<':break;
                    case '>': output += ", ";
                        break;
                    default:
                        {
                            output += c;
                        }
                        break;
                }
            }

            output = output.Remove(output.Length - 2, 2);
            return output;
        }

        string ParsePlantLife(string input)
        {
            var rows = SplitToRows(input);
            var initialstate = rows.Find(r => r[0] == 'i').Remove(0, 15);
            var isAsList = new List<string>();
            for (int i = 0; i < initialstate.Length; ++i)
            {
                isAsList.Add((initialstate[i] == '#' ? "1" : "0") + string.Empty);
            }

            rows.RemoveAt(0);
            rows.RemoveAt(0);
            rows.RemoveAll(r => r.Length < 8);
            rows.RemoveAll(r => r[9] != '#');
            for (int i = 0; i < rows.Count; ++i)
            {
                rows[i] = rows[i].Substring(0, 5);
                //rows[i] = rows[i].Replace(" ", string.Empty).Replace("=", string.Empty).Replace(">", string.Empty);
            }
            var output = string.Empty;
            foreach (var s in isAsList)
            {
                output += s + ", ";
            }
            output = output.Remove(output.Length - 2, 2);
            output += "\r\n";
            foreach (var row in rows)
            {
                foreach (var c in row)
                {
                    output += (c == '#' ? "1" : "0") + ", ";
                }
            }
            output = output.Remove(output.Length - 2, 2);
            return output;
        }

        string ParseTracks(string input)
        {
            return input.Replace("\r", string.Empty).Replace("\n", "!");//.Replace("\", "a");
        }


        string ParseMap(string input)
        {
            return input.Replace("\r", string.Empty).Replace("\n", "!");
        }

        string ParseInstructions(string input, int set)
        {
            var output1 = string.Empty;
            var rows = SplitToRows(input);
            rows.RemoveAll(r => r.Length == 0);
            int i;
            for (i = 0; i < rows.Count; i += 3)
            {
                if (rows[i][0] != 'B')
                {
                    break;
                }
                var firstNumber = rows[i].IndexOf('[') + 1;
                var until = rows[i].IndexOf(']');
                var before = rows[i].Substring(firstNumber, until - firstNumber).Replace(" ", string.Empty) + ",";

                var instructionInput = rows[i + 1].Replace(" ", ",") + ",";

                firstNumber = rows[i + 2].IndexOf('[') + 1;
                until = rows[i + 2].IndexOf(']');
                var after = rows[i + 2].Substring(firstNumber, until - firstNumber).Replace(" ", string.Empty);

                output1 += before.Replace(" ", string.Empty);
                output1 += instructionInput;
                output1 += after.Replace(" ", string.Empty);
                output1 += string.Empty + '!';
            }
            output1 = output1.Remove(output1.Length - 1, 1);
            var output2 = string.Empty;
            for (; i < rows.Count; ++i)
            {
                var row = rows[i].Split(' ');
                foreach (var n in row) output2 += n + ",";
                output2 += "!";
            }
            output2 = output2.Remove(output2.Length - 1, 1);
            return set == 1 ? output1 : output2;
        }

        string ParseRows(string input)
        {
            var output = string.Empty;
            var rows = SplitToRows(input);
            foreach (var r in rows)
            {
                output += r.Trim() + "!";
            }
            output = output.Remove(output.Length - 1, 1);
            return output;
            //return input.Replace('\r', '!').Replace("\n", string.Empty);
        }

        string ParseClayCoordinates(string input)
        {
            var output = string.Empty;
            var rows = SplitToRows(input);
            foreach (var row in rows)
            {
                if (row[0] == 'x')
                {
                    var start = row.IndexOf("x=") + 2;
                    var end = row.IndexOf(",");
                    var x = row.Substring(start, end - start);
                    start = row.IndexOf("y=") + 2;
                    end = row.IndexOf(".");
                    var yFrom = row.Substring(start, end - start);
                    var yTo = row.Substring(end + 2, row.Length - (end + 2));
                    output += x + "," + x + "," + yFrom + "," + yTo + "!";
                }
                else
                {
                    var start = row.IndexOf("y=") + 2;
                    var end = row.IndexOf(",");
                    var y = row.Substring(start, end - start);
                    start = row.IndexOf("x=") + 2;
                    end = row.IndexOf(".");
                    var xFrom = row.Substring(start, end - start);
                    var xTo = row.Substring(end + 2, row.Length - (end + 2));
                    output += xFrom + "," + xTo + "," + y + "," + y + "!";
                }
            }
            output = output.Substring(0, output.Length - 1);
            return output;
        }

        string ParseProgramStrip(string input)
        {
            var output = string.Empty;
            var rows = SplitToRows(input);
            foreach (var r in rows)
            {
                if (r.Length == 0) continue;
                if (r[0] == '#')
                {
                    output += r.Split(' ')[1] + "!";
                }
                else
                {
                    output += r.Replace(" ", ",") + "!";
                }
            }
            output = output.Substring(0, output.Length - 1);
            return output;
        }

        string ParseRobots(string input)
        {
            var output = string.Empty;
            var rows = SplitToRows(input);
            foreach (var r in rows)
            {
                var posStart = r.IndexOf("<") + 1;
                var posEnd = r.IndexOf(">");
                var radiusStart = r.IndexOf("r=") + 2;
                output += string.Empty + r.Substring(posStart, posEnd - posStart) + "," + r.Substring(radiusStart, r.Length - radiusStart) + "!";
            }
            output = output.Remove(output.Length - 1, 1);
            return output;
        }

        string ParseGroup(string input)
        {
            var output = string.Empty;
            var countStop = input.IndexOf(" units");
            var unitCount = input.Substring(0, countStop);
            var hpStart = input.IndexOf("with") + 5;
            var hpStop = input.IndexOf(" hit");
            var hp = input.Substring(hpStart, hpStop - hpStart);
            List<string> weakTypes = new List<string>();
            List<string> immuneTypes = new List<string>();
            var weakimmuneStart = input.IndexOf("(") + 1;
            if (weakimmuneStart > 0)
            {
                var weakimmuneStop = input.IndexOf(")");
                var weakImmune = input.Substring(weakimmuneStart, weakimmuneStop - weakimmuneStart).Split(';');
                for (int i = 0; i < weakImmune.Length; ++i) weakImmune[i] = weakImmune[i].Trim(' ');
                var weak = weakImmune.Where(s => s.Substring(0, 4) == "weak").ToList();
                if (weak.Count > 0)
                {
                    weak[0] = weak[0].Remove(0, 8);
                    weak[0] = weak[0].Replace(" ", string.Empty);
                    weakTypes = weak[0].Split(',').ToList();
                }
                var immune = weakImmune.Where(s => s.Substring(0, 6) == "immune").ToList();
                if (immune.Count > 0)
                {
                    immune[0] = immune[0].Remove(0, 10);
                    immune[0] = immune[0].Replace(" ", string.Empty);
                    immuneTypes = immune[0].Split(',').ToList();
                }
            }
            var attackPowerStart = input.IndexOf("does") + 5;
            var attackPowerStop = input.Substring(attackPowerStart, input.Length - attackPowerStart).IndexOf(" ") + attackPowerStart;
            var attackPower = input.Substring(attackPowerStart, attackPowerStop - attackPowerStart);
            var damageTypeStart = input.Substring(attackPowerStart, input.Length - attackPowerStart).IndexOf(" ") + 1 + attackPowerStart;
            var damageTypeStop = input.Substring(damageTypeStart, input.Length - damageTypeStart).IndexOf(" ") + damageTypeStart;
            var damageType = input.Substring(damageTypeStart, damageTypeStop - damageTypeStart);
            var initiativeStart = input.IndexOf("initiative") + 11;
            var initiative = input.Substring(initiativeStart, input.Length - initiativeStart);
            output += initiative + "|" + unitCount + "|" + hp + "|" + attackPower + "|" + damageType + "|";
            foreach (var it in immuneTypes) output += it + ",";
            if (output[output.Length - 1] == ',') output = output.Remove(output.Length - 1, 1);
            output += ";";
            foreach (var wt in weakTypes) output += wt + ",";
            if (output[output.Length - 1] == ',') output = output.Remove(output.Length - 1, 1);
            return output;
        }
        string ParseGroups(string input)
        {
            var output = string.Empty;
            var rows = SplitToRows(input);
            rows.RemoveAll(r => string.IsNullOrWhiteSpace(r));
            var start1 = rows.FindIndex(r => r.Substring(0, 2) == "Im") + 1;
            var start2 = rows.FindIndex(r => r.Substring(0, 2) == "In") + 1;
            var immuneSystemCount = 0;
            for (int i = start1; i < start2 - 1; ++i)
            {
                var r = rows[i];
                immuneSystemCount++;
                output += ParseGroup(r);
                output += "!";
            }
            for (int i = start2; i < rows.Count; ++i)
            {
                var r = rows[i];
                output += ParseGroup(r);
                output += "!";
            }
            output = output.Remove(output.Length - 1, 1);
            output = output.Insert(0, immuneSystemCount.ToString() + "!");
            return output;
        }

        private void Form1_DragDrop(object sender, DragEventArgs e)
        {
            var input = e.Data.GetData(DataFormats.Text).ToString();
            var output = ParseRobots(input);

            textBox1.Text = output;
        }
    }
}
