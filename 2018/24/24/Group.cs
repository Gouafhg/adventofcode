﻿using System;
using System.Collections.Generic;

namespace _24
{
    public enum DamageType { Fire, Bludgeoning, Radiation, Cold, Slashing }
    public enum GroupType { ImmuneSystem, Infection }

    public class Group
    {
        public int id;
        public GroupType groupType;
        public int Initiative;
        public int UnitCount;
        public int UnitHP;
        public int AttackPower;
        public DamageType damageType;
        public HashSet<DamageType> immune;
        public HashSet<DamageType> weak;

        public int EffectivePower
        {
            get
            {
                return AttackPower * UnitCount;
            }
        }

        public Group()
        {
            groupType = GroupType.ImmuneSystem;
            Initiative = UnitCount = UnitHP = AttackPower = 0;
            damageType = DamageType.Slashing;
            immune = new HashSet<DamageType>();
            weak = new HashSet<DamageType>();
        }

        public override string ToString()
        {
            var result = UnitCount + " units each with " + UnitHP + " hit points";
            if (weak.Count > 0 || immune.Count > 0) result += " (";
            if (weak.Count > 0)
            {
                result += " weak to ";
                foreach (var weakness in weak) result += weakness.ToString() + ", ";
                result = result.Remove(result.Length - 2, 2);
            }
            if (weak.Count > 0 && immune.Count > 0) result += "; ";
            if (immune.Count > 0)
            {
                result += " immune to ";
                foreach (var immunity in immune) result += immunity.ToString() + ", ";
                result = result.Remove(result.Length - 2, 2);
            }
            if (weak.Count > 0 || immune.Count > 0) result += ") ";
            result += "with an attack that does " + AttackPower + " " + damageType.ToString() + " damage at initiative " + Initiative;
            return result;

            //return groupType.ToString() + " group " + id;
            /*
            var result = groupType.ToString() + ", " + EffectivePower + ", " + Initiative + " (";
            foreach (var i in immune) result += i.ToString() + ", ";
            if (result[result.Length - 2] == ',') result.Remove(result.Length - 2, 2);
            result += "; ";
            foreach (var i in weak) result += i.ToString() + ", ";
            if (result[result.Length - 2] == ',') result.Remove(result.Length - 2, 2);
            result += ")";
            return result;
            */
        }

        public void ReadGroup(string input)
        {
            var gData = input.Split('|');
            Initiative = int.Parse(gData[0]);
            UnitCount = int.Parse(gData[1]);
            UnitHP = int.Parse(gData[2]);
            AttackPower = int.Parse(gData[3]);
            damageType = (DamageType)Enum.Parse(typeof(DamageType), gData[4].Substring(0, 1).ToUpper() + gData[4].Substring(1, gData[4].Length - 1));
            var weakImmune = gData[5].Split(';');
            if (weakImmune[0].Length > 0)
            {
                var types = weakImmune[0].Split(',');
                foreach (var type in types)
                {
                    immune.Add((DamageType)Enum.Parse(typeof(DamageType), type.Substring(0, 1).ToUpper() + type.Substring(1, type.Length - 1)));
                }
            }
            if (weakImmune[1].Length > 0)
            {
                var types = weakImmune[1].Split(',');
                foreach (var type in types)
                {
                    weak.Add((DamageType)Enum.Parse(typeof(DamageType), type.Substring(0, 1).ToUpper() + type.Substring(1, type.Length - 1)));
                }
            }
        }

        public int CalculateDamage(Group b)
        {
            var result = EffectivePower;
            if (b.weak.Contains(damageType)) result *= 2;
            if (b.immune.Contains(damageType)) result *= 0;
            return result;
        }

        public int AttackGroup(Group b)
        {
            var damage = CalculateDamage(b);
            var deadUnits = Math.Min(damage / b.UnitHP, b.UnitCount);
            b.UnitCount -= deadUnits;
            b.CheckDeath();
            /*
            Console.WriteLine(
                (groupType == GroupType.ImmuneSystem ? "Immune System " : "Infection ") + "group " + id + " attacks " +
                (b.groupType == GroupType.ImmuneSystem ? "Immune System " : "Infection ") + "group " + b.id + " for " + damage + " damage, killing " + deadUnits + " units");
            */
            return deadUnits;
        }

        public void CheckDeath()
        {
            if (UnitCount < 1)
            {
                Program.toRemove.Add(this);
            }
        }
    }
}
