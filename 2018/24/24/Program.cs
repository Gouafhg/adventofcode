﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _24
{
    class Program
    {
        public static List<Group> ImmuneSystem;
        public static List<Group> Infection;
        public static HashSet<Group> toRemove;
        public static HashSet<Group> deadGroups;

        static Program()
        {
            ImmuneSystem = new List<Group>();
            Infection = new List<Group>();
            toRemove = new HashSet<Group>();
            deadGroups = new HashSet<Group>();
        }

        static void ReadGroups(string input)
        {
            ImmuneSystem.Clear();
            Infection.Clear();

            var data = input.Split('!');

            var immuneUnitCount = int.Parse(data[0]);
            for (int i = 1; i < immuneUnitCount + 1; ++i)
            {
                var group = new Group();
                group.ReadGroup(data[i]);
                group.groupType = GroupType.ImmuneSystem;
                group.id = ImmuneSystem.Count + 1;
                ImmuneSystem.Add(group);
            }
            for (int i = immuneUnitCount + 1; i < data.Length; ++i)
            {
                var group = new Group();
                group.ReadGroup(data[i]);
                group.groupType = GroupType.Infection;
                group.id = Infection.Count + 1;
                Infection.Add(group);
            }
        }

        static void Main(string[] args)
        {
            var inputTest = "2!2|17|5390|4507|fire|;radiation,bludgeoning!3|989|1274|25|slashing|fire;bludgeoning,slashing!1|801|4706|116|bludgeoning|;radiation!4|4485|2961|12|slashing|radiation;fire,cold";
            var inputReal = "10!12|2728|5703|18|cold|;fire!20|916|5535|55|slashing|;bludgeoning!8|2255|7442|31|bludgeoning|;radiation!9|112|4951|360|fire|cold;!4|7376|6574|7|bludgeoning|cold,slashing,fire;!6|77|5884|738|radiation|;slashing!19|6601|8652|11|fire|;fire,cold!13|3259|10067|29|cold|;bludgeoning!3|2033|4054|18|slashing|cold;fire,slashing!11|3109|3593|9|bludgeoning|;!7|1466|57281|58|slashing|;slashing,fire!15|247|13627|108|fire|;!14|1298|41570|63|fire|fire,bludgeoning;!5|2161|40187|33|slashing|;fire!17|57|55432|1687|radiation|;cold!10|3537|24220|11|fire|;cold!18|339|44733|258|cold|cold,bludgeoning;radiation,fire!2|1140|17741|25|fire|fire,slashing;bludgeoning!16|112|44488|749|radiation|cold;bludgeoning,radiation!1|2918|36170|24|radiation|bludgeoning;slashing,cold";

            var input = inputReal;

            var immuneBoost = 0;
            var winningTeam = -1;
            do
            {
                ReadGroups(input);
                foreach (var g in ImmuneSystem) g.AttackPower += immuneBoost;
                /*
                Console.WriteLine("Immune System:");
                foreach (var g in ImmuneSystem)
                    Console.WriteLine(g.ToString());
                Console.WriteLine("Infection:");
                foreach (var g in Infection)
                    Console.WriteLine(g.ToString());
                Console.ReadKey();
                */

                deadGroups.Clear();
                int deadUnits;
                do
                {
                    deadUnits = 0;
                    var groups = new List<Group>();
                    groups.AddRange(ImmuneSystem);
                    groups.AddRange(Infection);
                    groups = groups.OrderByDescending(g => g.EffectivePower).ThenByDescending(g => g.Initiative).ToList();
                    Dictionary<Group, Group> targets = new Dictionary<Group, Group>();
                    foreach (var a in groups)
                    {
                        var enemies =
                            a.groupType == GroupType.ImmuneSystem ?
                                Infection.Where(b => !targets.Values.Contains(b)).
                                OrderByDescending(b => a.CalculateDamage(b)).
                                ThenByDescending(b => b.EffectivePower).
                                ThenByDescending(b => b.Initiative) :
                            ImmuneSystem.Where(b => !targets.Values.Contains(b)).
                                OrderByDescending(b => a.CalculateDamage(b)).
                                ThenByDescending(b => b.EffectivePower).
                                ThenByDescending(b => b.Initiative);
                        if (enemies.Count() > 0)
                        {
                            Group target = enemies.First();
                            if (a.CalculateDamage(target) > 0) targets.Add(a, target); else targets.Add(a, null);
                        }
                        else
                        {
                            targets.Add(a, null);
                        }
                    }
                    groups = groups.OrderByDescending(g => g.Initiative).ToList();
                    foreach (var a in groups)
                    {
                        if (toRemove.Contains(a)) continue;
                        if (targets[a] != null)
                        {
                            deadUnits += a.AttackGroup(targets[a]);
                        }
                    }

                    ImmuneSystem.RemoveAll(g => toRemove.Contains(g));
                    Infection.RemoveAll(g => toRemove.Contains(g));
                    deadGroups.UnionWith(toRemove);
                    toRemove.Clear();
                    //Console.ReadKey();
                    //Console.WriteLine();
                } while (ImmuneSystem.Count > 0 && Infection.Count > 0 && deadUnits != 0);

                winningTeam = Infection.Count == 0 ? 0 : ImmuneSystem.Count == 0 ? 1 : 2;
                if (winningTeam == 1)
                {
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine("At Immuneboost " + immuneBoost.ToString().PadRight(10) + " the infection won");
                    immuneBoost++;
                }
                if (winningTeam == 2)
                {
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine("At Immuneboost " + immuneBoost.ToString().PadRight(10) + " there was a draw");
                    immuneBoost++;
                }
            } while (winningTeam != 0);

            var winningUnits = 0;
            Console.WriteLine("Immune System:");
            if (ImmuneSystem.Count == 0)
            {
                Console.WriteLine("No groups remain");
            }
            else
            {
                winningUnits = ImmuneSystem.Sum(g => g.UnitCount);
                foreach (var g in ImmuneSystem)
                {
                    Console.WriteLine("Group " + g.id + " contains " + g.UnitCount + " units");
                }
            }
            Console.WriteLine("Infection:");
            if (Infection.Count == 0)
            {
                Console.WriteLine("No groups remain");
            }
            else
            {
                winningUnits = Infection.Sum(g => g.UnitCount);
                foreach (var g in Infection)
                {
                    Console.WriteLine("Group " + g.id + " contains " + g.UnitCount + " units");
                }
            }

            Console.WriteLine();
            Console.WriteLine("The winning army has " + winningUnits + " units");

            Console.ReadKey();
        }
    }
}
