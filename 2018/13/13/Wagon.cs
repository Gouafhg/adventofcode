﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _13
{
    public class Wagon
    {
        public int x, y;
        public Direction dir;
        public Turn nextTurn;
        public TrackPiece currentPiece;

        public Wagon()
        {
            x = y = 0;
            dir = Direction.up;
            nextTurn = Turn.left;
            currentPiece = null;
        }

        public Wagon(int x, int y, Direction dir)
        {
            this.x = x;
            this.y = y;
            this.dir = dir;
            nextTurn = Turn.left;
            currentPiece = null;
        }

        public Direction TurnWagon(Direction d, Turn t)
        {
            if (t == Turn.left)
            {
                return d == Direction.up ? Direction.left : d == Direction.left ? Direction.down : d == Direction.down ? Direction.right : Direction.up;
            }
            else if (t == Turn.right)
            {
                return d == Direction.up ? Direction.right : d == Direction.right ? Direction.down : d == Direction.down ? Direction.left : Direction.up;
            }
            return d;
        }

        public bool DoTick(out Wagon CrashingWagon)
        {
            currentPiece.wagon = null;
            if (currentPiece.isIntersection)
            {
                dir = TurnWagon(dir, nextTurn);
                nextTurn = (Turn)(((int)nextTurn + 1) % 3);
            }

            if (!currentPiece.openDirections.Contains(dir))
            {
                var opposite = (Direction)(((int)dir + 2) % 4);
                dir = currentPiece.openDirections.Find(d => d != opposite);
            }

            switch (dir)
            {
                case Direction.up:
                    currentPiece = currentPiece.upPiece;
                    y--;
                    break;
                case Direction.down:
                    currentPiece = currentPiece.downPiece;
                    y++;
                    break;
                case Direction.left:
                    currentPiece = currentPiece.leftPiece;
                    x--;
                    break;
                case Direction.right:
                    currentPiece = currentPiece.rightPiece;
                    x++;
                    break;
            }

            if (currentPiece.wagon != null)
            {
                CrashingWagon = currentPiece.wagon;
                currentPiece.wagon = null;
                return true;
            }
            else
            {
                CrashingWagon = null;
                currentPiece.wagon = this;
                return false;
            }
        }

        public override string ToString()
        {
            switch (dir)
            {
                case Direction.up: return "^";
                case Direction.left: return "<";
                case Direction.down: return "v";
                default: return ">";
            }
        }
    }
}
