﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _13
{
    public class TrackPiece
    {
        public int x, y;
        public List<Direction> openDirections;
        public Wagon wagon;
        public bool isIntersection;

        public TrackPiece upPiece;
        public TrackPiece leftPiece;
        public TrackPiece downPiece;
        public TrackPiece rightPiece;

        public TrackPiece()
        {
            x = y = 0;
            openDirections = new List<Direction>();
            wagon = null;
            isIntersection = false;
        }

        public TrackPiece(int x, int y)
        {
            this.x = x;
            this.y = y;
            openDirections = new List<Direction>();
            wagon = null;
            isIntersection = false;
        }

        public override string ToString()
        {
            if (upPiece != null)
            {
                if (downPiece != null)
                {
                    if (leftPiece == null)
                        return "|";
                    else
                        return "+";
                }
                else if (leftPiece != null)
                {
                    return "/";
                }
                else
                {
                    return "\\";
                }
            }
            else if (leftPiece != null)
            {
                if (rightPiece != null)
                {
                    return "-";
                }
                else
                {
                    return "\\";
                }
            }
            else if (rightPiece != null)
            {
                return "/";
            }
            return "E";
        }
    }
}
