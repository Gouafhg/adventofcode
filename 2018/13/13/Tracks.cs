﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _13
{
    public enum Direction { up, left, down, right }
    public enum Turn { left, straight, right }

    public class Tracks
    {
        public List<Wagon> wagons;
        public List<TrackPiece> tracks;
        public List<Crash> crashes;

        public int width, height;

        public Tracks()
        {
            width = 0;
            height = 0;
            wagons = new List<Wagon>();
            tracks = new List<TrackPiece>();
            crashes = new List<Crash>();
        }

        public Tracks(List<string> rows)
        {
            wagons = new List<Wagon>();
            tracks = new List<TrackPiece>();
            crashes = new List<Crash>();
            ReadTracks(rows);
        }

        public void PrintState()
        {
            if (Program.printTracks)
            {
                foreach (var t in tracks)
                {
                    if (t.x >= Console.WindowWidth - 1 || t.y >= Console.WindowHeight - 1)
                        continue;
                    Console.SetCursorPosition(t.x, t.y);
                    Console.Write(t.ToString());
                }
            }
            if (Program.printCrashes)
            {
                foreach (var c in crashes)
                {
                    if (c.x >= Console.WindowWidth - 1 || c.y >= Console.WindowHeight - 1)
                        continue;
                    Console.SetCursorPosition(c.x, c.y);
                    Console.Write(c.ToString());
                }
            }
            foreach (Wagon w in wagons)
            {
                if (w.x >= Console.WindowWidth - 1 || w.y >= Console.WindowHeight - 1)
                    continue;

                Console.SetCursorPosition(w.x, w.y);
                Console.Write(w.ToString());
                if (Program.printAroundTracks)
                {
                    var cp = w.currentPiece;
                    if (cp.upPiece != null)
                    {
                        if (cp.upPiece.x < Console.WindowWidth - 1 && cp.upPiece.y < Console.WindowHeight - 1)
                        {
                            Console.SetCursorPosition(cp.upPiece.x, cp.upPiece.y);
                            Console.Write(cp.upPiece.ToString());
                        }
                    }
                    if (cp.leftPiece != null)
                    {
                        if (cp.leftPiece.x < Console.WindowWidth - 1 && cp.leftPiece.y < Console.WindowHeight - 1)
                        {
                            Console.SetCursorPosition(cp.leftPiece.x, cp.leftPiece.y);
                            Console.Write(cp.leftPiece.ToString());
                        }
                    }
                    if (cp.downPiece != null)
                    {
                        if (cp.downPiece.x < Console.WindowWidth - 1 && cp.downPiece.y < Console.WindowHeight - 1)
                        {
                            Console.SetCursorPosition(cp.downPiece.x, cp.downPiece.y);
                            Console.Write(cp.downPiece.ToString());
                        }
                    }
                    if (cp.rightPiece != null)
                    {
                        if (cp.rightPiece.x < Console.WindowWidth - 1 && cp.rightPiece.y < Console.WindowHeight - 1)
                        {
                            Console.SetCursorPosition(cp.rightPiece.x, cp.rightPiece.y);
                            Console.Write(cp.rightPiece.ToString());
                        }
                    }
                }
            }
        }

        public void ReadTracks(List<string> rows)
        {
            height = rows.Count;
            width = rows[0].Length;

            for (int y = 0; y < rows.Count; ++y)
            {
                var row = rows[y];
                for (int x = 0; x < row.Length; ++x)
                {
                    if (row[x] != ' ')
                    {
                        tracks.Add(new TrackPiece(x, y));
                    }
                }
            }
            for (int y = 0; y < rows.Count; ++y)
            {
                var row = rows[y];
                for (int x = 0; x < row.Length; ++x)
                {
                    if (row[x] != ' ')
                    {
                        var piece = tracks.Find(t => t.x == x && t.y == y);
                        var upPiece = tracks.Find(t => t.x == x && t.y == y - 1);
                        var leftPiece = tracks.Find(t => t.x == x - 1 && t.y == y);
                        var downPiece = tracks.Find(t => t.x == x && t.y == y + 1);
                        var rightPiece = tracks.Find(t => t.x == x + 1 && t.y == y);

                        switch (row[x])
                        {
                            case '-':
                                piece.openDirections.Add(Direction.left);
                                piece.leftPiece = leftPiece;
                                piece.openDirections.Add(Direction.right);
                                piece.rightPiece = rightPiece;
                                break;
                            case '|':
                                piece.openDirections.Add(Direction.up);
                                piece.upPiece = upPiece;
                                piece.openDirections.Add(Direction.down);
                                piece.downPiece = downPiece;
                                break;
                            case '/':
                                if (upPiece != null && rows[y - 1][x] != '-' && rows[y - 1][x] != '\\' && rows[y - 1][x] != '/')
                                {
                                    piece.openDirections.Add(Direction.up);
                                    piece.upPiece = upPiece;
                                    piece.openDirections.Add(Direction.left);
                                    piece.leftPiece = leftPiece;
                                }
                                else
                                {
                                    piece.openDirections.Add(Direction.down);
                                    piece.downPiece = downPiece;
                                    piece.openDirections.Add(Direction.right);
                                    piece.rightPiece = rightPiece;
                                }
                                break;
                            case '\\':
                                if (upPiece != null && rows[y - 1][x] != '-' && rows[y - 1][x] != '\\' && rows[y - 1][x] != '/')
                                {
                                    piece.openDirections.Add(Direction.up);
                                    piece.upPiece = upPiece;
                                    piece.openDirections.Add(Direction.right);
                                    piece.rightPiece = rightPiece;
                                }
                                else
                                {
                                    piece.openDirections.Add(Direction.down);
                                    piece.downPiece = downPiece;
                                    piece.openDirections.Add(Direction.left);
                                    piece.leftPiece = leftPiece;
                                }
                                break;
                            case '+':
                                piece.isIntersection = true;
                                piece.openDirections.Add(Direction.up);
                                piece.upPiece = upPiece;
                                piece.openDirections.Add(Direction.left);
                                piece.leftPiece = leftPiece;
                                piece.openDirections.Add(Direction.down);
                                piece.downPiece = downPiece;
                                piece.openDirections.Add(Direction.right);
                                piece.rightPiece = rightPiece;
                                break;
                            default:
                                if (upPiece != null)
                                {
                                    piece.openDirections.Add(Direction.up);
                                    piece.upPiece = upPiece;
                                }
                                if (leftPiece != null)
                                {
                                    piece.openDirections.Add(Direction.left);
                                    piece.leftPiece = leftPiece;
                                }
                                if (downPiece != null)
                                {
                                    piece.openDirections.Add(Direction.down);
                                    piece.downPiece = downPiece;
                                }
                                if (rightPiece != null)
                                {
                                    piece.openDirections.Add(Direction.right);
                                    piece.rightPiece = rightPiece;
                                }
                                break;
                        }
                    }
                }
            }
            for (int y = 0; y < rows.Count; ++y)
            {
                var row = rows[y];
                for (int x = 0; x < row.Length; ++x)
                {
                    Wagon wagon = null;
                    if (row[x] == '^')
                    {
                        wagon = new Wagon(x, y, Direction.up);
                    }
                    else if (row[x] == '<')
                    {
                        wagon = new Wagon(x, y, Direction.left);
                    }
                    else if (row[x] == 'v')
                    {
                        wagon = new Wagon(x, y, Direction.down);
                    }
                    else if (row[x] == '>')
                    {
                        wagon = new Wagon(x, y, Direction.right);
                    }
                    if (wagon != null)
                    {
                        wagon.currentPiece = tracks.Find(t => t.x == x && t.y == y);
                        wagons.Add(wagon);
                    }
                }
            }
            wagons = wagons.OrderBy(w => w.y * width + w.x).ToList();
        }

        public void DoTick()
        {
            var wagonsToRemove = new HashSet<Wagon>();
            foreach (var w in wagons)
            {
                if (wagonsToRemove.Contains(w))
                {
                    continue;
                }
                Wagon crashingWagon;
                if (w.DoTick(out crashingWagon))
                {
                    wagonsToRemove.Add(w);
                    wagonsToRemove.Add(crashingWagon);
                    crashes.Add(new Crash(w.x, w.y, w, crashingWagon));
                }
                /*
                if (wagons.FindAll(wagon => !wagonsToRemove.Contains(wagon)).Count < 2)
                {
                    break;
                }
                */
            }
            wagons.RemoveAll(w => wagonsToRemove.Contains(w));
            wagons = wagons.OrderBy(w => w.y * width + w.x).ToList();
        }
    }
}
