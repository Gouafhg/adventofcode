﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _13
{
    public class Crash
    {
        public int x, y;
        public Wagon w1, w2;

        public Crash()
        {
            x = y = 0;
            w1 = w2 = null;
        }

        public Crash(int x, int y, Wagon w1, Wagon w2)
        {
            this.x = x;
            this.y = y;
            this.w1 = w1;
            this.w2 = w2;
        }

        public override string ToString()
        {
            return "X";
        }
    }
}
