﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12
{
    class Program
    {
        public static bool findRule(ref List<int> state, int startIndex, ref List<int[]> rules)
        {
            foreach (var rule in rules)
            {
                if (state[startIndex] == rule[0] && state[startIndex + 1] == rule[1] && state[startIndex + 2] == rule[2] && state[startIndex + 3] == rule[3] && state[startIndex + 4] == rule[4])
                {
                    return true;
                }
            }
            return false;
        }

        public static long CalculateChecksum(List<int> state, int offset)
        {
            long checksum = 0;
            for (int i = 0; i < state.Count; ++i)
            {
                if (state[i] == 1)
                {
                    checksum += i + offset;
                }
            }
            return checksum;
        }
        /*
        static bool runBreak = true;
        static bool threadLock = false;

        protected static void DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker sendingWorker = (BackgroundWorker)sender;//Capture the BackgroundWorker that fired the event
            object[] arrObjects = (object[])e.Argument;//Collect the array of objects the we received from the main thread


            var indicesToRun = (List<bool>)arrObjects[0];
            var indicesDone = (List<bool>)arrObjects[1];
            var rules = (List<int[]>)arrObjects[2];
            var currentState = (List<int>)arrObjects[3];
            var nextState = (List<int>)arrObjects[4];

            while (true)
            {
                while (runBreak) { Console.Write("|"); System.Threading.Thread.Sleep(1); if (sendingWorker.CancellationPending) break; }
                while (threadLock && !sendingWorker.CancellationPending) { Console.Write("|"); System.Threading.Thread.Sleep(1); }
                threadLock = true;
                int indexToCheck = -1;
                while (!sendingWorker.CancellationPending && (indexToCheck = indicesToRun.FindIndex(b => b)) == -1) { Console.Write("-"); System.Threading.Thread.Sleep(1); }
                threadLock = false;
                if (sendingWorker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                indicesToRun[indexToCheck] = false;


                var foundRule = findRule(ref currentState, indexToCheck, ref rules);

                if (foundRule)
                {
                    nextState[indexToCheck] = 1;
                    if (indexToCheck < 4)
                    {
                        addPrevDots++;
                    }
                    if (indexToCheck > currentState.Count - 5)
                    {
                        addPostDots++;
                    }
                }
                else
                {
                    nextState[indexToCheck] = 0;
                }
                indicesDone[indexToCheck] = true;
            }
        }
        */

        static ulong stepCount = 50000;
        static ulong step = 0;

        static int addPrevDots;
        static int addPostDots;

        static void Main(string[] args)
        {
            var startPot = 0;
            var initialState = new List<int>() { 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1 };
            var ruleString = new int[] { 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1 };
            var rules = new List<int[]>();
            for (int i = 0; i < ruleString.Length; i += 5)
            {
                var rule = new int[5];
                rule[0] = ruleString[i];
                rule[1] = ruleString[i + 1];
                rule[2] = ruleString[i + 2];
                rule[3] = ruleString[i + 3];
                rule[4] = ruleString[i + 4];
                rules.Add(rule);
            }

            /*
            List<BackgroundWorker> threads = new List<BackgroundWorker>();
            foreach (var r in rules)
            {
                var bw = new BackgroundWorker();
                bw.DoWork += new DoWorkEventHandler(DoWork);
                bw.WorkerSupportsCancellation = true;
                threads.Add(bw);
            }
            */

            startPot -= 40;
            for (int i = startPot; i < 0; ++i)
                initialState.Insert(0, 0);
            for (int i = startPot; i < 0; ++i)
                initialState.Add(0);

            var currentState = new List<int>();
            currentState.AddRange(initialState);
            var nextState = new List<int>();

            var indicesToRun = new List<bool>();
            var indicesDone = new List<bool>();

            //var objectArr = new object[] { indicesToRun, indicesDone, rules, currentState, nextState };
            //threads.ForEach(bw => bw.RunWorkerAsync(objectArr));

            long lastChecksum = 0;
            long currentChecksum = 0;
            for (step = 0; step < stepCount; ++step)
            {
                /*
                Console.Write((step < 10 ? " " : "") + step + ": ");
                currentState.ForEach(s => Console.Write(s == 1 ? "#" : "."));
                Console.WriteLine();
                */
                lastChecksum = currentChecksum;
                currentChecksum = CalculateChecksum(currentState, startPot);
                string stepString = step.ToString().PadRight(20);
                string checksumString = currentChecksum.ToString().PadRight(20);
                Console.WriteLine("Step: " + stepString + " Checksum: " + checksumString + " Delta: " + (currentChecksum - lastChecksum));
                Console.ReadKey();

                addPrevDots = 0;
                addPostDots = 0;
                nextState.Clear();
                nextState.AddRange(currentState);
                indicesToRun.Clear();
                for (int i = 0; i < currentState.Count; ++i) indicesToRun.Add((i < 2 || i > currentState.Count - 3) ? false : true);
                indicesDone.Clear();
                for (int i = 0; i < currentState.Count; ++i) indicesDone.Add(false);

                for (int i = 2; i < currentState.Count - 2; ++i)
                {
                    /*
                    runBreak = false;
                    while (indicesDone.Find(b => !b)) { }
                    runBreak = true;
                    */

                    var foundRule = findRule(ref currentState, i - 2, ref rules);
                    if (foundRule)
                    {
                        nextState[i] = 1;
                        if (i < 4)
                        {
                            addPrevDots++;
                        }
                        if (i > currentState.Count - 5)
                        {
                            addPostDots++;
                        }
                    }
                    else
                    {
                        nextState[i] = 0;
                    }
                }
                /*
                var firstPlant = nextState.FindIndex(i => i == 1);
                for (int i = 0; i < firstPlant - 5; ++i)
                {
                    startPot += 1;
                    nextState.RemoveAt(0);
                }
                var lastPlant = nextState.FindLastIndex(i => i == 1);
                for (int i = nextState.Count - 1; i > lastPlant + 5; i--)
                {
                    nextState.RemoveAt(nextState.Count - 1);
                }
                while (addPrevDots > 0)
                {
                    startPot -= 1;
                    nextState.Insert(0, 0);
                    addPrevDots--;
                }
                */
                while (addPostDots > 0)
                {
                    nextState.Add(0);
                    addPostDots--;
                }

                currentState.Clear();
                currentState.AddRange(nextState);
            }
            //threads.ForEach(bw => bw.CancelAsync());

            var output = string.Empty;
            nextState.ForEach(s => output += s == 1 ? "#" : ".");
            Console.WriteLine(step + ": " + output);

            var checksum = CalculateChecksum(currentState, startPot);
            Console.WriteLine();
            Console.WriteLine("Checksum: " + checksum);
            Console.ReadKey();


        }
    }
    /*
    class PlantRule
    {
        public bool ll, l, m, r, rr;
        public bool result;

        public PlantRule()
        {
            ll = l = m = r = rr = result = false;
        }

        public PlantRule(bool ll, bool l, bool r, bool rr)
    }
    */
}
