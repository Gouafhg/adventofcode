﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _15
{
    public class TileMap
    {
        public int w, h;
        public Tile[,] tiles;
        public List<Creature> creatures;
        public List<Creature> deadCreatures;
        public HashSet<Creature> elves;
        public HashSet<Creature> goblins;

        public HashSet<Creature> removeAtEndOfCombat;

        public TileMap()
        {
            w = h = 0;
            tiles = new Tile[0, 0];
            creatures = new List<Creature>();
            deadCreatures = new List<Creature>();
            elves = new HashSet<Creature>();
            goblins = new HashSet<Creature>();
            removeAtEndOfCombat = new HashSet<Creature>();
        }

        public TileMap(int w, int h)
        {
            this.w = w;
            this.h = h;
            tiles = new Tile[w, h];
            for (int y = 0; y < h; ++y)
                for (int x = 0; x < w; ++x)
                    tiles[x, y] = new Tile(this, x, y, false);
            creatures = new List<Creature>();
            deadCreatures = new List<Creature>();
            elves = new HashSet<Creature>();
            goblins = new HashSet<Creature>();
            removeAtEndOfCombat = new HashSet<Creature>();
        }

        public void AddCreature(Creature c)
        {
            creatures.Add(c);
            if (c.type == CreatureType.Elf) elves.Add(c);
            else goblins.Add(c);
        }

        internal void PrintMap(int offsetX, int offsetY)
        {
            foreach (var t in tiles)
            {
                Console.SetCursorPosition(t.x + offsetX, t.y + offsetY);
                Console.Write(t.closed ? "#" : ".");
            }
            foreach (var c in creatures)
            {
                Console.SetCursorPosition(c.x + offsetX, c.y + offsetY);
                Console.Write(c.type == CreatureType.Elf ? "E" : "G");
            }
            Console.WriteLine();
        }

        internal void PrintCreatureInfo(int offsetX, int offsetY)
        {
            var rows = 0;
            foreach (var c in creatures)
            {
                Console.SetCursorPosition(offsetX, rows + offsetY);
                Console.WriteLine(c.type.ToString().PadRight(11) + c.x.ToString().PadLeft(3) + ", " + c.y.ToString().PadRight(5) + c.hp);
                rows++;
            }
            Console.WriteLine();
        }

        public void ReadMap(string input)
        {
            var rows = input.Split('!');
            h = rows.Length;
            w = rows[0].Length;
            tiles = new Tile[w, h];
            creatures.Clear();
            deadCreatures.Clear();
            goblins.Clear();
            elves.Clear();
            removeAtEndOfCombat.Clear();

            for (int y = 0; y < h; ++y)
            {
                for (int x = 0; x < w; ++x)
                {
                    var c = rows[y][x];

                    tiles[x, y] = new Tile(this, x, y, false);
                    if (c == '#')
                    {
                        tiles[x, y].closed = true;
                    }
                    if (c == 'E')
                    {
                        var elf = new Creature(this, x, y);
                        elf.type = CreatureType.Elf;
                        creatures.Add(elf);
                        elves.Add(elf);
                    }
                    if (c == 'G')
                    {
                        var goblin = new Creature(this, x, y);
                        goblin.type = CreatureType.Goblin;
                        creatures.Add(goblin);
                        goblins.Add(goblin);
                    }
                }
            }
            for (int y = 0; y < h; ++y)
            {
                for (int x = 0; x < w; ++x)
                {
                    if (x > 0) tiles[x, y].neighbours.Add(tiles[x - 1, y]);
                    if (x < w - 1) tiles[x, y].neighbours.Add(tiles[x + 1, y]);
                    if (y > 0) tiles[x, y].neighbours.Add(tiles[x, y - 1]);
                    if (y < h - 1) tiles[x, y].neighbours.Add(tiles[x, y + 1]);
                    tiles[x, y].neighbours.RemoveWhere(t => t.closed);

                    var creature = creatures.Find(c => c.x == x && c.y == y);
                    if (creature != null)
                    {
                        creature.tile = tiles[x, y];
                        tiles[x, y].occupant = creature;
                    }
                }
            }
        }

        private int hDist(Tile a, Tile b) { return Math.Abs(a.x - b.x) + Math.Abs(a.y - b.y); }
        private List<Tile> reconstructPath(Dictionary<Tile, Tile> cameFrom, Tile current)
        {
            var totalPath = new List<Tile>();
            totalPath.Add(current);
            while (cameFrom.ContainsKey(current))
            {
                current = cameFrom[current];
                totalPath.Add(current);
            }
            return totalPath;
        }
        public List<Tile> FindPathAStar(Tile start, Tile destination)
        {
            HashSet<Tile> closedSet = new HashSet<Tile>();
            List<Tile> openSet = new List<Tile>();
            openSet.Add(start);
            Dictionary<Tile, Tile> cameFrom = new Dictionary<Tile, Tile>();
            Dictionary<Tile, int> gScore = new Dictionary<Tile, int>();
            gScore.Add(start, 0);
            Dictionary<Tile, int> fScore = new Dictionary<Tile, int>();
            fScore.Add(start, hDist(start, destination));
            while (openSet.Count > 0)
            {
                var current = openSet[0];
                if (current == destination)
                {
                    return reconstructPath(cameFrom, current);
                }
                openSet.Remove(current);
                closedSet.Add(current);

                var openNeigbours = current.neighbours.Where(t => !t.closed && !t.occupied);
                foreach (var t in openNeigbours)
                {
                    if (closedSet.Contains(t))
                    {
                        continue;
                    }
                    var tentative_gScore = gScore[current] + 1;
                    if (!openSet.Contains(t))
                    {
                        openSet.Add(t);
                    }
                    else if (tentative_gScore >= gScore[t])
                    {
                        continue;
                    }

                    if (!cameFrom.TryAdd(t, current))
                    {
                        cameFrom[t] = current;
                    }
                    if (!gScore.TryAdd(t, tentative_gScore))
                    {
                        gScore[t] = tentative_gScore;
                    }
                    var distanceToGoal = hDist(t, destination);
                    if (!fScore.TryAdd(t, gScore[t] + distanceToGoal));
                    {
                        fScore[t] = gScore[t] + distanceToGoal;
                    }
                }

                openSet = openSet.OrderBy(t => fScore[t]).ThenBy(t => t.Value).ToList();
            }

            return null;
        }

        public void SortCreatures()
        {
            creatures = creatures.OrderBy(c => c.tile.Value).ToList();
        }

        public void EndCombat()
        {
            deadCreatures.AddRange(removeAtEndOfCombat);
            creatures.RemoveAll(c => removeAtEndOfCombat.Contains(c));
            elves.RemoveWhere(c => removeAtEndOfCombat.Contains(c));
            goblins.RemoveWhere(c => removeAtEndOfCombat.Contains(c));

            foreach(var c in removeAtEndOfCombat)
            {
                c.tile.occupant = null;
            }
            removeAtEndOfCombat.Clear();
        }
    }
}
