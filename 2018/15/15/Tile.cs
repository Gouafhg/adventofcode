﻿using System.Collections.Generic;

namespace _15
{
    public class Tile
    {
        public TileMap map;
        public int x, y;
        public Creature occupant;
        public bool closed;
        public bool occupied
        {
            get { return occupant != null; }
        }

        public HashSet<Tile> neighbours;

        public Tile()
        {
            map = null;
            x = y = 0;
            occupant = null;
            closed = false;
            neighbours = new HashSet<Tile>();
        }

        public Tile(TileMap map, int x, int y, bool closed)
        {
            this.map = map;
            this.x = x;
            this.y = y;
            this.closed = closed;
            neighbours = new HashSet<Tile>();
        }

        public int Value
        {
            get
            {
                return y * map.w + x;
            }
        }

        public override string ToString()
        {
            if (closed)
            {
                return ("# : (" + x + ", " + y + ")");
            }
            else
            {
                return (". : (" + x + ", " + y + ")");
            }
        }
    }
}
