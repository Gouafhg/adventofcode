﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace _15
{
    class Program
    {
        static TileMap map;

        static Program()
        {
            map = new TileMap();
        }

        static bool print = false;
        static int printAfterRounds = 0;

        static void p1(string input)
        {
            map.ReadMap(input);

            if (printAfterRounds == 0 && print)
            {
                Console.Clear();
                Console.WriteLine("Initially");
                map.PrintMap(0, 2);
                Console.ReadKey();
            }

            var fullRounds = -1;
            var rounds = 0;
            while (fullRounds == -1)
            {
                map.SortCreatures();
                foreach (var creature in map.creatures)
                {
                    if (map.removeAtEndOfCombat.Contains(creature))
                    {
                        continue;
                    }

                    HashSet<Creature> enemies;
                    if (creature.type == CreatureType.Elf)
                    {
                        enemies = map.goblins;
                    }
                    else
                    {
                        enemies = map.elves;
                    }
                    if (enemies.Where(e => !map.removeAtEndOfCombat.Contains(e)).Count() == 0)
                    {
                        fullRounds = rounds;
                        break;
                    }

                    //Move
                    bool enemyClose = false;
                    foreach (var t in creature.tile.neighbours)
                    {
                        if (t.occupied && enemies.Contains(t.occupant))
                        {
                            enemyClose = true;
                        }
                    }
                    if (!enemyClose)
                    {
                        //A*
                        var paths = new Dictionary<Tile, List<List<Tile>>>();
                        var possibleSteps = creature.tile.neighbours.Where(t => !t.closed && !t.occupied);
                        foreach (var s in possibleSteps)
                        {
                            var livingEnemies = enemies.Where(e => !e.dead);
                            foreach (var e in livingEnemies)
                            {
                                var possibleTargets = e.tile.neighbours.Where(t => !t.closed && !t.occupied);
                                foreach (var d in possibleTargets)
                                {
                                    var p = map.FindPathAStar(s, d);
                                    if (p != null)
                                    {
                                        paths.TryAdd(d, new List<List<Tile>>());
                                        paths[d].Add(p);
                                    }
                                }
                            }
                        }
                        if (paths.Count > 0)
                        {
                            var allPaths = new List<List<Tile>>();
                            foreach (var k in paths.Keys) allPaths.AddRange(paths[k]);
                            var shortestPath = allPaths.OrderBy(p => p.Count).ThenBy(p => p[0].Value).ThenBy(p => p[p.Count - 1].Value).First();
                            shortestPath.Reverse();
                            creature.Move(shortestPath[0]);
                        }
                    }

                    //Attack
                    enemyClose = false;
                    foreach (var t in creature.tile.neighbours)
                    {
                        if (t.occupied && enemies.Contains(t.occupant))
                        {
                            enemyClose = true;
                        }
                    }
                    if (enemyClose)
                    {
                        var closeEnemies = creature.tile.neighbours.Where(t => t.occupied && !t.occupant.dead && enemies.Contains(t.occupant)).Select(t => t.occupant).OrderBy(e => e.hp).ThenBy(e => e.tile.Value).ToList();
                        if (closeEnemies.Count > 0)
                        {
                            creature.Attack(closeEnemies[0]);
                        }
                    }
                }

                map.SortCreatures();
                map.EndCombat();

                rounds++;

                if (rounds >= printAfterRounds && print && true)
                {
                    //Console.Clear();

                    //Console.SetCursorPosition(map.w + 5, 2);
                    var mittenChecksum = map.creatures.Sum(c => c.x + c.y * 12345);
                    mittenChecksum += map.deadCreatures.Sum(c => c.x + c.y * 12345);
                    Console.WriteLine((rounds - 1).ToString().PadRight(10) + mittenChecksum);
                    Console.ReadKey();
                }

                if (rounds >= printAfterRounds && print && false)
                {
                    Console.WriteLine("After " + rounds + " rounds");
                    map.PrintMap(0, 2);
                    map.PrintCreatureInfo(0, 2 + map.h + 2);
                    Console.ReadKey();
                }
            }

            var hpLeft = 0;
            foreach (var c in map.creatures)
            {
                hpLeft += c.hp;
            }
            var checksum = rounds * hpLeft;

            Console.Clear();
            Console.WriteLine("After " + fullRounds + " rounds");
            map.PrintMap(0, 2);
            map.PrintCreatureInfo(0, 2 + map.h + 2);

            Console.SetCursorPosition(0, 2 + map.h + 2 + map.creatures.Count + 3);
            Console.WriteLine("With " + map.elves.Count + " elves out of " + (map.creatures.FindAll(c => c.type == CreatureType.Elf).Count + map.deadCreatures.FindAll(c => c.type == CreatureType.Elf).Count) + " left");
            Console.WriteLine();
            Console.WriteLine("Combat ends after " + fullRounds + " full rounds");
            Console.Write(map.elves.Count == 0 ? "Goblins " : "Elves ");
            Console.WriteLine("win with " + hpLeft + " total hit points left");
            Console.WriteLine("Outcome: " + fullRounds + " * " + hpLeft + " = " + (fullRounds * hpLeft).ToString());
        }

        static void p2(string input)
        {
            FindLeastAttackPower(input);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Least attackpower found: " + Creature.ElfAttack);
            Console.WriteLine();
            Console.WriteLine();
            p1(input);
        }

        static int FindLeastAttackPower(string input)
        {
            int lowestAttackWithPerfectVictory = int.MaxValue;

            int elfCount;
            int elfAttackHigh = 200;
            int elfAttackLow = 0;
            Creature.ElfAttack = (elfAttackHigh + elfAttackLow) / 2;
            var fullRounds = -1;
            var rounds = 0;
            while (elfAttackHigh - elfAttackLow > 1)
            {
                map.ReadMap(input);
                elfCount = map.elves.Count;

                if (printAfterRounds == 0 && print)
                {
                    Console.Clear();
                    Console.WriteLine("Initially");
                    map.PrintMap(0, 2);
                    Console.ReadKey();
                }

                fullRounds = -1;
                rounds = 0;
                while (fullRounds == -1)
                {
                    map.SortCreatures();
                    foreach (var creature in map.creatures)
                    {
                        if (map.removeAtEndOfCombat.Contains(creature))
                        {
                            continue;
                        }

                        HashSet<Creature> enemies;
                        if (creature.type == CreatureType.Elf)
                        {
                            enemies = map.goblins;
                        }
                        else
                        {
                            enemies = map.elves;
                        }
                        if (enemies.Where(e => !map.removeAtEndOfCombat.Contains(e)).Count() == 0)
                        {
                            fullRounds = rounds;
                            break;
                        }

                        //Move
                        bool enemyClose = false;
                        foreach (var t in creature.tile.neighbours)
                        {
                            if (t.occupied && enemies.Contains(t.occupant))
                            {
                                enemyClose = true;
                            }
                        }
                        if (!enemyClose)
                        {
                            //A*
                            var paths = new Dictionary<Tile, List<List<Tile>>>();
                            var possibleSteps = creature.tile.neighbours.Where(t => !t.closed && !t.occupied);
                            foreach (var s in possibleSteps)
                            {
                                var livingEnemies = enemies.Where(e => !e.dead);
                                foreach (var e in livingEnemies)
                                {
                                    var possibleTargets = e.tile.neighbours.Where(t => !t.closed && !t.occupied);
                                    foreach (var d in possibleTargets)
                                    {
                                        var p = map.FindPathAStar(s, d);
                                        if (p != null)
                                        {
                                            paths.TryAdd(d, new List<List<Tile>>());
                                            paths[d].Add(p);
                                        }
                                    }
                                }
                            }
                            if (paths.Count > 0)
                            {
                                var allPaths = new List<List<Tile>>();
                                foreach (var k in paths.Keys) allPaths.AddRange(paths[k]);
                                var shortestPath = allPaths.OrderBy(p => p.Count).ThenBy(p => p[0].Value).ThenBy(p => p[p.Count - 1].Value).First();
                                shortestPath.Reverse();
                                creature.Move(shortestPath[0]);
                            }
                        }

                        //Attack
                        enemyClose = false;
                        foreach (var t in creature.tile.neighbours)
                        {
                            if (t.occupied && enemies.Contains(t.occupant))
                            {
                                enemyClose = true;
                            }
                        }
                        if (enemyClose)
                        {
                            var closeEnemies = creature.tile.neighbours.Where(t => t.occupied && t.occupant.hp > 0 && enemies.Contains(t.occupant)).Select(t => t.occupant).OrderBy(e => e.hp).ThenBy(e => e.tile.Value).ToList();
                            if (closeEnemies.Count > 0)
                            {
                                creature.Attack(closeEnemies[0]);
                            }
                        }
                    }

                    map.SortCreatures();
                    map.EndCombat();

                    rounds++;

                    if (rounds >= printAfterRounds && print && true)
                    {
                        //Console.Clear();

                        //Console.SetCursorPosition(map.w + 5, 2);
                        var mittenChecksum = map.creatures.Sum(c => c.x + c.y * 12345);
                        mittenChecksum += map.deadCreatures.Sum(c => c.x + c.y * 12345);
                        Console.WriteLine((rounds - 1).ToString().PadRight(10) + mittenChecksum);
                        Console.ReadKey();
                    }

                    if (rounds >= printAfterRounds && print && false)
                    {
                        Console.WriteLine("After " + rounds + " rounds");
                        map.PrintMap(0, 2);
                        map.PrintCreatureInfo(0, 2 + map.h + 2);
                        Console.ReadKey();
                    }

                    if (map.deadCreatures.Any(c => c.type == CreatureType.Elf))
                    {
                        Console.WriteLine("At " + Creature.ElfAttack + " elf died");
                        elfAttackLow = Creature.ElfAttack;
                        Creature.ElfAttack = (int)Math.Ceiling((elfAttackHigh + elfAttackLow) / 2f);
                        Console.WriteLine("Changing to " + Creature.ElfAttack);
                        Console.WriteLine();
                        break;
                    }
                    else if (map.goblins.Count == 0)
                    {
                        lowestAttackWithPerfectVictory = Creature.ElfAttack;
                        Console.WriteLine("At " + Creature.ElfAttack + ", elves won perfect victory");
                        elfAttackHigh = Creature.ElfAttack;
                        Creature.ElfAttack = (int)Math.Ceiling((elfAttackHigh + elfAttackLow) / 2f);
                        Console.WriteLine("Changing to " + Creature.ElfAttack);
                        Console.WriteLine();
                        break;
                    }
                }
            }
            Console.WriteLine("Binary search done. Going to test lower attack powers");
            Console.WriteLine();
            for (int i = lowestAttackWithPerfectVictory - 1; i > 0; --i)
            {
                Creature.ElfAttack = i;
                Console.WriteLine("Testing elf attack power: " + Creature.ElfAttack);
                map.ReadMap(input);
                elfCount = map.elves.Count;

                if (printAfterRounds == 0 && print)
                {
                    Console.Clear();
                    Console.WriteLine("Initially");
                    map.PrintMap(0, 2);
                    Console.ReadKey();
                }

                fullRounds = -1;
                rounds = 0;
                while (fullRounds == -1)
                {
                    map.SortCreatures();
                    foreach (var creature in map.creatures)
                    {
                        if (map.removeAtEndOfCombat.Contains(creature))
                        {
                            continue;
                        }

                        HashSet<Creature> enemies;
                        if (creature.type == CreatureType.Elf)
                        {
                            enemies = map.goblins;
                        }
                        else
                        {
                            enemies = map.elves;
                        }
                        if (enemies.Where(e => !map.removeAtEndOfCombat.Contains(e)).Count() == 0)
                        {
                            fullRounds = rounds;
                            break;
                        }

                        //Move
                        bool enemyClose = false;
                        foreach (var t in creature.tile.neighbours)
                        {
                            if (t.occupied && enemies.Contains(t.occupant))
                            {
                                enemyClose = true;
                            }
                        }
                        if (!enemyClose)
                        {
                            //A*
                            var paths = new Dictionary<Tile, List<List<Tile>>>();
                            var possibleSteps = creature.tile.neighbours.Where(t => !t.closed && !t.occupied);
                            foreach (var s in possibleSteps)
                            {
                                var livingEnemies = enemies.Where(e => !e.dead);
                                foreach (var e in livingEnemies)
                                {
                                    var possibleTargets = e.tile.neighbours.Where(t => !t.closed && !t.occupied);
                                    foreach (var d in possibleTargets)
                                    {
                                        var p = map.FindPathAStar(s, d);
                                        if (p != null)
                                        {
                                            paths.TryAdd(d, new List<List<Tile>>());
                                            paths[d].Add(p);
                                        }
                                    }
                                }
                            }
                            if (paths.Count > 0)
                            {
                                var allPaths = new List<List<Tile>>();
                                foreach (var k in paths.Keys) allPaths.AddRange(paths[k]);
                                var shortestPath = allPaths.OrderBy(p => p.Count).ThenBy(p => p[0].Value).ThenBy(p => p[p.Count - 1].Value).First();
                                shortestPath.Reverse();
                                creature.Move(shortestPath[0]);
                            }
                        }

                        //Attack
                        enemyClose = false;
                        foreach (var t in creature.tile.neighbours)
                        {
                            if (t.occupied && enemies.Contains(t.occupant))
                            {
                                enemyClose = true;
                            }
                        }
                        if (enemyClose)
                        {
                            var closeEnemies = creature.tile.neighbours.Where(t => t.occupied && t.occupant.hp > 0 && enemies.Contains(t.occupant)).Select(t => t.occupant).OrderBy(e => e.hp).ThenBy(e => e.tile.Value).ToList();
                            if (closeEnemies.Count > 0)
                            {
                                creature.Attack(closeEnemies[0]);
                            }
                        }
                    }

                    map.SortCreatures();
                    map.EndCombat();

                    rounds++;

                    if (rounds >= printAfterRounds && print && true)
                    {
                        //Console.Clear();

                        //Console.SetCursorPosition(map.w + 5, 2);
                        var mittenChecksum = map.creatures.Sum(c => c.x + c.y * 12345);
                        mittenChecksum += map.deadCreatures.Sum(c => c.x + c.y * 12345);
                        Console.WriteLine((rounds - 1).ToString().PadRight(10) + mittenChecksum);
                        Console.ReadKey();
                    }

                    if (rounds >= printAfterRounds && print && false)
                    {
                        Console.WriteLine("After " + rounds + " rounds");
                        map.PrintMap(0, 2);
                        map.PrintCreatureInfo(0, 2 + map.h + 2);
                        Console.ReadKey();
                    }

                    if (map.deadCreatures.Any(c => c.type == CreatureType.Elf))
                    {
                        Console.WriteLine("At " + Creature.ElfAttack + " elf died");
                        Console.WriteLine();
                        break;
                    }
                    else if (map.goblins.Count == 0)
                    {
                        lowestAttackWithPerfectVictory = Math.Min(lowestAttackWithPerfectVictory, Creature.ElfAttack);
                        Console.WriteLine("At " + Creature.ElfAttack + ", elves won perfect victory");
                        Console.WriteLine();
                        break;
                    }
                }
            }

            Creature.ElfAttack = lowestAttackWithPerfectVictory;
            Console.WriteLine("Setting Elf attack to " + Creature.ElfAttack);
            Console.WriteLine();
            Console.WriteLine();
            return lowestAttackWithPerfectVictory;
        }

        static void Main(string[] args)
        {
            var inputTest1 = "#######!#E..G.#!#...#.#!#.G.#G#!#######";
            var inputTest2 = "#########!#G..G..G#!#.......#!#.......#!#G..E..G#!#.......#!#.......#!#G..G..G#!#########";
            var inputTest3 = "#######!#.G...#!#...EG#!#.#.#G#!#..G#E#!#.....#!#######";
            var inputTest4 = "#######!#G..#E#!#E#E.E#!#G.##.#!#...#E#!#...E.#!#######";
            var inputTest5 = "#######!#E.G#.#!#.#G..#!#G.#.G#!#G..#.#!#...E.#!#######";
            var inputTest6 = "#######!#.E...#!#.#..G#!#.###.#!#E#G#G#!#...#G#!#######";
            var inputTest7 = "#########!#G......#!#.E.#...#!#..##..G#!#...##..#!#...#...#!#.G...G.#!#.....G.#!#########";
            var inputTest8 = "#######!#.E...#!#.....#!#...G.#!#######";
            var inputTest9 = "######!#E...#!#....#!#..#.#!#....#!#.#G##!######";
            var inputTest10 = "#########!#G......#!#.E.#...#!#..##..G#!#...##..#!#...#...#!#.G...G.#!#.....G.#!#########";
            var inputReal = "################################!##############..######....######!###########GG.G.#######.########!############....######..#..#####!############...#######.....#####!##############..#G.####....#####!#############..G#..####...######!######.#####.G...G..###.#.######!######...###..........#.########!######G.................#.######!######....G.#............G.#####!######G......G............######!######.......E#####E.G.....#####!#####...G....#######.......#####!#####.......#########......#####!########....#########.....######!########G.G.#########...########!#########...#########.......#.##!########.G..#########..........#!#######.E....#######........#..#!#...........G.#####...E...######!####.....##................#####!#####..#.####.#.............####!########...##EE..G....E.#..E.###!##########..#................###!##########.............#.....###!###########.E.G..........##.####!###########.........###..##.####!############.##........E.#######!################.###.###########!################.###############!################################";

            var input = inputReal;

            p2(input);
            Console.ReadKey();
            return;
        }
    }
}
