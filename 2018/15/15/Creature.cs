﻿namespace _15
{
    public enum CreatureType { Elf, Goblin }

    public class Creature
    {
        public static int ElfAttack = 3;
        public static int GoblinAttack = 3;

        TileMap map;
        public int x
        {
            get
            {
                return tile.x;
            }
        }
        public int y
        {
            get
            {
                return tile.y;
            }
        }
        public Tile tile;
        public CreatureType type;
        public bool dead = false;
        public int hp = 200;
        public int power
        {
            get
            {
                if (type == CreatureType.Elf) return ElfAttack;
                return GoblinAttack;
            }
        }

        public Creature()
        {
            map = null;
            tile = null;
            type = CreatureType.Elf;
        }

        public Creature(TileMap map, int x, int y)
        {
            this.map = map;
            tile = map.tiles[x, y];
            tile.occupant = this;
            type = CreatureType.Elf;
        }

        public void Move(Tile t)
        {
            tile.occupant = null;
            tile = t;
            tile.occupant = this;
        }

        public void Attack(Creature c)
        {
            c.hp -= power;
            c.CheckDeath();
        }

        public void CheckDeath()
        {
            if (hp <= 0)
            {
                dead = true;
                map.removeAtEndOfCombat.Add(this);
                if (type == CreatureType.Elf) map.elves.Remove(this);
                else map.goblins.Remove(this);
                tile.occupant = null;
            }
        }

        public override string ToString()
        {
            return (type.ToString() + " at (" + x + ", " + y + ")");
        }
    }
}
